<?php

return [
    'user management' => [
        "icon" => "fa-user",
        "menu" => [
            'roles' => [
                "display_name" => "Roles",
                "actions" => ["create", "update", "delete"],
				"url"	  => "admin/roles"
            ],
            'users' => [
                "display_name" => "Users",
                "actions" => ["create", "update", "delete"],
                "url"	  => "admin/users"
            ],
            'user_activities' => [
                "display_name" => "User Activities",
                "actions" => ["show"],
                "url"	  => "admin/user_activities"
            ],
            'blocked_visitors' => [
                "display_name" => "Blocked Visitors",
                "actions" => ["show", "delete"],
                "url"	  => "admin/blocked_visitors"
            ]
        ]

    ],
	 'Basic Module' => [
	    "icon" => "fa-cogs",
		 "menu" => [
            'set_up' => [
                "display_name" => "Company Balance",
                "actions" => ["create", "update", "delete"],
                "url"     => "master/set_up"
            ],
			'customers' => [
                "display_name" => "Customers",
                "actions" => ["create", "update", "delete"],
                "url"     => "master/customers"
            ],
			'agents' => [
                "display_name" => "Agents",
                "actions" => ["create", "update", "delete"],
                "url"     => "master/agents"
            ],
			'agent_companies' => [
                "display_name" => "Company",
                "actions" => ["create", "update", "delete"],
                "url"     => "master/agent_companies"
            ],
			/*'banks' => [
                "display_name" => "Manage Bank",
                "actions" => ['create', "update", "delete"],
                "url"    => "master/set_up/bank"
            ],*/
			'accounts' => [
                "display_name" => "Set Up",
                "actions" => ['create', "update", "delete"],
                "url"    => "master/accounts"
            ],
			'accounts_' => [
                "display_name" => "Set Up Genting 2",
                "actions" => ['create', "update", "delete"],
                "url"    => "master/accounts/genting"
            ],
			'journals' => [
                "display_name" => "Journal Entry",
                "actions" => ['create', "update", "delete"],
                "url"    => "master/journals"
            ],
			'journals_' => [
                "display_name" => "Journal Entry Genting 2",
                "actions" => ['create', "update", "delete"],
                "url"    => "master/journals/genting"
            ]
		]
	 ],
    'Transaction Module' => [
        "icon" => "fa-money",
		"menu" => [
			'workin' => [
                "display_name" => "Work In",
                "actions" => ["show"],
                "url"    => "app/workin"
            ],
			'purchases' => [
                "display_name" => "Purchases",
                "actions" => ['create', "update", "delete"],
                "url"    => "app/purchases"
            ],
			'claim_commission' => [
                "display_name" => "Purchases Commission",
                "actions" => ['create', "update", "delete"],
                "url"    => "app/claim_commission"
            ],
			'purchases_from_customer' => [
                "display_name" => "Sell NN Chips",
                "actions" => ['create', "update", "delete"],
                "url"    => "app/purchases_from_customer"
            ],
			'return_chip' => [
                "display_name" => "Return NN Chip",
                "actions" => ['create', "update", "delete"],
                "url"    => "app/return_chips"
            ],
            'shift_transfer' => [
                "display_name" => "Shift Transfer",
                "actions" => ["show","create","update","delete"],
                "url"    => "app/shift_transfer"
            ],
            /*'shift_transfer_runner' => [
                "display_name" => "Shift Transfer (Runner)",
                "actions" => ["show","create","update","delete"],
                "url"    => "app/shift_transfer/runner"
            ],*/
            'sell_chip' => [
                "display_name" => "Chips Out",
                "actions" => ["show", 'create', "update", "delete"],
                "url"    => "master/sell_chip"
            ],
            'chip_from_runner_or_assist' => [
                "display_name" => "Chips Return",
                "actions" => ["show", 'create', "update", "delete"],
                "url"    => "master/chip_from_runner"
            ],
            'transactions' => [
                "display_name" => "Open Trip",
                "actions" => ['create', "update", "delete"],
                "url"    => "app/transactions"
            ],
            'expenses' => [
                "display_name" => "Expenses",
                "actions" => ['create', "update", "delete"],
                "url"    => "app/expenses"
            ],
            'transaction_banks' => [
                "display_name" => "Transaction Bank",
                "actions" => ['create', "update", "delete"],
                "url"    => "app/bank"
            ],
            'loan' => [
                "display_name" => "Loan",
                "actions" => ['create', "update", "delete"],
                "url"    => "app/loan"
            ],
            'payment_debts' => [
                "display_name" => "Payment Received",
                "actions" => ['create', "update", "delete"],
                "url"    => "app/payment_debit"
            ],
            'customer_debts' => [
                "display_name" => "Collection",
                "actions" => ["show","create"],
				"url"	  => "report/app/collection"
            ],
            'commission_genting2' => [
                "display_name" => "Commission Genting 2",
                "actions" => ['create', "update", "delete"],
                "url"    => "app/commission_gentings2"
            ],
            'deposit_genting2' => [
                "display_name" => "Deposit Genting 2",
                "actions" => ['create', "update", "delete"],
                "url"    => "app/deposit_gentings2"
            ],
		    'calendar_activity' => [
                "display_name" => "Calender Activity",
                "actions" => ["show", 'create', "update", "delete"],
                "url"    => "app/calendar_activity"
            ],
            'tips_management' => [
                "display_name" => "Tips Management",
                "actions" => ["show", 'create', "update", "delete"],
                "url"    => "app/tips"
            ],
            'withdrawal_genting2' => [
                "display_name" => "Withdrawal Genting 2",
                "actions" => ["show", 'create', "update", "delete"],
                "url"    => "app/wg2"
            ],
			'current_chip' => [
                "display_name" => "Current Chip",
                "actions" => ["show"],
                "url"    => "report/app/current_chips"
            ],
			'vendor' => [
                "display_name" => "Vendor management",
                "actions" => ["show", 'create', 'update', 'delete'],
                "url"    => "app/vendor"
            ],
			'redeem_commision' => [
                "display_name" => "Redeem commission",
                "actions" => ["show", 'create', 'update', 'delete'],
                "url"    => "app/redeem_commision"
            ]
            /*'bank_genting2' => [
                "display_name" => "Bank Genting 2",
                "actions" => ["show", 'create', "update", "delete"],
                "url"    => "app/bank_gentings2"
            ],
			'chip_runner_report' => [
                "actions" => ['show', 'create', "update", "delete"],
                "url"    => "report/chip_runner_report"
            ],
            'report_by_trip' => [
                "actions" => ['show', 'create', "update", "delete"],
                "url"    => "report/trip/report_by_trip"
            ]*/
        ]
    ],
    'report' => [
        "icon" => "fa-print",
        "menu" => [
            'final' => [
                "display_name" => "Final",
                "actions" => ["show"],
                "url"	  => "report/app/final"
            ],
            'manager' => [
                "display_name" => "Manager",
                "actions" => ["show"],
                "url"	  => "report/app/manager"
            ],
            'bank_statement' => [
                "display_name" => "Bank Statement",
                "actions" => ["show"],
                "url"	  => "report/app/bank"
            ],
            'shift_histories' => [
                "display_name" => "Shift Histories",
                "actions" => ['create', "update", "delete"],
                "url"    => "master/shift_histories"
            ],
            'daily_report' => [
                "display_name" => "Summary Final Report",
                "actions" => ['show'],
                "url"    => "report/app/daily"
            ],
            'customer_report' => [
                "display_name" => "Final Customer Report",
                "actions" => ['show'],
                "url"    => "report/app/customer"
            ],
            'rolling_report' => [
                "display_name" => "Rolling Report",
                "actions" => ['show'],
                "url"    => "report/app/rolling"
            ],
            'end_day' => [
                "display_name" => "End Day Report",
                "actions" => ['show'],
                "url"    => "report/app/end_day"
            ],
            'runner_report' => [
                "display_name" => "Runner Report",
                "actions" => ['show'],
                "url"    => "report/app/runner"
            ],
            'compare_final_end' => [
                "display_name" => "Compare Final End",
                "actions" => ['show'],
                "url"    => "report/app/compare_final_end"
            ],
            'agent_insurance' => [
                "display_name" => "Rolling 2 Report",
                "actions" => ['show'],
                "url"    => "report/app/agent_insurance"
            ],
            'purchase_commission' => [
                "display_name" => "Purchase Commission",
                "actions" => ['show'],
                "url"    => "report/app/purchase_commission"
            ]
        ]

    ],
];
