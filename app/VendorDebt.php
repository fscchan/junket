<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VendorDebt extends Model {
    use SoftDeletes;

    protected $fillable = [
        "vendor_id", "transaction_id", "date", "amount", "is_paid", "interest_type", "interest_rate", "interest_amount", "staff_id", "due_date","amount_due"
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    public function customer(){
        return $this->belongsTo(Vendor::class)->withTrashed();
    }

    public function transaction(){
        return $this->belongsTo(Transaction::class)->withTrashed();
    }

    public function users(){
        return $this->belongsTo(User::class,'staff_id');
    }
}
