<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use DB;

class AgentCompany extends Model
{
    use SoftDeletes, Sortable;

    protected $fillable = [
        'company_name',
        'pic',
        'address',
        'postal_code',
        'company_contact',
        'company_email'
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at','updated_at','deleted_at'];
	
	public function agent(){
		return $this->belongsTo(Agent::class);
	}
	
    protected $sortable = [
        'company_name',
        'pic',
        'address',
        'postal_code',
        'company_contact',
        'company_email'
    ];

}
