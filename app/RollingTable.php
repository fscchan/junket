<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use App\Transaction;

class RollingTable extends Model
{
    use Sortable;
    protected $fillable = [
        'transaction_id',
        'print_date',
        'trade_date',
        'terminal',
        'cash_chip',
        'nn_chip'
    ];
    protected $hidden = ['created_at', 'updated_at'];
    protected $dates = ['print_date','trade_date', 'created_at', 'updated_at'];

    public function transaction(){
        return $this->belongsTo(Transactions::class);
    }
}
