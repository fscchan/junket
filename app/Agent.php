<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use DB;

class Agent extends Model
{
    use SoftDeletes, Sortable;

    protected $fillable = [
        'id_no',
        'name',
        'phone_code',
        'contact',
        'email',
		'agent_company_id',
        'company_name',
        'pic',
        'address',
        'postal_code',
        'company_contact',
        'company_email',
        'nn_chips',
        'cash_chips',
        'cash_real'
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at','updated_at','deleted_at'];
	
	protected $with = ['agent_company'];
	
	public function agent_share(){
		return $this->hasOne(Agent::class);
	}
	
	public function agent_company(){
		return $this->belongsTo(AgentCompany::class);
	}
	
    protected $sortable = [
        'id_no',
        'name',
        'phone_code',
        'contact',
        'email',
        'company_name',
        'pic',
        'address',
        'postal_code',
        'company_contact',
        'company_email',
        'nn_chips',
        'cash_chips',
        'cash_real'
    ];

}
