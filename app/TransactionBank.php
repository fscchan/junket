<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class TransactionBank extends Model
{
    use SoftDeletes, Sortable;

    protected $fillable = [
        "date",
        "credit_debit",
        "payment_method_id",
		"bank_id",
		"bank_account_id",
        "pic",
        "ref_no",
        "remark",
        "amount",
        "user_id"
    ];

    protected $hidden = ["created_at", "deleted_at", "updated_at"];

    protected $with = ["payment_method","bank","bank_account","user"];

    public function payment_method(){
        return $this->belongsTo(PaymentMethod::class)->withTrashed();
    }

    public function bank(){
        return $this->belongsTo(Bank::class)->withTrashed();
    }

    public function bank_account(){
        return $this->belongsTo(Account::class,'bank_account_id')->withTrashed();
    }

    public function user(){
        return $this->belongsTo(User::class)->withTrashed();
    }

    protected $sortable = [
        "date",
        "credit_debit",
        "ref_no",
        "remark",
        "amount"
    ];
}
