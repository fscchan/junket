<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentMethod extends Model
{
	use SoftDeletes;
	
    protected $fillable = ['name','value'];

    protected $hidden = ['created_at', 'updated_at','deleted_at'];

    public function purchases() {
        return $this->hasMany(Purchase::class);
    }

    public function transaction_banks() {
        return $this->hasMany(TransactionBank::class);
    }

}
