<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Account extends Model
{
    use SoftDeletes, Sortable;

    protected $fillable = [
        'code',
        'name',
        'parent_id',
        'account_type_id',
        'nature_type',
		'nn_chips_id',
		'nn_chips',
		'junket_chips_id',
		'junket_chips',
		'cash_chips_id',
		'cash_chips',
		'cash_real_id',
		'cash_real',
		'balance_id',
		'balance',
		'balance_date',
		'company_id'
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at','updated_at','deleted_at'];
	
	protected $with = ['account_type','child'];
	
	public function account_type(){
		return $this->belongsTo(AccountType::class)->withTrashed();
	}
	
	public function child(){
		return $this->hasMany(Account::class,'parent_id');
	}

    public function withdrawal_genting2(){
        return $this->hasMany(WithdrawalGenting2::class);
    }
	
    protected $sortable = [
        'code',
        'name',
        'parent_id',
        'account_type_id',
        'nature_type'
    ];

}
