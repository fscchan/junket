<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use DB;

class ClaimCommission extends Model
{
    use SoftDeletes, Sortable;

    protected $fillable = [
        'date',
        'vendor_id',
        'year',
        'month',
        'amount',
		'account_id',
		'remark'
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at','updated_at','deleted_at'];
	
	protected $with = ['vendor','account'];
	
	public function vendor(){
		return $this->belongsTo(Vendor::class)->withTrashed();
	}
	
	public function account(){
		return $this->belongsTo(Account::class)->withTrashed();
	}
	
    protected $sortable = [
        'date',
        'year',
        'month',
        'amount',
        'remark'
    ];
	
	public static function listHead($sort_by='date',$sort_type='desc'){
		return DB::table(DB::raw('(select date,`year`,`month`,commission_amount,
			 paid_amount,commission_amount-paid_amount as outstanding_amount
			from(
				select date_format(p.date,"%M %Y")as date,
				 year(p.date)as `year`,month(p.date)as `month`,
				 sum(p.commission)as commission_amount,sum(ifnull(cc.amount,0))as paid_amount
				from purchases p
				left join(
					select * from claim_commissions where deleted_at is null
				)cc on cc.year=year(p.date) and cc.month=month(p.date)
				 and cc.deleted_at is null
				where p.deleted_at is null
				group by year(p.date),month(p.date)
			)as sub
		)as sub order by '.$sort_by.' '.$sort_type));
	}
	
	public static function listDetail($year,$month){
		return DB::table(DB::raw('(select name,commission_amount,paid_amount,
			 commission_amount-paid_amount as outstanding_amount
			from(
				select v.name,sum(p.commission)as commission_amount,sum(ifnull(cc.amount,0))as paid_amount
				from purchases p
				join (
					select * from vendors where deleted_at is null
				)v on v.id=p.vendor_id 
				left join(
					select * from claim_commissions 
					where year='.$year.' and month='.$month.' and deleted_at is null
				)cc on cc.vendor_id=p.vendor_id
				 and cc.deleted_at is null
				where p.deleted_at is null and year(p.date)='.$year.' and month(p.date)='.$month.'
				group by p.vendor_id
			)as sub
		)as sub'));
	}

}
