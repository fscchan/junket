<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class TipsManagement extends Model
{
    protected $table = "tipsmanagement";

    protected $fillable = [
        'trans_date',
        'trans_id',
		'customer_id',
        'employee_id',
        'amount',
    ];

    protected $hidden = ['created_at', 'updated_at'];

    protected $dates = ['created_at','updated_at'];


    public function user(){
        return $this->belongsTo("App\User", "employee_id");
    }
    protected $with = ['user'];
}
