<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class RedeemCommision extends Model {
    protected $table = "redeem_commision";
	protected $fillable = [
		"trans_id",
		"amount",
		"redeemed",
		"account_id",
		"redeem_date"
	];
	
    protected $dates = ['redeem_date','created_at','updated_at'];
	
	protected $with=['account'];
	
	public function account(){
		return $this->belongsTo(Account::class);
	}
	
	public function transaction() {
		return $this->hasOne(Transaction::class, "id", "trans_id");
	}
}