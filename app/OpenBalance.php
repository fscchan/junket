<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenBalance extends Model
{
    protected $fillable = ['manager_id', 'nn_chips', 'cash_chips', 'cash_real'];

    protected $with = ['user'];

    public function user(){
        return $this->belongsTo(User::class, 'manager_id')->withTrashed();
    }
}
