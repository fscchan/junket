<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class PaymentDebt extends Model
{
	use SoftDeletes;
	
    protected $fillable = [
        "customer_id", "agent_id", "vendor_id", "company_id",
		"number", "date", "bank_id", "bank_account_id","payment_method_id","payment_mode","staff_id", "amount","remark","user_id",
        "image_name",
        "image_size",
        "image_ext",
        "image_path"
    ];

    protected $hidden = ['created_at', 'updated_at','deleted_at'];

    protected $with = ["customer","bank","bank_account","detail","user","payment_method","staff"];

    public function customer(){
        return $this->belongsTo(Customer::class)->withTrashed();
    }
	
    public function bank(){
        return $this->belongsTo(Bank::class)->withTrashed();
    }
	
    public function bank_account(){
        return $this->belongsTo(Account::class)->withTrashed();
    }
	
    public function detail(){
        return $this->belongsTo(PaymentDebtDetail::class);
    }
	
	public function user(){
		return $this->belongsTo(User::class);
	}
	
	public function payment_method(){
		return $this->belongsTo(PaymentMethod::class);
	}
	
	public function staff(){
		return $this->belongsTo(User::class,'staff_id');
	}
}
