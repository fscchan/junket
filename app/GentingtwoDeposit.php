<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use DB;

class GentingtwoDeposit extends Model
{
    use SoftDeletes, Sortable;

    protected $fillable = [
        'date',
        'nn_chips',
        'junket_chips',
        'cash_chips',
        'cash_real',
        'user_id',
		'remark'
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at','updated_at','deleted_at'];
	
	protected $with = ['user'];
	
	public function user(){
		return $this->belongsTo(User::Class);
	}
	
    protected $sortable = [
        'date',
        'nn_chips',
        'junket_chips',
        'cash_chips',
        'cash_real',
        'user_id',
		'remark'
    ];

}
