<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenBalanceAgent extends Model
{
    protected $fillable = ['agent_id', 'junket_chips', 'nn_chips', 'cash_chips', 'cash_real'];

    protected $with = ['agent'];

    public function agent(){
        return $this->belongsTo(Agent::class)->withTrashed();
    }
}
