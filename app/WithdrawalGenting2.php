<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WithdrawalGenting2 extends Model
{
    protected $table = "withdrawal_genting_2";
    protected $fillable = ['date', 'account_id', 'amount', 'user_id', 'type', 'type_chip'];

    protected $hidden = ['created_at', 'updated_at'];

    protected $dates = ['created_at','updated_at'];

    public function account(){

        return $this->belongsTo(Account::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    protected $with = ['user', 'account'];
}
