<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenBalanceBoss extends Model
{
	protected $table = 'open_balance_boss';
	
    protected $fillable = ['boss_id', 'junket_chips', 'nn_chips', 'cash_chips', 'cash_real'];

    protected $with = ['boss'];

    public function boss(){
        return $this->belongsTo(User::class,'boss_id')->withTrashed();
    }
}
