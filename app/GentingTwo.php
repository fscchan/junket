<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GentingTwo extends Model {
	use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table = "account_gentingtwo";
	protected $fillable = [
		"code",
		"name",
		"nn_chips",
		"junket_chips",
		"cash_chips",
		"cash_real",
		"balance",
		"balance_date"
	];
}
