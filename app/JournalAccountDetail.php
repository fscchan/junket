<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use DB;

class JournalAccountDetail extends Model
{
    use Sortable;

    protected $fillable = [
        'journal_id',
        'account_id',
        'debit',
        'credit',
		'entity',
		'entity_id'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    protected $dates = ['created_at','updated_at'];
	
	protected $with = ['account'];
	
	public function account(){
		return $this->belongsTo(Account::class)->withTrashed();
	}
	
    protected $sortable = [
        'journal_id',
        'account_id',
        'debit',
        'credit'
    ];

}
