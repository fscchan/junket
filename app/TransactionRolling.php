<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionRolling extends Model
{
    protected $fillable = ['transaction_id', 'amount', 'total', 'odds', 'points','rolling','insurance','status','room','scenario','agent_shared'];

    protected $hidden = ["created_at","updated_at"];

    public function transaction(){
        return $this->belongsTo(TransactionDetail::class);
    }
}
