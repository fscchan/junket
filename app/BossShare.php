<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BossShare extends Model
{
    protected $fillable = ['boss_id', 'amount'];

    protected $with = ['user'];

    public function user(){
        return $this->belongsTo(User::class,'boss_id')->withTrashed();
    }
}
