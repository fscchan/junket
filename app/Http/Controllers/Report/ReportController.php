<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Transaction;
use App\TransactionDetail;
use App\TransactionRolling;
use App\ShiftHistory;
use App\Customer;
use App\TransactionBank;
use App\PaymentMethod;
use App\Expense;
use Validator;
use App\UserActivity;
use View;
use DB;

require_once base_path().'/vendor/mpdf/mpdf/mpdf.php';

class ReportController extends Controller
{

    public function showRollingPerTransaction(Request $request, $id)
    {
		$token = $request->input('token');
        $user = User::where('token',$token)->first();
        $cek = check_auth($user,'read transactions');
        if($cek['result']==1){
			$master = Transaction::find($id);

			$details = TransactionDetail::where('transaction_id', $id)
				->where('flags', '=', 'role')
				->get();

			$mpdf = new \mPDF();
			$html = View::make('pdf.rolling',['master'=>$master,'details'=>$details])->render();
			// exit($html);
			$mpdf->WriteHTML($html);
			$mpdf->Output();
			exit;
		}else{
            return error_unauthorized();
		}
    }

    public function showRolling2PerTransaction(Request $request, $id)
    {
		$token = $request->input('token');
        $user = User::where('token',$token)->first();
        $cek = check_auth($user,'read transactions');
        if($cek['result']==1){
			$master = Transaction::find($id);

			$details = TransactionRolling::where('transaction_id', $id)->get();

			$mpdf = new \mPDF();
			$html = View::make('pdf.rolling2',['master'=>$master,'details'=>$details])->render();
			// exit($html);
			$mpdf->WriteHTML($html);
			$mpdf->Output();
			exit;
		}else{
            return error_unauthorized();
		}
    }

    public function showCollectionPerTransaction(Request $request, $id)
    {
		$token = $request->input('token');
        $user = User::where('token',$token)->first();
        $cek = check_auth($user,'read transactions');
        if($cek['result']==1){
			$master = Transaction::find($id);

			$details = TransactionDetail::where('transaction_id', $id)
				->where('flags', '=', 'credit')
				->orWhere('transaction_id', '=', $id)
				->where('flags', '=', 'open')
				->orderBy('flags','asc')
				->get();

			$mpdf = new \mPDF();
			$html = View::make('pdf.collection',['master'=>$master,'details'=>$details])->render();
			// exit($html);
			$mpdf->WriteHTML($html);
			$mpdf->Output();
			exit;
		}else{
            return error_unauthorized();
		}
    }

	public function showRunnerPerShift(Request $request,$id)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read transactions');
		if($cek['result']==1){
			$master = Transaction::find($id);

			$details = DB::table(DB::raw('(select u.name, sh.nn_chips, "buy" as pos, sh.created_at
				from shift_histories sh
				join users u on u.id=sh.to_user_id
				where sh.to_user_id="'.$master->user_id.'" and sh.date="'.$master->date.'" and sh.shift_to="'.$master->shift.'"
				union all
				select concat(c.first_name," ",ifnull(c.first_name,"")," ",ifnull(c.last_name,""))as name, sum(td.rolling)as nn_chips, "buy" as pos, t.created_at
				from transactions t
				join transaction_details td on td.transaction_id=t.id
				join customers c on c.id=t.customer_id
				where t.id="'.$id.'" and (td.flags="1credit" or td.flags="credit")
				union all
				select concat(c.first_name," ",c.first_name," ",c.last_name)as name, sum(td.rolling)as nn_chips, "sell" as pos, t.created_at
				from transactions t
				join transaction_details td on td.transaction_id=t.id
				join customers c on c.id=t.customer_id
				where t.id="'.$id.'" and (td.flags="open" or td.flags="role")
				)as sub order by created_at'))->get();

			$mpdf = new \mPDF();
			$html = View::make('pdf.runner',['master'=>$master,'details'=>$details])->render();
			// exit($html);
			$mpdf->WriteHTML($html);
			$mpdf->Output();
			exit;
		}else{
			return error_unauthorized();
		}
	}

	public function showCollectionPerCustomer(Request $request,$id)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read transactions');

		$start_date=$request->input('start_date');
		$start_date=isset($start_date)?date('Y-m-d',strtotime($start_date)):'';
		$end_date=$request->input('end_date');
		$end_date=isset($end_date)?date('Y-m-d',strtotime($end_date)):'';
		if($cek['result']==1){
			$master = Customer::find($id);

			$details = DB::table(DB::raw('(select name,date(date)as date,credit,credit-closing-(rolling/100*commission)as collection from(
				select u.name, t.id,t.date, (sum(if((td.flags="1credit" || td.flags)="credit",td.rolling,0)))as credit,
				ifnull(t.closing_nn_chips,0)+ifnull(t.closing_cash_chips,0)+ifnull(t.closing_cash,0)as closing,
				sum(if(td.flags="role",td.rolling,0))as rolling,t.commission
				from transactions t
				join transaction_details td on td.transaction_id=t.id
				join users u on u.id=t.user_id
				where date(t.date)>="'.$start_date.'" and  date(t.date)<="'.$end_date.'" '.(isset($id)?'and t.customer_id="'.$id.'"':'').'
				group by t.id order by t.created_at)as sub)as sub group by name,date'))->get();
			
			$data=['data'=>['master'=>$master,'details'=>$details]];

			return $data;
		}else{
			return error_unauthorized();
		}
	}

	public function reportCollectionPerCustomer(Request $request,$id)
	{
		// return $request;
		$data=$this->showCollectionPerCustomer($request,$id);
		// return $data['data'];

		$mpdf = new \mPDF();
		$html = View::make('pdf.collectioncustomer',$data['data'])->render();
		// exit($html);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}

	public function showFinalReport(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read transactions');

		$date = $request->input('date');
		if($cek['result']==1){
			$rolling = DB::table(DB::raw('(select concat(c.first_name," ",ifnull(c.first_name,"")," ",ifnull(c.last_name,""))as name,sum(td.rolling)as rolling,t.check_in_commision,t.remarks
				from transactions t
				join transaction_details td on td.transaction_id=t.id
				join customers c on c.id=t.customer_id
				where td.flags="role" and date(t.date)="'.$date.'" group by t.id order by t.created_at)as sub'))->get();

			$rolling_trans = DB::table(DB::raw('(select concat(c.first_name," ",ifnull(c.first_name,"")," ",ifnull(c.last_name,""))as name,
				t.commission,sum(td.rolling)as rolling,sum(td.rolling)/100*ifnull(t.commission,1) as total_commission,t.closing_cash,t.remarks
				from transactions t
				join transaction_details td on td.transaction_id=t.id
				join customers c on c.id=t.customer_id
				where td.flags="role" and date(t.date)="'.$date.'" group by t.id order by t.created_at)as sub'))->get();

			$expense = Expense::where('date','=',$date)->get();

			$last_day=date('Y-m-d',strtotime($date.' -1 days'));
			$balance = DB::table(DB::raw('(select *,debit-credit-expense-spending_fee as today_balance,debit-credit-expense-spending_fee+last_day_balance as total_balance_in,balance_earn-expense as total_earn from
				(select ifnull(sum(if(credit_debit="debit",amount,0)),0)as debit,ifnull(sum(if(credit_debit="credit",amount,0)),0)as credit from transaction_banks where date(date)="'.$date.'")as sub1,
				(select ifnull(sum(amount),0)as expense from expenses where date(date)="'.$date.'")as sub2,
				(select ifnull(sum(closing_cash),0)as spending_fee from transactions where date(date)="'.$date.'")as sub3,
				(select
					(select ifnull(sum(if(credit_debit="debit",amount,0)),0)-ifnull(sum(if(credit_debit="credit",amount,0)),0)as bank from transaction_banks where date(date)<"'.$last_day.'")-
					(select ifnull(sum(amount),0)as expense from expenses where date(date)="'.$last_day.'")-
					(select ifnull(sum(closing_cash),0)as spending_fee from transactions where date(date)="'.$last_day.'")as last_day_balance
				)as sub4,
				(select ifnull(sum(check_in_commision),0)as balance_earn from transactions where date(date)="'.$date.'")as sub5)as sub'))->get();

			$commission = DB::table(DB::raw('(select *, company_commission+spending_commission as total_commission,0 as collection_commission, company_commission-0 as total_collection from
				(select sum(td.rolling)as transaction from transaction_details td join transactions t on t.id=td.transaction_id where td.flags="role" and date(t.date)="'.$date.'")as sub1,
				(select ifnull(sum(closing_cash),0)as spending_commission from transactions where date(date)="'.$date.'")as sub2,
				(select ifnull(sum(company_commission),0)as company_commission from
					(select ifnull(sum(td.rolling),0)*100/t.commission as company_commission from transaction_details td join transactions t on t.id=td.transaction_id where td.flags="role" and date(t.date)="'.$date.'" group by t.id)
				as sub)as sub3,
				(select ifnull(sum(t.check_in_commision),0)as balance_earn from transactions t where date(t.date)="'.$date.'")as sub4)as sub'))->get();

			$mpdf = new \mPDF();
			$mpdf->AddPage('L', 'A4');
			$html = View::make('pdf.final',['rolling'=>$rolling,'rolling_trans'=>$rolling_trans,'expense'=>$expense,'balance'=>$balance,'commission'=>$commission])->render();
			// exit($html);
			$mpdf->WriteHTML($html);
			$mpdf->Output();
			exit;
		}else{
			return error_unauthorized();
		}
	}

	public function showDailyManager(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read transactions');

		$date=$request->input('date');
		$date=isset($date)?date('Y-m-d',strtotime($date)):'';
		if($cek['result']==1){
			$data=DB::table(DB::raw('(select distinct u.id,u.name
				from users u
				join user_has_roles uhr on uhr.user_id=u.id and uhr.role_id=4
				join shift_histories sh on sh.from_user_id=u.id or sh.to_user_id=u.id
				where date(sh.date)="'.$date.'"
			)as sub'))->get();

			foreach($data as $key=>$values_data){
				$data[$key]->details=DB::table(DB::raw('(
					select u.name,sum(ifnull(sh.nn_chips,0)+ifnull(sh.cash_chips,0)+ifnull(sh.cash_real,0))as balancein,0 as balanceout
					from shift_histories sh
					join users u on u.id=sh.from_user_id
					where date(sh.date)="'.$date.'" and sh.to_user_id="'.$values_data->id.'"
					group by u.id
					union all
					select u.name,0 as balancein, sum(ifnull(sh.nn_chips,0)+ifnull(sh.cash_chips,0)+ifnull(sh.cash_real,0))as balanceout
					from shift_histories sh
					join users u on u.id=sh.to_user_id
					where date(sh.date)="'.$date.'" and sh.from_user_id="'.$values_data->id.'"
					group by u.id
				)as sub order by name asc'))->get();

				$data[$key]->ceil=ceil(count($data[$key]->details)/19);
			}

			$data=['data'=>$data];

			return $data;
		}else{
			return error_unauthorized();
		}
	}

	public function reportDailyManager(Request $request)
	{
		// return $request;
		$data=$this->showDailyManager($request);
		// exit(json_encode($data));

		$mpdf = new \mPDF();
		$mpdf->AddPage('L', 'A4');
		$html = View::make('pdf.manager',$data)->render();
		// exit($html);

		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}

	public function dashboardBalance(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		if(count($user)>0){
			$date=date('Y-m').'-01';

			$data=DB::table(DB::raw('(select 
				(ifnull((select c.nn_chips
				from companies c where c.id=1),0)
				+
				ifnull((select sum(p.value)as value 
				from purchases p where p.deleted_at is null),0)
				-
				ifnull((select sum(sh.nn_chips)as nn_chips
				from shift_histories sh where sh.deleted_at is null),0))as chip,
				ifnull((select sum(td.rolling)as rolling 
				from transaction_details td 
				join transactions t on t.id=td.transaction_id 
				where t.date>="'.$date.'" and t.deleted_at is null and td.flags="role"),0)as rolling)as sub'))->first();

			return ['data'=>$data];
		}else{
			return error_unauthorized();
		}
	}

	public function dashboardDaily(Request $request)
	{
		// dd($request->input('token'));
		// die();
		$startdate = $request->input('startdate');
		$enddate = $request->input('enddate');

		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read transactions');
		$data=array();
		if($cek['result']==1){

			$startTime = strtotime($startdate);
			$endTime = strtotime($enddate);
			$reportData = [];
			$log = [];
			$i = 0;
  				// Loop between timestamps, 24 hours at a time
			for ( $i = $startTime; $i <= $endTime; $i = $i + 86400 ) {

				$thisDate = date( 'Y-m-d', $i );

				$dateTransaction = DB::table('transactions')->whereDate('date', '=', $thisDate)->whereNull('deleted_at')->value('date');
				$purchaseValue = DB::table('purchases')->whereDate('date', '=', $thisDate)->whereNull('deleted_at')->sum('value');
				$shiftHistoryValue = DB::table('shift_histories')->whereDate('date', '=', $thisDate)->whereNull('deleted_at')->sum('nn_chips');
				$transactions = DB::table('transaction_details')->join('transactions', 'transactions.id', '=', 'transaction_details.transaction_id')->whereDate('transactions.date', '=', $thisDate)->where('transaction_details.flags','=','role')->whereNull('transactions.deleted_at')->sum('transaction_details.rolling');

  					if($purchaseValue||$shiftHistoryValue||$transactions) {
						$log[] = array(
							'date' => $thisDate,
							'chip' => (($purchaseValue?$purchaseValue:0) - ($shiftHistoryValue?$shiftHistoryValue:0)),
							'rolling' => $transactions
						);
  					}

				}
			} else{
					return error_unauthorized();
			}

		if($log){
			return response()->json([
				"status" => "success",
				"data" => $log
			],200);
		}else{
			return response()->json([
				"status" => "error",
				"data" => null
			],403);
		}

	}

	public function dashboardWeekly(Request $request)
	{
			// dd($request->input('token'));
			// die();
			$startdate = $request->input('startdate');
			$enddate = $request->input('enddate');

			$token = $request->input('token');
			$user = User::where('token',$token)->first();
			$cek = check_auth($user,'read transactions');
			if($cek['result']==1){

  				$startTime = date('Y-m-d',strtotime($startdate));
  				$endTime = date('Y-m-d',strtotime($enddate));
				
				$log=DB::table(DB::raw('(select date,sum(chip)as chip,sum(rolling)as rolling
					from(select concat("Week ",(FLOOR((DAYOFMONTH(p.date)-1)/7)+1),", ",MONTHNAME(p.date)," ",YEAR(p.date))as date,p.value as chip,0 as rolling
					from purchases p where p.deleted_at is null and p.date>="'.$startTime.'" and p.date<="'.$endTime.'"
					union all
					select concat("Week ",(FLOOR((DAYOFMONTH(sh.date)-1)/7)+1),", ",MONTHNAME(sh.date)," ",YEAR(sh.date))as date,-sh.nn_chips as chip,0 as rolling
					from shift_histories sh where sh.deleted_at is null and sh.date>="'.$startTime.'" and sh.date<="'.$endTime.'"
					union all
					select concat("Week ",(FLOOR((DAYOFMONTH(t.date)-1)/7)+1),", ",MONTHNAME(t.date)," ",YEAR(t.date))as date,0 as chip,td.rolling as rolling
					from transaction_details td
					join transactions t on t.id=td.transaction_id 
					where t.deleted_at is null and td.flags="role" and t.date>="'.$startTime.'" and t.date<="'.$endTime.'")as sub group by date)as sub'))->get();
			} else{
				return error_unauthorized();
			}

      if($log){
          return response()->json([
              "status" => "success",
              "data" => $log
          ],200);
      }else{
          return response()->json([
              "status" => "error",
              "data" => null
          ],403);
      }

	}

	public function dashboardMonthly(Request $request)
	{

			$startdate = $request->input('startdate');
			$enddate = $request->input('enddate');

			$token = $request->input('token');
			$user = User::where('token',$token)->first();
			$cek = check_auth($user,'read transactions');
			if($cek['result']==1){



  				$startMonth = date('m', strtotime($startdate));
  				$endMonth = date('m', strtotime($enddate));
  				$startYear = date('Y', strtotime($startdate));
  				$endYear = date('Y', strtotime($enddate));

          $month = '';
          $year = '';
          $log = [];

          // Loop by year
  				for($year = $startYear; $year <= $endYear; $year++) {

            if($year != $endYear) {
              $endMonth = 12;
            } else {
              $endMonth = date('m', strtotime($enddate));
            }

            // Loop by month
            for($month = $startMonth; $month <= $endMonth; $month++) {

              $firstDate  = date('Y-m-d', strtotime($year.'-'.$month.'-'.'01'));
              $secondDate  = date('Y-m-d', strtotime($year.'-'.$month.'-'.'31'));

              $dateTransaction = DB::table('transactions')->whereBetween('date', array( $firstDate, $secondDate ))->whereNull('deleted_at')->value('date');
              $purchaseValue = DB::table('purchases')->whereBetween('date', array( $firstDate, $secondDate ))->whereNull('deleted_at')->sum('value');
              $shiftHistoryValue = DB::table('shift_histories')->whereBetween('date', array( $firstDate, $secondDate ))->whereNull('deleted_at')->sum('nn_chips');
              $transactions = DB::table('transaction_details')->join('transactions', 'transactions.id', '=', 'transaction_details.transaction_id')->whereBetween('date', array( $firstDate, $secondDate ))->where('transaction_details.flags','=','role')->whereNull('transactions.deleted_at')->sum('transaction_details.rolling');

              // if($dateTransaction) {
                $log[] = array(
                          'date' => date('F Y', strtotime($firstDate)),
                          'chip' => (($purchaseValue?$purchaseValue:0) - ($shiftHistoryValue?$shiftHistoryValue:0)),
                          'rolling' => $transactions
                      );
    					// }

            }

            if($year != $endYear){
              $startMonth = 01;
            }

				  }

			} else{
					return error_unauthorized();
			}

      if($log){
          return response()->json([
              "status" => "success",
              "data" => $log
          ],200);
      }else{
          return response()->json([
              "status" => "error",
              "data" => null
          ],403);
      }

	}

	public function dashboardMonthly2(Request $request){
		$data=DB::table(DB::raw('(select month_year as date,sum(chip)as chip,sum(rolling)as rolling
					from(select concat(monthname(p.date)," ",year(p.date))as month_year,sum(p.value)as chip,0 as rolling
					from purchases p group by month(p.date),year(p.date)
					union all
					select concat(monthname(sh.date)," ",year(sh.date))as month_year,-sum(sh.nn_chips)as chip,0 as rolling
					from shift_histories sh group by month(sh.date),year(sh.date)
					union all
					select concat(monthname(t.date)," ",year(t.date))as month_year,0 as chip,sum(td.rolling)as rolling
					from transaction_details td
					join transactions t on t.id=td.transaction_id group by month(t.date),year(t.date))as sub group by month_year)as sub'))->get();
	}

  public function dashboardBalanceReport(Request $request)
	{
			$token = $request->input('token');
			$user = User::where('token',$token)->first();
			$cek = check_auth($user,'read transactions');
      $log = [];

      if($cek['result']==1){

      $purchaseValue = DB::table('purchases')->sum('value');
      $shiftHistoryValue = DB::table('shift_histories')->sum('nn_chips');
      $transactions = DB::table('transaction_details')->join('transactions', 'transactions.id', '=', 'transaction_details.transaction_id')->sum('transaction_details.rolling');

      $log = array(
                'balanceChip' => ($purchaseValue - $shiftHistoryValue),
                'balanceRolling' => $transactions
            );

			} else{
				return error_unauthorized();
			}

      if($data){
          return response()->json([
              "status" => "success",
              "data" => $data
          ],200);
      }else{
          return response()->json([
              "status" => "error",
              "data" => null
          ],403);
      }

	}

	public function showBankStatement(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read transaction_banks');

		$bank_id=$request->input('bank_id');
		$start_date=$request->input('start_date');
		$start_date=isset($start_date)?date('Y-m-d',strtotime($start_date)):'';
		$end_date=$request->input('end_date');
		$end_date=isset($end_date)?date('Y-m-d',strtotime($end_date)):'';
		if($cek['result']==1){
			$data=DB::table(DB::raw('(select "B/F" as ref_no, "'.$start_date.'" as date,"" as pic,"0000-00-00" as created_at, 0 as debit, 0 as credit,
				(select ifnull(sum(if(credit_debit="IN",amount,if(credit_debit="OUT",-amount,0))),0)as balance from transaction_banks where bank_id="'.$bank_id.'" and date<"'.$start_date.'")as balance
				union all
				select tb.ref_no,tb.date,tb.pic,tb.created_at,
				if(tb.credit_debit="IN",amount,0)as debit,if(tb.credit_debit="OUT",amount,0)as credit,
				if(tb.credit_debit="IN",@i:=@i+amount,if(tb.credit_debit="OUT",@i:=@i-amount,@i:=@i))as balance
				from transaction_banks tb
				join (select @i:=0)as init
				where tb.bank_id="'.$bank_id.'" and tb.date>="'.$start_date.'" and tb.date<="'.$end_date.'")as sub order by date asc, created_at asc'))->get();

			$data=['data'=>$data];

			return $data;
		}else{
			return error_unauthorized();
		}
	}

	public function reportBankStatement(Request $request)
	{
		// return $request;
		$data=$this->showBankStatement($request);
		// exit(json_encode($data));
		$data['payment_method']=PaymentMethod::find($request->input('payment_method_id'));
		$data['start_date']=date('d-m-Y',strtotime($request->input('start_date')));
		$data['end_date']=date('d-m-Y',strtotime($request->input('end_date')));

		$mpdf = new \mPDF();
		$mpdf->AddPage('L', 'A4');
		$html = View::make('pdf.bank',$data)->render();
		// exit($html);

		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}

}
