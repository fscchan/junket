<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\UserActivity;
use Validator;
use App\JournalAccount;
use App\JournalAccountDetail;
use App\AccountConfig;
use DB;

class JournalAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $date = $request->input('date');
        $sort_by = $request->input('sort_by');
        $sort_type = $request->input('sort_type');
		$company_id=$request->input('company_id');
		$company_id=$company_id?$company_id:1;
		
		$where='(company_id='.$company_id.' '.($company_id==1?'or company_id is null':'').')';

		$journals = JournalAccount::select('journal_accounts.*')
			->whereRaw($where);
		
		if($date!=null){
			$journals = $journals->whereDate('date','=',$date);
		}
		
		if(!$sort_by){
			$sort_by='created_at';
		}
		$sort_type=$sort_type?$sort_type:'desc';
		
		$journals = $journals->orderBy($sort_by,$sort_type)->paginate(10);
		
        return response()->json(transformCollection($journals), 200);
    }
	
	/**
     * Get number.
     *
     * @return \Illuminate\Http\Response
	*/
	public function number(Request $request)
	{
		$user = User::where('token',$request->header('x-auth-token'))->first();
		$cek = check_auth($user,'read journals');

		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$data=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
		
		return [
			'status'=>'success',
			'data'=>$data
		];
	}
	
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'create journals');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $validator = Validator::make($request->all(), [
            'je_num' => 'unique:journal_accounts',
            'date' => 'required|date'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

		$data = $request->all();
		$data['user_id']=$user->id;
		$journal_account = JournalAccount::create($data);
		
		UserActivity::storeActivity(array(
			"activity" => "add journal account for id: ".$journal_account->id,
			"user" => $user->id,
			"menu" => "transactions",
			"ipaddress" => $request->ip()
		));

        if($journal_account){
            return response()->json([
                "status" => "success",
                "message" => "journal account has been saved!",
                "data" => $journal_account,
				"id" => $journal_account->id
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save journal account!",
                "data" => null
            ],403);
        }
    }
	
	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'read journals');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $journal_account = JournalAccount::find($id);
		
        if($journal_account!=null){
            return response()->json([
                "message" => "success",
                "data" => $journal_account,
            ],200);
        }else{
            return response()->json(["message" => "journal account not exists!"],404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'update journals');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $validator = Validator::make($request->all(), [
            'je_num' => 'unique:journal_accounts,id,'.$id,
            'date' => 'required|date'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

		$data = $request->all();
		$data['user_id']=$user->id;
		$journal_account = JournalAccount::find($id)->update($data);
		
		UserActivity::storeActivity(array(
			"activity" => "update journal account for id: ".$id,
			"user" => $user->id,
			"menu" => "journals",
			"ipaddress" => $request->ip()
		));

        if($journal_account){
            return response()->json([
                "status" => "success",
                "message" => "journal account has been saved!",
                "data" => $journal_account,
				"id" => $id
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save journal account!",
                "data" => null
            ],403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'delete journals');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $journal_account = JournalAccount::find($id);

        if($journal_account == null){
            return response()->json(["message"=>"journal account not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();

        if (JournalAccount::destroy($id)) {
            UserActivity::storeActivity(array(
                "activity" => "delete journal account for id: ".$id,
                "user" => $user->id,
                "menu" => "journals",
                "ipaddress" => $request->ip()
            ));

            return response()->json([
                "status" => "success",
                "message" => "journal account has been deleted!"
            ], 200);
        } else {
            return response()->json([
                "status" => "error",
                "message" => "Failed delete journal account!"
            ], 403);
        }
    }
	
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDetail(Request $request,$id)
    {
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'create journals');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $validator = Validator::make($request->all(), [
            'account_id' => 'required|integer',
            'debit' => 'numeric',
            'credit' => 'numeric'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

		$data = $request->all();
		
		$data['journal_id']=$id;
		$journal_account = JournalAccountDetail::create($data);
		
		$this->journal_cash($id);
		
		UserActivity::storeActivity(array(
			"activity" => "add journal account detail for id: ".$id,
			"user" => $user->id,
			"menu" => "transactions",
			"ipaddress" => $request->ip()
		));

        if($journal_account){
            return response()->json([
                "status" => "success",
                "message" => "journal account detail has been saved!",
                "data" => $journal_account
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save journal account detail!",
                "data" => null
            ],403);
        }
    }
	
	function journal_cash($id){
		$account_config=AccountConfig::first();
		$cash=JournalAccountDetail::where('journal_id','=',$id)
			->where('account_id','=',$account_config['cash_real'])
			->first();
			
		$sum_cash=JournalAccountDetail::select([DB::raw('sum(credit-debit)as balance')])
			->where('journal_id','=',$id)
			->where('account_id','!=',$account_config['cash_real'])
			->first();
			
		$data_c['journal_id']=$id;
		$data_c['account_id']=$account_config['cash_real'];
		$data_c['debit']=$sum_cash['balance']>0?$sum_cash['balance']:0;
		$data_c['credit']=$sum_cash['balance']<0?-$sum_cash['balance']:0;
		if(!$cash){
			JournalAccountDetail::create($data_c);
		}else{
			JournalAccountDetail::where('id','=',$cash['id'])->update($data_c);
		}
	}
	
	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function showDetailByMaster(Request $request, $id)
	{
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'update journals');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$account_config=AccountConfig::first();
		$data=JournalAccountDetail::select(['journal_account_details.*'])
			->join('journal_accounts','journal_accounts.id','=','journal_account_details.journal_id')
			->where('journal_accounts.id','=',$id)
			->whereRaw('((journal_account_details.account_id!='.$account_config['cash_real'].' 
				 and journal_accounts.activity is null)
				 or journal_accounts.activity is not null)')
			->get();
		
		return [
			'status'=>'success',
			'data'=>$data
		];
	}
	
	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function showDetail(Request $request, $id)
	{
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'update journals');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$data=JournalAccountDetail::find($id);
		
		return [
			'status'=>'success',
			'data'=>$data
		];
	}

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateDetail(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'update journals');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $validator = Validator::make($request->all(), [
            'account_id' => 'required|integer',
            'debit' => 'numeric',
            'credit' => 'numeric'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

		$data = $request->all();

		$journal_account=JournalAccountDetail::find($id);
		$journal_account->update($data);
		
		$this->journal_cash($journal_account['journal_id']);
		
		UserActivity::storeActivity(array(
			"activity" => "update journal account detail for id: ".$id,
			"user" => $user->id,
			"menu" => "journals",
			"ipaddress" => $request->ip()
		));

        if($journal_account){
            return response()->json([
                "status" => "success",
                "message" => "journal account detail has been saved!",
                "data" => $journal_account
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save journal account detail!",
                "data" => null
            ],403);
        }
    }
	
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyDetailByMaster(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'delete journals');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $journal_account = JournalAccountDetail::find($id);

        if($journal_account == null){
            return response()->json(["message"=>"journal account detail not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();

        if (JournalAccountDetail::where('journal_id','=',$id)->delete()) {
            UserActivity::storeActivity(array(
                "activity" => "delete journal account detail for id: ".$id,
                "user" => $user->id,
                "menu" => "journals",
                "ipaddress" => $request->ip()
            ));

            return response()->json([
                "status" => "success",
                "message" => "journal account detail has been deleted!"
            ], 200);
        } else {
            return response()->json([
                "status" => "error",
                "message" => "Failed delete journal account detail!"
            ], 403);
        }
    }
	
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyDetail(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'delete journals');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $journal_account = JournalAccountDetail::find($id);
		$journal_id=$journal_account['journal_id'];

        if($journal_account == null){
            return response()->json(["message"=>"journal account detail not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();

        if (JournalAccountDetail::destroy($id)) {
			$this->journal_cash($journal_id);
            UserActivity::storeActivity(array(
                "activity" => "delete journal account detail for id: ".$id,
                "user" => $user->id,
                "menu" => "journals",
                "ipaddress" => $request->ip()
            ));

            return response()->json([
                "status" => "success",
                "message" => "journal account detail has been deleted!"
            ], 200);
        } else {
            return response()->json([
                "status" => "error",
                "message" => "Failed delete journal account detail!"
            ], 403);
        }
    }
}
