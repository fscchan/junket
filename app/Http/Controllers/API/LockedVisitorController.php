<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\LockedVisitor;
use Validator;
use App\User;

class LockedVisitorController extends Controller
{

    public function index(Request $request)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        if($user){
            if($user->roles[0]->id == 1){  //only admin
                $list_locked = LockedVisitor::groupBy('ip_address')->paginate(25);
            }
            return response()->json(transformCollection($list_locked), 200);
        }

        return error_unauthorized();
    }

    public function store(Request $request)
    {
        $log = LockedVisitor::create(["ip_address" => $request->ip()]);

        if($log){
            return response()->json([
                "status" => "success",
                "message"=>"Locked Visitor has been saved!",
                "data" => $log
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save locked visitor!",
                "data" => null
            ],403);
        }
    }

    public function show($id)
    {
        $locked = LockedVisitor::find($id);
        if($locked == null){
            return response()->json([
                "message" => "Data not found!",
                "data" => null
            ], 404);
        }else{
            return response()->json([
                "message" => "Success",
                "data" => $locked
            ],200);
        }
    }

    public function destroy(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        if($user){
            if($user->roles[0]->id == 1) {
                $visitor = LockedVisitor::find($id);
                if($visitor == null){
                    return response()->json(['message' => 'Data not found'], 404);
                }else{
                    if (LockedVisitor::where('ip_address', $visitor->ip_address)->delete()) {
                        return response()->json([
                            "status" => "success",
                            "message" => "Locked visitor has been deleted!"
                        ], 200);
                    } else {
                        return response()->json([
                            "status" => "error",
                            "message" => "Failed delete locked visitor!"
                        ], 403);
                    }
                }
            }
        }
        return error_unauthorized();
    }

    public function clear(Request $request)
    {
        $ip_address = $request->ip();
        $log = LockedVisitor::where('ip_address', $ip_address)->delete();

        if($log){
            return response()->json([
                "status" => "success",
                "message" => "Locked visitor has been cleared!"
            ], 200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed clear locked visitor!"
            ], 200);
        }
    }

}
