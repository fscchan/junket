<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Company;
use App\User;
use App\AccountConfig;
use Validator;
use DB;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Company::first();
        return response()->json(["data" => $company], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'nn_chips' => 'required|numeric',
            'cash_chips' => 'required|numeric',
            'cash_real' => 'required|numeric',
            'date' => 'required',
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $data = $request->all();
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            if($user->roles[0]->id == 1) {
                $log = Company::create($data);
            }else{
                return error_unauthorized();
            }

            if($log){
                return response()->json([
                    "status" => "success",
                    "message" => "Company has been saved!",
                    "data" => $log
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed save company!",
                    "data" => null
                ],403);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);
        if($company == null){
            return response()->json([
                "message" => "Data not found!",
                "data" => null
            ], 404);
        }else{
            return response()->json([
                "message" => "Success",
                "data" => $company
            ],200);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'nn_chips' => 'required|numeric',
            'cash_chips' => 'required|numeric',
            'cash_real' => 'required|numeric',
            'date' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $data = $request->all();
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            if($user->roles[0]->id == 1) {
                $company = Company::find($id);
                if($company == null){
                    return response()->json(['message' => 'Data not found'], 404);
                }else{
                    $log = $company->update($data);
                }
            }else{
                return error_unauthorized();
            }

            if($log){
                return response()->json([
                    "status" => "success",
                    "message" => "Company has been updated!",
                    "data" => $company
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed update company!",
                    "data" => null
                ],403);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        if($user->roles[0]->id == 1) {
            $company = Company::find($id);
            if($company == null){
                return response()->json(['message' => 'Data not found'], 404);
            }else{
                if (Company::destroy($id)) {
                    return response()->json([
                        "status" => "success",
                        "message" => "Company has been deleted!"
                    ], 200);
                } else {
                    return response()->json([
                        "status" => "error",
                        "message" => "Failed delete company!"
                    ], 403);
                }
            }
        }else{
            return error_unauthorized();
        }

    }
	
	public function info(){
		$account_config=AccountConfig::first();
		$data=DB::table(DB::raw('(select 1 as id,
			sum(if(account_id='.$account_config['nn_chips'].',debit-credit,0))as nn_chips,
			sum(if(account_id='.$account_config['junket_chips'].',debit-credit,0))as junket_chips,
			sum(if(account_id='.$account_config['cash_chips'].',debit-credit,0))as cash_chips,
			sum(if(account_id='.$account_config['cash_real'].',debit-credit,0))as cash_real,
			sum(if(account_id='.$account_config['bank'].',debit-credit,0))as bank_balance,
			sum(if(account_id=5,credit-debit,0))as boss_share,
			sum(if(account_id=6,credit-debit,0))as agent_share,
			sum(if(account_id='.$account_config['open_balance_asset'].',credit-debit,0))as open_balance_asset
			from (
				select jad.account_id,a.nature_type,
					sum(if(jad.debit>0,jad.debit,if(jad.credit<0,-jad.credit,0)))as debit,
					sum(if(jad.credit>0,jad.credit,if(jad.debit<0,-jad.debit,0)))as credit
				from journal_accounts ja
				join journal_account_details jad on jad.journal_id=ja.id
				join accounts a on a.id=jad.account_id
				where ja.deleted_at is null and a.deleted_at is null and
				     (jad.account_id='.$account_config['nn_chips'].' or
					 jad.account_id='.$account_config['cash_chips'].' or
					 jad.account_id='.$account_config['junket_chips'].' or
					 jad.account_id='.$account_config['cash_real'].')
				and ja.id not in (select distinct jaw.id
					from journal_accounts jaw
					join journal_account_details jadw on jadw.journal_id=jaw.id
					join accounts aw on aw.parent_id=jadw.account_id or aw.id=jadw.account_id
					where (aw.parent_id=jadw.account_id and jaw.activity="account")
					union all
					select distinct jaw.id
					from journal_accounts jaw
					join journal_account_details jadw on jadw.journal_id=jaw.id
					join accounts aw on aw.id=jadw.account_id
					left join accounts aw2 on aw2.id=aw.parent_id
					where (aw.parent_id is null and aw.deleted_at is not null)
					 or (aw2.id=aw.parent_id and aw2.deleted_at is not null 
					   and aw.id not in ('.$account_config['nn_chips'].','.$account_config['junket_chips'].',
					   '.$account_config['cash_chips'].','.$account_config['cash_real'].',
					   '.$account_config['open_balance_asset'].')))
				group by jad.account_id
				union all
				select '.$account_config['bank'].' as account_id,a.nature_type,
					sum(if(jad.debit>0,jad.debit,if(jad.credit<0,-jad.credit,0)))as debit,
					sum(if(jad.credit>0,jad.credit,if(jad.debit<0,-jad.debit,0)))as credit
				from journal_accounts ja
				join journal_account_details jad on jad.journal_id=ja.id
				join accounts a on a.id=jad.account_id
				where ja.deleted_at is null and a.parent_id='.$account_config['bank'].' and a.deleted_at is null
				union all
				select '.$account_config['open_balance_asset'].' as account_id,aw.nature_type,
				 sum(ifnull(aw2.debit,
				  (if(aw.nature_type="debit",if(aw.balance>0,aw.balance,0),if(aw.nature_type="credit",if(aw.balance<0,-aw.balance,0),0)))))as debit,
				 sum(ifnull(aw2.credit,
				  (if(aw.nature_type="credit",if(aw.balance>0,aw.balance,0),if(aw.nature_type="debit",if(aw.balance<0,-aw.balance,0),0)))))as credit
				from accounts aw
				left join (
					select parent_id,
					sum(if(nature_type="debit",if(balance>0,balance,0),if(nature_type="credit",if(balance<0,-balance,0),0)))as debit,
					sum(if(nature_type="credit",if(balance>0,balance,0),if(nature_type="debit",if(balance<0,-balance,0),0)))as credit
					from accounts 
					where parent_id is not null and deleted_at is null
					group by parent_id
				)as aw2 on aw2.parent_id=aw.id
				where aw.deleted_at is null and aw.parent_id is null
			)as sub)as sub'))->first();
			
		return response()->json([
			'status'=>'success',
			'data'=>$data
		]);
	}
}
