<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\PurchasesFromCustomer;
use App\Customer;
use App\UserActivity;
use Validator;
use App\JournalAccount;
use App\JournalAccountDetail;
use App\AccountConfig;
use App\UserHasAccount;
use DB;

class PurchasesFromCustomerController extends Controller
{
    /**
     * Index Purchase List
     * GET /api/purchases_from_customer?sort_by=&sort_type=&date=
     * Available sort = date, user, value
     *
     * @param string $token     The token for authentication
     * @return Response
     **/
    public function index(Request $request)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read purchases_from_customer');
		
        if($cek['result']!=1){
            return error_unauthorized();
        }
		
		$date = $request->input('date');
		$sort_by = $request->input('sort_by');
		$sort_type = $request->input('sort_type');
		
		if(!$sort_by){
			$sort_by='purchases_from_customers.created_at';
		}
		$sort_type=$sort_type?$sort_type:'desc';
		
		switch($sort_by){
			case 'user':
				$sort_by='users.name';
				break;
			case 'vendor':
				$sort_by='vendors.name';
				break;
			case 'method':
				$sort_by='payment_methods.name';
				break;
			case 'customer':
				$sort_by='customers.first_name';
				break;
		}
		
		$purchases = PurchasesFromCustomer::select('purchases_from_customers.*')
			->join('users', 'users.id', '=', 'purchases_from_customers.user_id','left')
			->join('customers', 'customers.id', '=', 'purchases_from_customers.customer_id','left')
			->orderBy($sort_by,$sort_type);
			
		if($date){
			$purchases = $purchases->where('date','=',$date);
		}
		
		$purchases = $purchases->paginate(10);

        return response()->json(transformCollection($purchases), 200);
    }

    /**
     * Store purchases_from_customer
     * POST /api/purchases_from_customer
     *
     * @param string $token                     The token for authentication
     * @param date $date                        The purchasing date
     * @param integer $user_id                  The user id
     * @param double $value                     The value of purchasing
     * @return Response
     **/
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'user_id' => 'required',
            'value' => 'required|numeric',
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $data = $request->all();
			
			$customer=Customer::find($data['customer_id']);
			switch($customer->customer_type_id){
				case 1:
					$data['commission_percent']=$data['commission_percent']?$data['commission_percent']:1.1;
				break;
				case 2:
					$data['commission_percent']=$data['commission_percent']?$data['commission_percent']:1.5;
				break;
			}
			
			$data['commission_percent']=$data['commission_percent']?$data['commission_percent']:0;
			$data['commission']=$data['value']*$data['commission_percent']/100;
			
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'create purchases_from_customer');
            if($cek['result']==1){
                $log = PurchasesFromCustomer::create($data);
				$this->journal($log->id,$request);
                UserActivity::storeActivity(array(
                    "activity" => "add purchase for id: ".$log->id,
                    "user" => $user->id,
                    "menu" => "purchases_from_customer",
                    "ipaddress" => $request->ip()
                ));
            }else{
                return error_unauthorized();
            }

            if($log){
                return response()->json([
                    "status" => "success",
                    "message" => "Purchasing has been saved!",
                    "data" => $log
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed save purchasing!",
                    "data" => null
                ],403);
            }
        }
    }

    /**
     * Journal Purchase.
     *
     * @param  int  $id
     * @param  object  $request from Request Class
     * @return boolean
     */
	public function journal($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = PurchasesFromCustomer::find($id);
		
		DB::beginTransaction();
		
		try{
			$uha=UserHasAccount::where('user_id','=',$data['user_id'])->first();
			$account_config=AccountConfig::first();
			$journal=JournalAccount::where('activity','=','purchases_from_customer')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>$data['date'],
				'user_id'=>$user['id'],
				'entity'=>'manager',
				'entity_id'=>$data['user_id'],
				'activity'=>'purchases_from_customer',
				'activity_id'=>$id,
				'remark'=>'Purchase ('.$data['user']['name'].')'
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$data['value']=isset($data['value'])?$data['value']:0;
				$data['commission']=isset($data['commission'])?$data['commission']:0;
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$data['account_id'],//cash real
					'debit'=>0,
					'credit'=>$data['commission'],
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_real'],//cash real
					'debit'=>0,
					'credit'=>$data['value'],
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_real'],//commission
					'debit'=>$data['commission'],
					'credit'=>0,
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
				
				$account_id=$account_config['nn_chips'];
				switch($data['type_chip']){
					case 'NN Chip':
						$account_id=$account_config['nn_chips'];
					break;
					case 'Cash Chip':
						$account_id=$account_config['cash_chips'];
					break;
					case 'Junket NN Chip':
						$account_id=$account_config['junket_chips'];
					break;
				}
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_id,//chips
					'debit'=>$data['value'],
					'credit'=>0,
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
			}
			
			DB::commit();
		}catch(\Exception $exc){
			echo $exc;
			DB::rollback();
		}
			
		return true;
	}
	
    /**
     * Index Purchase List
     * GET /api/purchases_from_customer/{id}
     *
     * @param string $token     The token for authentication
     * @return Response
     **/
    public function show(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read purchases_from_customer');
        if($cek['result']==1){
            $purchase = PurchasesFromCustomer::find($id);
            if($purchase!=null){
                return response()->json([
                    "message" => "success",
                    "data" => $purchase
                ],200);
            }else{
                return response()->json(["message" => "Purchasing not exists!"],404);
            }
        }else{
            return error_unauthorized();
        }

    }

    /**
     * Update purchases_from_customer
     * PUT /api/purchases_from_customer/{id}
     *
     * @param string $token                     The token for authentication
     * @param date $date                        The purchasing date
     * @param integer $user_id                  The user id
     * @param integer $vendor_id                The vendor id
     * @param integer $payment_method_id        The payment method id
     * @param double $value                     The value of purchasing
     * @return Response
     **/
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'user_id' => 'required',
            'value' => 'required|numeric',
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $data = $request->all();
			
			$customer=Customer::find($data['customer_id']);
			switch($customer->customer_type_id){
				case 1:
					$data['commission_percent']=$data['commission_percent']?$data['commission_percent']:1.1;
				break;
				case 2:
					$data['commission_percent']=$data['commission_percent']?$data['commission_percent']:1.5;
				break;
			}
			
			$data['commission_percent']=$data['commission_percent']?$data['commission_percent']:0;
			$data['commission']=$data['value']*$data['commission_percent']/100;

            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'update purchases_from_customer');
            if($cek['result']==1){
                UserActivity::storeActivity(array(
                    "activity" => "update purchase for id: ".$id,
                    "user" => $user->id,
                    "menu" => "purchases_from_customer",
                    "ipaddress" => $request->ip()
                ));

                $purchase = PurchasesFromCustomer::find($id);
                if($purchase==null){
                    return response()->json(["message" => "Purchasing not exists!"],404);
                }else{
                    $purchase->update($data);
					$this->journal($id,$request);
                }
            }else{
                return error_unauthorized();
            }

            if($purchase){
                return response()->json([
                    "status" => "success",
                    "message" => "Purchasing has been updated!",
                    "data" => $purchase
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed update purchasing!",
                    "data" => null
                ],403);
            }
        }
    }

    /**
     * Delete Purchase List
     * DELETE /api/purchases_from_customer/{id}
     *
     * @param string $token     The token for authentication
     * @return Response
     **/
    public function destroy(Request $request, $id)
    {
        $purchase = PurchasesFromCustomer::find($id);

        if($purchase == null){
            return response()->json(["message"=>"Purchasing not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete purchases_from_customer');
        if($cek['result']==1) {
            UserActivity::storeActivity(array(
                "activity" => "delete purchasing for id: ".$id,
                "user" => $user->id,
                "menu" => "purchases_from_customer",
                "ipaddress" => $request->ip()
            ));
            if (Purchase::destroy($id)) {
				JournalAccount::where('activity','=','purchases_from_customer')->where('activity_id','=',$id)->delete();
                return response()->json([
                    "status" => "success",
                    "message" => "Purchasing has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete purchasing!"
                ], 403);
            }
        }else{
            return error_unauthorized();
        }
    }
}
