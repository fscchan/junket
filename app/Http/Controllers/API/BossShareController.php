<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\BossShare;
use App\UserActivity;
use Validator;
use App\JournalAccount;
use App\JournalAccountDetail;
use DB;

class BossShareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shares = BossShare::all();

        return response()->json(['data' => $shares], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'boss_id' => 'required|unique:boss_shares',
            'amount' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $data = $request->all();
        $log = BossShare::create($data);
		$this->journal($log->id,$request);

        UserActivity::storeActivity(array(
            "activity" => "add boss share for id: ".$log->id,
            "user" => $user->id,
            "menu" => "set up",
            "ipaddress" => $request->ip()
        ));

        if($log){
            return response()->json([
                "status" => "success",
                "message" => "Boss share has been saved!",
                "data" => $log,
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save boss share!",
                "data" => null
            ],403);
        }
    }

	/**
     * Journal Boss Share.
     *
     * @param  int  $id
     * @param  object  $request from Request Class
     * @return boolean
     */
	public function journal($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = BossShare::find($id);
		
		DB::beginTransaction();
		
		try{
			$journal=JournalAccount::where('activity','=','boss_share')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>$data['date'],
				'user_id'=>$user['id'],
				'entity'=>'boss',
				'entity_id'=>$data['boss_id'],
				'activity'=>'boss_share',
				'activity_id'=>$id,
				'remark'=>'Boss Share ('.$data['user']['name'].')'
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$journal->save();
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$data['amount']=isset($data['amount'])?$data['amount']:0;
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>5,//boss share
					'debit'=>0,
					'credit'=>$data['amount']
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>8,//opening balance asset
					'debit'=>$data['amount'],
					'credit'=>0
				]);
			}
			
			DB::commit();
		}catch(\Exception $exc){
			// echo $exc;
			DB::rollback();
		}
			
		return true;
	}
	
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $share = BossShare::find($id);

        if($share != null){
            return response()->json([
                "message" => "success",
                "data" => $share
            ],200);
        }else {
            return response()->json(["message" => "Data not found!"],404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'boss_id' => 'required|unique:boss_shares,id,'.$id,
            'amount' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $data = $request->all();
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $share = BossShare::find($id);
            if($share == null){
                return response()->json(['message' => 'Data not found'], 404);
            }else{
                $log = $share->update($data);
				$this->journal($id,$request);
            }

            if($log){
                UserActivity::storeActivity(array(
                    "activity" => "update boss share for id: ".$id,
                    "user" => $user->id,
                    "menu" => "set up",
                    "ipaddress" => $request->ip()
                ));

                return response()->json([
                    "status" => "success",
                    "message" => "Boss share has been updated!",
                    "data" => $share
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed update Boss share!",
                    "data" => null
                ],403);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $share = BossShare::find($id);
        if($share == null){
            return response()->json(['message' => 'Data not found'], 404);
        }else{
            if (BossShare::destroy($id)) {
				JournalAccount::where('activity','=','boss_share')->where('activity_id','=',$id)->delete();
                UserActivity::storeActivity(array(
                    "activity" => "delete boss share for id: ".$id,
                    "user" => $user->id,
                    "menu" => "set up",
                    "ipaddress" => $request->ip()
                ));

                return response()->json([
                    "status" => "success",
                    "message" => "Boss share has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete boss share!"
                ], 403);
            }
        }
    }
}
