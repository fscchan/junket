<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\PaymentMethod;
use App\TransactionBank;
use Validator;
use App\UserActivity;
use App\JournalAccount;
use App\JournalAccountDetail;
use App\AccountConfig;
use DB;

class TransactionBankController extends Controller
{
    /**
     * Index Transaction Bank
     * GET /api/transactions?date=
     *
     * @param string $token                 The token for authentication
     * @return Response
     **/
    public function index(Request $request){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read transaction_banks');
        if($cek['result']==1){
            $date = $request->input('date');
            $sort_by = $request->input('sort_by');
            $sort_type = $request->input('sort_type');
			
			if(!$sort_by){
				$sort_by='transaction_banks.created_at';
			}
			$sort_type=$sort_type?$sort_type:'desc';
			
			switch($sort_by){
				case 'payment_method':
					$sort_by='payment_methods.name';
					break;
				case 'bank':
					$sort_by='banks.name';
					break;
				case 'bank_account':
					$sort_by='accounts.name';
					break;
			}
			
			$transactions = TransactionBank::select('transaction_banks.*')
				->join('payment_methods','payment_methods.id','=','transaction_banks.payment_method_id','left')
				->join('banks','banks.id','=','transaction_banks.bank_id','left')
				->join('accounts','accounts.id','=','transaction_banks.bank_account_id','left');
			
            if($date!=null){
                $trasactions = $transactions->where('date', $date);
            }
			
			$trasactions = $transactions->orderBy($sort_by,$sort_type)->paginate();
        }else{
            return error_unauthorized();
        }

        return response()->json(transformCollection($trasactions), 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'credit_debit' => 'required|in:IN,OUT',
            'bank_account_id' => 'required|integer',
            'amount' => 'required|numeric',
			'remark' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else {
            $data = $request->all();
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'create transaction_banks');
            if($cek['result'] == 1){
                $data['user_id'] = $user->id;
                $transaction = TransactionBank::create($data);
				$this->journal($transaction->id,$request);
                UserActivity::storeActivity(array(
                    "activity" => "add transacton bank for id: ".$transaction->id,
                    "user" => $user->id,
                    "menu" => "transaction_banks",
                    "ipaddress" => $request->ip()
                ));
            }else{
                return error_unauthorized();
            }
        }

        if($transaction){
            return response()->json([
                "status" => "success",
                "message" => "Transaction has been saved!",
                "data" => $transaction
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save transaction!",
                "data" => null
            ],403);
        }
    }
	
    /**
     * Journal Transaction.
     *
     * @param  int  $id
     * @param  object  $request from Request Class
     * @return boolean
     */
	public function journal($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = TransactionBank::find($id);
		
		DB::beginTransaction();
		
		try{
			$entity='account';
			$entity_id=$data['bank_account_id'];
			
			$journal=JournalAccount::where('activity','=','transaction_bank')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>$data['date'],
				'user_id'=>$user['id'],
				'entity'=>$entity,
				'entity_id'=>$entity_id,
				'activity'=>'transaction_bank',
				'activity_id'=>$id,
				'remark'=>$data['remark']
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$account_config=AccountConfig::first();
				$data['amount']=isset($data['amount'])?$data['amount']:0;
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_real'],
					'debit'=>$data['credit_debit']=='IN'?$data['amount']:0,
					'credit'=>$data['credit_debit']=='OUT'?$data['amount']:0,
					'entity'=>$entity,
					'entity_id'=>$entity_id
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$data['bank_account_id'],//bank account
					'debit'=>$data['credit_debit']=='OUT'?$data['amount']:0,
					'credit'=>$data['credit_debit']=='IN'?$data['amount']:0,
					'entity'=>$entity,
					'entity_id'=>$entity_id
				]);
			}
			
			DB::commit();
		}catch(\Exception $exc){
			// echo $exc;
			DB::rollback();
		}
			
		return true;
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read transaction_banks');
        if($cek['result']==1){
            $transaction = TransactionBank::find($id);
            if($transaction!=null){
                return response()->json([
                    "message" => "success",
                    "data" => $transaction
                ],200);
            }else{
                return response()->json(["message" => "Transaction not exists!"],404);
            }
        }else{
            return error_unauthorized();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'credit_debit' => 'required|in:IN,OUT',
            'bank_account_id' => 'required|integer',
            'amount' => 'required|numeric',
			'remark' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else {
            $data = $request->all();
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'update transaction_banks');
            if($cek['result'] == 1){
                $data['user_id'] = $user->id;

                $transaction = TransactionBank::find($id);
                if($transaction == null){
                    return response()->json(["message" => "Transaction not exists!"],404);
                }else{
                    $transaction->update($data);
					$this->journal($id,$request);
                    UserActivity::storeActivity(array(
                        "activity" => "update transaction bank for id: ".$id,
                        "user" => $user->id,
                        "menu" => "transaction_banks",
                        "ipaddress" => $request->ip()
                    ));
                }
            }else{
                return error_unauthorized();
            }
        }

        if($transaction){
            return response()->json([
                "status" => "success",
                "message" => "Transaction bank has been updated!",
                "data" => $transaction
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed update transaction bank!",
                "data" => null
            ],403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $transaction = TransactionBank::find($id);

        if($transaction == null){
            return response()->json(["message"=>"Transaction not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete transaction_banks');
        if($cek['result']==1) {
            if (TransactionBank::destroy($id)) {
				JournalAccount::where('activity','=','transaction_bank')->where('activity_id','=',$id)->delete();
                UserActivity::storeActivity(array(
                    "activity" => "delete transaction bank for id: ".$id,
                    "user" => $user->id,
                    "menu" => "transaction_banks",
                    "ipaddress" => $request->ip()
                ));
                return response()->json([
                    "status" => "success",
                    "message" => "Transaction bank has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete transaction bank!"
                ], 403);
            }
        }else{
            return error_unauthorized();
        }
    }
}
