<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Expense;
use App\UserActivity;
use App\User;
use Validator;
use App\JournalAccount;
use App\JournalAccountDetail;
use App\AccountConfig;
use App\UserHasAccount;
use DB;

class ExpenseController extends Controller
{
    /**
     * Index Transaction List
     * GET /api/expenses?date=&sort_by=&sort_type=
     * Available sort = date, expense_no, user, annotation, amount
     *
     * @param string $token     The token for authentication
     * @return Response
     **/
    public function index(Request $request)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read expenses');
		
        if($cek['result']!=1){
            return error_unauthorized();
        }
		
		$date = $request->input('date');
		$sort_by = $request->input('sort_by');
		$sort_type = $request->input('sort_type');
		
		if(!$sort_by){
			$sort_by='expenses.created_at';
		}
		$sort_type=$sort_type?$sort_type:'desc';
		
		switch($sort_by){
			case 'user':
				$sort_by='users.name';
				break;
			case 'manager':
				$sort_by='users2.name';
				break;
		}
		
		$expenses = Expense::select('expenses.*')
			->join('users', 'users.id', '=', 'expenses.user_id','left')
			->join('users as users2', 'users2.id', '=', 'expenses.manager_id','left')
			->orderBy($sort_by,$sort_type);
			
		if($date){
			$expenses = $expenses->where('date','=',$date);
		}
		
		$expenses = $expenses->paginate(10);

        return response()->json(transformCollection($expenses), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'annotation' => 'required',
            'amount' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'create expenses');
        if($cek['result']==1){
            $data = $request->all();
            $data['expense_no'] = getAutoNumber('expense_no','expenses','EX-'.date('my')."-",4);
			$data['user_id']=isset($data['user_id'])?$data['user_id']:$user->id;
            $log = Expense::create($data);
			$this->journal($log->id,$request);
            if($log){
                if($request->hasFile("image")) {
                    $this->uploadImage($request, $log);
                }
            }
            UserActivity::storeActivity(array(
                "activity" => "add expense for id: ".$log->id,
                "user" => $user->id,
                "menu" => "expenses",
                "ipaddress" => $request->ip()
            ));
        }else{
            return error_unauthorized();
        }

        if($log){
            if($request->hasFile("image")) {
                $log['image_url'] = url("/") . $log->image_path . $log->image_name;
            }
            return response()->json([
                "status" => "success",
                "message" => "Expense has been saved!",
                "data" => $log,
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save expense!",
                "data" => null
            ],403);
        }
    }

    /**
     * Journal Expense.
     *
     * @param  int  $id
     * @param  object  $request from Request Class
     * @return boolean
     */
	public function journal($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = Expense::find($id);
		
		DB::beginTransaction();
		
		try{
			$uha=UserHasAccount::where('user_id','=',$data['manager_id'])->first();
			$account_config=AccountConfig::first();
			$journal=JournalAccount::where('activity','=','expense')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>$data['date'],
				'user_id'=>$user['id'],
				'entity'=>'user',
				'entity_id'=>$data['manager_id']?$data['manager_id']:$data['user_id'],
				'activity'=>'expense',
				'activity_id'=>$id,
				'remark'=>$data['annotation']
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$data['value']=isset($data['value'])?$data['value']:0;
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>(isset($uha['account_id'])?$uha['account_id']:(strtolower($data['shift'])=='night'?$account_config['expense_night']:$account_config['expense_morning'])),//expense
					'debit'=>$data['amount'],
					'credit'=>0,
					'entity'=>'user',
					'entity_id'=>$data['manager_id']?$data['manager_id']:$data['user_id']
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_real'],//cash
					'debit'=>0,
					'credit'=>$data['amount'],
					'entity'=>'user',
					'entity_id'=>$data['manager_id']?$data['manager_id']:$data['user_id']
				]);
			}
			
			DB::commit();
		}catch(\Exception $exc){
			// echo $exc;
			DB::rollback();
		}
			
		return true;
	}
	
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read expenses');
        if($cek['result']==1){
            $expense = Expense::find($id);
            if($expense!=null){
                $expense['image_url'] = url("/public/") . $expense->image_path . $expense->image_name;
                return response()->json([
                    "message" => "success",
                    "data" => $expense
                ],200);
            }else{
                return response()->json(["message" => "Expense not found!"],404);
            }
        }else{
            return error_unauthorized();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'annotation' => 'required',
            'amount' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'update expenses');
        if($cek['result']==1){
            $expense = Expense::find($id);

            if ($expense == null){
                return response()->json(["message" => "Expense not found!"],404);
            }

            $data = $request->all();
			$data['user_id']=isset($data['user_id'])?$data['user_id']:$user->id;
            $log = $expense->update($data);
			$this->journal($id,$request);
            if($request->hasFile("image")) {
                $this->uploadImage($request, $expense);
            }
            UserActivity::storeActivity(array(
                "activity" => "update expense for id: ".$id,
                "user" => $user->id,
                "menu" => "expenses",
                "ipaddress" => $request->ip()
            ));
        }else{
            return error_unauthorized();
        }

        if($log){
            $expense['image_url'] = url("/") . $expense->image_path . $expense->image_name;
            return response()->json([
                "status" => "success",
                "message" => "Expense has been updated!",
                "data" => $expense
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed update expense!",
                "data" => null
            ],403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $expense = Expense::find($id);

        if($expense == null){
            return response()->json(["message"=>"Expense not found"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete expenses');
        if($cek['result']==1) {
            UserActivity::storeActivity(array(
                "activity" => "delete expense for id: ".$id,
                "user" => $user->id,
                "menu" => "expenses",
                "ipaddress" => $request->ip()
            ));
            if (Expense::destroy($id)) {
				JournalAccount::where('activity','=','expense')->where('activity_id','=',$id)->delete();
                return response()->json([
                    "status" => "success",
                    "message" => "Expense has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete expense!"
                ], 403);
            }
        }else{
            return error_unauthorized();
        }
    }

    public function uploadImage($request, $expense)
    {
        if($request->hasFile("image")){
            $file = array();
            $file["name"] = $expense->id."_".$request->file("image")->getClientOriginalName();
            $file["size"] = $request->file("image")->getClientSize();
            $file["type"] = $request->file("image")->getMimeType();

            $arr_image = array(
                "image_name" => $file["name"],
                "image_size" => $file["size"],
                "image_ext" => $file["type"],
                "image_path" => ""
            );
            $image = $expense->update($arr_image);

            if($image){
                $file["url"] = '/uploads/expenses/'.$file["name"];
                $file["path"] = '/uploads/expenses/';

                if($expense->update(["image_path" => $file["path"]])){
                    $request->file("image")->move(public_path().$file["path"], $expense->image_name);

                    return $image;
                }
            }
        }
    }
	
	public function getByTransaction(Request $request)
	{
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read expenses');
        if($cek['result']==1){
            $expense = Expense::where(['transaction_id'=>$request->input('id')])->get();
			return response()->json([
				"message" => "success",
				"data" => $expense
			],200);
        }else{
            return error_unauthorized();
        }
	}
}
