<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Bank;
use App\User;
use Validator;
use App\UserActivity;
use App\Account;
use App\AccountConfig;
use App\JournalAccount;
use App\JournalAccountDetail;
use DB;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read banks');
        if($cek['result']==1){
            $search = $request->input('search');
            $sort_by = $request->input('sort_by');
            $sort_type = $request->input('sort_type');
            if($search!=null){
                $customers = Bank::sortable([$sort_by => $sort_type])->paginate(10);
            }else{
                if($sort_by != null){
                    $customers = Bank::sortable([$sort_by => $sort_type])->paginate();
                }else{
                    $customers = Bank::orderBy('created_at','desc')->paginate();
                }
            }
        }else{
            return error_unauthorized();
        }

        return response()->json(transformCollection($customers), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100|unique:banks,deleted_at,NULL',
            'account_id' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else {
            $data = $request->all();
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'create banks');
            if($cek['result'] == 1){
                $bank = Bank::create($data);
				
				// $this->journal($bank->id,$request);
				
                UserActivity::storeActivity(array(
                    "activity" => "add bank for id: ".$bank->id,
                    "user" => $user->id,
                    "menu" => "banks",
                    "ipaddress" => $request->ip()
                ));
            }else{
                return error_unauthorized();
            }
        }

        if($bank){
            return response()->json([
                "status" => "success",
                "message" => "Bank has been saved!",
                "data" => $bank
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save bank!",
                "data" => null
            ],403);
        }
    }
	
    /**
     * Journal Bank Balance.
     *
     * @param  int  $id
     * @param  object  $request from Request Class
     * @return boolean
     */
	public function journal($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = Bank::find($id);
		$account_id=$data['account_id'];
		
		$account=Account::find($data['account_id']);
		
		DB::beginTransaction();
		
		try{
		
			if(!$account){
				$account_config=AccountConfig::first();
				$new_account=Account::create([
					'name'=>'Bank ('.$data['name'].')',
					'parent_id'=>$account_config['bank']?$account_config['bank']:4,//account bank parent
					'nature_type'=>'debit',
					'account_type_id'=>1
				]);
				$account_id=$new_account->id;
				if(!$new_account){
					$account_id=Account::where('name','=','Bank ('.$data['name'].')')->value('id');
				}
				$data['account_id']=$account_id;
				$data->save();	
			}else{
				$account['name']=$data['name'];
				$account->save();
			}
			
			$journal=JournalAccount::where('activity','=','bank')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>$data['date'],
				'user_id'=>$user['id'],
				'entity'=>'bank',
				'entity_id'=>$id,
				'activity'=>'bank',
				'activity_id'=>$id,
				'remark'=>'Open Balance bank ('.$data['name'].')'
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$journal['remark']='Open Balance bank ('.$data['name'].')';
				$journal->save();
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$data['balance']=isset($data['balance'])?$data['balance']:0;
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>8,//opening balance asset
					'debit'=>0,
					'credit'=>$data['balance']
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_id,
					'debit'=>$data['balance'],
					'credit'=>0
				]);
			}
			
			DB::commit();
		}catch(\Exception $exc){
			// echo $exc;
			DB::rollback();
		}
			
		return true;
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read banks');
        if($cek['result']==1){
            $bank = Bank::find($id);
            if($bank!=null){
                return response()->json([
                    "message" => "success",
                    "data" => $bank
                ],200);
            }else{
                return response()->json(["message" => "Bank not exists!"],404);
            }
        }else{
            return error_unauthorized();
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100|unique:banks,name,'.$id.',id,deleted_at,NULL',
            'account_id' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else {
            $data = $request->all();
            $user = User::where('token', $request->header("X-Auth-Token"))->first();
            $cek = check_auth($user, 'update banks');
            if ($cek['result'] == 1) {
                $bank = Bank::find($id);
				
                if ($bank == null) {
                    return response()->json(["message" => "Bank not exists!"], 404);
                } else {
                    $bank->update($data);
				
					// $this->journal($id,$request);
                    UserActivity::storeActivity(array(
                        "activity" => "update bank for id: " . $id,
                        "user" => $user->id,
                        "menu" => "banks",
                        "ipaddress" => $request->ip()
                    ));
                }
            } else {
                return error_unauthorized();
            }

            if($bank){
                return response()->json([
                    "status" => "success",
                    "message" => "Bank has been updated!",
                    "data" => $bank
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed update bank!",
                    "data" => null
                ],403);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request ,$id)
    {
        $bank = Bank::find($id);

        if($bank == null){
            return response()->json(["message"=>"Bank not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete banks');
        if($cek['result']==1) {
            UserActivity::storeActivity(array(
                "activity" => "delete bank for id: ".$id,
                "user" => $user->id,
                "menu" => "bank",
                "ipaddress" => $request->ip()
            ));
            if (Bank::destroy($id)) {
				// JournalAccount::where('activity','=','bank')->where('activity_id','=',$id)->delete();
                return response()->json([
                    "status" => "success",
                    "message" => "Bank has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete bank!"
                ], 403);
            }
        }else{
            return error_unauthorized();
        }
    }
}
