<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\ShiftHistory;
use Validator;
use App\UserActivity;
use DB;

class SellChipController extends Controller
{
    /**
     * Index Sell Chips
     * GET /api/sell_chips?date=&sort_by=&sort_type=
     * Available sort = date, from_user, shift_from, nn_chips
     *
     * @param string $token                 The token for authentication
     * @return Response
     **/
    public function index(Request $request)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read sell_chip');
        if($cek['result']==1){
            $date = $request->input('date');
            $sort_by = $request->input('sort_by');
            $sort_type = $request->input('sort_type');
            if($user->roles[0]->id == 1){
                if($date == null){
                    if($sort_by != null && $sort_by != 'from_user' && $sort_by != 'to_user'){
                        $histories = ShiftHistory::select('id','date', 'from_user_id', 'shift_from',
                            DB::raw('SUM(nn_chips) as nn_chips'))
                            ->where('menu_name', 'sell_chip')
                            ->groupBy('date', 'from_user_id', 'shift_from')
                            ->sortable([$sort_by => $sort_type])->paginate(10);
                    }elseif ($sort_by == 'from_user') {
                        $histories = ShiftHistory::select('shift_histories.id','shift_histories.date', 'shift_histories.from_user_id',
                            'shift_histories.shift_from', DB::raw('SUM(shift_histories.nn_chips) as nn_chips'))
                            ->join('users', 'users.id', '=', 'shift_histories.from_user_id')
                            ->where('shift_histories.menu_name', 'sell_chip')
                            ->groupBy('shift_histories.date', 'shift_histories.from_user_id', 'shift_histories.shift_from')
                            ->orderBy('users.name', $sort_type)->paginate(10);
                    }else{
                        $histories = ShiftHistory::select('id','date', 'from_user_id', 'shift_from',
                            DB::raw('SUM(nn_chips) as nn_chips'))
                            ->where('menu_name', 'sell_chip')
                            ->groupBy('date', 'from_user_id', 'shift_from')
                            ->orderBy('created_at','desc')->paginate(10);
                    }
                }else{
                    if($sort_by != null && $sort_by != 'from_user' && $sort_by != 'to_user'){
                        $histories = ShiftHistory::select('id','date', 'from_user_id', 'shift_from',
                            DB::raw('SUM(nn_chips) as nn_chips'))
                            ->where('date', $date)
                            ->where('menu_name', 'sell_chip')
                            ->groupBy('date', 'from_user_id', 'shift_from')
                            ->sortable([$sort_by => $sort_type])->paginate(10);
                    }elseif ($sort_by == 'from_user') {
                        $histories = ShiftHistory::select('shift_histories.id','shift_histories.date', 'shift_histories.from_user_id',
                            'shift_histories.shift_from', DB::raw('SUM(shift_histories.nn_chips) as nn_chips'))
                            ->join('users', 'users.id', '=', 'shift_histories.from_user_id')
                            ->where('shift_histories.date', $date)
                            ->where('shift_histories.menu_name', 'sell_chip')
                            ->groupBy('shift_histories.date', 'shift_histories.from_user_id', 'shift_histories.shift_from')
                            ->orderBy('users.name', $sort_type)->paginate(10);
                    }else{
                        $histories = ShiftHistory::select('id','date', 'from_user_id', 'shift_from',
                            DB::raw('SUM(nn_chips) as nn_chips'))
                            ->where('date', $date)
                            ->where('menu_name', 'sell_chip')
                            ->groupBy('date', 'from_user_id', 'shift_from')
                            ->orderBy('created_at','desc')->paginate(10);
                    }
                }
            }elseif($user->roles[0]->id == 4){
                if($date == null){
                    if($sort_by != null && $sort_by != 'from_user' && $sort_by != 'to_user'){
                        $histories = ShiftHistory::select('id','date', 'from_user_id', 'shift_from',
                            DB::raw('SUM(nn_chips) as nn_chips'))
                            ->where('from_user_id', $user->id)
                            ->where('menu_name', 'sell_chip')
                            ->groupBy('date', 'from_user_id', 'shift_from')
                            ->sortable([$sort_by => $sort_type])->paginate(10);
                    }elseif ($sort_by == 'from_user') {
                        $histories = ShiftHistory::select('shift_histories.id','shift_histories.date', 'shift_histories.from_user_id',
                            'shift_histories.shift_from', DB::raw('SUM(shift_histories.nn_chips) as nn_chips'))
                            ->join('users', 'users.id', '=', 'shift_histories.from_user_id')
                            ->where('shift_histories.from_user_id', $user->id)
                            ->where('shift_histories.menu_name', 'sell_chip')
                            ->groupBy('shift_histories.date', 'shift_histories.from_user_id', 'shift_histories.shift_from')
                            ->orderBy('users.name', $sort_type)->paginate(10);
                    }else{
                        $histories = ShiftHistory::select('id','date', 'from_user_id', 'shift_from',
                            DB::raw('SUM(nn_chips) as nn_chips'))
                            ->where('from_user_id', $user->id)
                            ->where('menu_name', 'sell_chip')
                            ->groupBy('date', 'from_user_id', 'shift_from')
                            ->orderBy('created_at','desc')->paginate(10);
                    }
                }else{
                    if($sort_by != null && $sort_by != 'from_user' && $sort_by != 'to_user'){
                        $histories = ShiftHistory::select('id','date', 'from_user_id', 'shift_from',
                            DB::raw('SUM(nn_chips) as nn_chips'))
                            ->where('date', $date)
                            ->where('from_user_id', $user->id)
                            ->where('menu_name', 'sell_chip')
                            ->groupBy('date', 'from_user_id', 'shift_from')
                            ->sortable([$sort_by => $sort_type])->paginate(10);
                    }elseif ($sort_by == 'from_user') {
                        $histories = ShiftHistory::select('shift_histories.id','shift_histories.date', 'shift_histories.from_user_id',
                            'shift_histories.shift_from', DB::raw('SUM(shift_histories.nn_chips) as nn_chips'))
                            ->join('users', 'users.id', '=', 'shift_histories.from_user_id')
                            ->where('shift_histories.date', $date)
                            ->where('shift_histories.from_user_id', $user->id)
                            ->where('shift_histories.menu_name', 'sell_chip')
                            ->groupBy('shift_histories.date', 'shift_histories.from_user_id', 'shift_histories.shift_from')
                            ->orderBy('users.name', $sort_type)->paginate(10);
                    }else{
                        $histories = ShiftHistory::select('id','date', 'from_user_id', 'shift_from',
                            DB::raw('SUM(nn_chips) as nn_chips'))
                            ->where('date', $date)
                            ->where('from_user_id', $user->id)
                            ->where('menu_name', 'sell_chip')
                            ->groupBy('date', 'from_user_id', 'shift_from')
                            ->orderBy('created_at','desc')->paginate(10);
                    }
                }
            }elseif($user->roles[0]->id == 5 || $user->roles[0]->id == 6) {
                if ($date == null) {
                    if ($sort_by != null && $sort_by != 'from_user' && $sort_by != 'to_user') {
                        $histories = ShiftHistory::select('id', 'date', 'from_user_id', 'shift_from',
                            DB::raw('SUM(nn_chips) as nn_chips'))
                            ->where('to_user_id', $user->id)
                            ->where('menu_name', 'sell_chip')
                            ->groupBy('date', 'from_user_id', 'shift_from')
                            ->sortable([$sort_by => $sort_type])->paginate(10);
                    } elseif ($sort_by == 'from_user') {
                        $histories = ShiftHistory::select('shift_histories.id', 'shift_histories.date', 'shift_histories.from_user_id',
                            'shift_histories.shift_from', DB::raw('SUM(shift_histories.nn_chips) as nn_chips'))
                            ->join('users', 'users.id', '=', 'shift_histories.from_user_id')
                            ->where('shift_histories.to_user_id', $user->id)
                            ->where('shift_histories.menu_name', 'sell_chip')
                            ->groupBy('shift_histories.date', 'shift_histories.from_user_id', 'shift_histories.shift_from')
                            ->orderBy('users.name', $sort_type)->paginate(10);
                    } else {
                        $histories = ShiftHistory::select('id', 'date', 'from_user_id', 'shift_from',
                            DB::raw('SUM(nn_chips) as nn_chips'))
                            ->where('to_user_id', $user->id)
                            ->where('menu_name', 'sell_chip')
                            ->groupBy('date', 'from_user_id', 'shift_from')
                            ->orderBy('created_at', 'desc')->paginate(10);
                    }
                } else {
                    if ($sort_by != null && $sort_by != 'from_user' && $sort_by != 'to_user') {
                        $histories = ShiftHistory::select('id', 'date', 'from_user_id', 'shift_from',
                            DB::raw('SUM(nn_chips) as nn_chips'))
                            ->where('date', $date)
                            ->where('to_user_id', $user->id)
                            ->where('menu_name', 'sell_chip')
                            ->groupBy('date', 'from_user_id', 'shift_from')
                            ->sortable([$sort_by => $sort_type])->paginate(10);
                    } elseif ($sort_by == 'from_user') {
                        $histories = ShiftHistory::select('shift_histories.id', 'shift_histories.date', 'shift_histories.from_user_id',
                            'shift_histories.shift_from', DB::raw('SUM(shift_histories.nn_chips) as nn_chips'))
                            ->join('users', 'users.id', '=', 'shift_histories.from_user_id')
                            ->where('shift_histories.date', $date)
                            ->where('shift_histories.to_user_id', $user->id)
                            ->where('shift_histories.menu_name', 'sell_chip')
                            ->groupBy('shift_histories.date', 'shift_histories.from_user_id', 'shift_histories.shift_from')
                            ->orderBy('users.name', $sort_type)->paginate(10);
                    } else {
                        $histories = ShiftHistory::select('id', 'date', 'from_user_id', 'shift_from',
                            DB::raw('SUM(nn_chips) as nn_chips'))
                            ->where('date', $date)
                            ->where('to_user_id', $user->id)
                            ->where('menu_name', 'sell_chip')
                            ->groupBy('date', 'from_user_id', 'shift_from')
                            ->orderBy('created_at', 'desc')->paginate(10);
                    }
                }
            }
        }else{
            return error_unauthorized();
        }

        return response()->json(transformCollection($histories), 200);
    }


    /**
     * Display the specified resource.
     * GET /api/sell_chips/{date}/{from_user}/{shift_from}
     *
     * @param string $token                 The token for authentication
     * @return \Illuminate\Http\Response
     */
    public function getByGroup($date, $from_user, $shift_from)
    {
        $data = ShiftHistory::where('date', $date)
            ->where('from_user_id', $from_user)
            ->where('shift_from', $shift_from)->get();

        if($data == null){
            return response()->json(["message" => "Data not exists!"],404);
        }else{
            return response()->json([
                "message" => "success",
                "data" => $data
            ],200);
        }
    }


}
