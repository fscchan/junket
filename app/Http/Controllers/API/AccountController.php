<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Account;
use App\AccountConfig;
use App\AccountType;
use App\JournalAccount;
use App\JournalAccountDetail;
use App\UserActivity;
use App\GentingTwo;
use Validator;
use DB;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$company_id = $request->input('company_id');
		$company_id = $company_id?$company_id:1;
		
		$where='(company_id='.$company_id.' '.($company_id==1?'or company_id is null':'').') 
		  and deleted_at is null and parent_ide is null and((parent_id is not null and ide is not null)or parent_id is null)';
        $accounts = Account::join(DB::raw('(select id as ide from accounts where deleted_at is null 
		      and parent_id is null)as parent'),
		    'parent_id','=','ide','left')
			->join(DB::raw('(select distinct parent_id as parent_ide from accounts where parent_id is not null)as parent2'),
		    'parent_ide','=','id','left')
			->whereRaw($where);
		  
		
		$accounts=$accounts->orderBy('name','asc')->get();

        return response()->json(['data' => $accounts], 200);
    }
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function accountType()
    {
        $accounts = AccountType::all();

        return response()->json(['data' => $accounts], 200);
    }

	/**
     * Get number.
     *
     * @return \Illuminate\Http\Response
	*/
	public function number(Request $request)
	{
		$user = User::where('token',$request->header('x-auth-token'))->first();
		$cek = check_auth($user,'read accounts');

		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$data=getAutoNumber('code','accounts','A/C-',4);
		
		return [
			'status'=>'success',
			'data'=>$data
		];
	}
	
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$company_id = $request->input('company_id');
		$company_id = $company_id?$company_id:1;
        $validator = Validator::make($request->all(), [
            'code' => 'required|unique:accounts,code,'.($company_id?$company_id:1).',company_id,deleted_at,NULL',
            'name' => 'required|unique:accounts,name,'.($company_id?$company_id:1).',company_id,deleted_at,NULL'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'create accounts');

		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $data = $request->all();
		$data['nn_chips']=isset($data['nn_chips'])?$data['nn_chips']:0;
		$data['junket_chips']=isset($data['junket_chips'])?$data['junket_chips']:0;
		$data['cash_chips']=isset($data['cash_chips'])?$data['cash_chips']:0;
		$data['cash_real']=isset($data['cash_real'])?$data['cash_real']:0;
		$data['balance']=$data['nn_chips']+$data['junket_chips']+$data['cash_chips']+$data['cash_real'];
        $log = Account::create($data);
		$this->journal($log->id,[],$request);

		if(isset($data['is_genting2'])){
			AccountConfig::find(1)->update(['insurance'=>$log->id]);
		}
		
        UserActivity::storeActivity(array(
            "activity" => "add account for id: ".$log->id,
            "user" => $user->id,
            "menu" => "set up",
            "ipaddress" => $request->ip()
        ));

        if($log){
            return response()->json([
                "status" => "success",
                "message" => "account has been saved!",
                "data" => $log,
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save account!",
                "data" => null
            ],403);
        }
    }
	
	/**
     * Journal Account.
     *
     * @param  int  $id
     * @param  object  $request from Request Class
     * @return boolean
     */
	public function journal($id,$last_data,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = Account::where('id','=',$id)
			->whereRaw('(nn_chips<>0 or junket_chips<>0 or cash_chips<>0 or cash_real<>0 or balance<>0)')
			->first();
			
		$child = Account::where('parent_id','=',$id)->get();
		// echo $id;
		// exit(json_encode(['child'=>$child,'data'=>$data]));
		
		if(count($child)>0){
			return false;
		}
		
		if(!$data){
			return false;
		}
		
		DB::beginTransaction();
		
		try{
			$journal=JournalAccount::where('activity','=','account')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>$data['balance_date'],
				'user_id'=>$user['id'],
				'entity'=>'account',
				'entity_id'=>$id,
				'activity'=>'account',
				'activity_id'=>$id,
				'remark'=>'Opening balance for '.$data['name'],
				'company_id'=>$data['company_id']
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$data_journal['remark']='Adjustment for '.$data['name'];
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			/* $journal=JournalAccount::create($data_journal);
			
			$journal2=[];
			if(count($last_data)>0){
				$number2=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
				$data_journal['je_num']=$number2;
				$data_journal['remark']='Adjustment for '.$data['name'];
				$journal2=JournalAccount::create($data_journal);
			}*/
			
			$balance=0;
			$last_balance=0;
			/*if($data['nn_chips_id'] && $data['nn_chips']!=0 && 
			(count($last_data)==0 || (count($last_data)>0 && $data['nn_chips']!=$last_data['nn_chips']))){*/
				$balance+=$data['nn_chips'];
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$data['nn_chips_id'],//nn chips
					'debit'=>$data['nn_chips'],
					'credit'=>0,
					'entity'=>'account',
					'entity_id'=>$id
				]);
				
				/*if(count($last_balance)>0 && isset($last_data['nn_chips_id']) && $last_data['nn_chips']!=0){
					$last_balance+=$last_data['nn_chips'];
					JournalAccountDetail::create([
						'journal_id'=>$journal2->id,
						'account_id'=>$last_data['nn_chips_id'],//nn chips
						'debit'=>0,
						'credit'=>$last_data['nn_chips'],
						'entity'=>'account',
						'entity_id'=>$id
					]);
				}
			}
			if($data['junket_chips_id'] && $data['junket_chips']!=0 && 
			(count($last_data)==0 || (count($last_data)>0 && $data['junket_chips']!=$last_data['junket_chips']))){*/
				$balance+=$data['junket_chips'];
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$data['junket_chips_id'],//junket chips
					'debit'=>$data['junket_chips'],
					'credit'=>0,
					'entity'=>'account',
					'entity_id'=>$id
				]);
				
				/*if(count($last_balance)>0 && isset($last_data['junket_chips_id']) && $last_data['junket_chips']!=0){
					$last_balance+=$last_data['junket_chips'];
					JournalAccountDetail::create([
						'journal_id'=>$journal2->id,
						'account_id'=>$last_data['junket_chips_id'],//junket chips
						'debit'=>0,
						'credit'=>$last_data['junket_chips'],
						'entity'=>'account',
						'entity_id'=>$id
					]);
				}
			}
			if($data['cash_chips_id'] && $data['cash_chips']!=0 && 
			(count($last_data)==0 || (count($last_data)>0 && $data['cash_chips']!=$last_data['cash_chips']))){*/
				$balance+=$data['cash_chips'];
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$data['cash_chips_id'],//cash chips
					'debit'=>$data['cash_chips'],
					'credit'=>0,
					'entity'=>'account',
					'entity_id'=>$id
				]);
				
				/*if(count($last_data)>0 && isset($last_data['cash_chips_id']) && $last_data['cash_chips']!=0){
					$last_balance+=$last_data['cash_chips'];
					JournalAccountDetail::create([
						'journal_id'=>$journal2->id,
						'account_id'=>$last_data['cash_chips_id'],//cash chips
						'debit'=>0,
						'credit'=>$last_data['cash_chips'],
						'entity'=>'account',
						'entity_id'=>$id
					]);
				}
			}
			if($data['cash_real_id'] && $data['cash_real']!=0 && 
			(count($last_data)==0 || (count($last_data)>0 && $data['cash_real']!=$last_data['cash_real']))){*/
				$balance+=$data['cash_real'];
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$data['cash_real_id'],//cash real
					'debit'=>$data['cash_real'],
					'credit'=>0,
					'entity'=>'account',
					'entity_id'=>$id
				]);
				
				/*if(count($last_data)>0 && isset($last_data['cash_real_id']) && $last_data['cash_real']!=0){
					$last_balance+=$last_data['cash_real'];
					JournalAccountDetail::create([
						'journal_id'=>$journal2->id,
						'account_id'=>$last_data['cash_real_id'],//cash real
						'debit'=>0,
						'credit'=>$last_data['cash_real'],
						'entity'=>'account',
						'entity_id'=>$id
					]);
				}
			}
			
			if($data['balance']!=0 && (count($last_data)==0 || count($last_data)>0 && $data['balance']!=$last_data['balance'])){*/
				$balance=$data['nature_type']=='debit'?($balance+$data['balance']):($balance-$data['balance']);
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$id,//account
					'debit'=>$data['nature_type']=='debit'?$data['balance']:0,
					'credit'=>$data['nature_type']=='credit'?$data['balance']:0,
					'entity'=>'account',
					'entity_id'=>$id
				]);
				
				/*if(count($last_data)>0 && $last_data['balance']!=0){
					$last_balance=$last_data['nature_type']=='debit'?($last_balance+$last_data['balance']):($last_balance-$last_data['balance']);
					JournalAccountDetail::create([
						'journal_id'=>$journal2->id,
						'account_id'=>$id,//account
						'debit'=>$last_data['nature_type']=='credit'?$last_data['balance']:0,
						'credit'=>$last_data['nature_type']=='debit'?$last_data['balance']:0,
						'entity'=>'account',
						'entity_id'=>$id
					]);
				}
			}
			
			if($data['balance_id']){*/
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$data['balance_id'],//balance
					'debit'=>0,
					'credit'=>$balance,
					'entity'=>'account',
					'entity_id'=>$id
				]);
				
				/*if(count($last_data)>0 && isset($last_data['balance_id'])){
					JournalAccountDetail::create([
						'journal_id'=>$journal2->id,
						'account_id'=>$last_data['balance_id'],//balance
						'debit'=>$last_balance,
						'credit'=>0,
						'entity'=>'account',
						'entity_id'=>$id
					]);
				}
			}
			
			$result_data=JournalAccountDetail::where('journal_id','=',$journal->id)->get();
			if(count($result_data)==0){
				DB::rollback();
			}
			
			if($journal2){
				$result_data2=JournalAccountDetail::where('journal_id','=',$journal2->id)->get();
				if(count($result_data2)==0){
					JournalAccount::where('id','=',$journal2->id)->delete();
				}
			}*/
			
			DB::commit();
		}catch(\Exception $exc){
			// echo $exc;
			DB::rollback();
		}
			
		return true;
	}
	
	/**
     * All Account
     * GET /api/accounts
     *
     * @param string $token | The token for authentication
     * @param string $search | Searching value
     * @param string $sort_by | Sorting field
     * @param string $sort_type | Sorting asc, desc
     * @return Response
     **/
    public function all(Request $request){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read accounts');
        if($cek['result']!=1){
            return error_unauthorized();
		}
	
		$search = $request->input('search');
		$sort_by = $request->input('sort_by');
		$sort_type = $request->input('sort_type');
		
		$company_id = $request->input('company_id');
		$company_id = $company_id?$company_id:1;
		$account = Account::select(['accounts.id','accounts.code','accounts.name',
		     'accounts.parent_id','accounts.nature_type','accounts.account_type_id','accounts.balance_date',
			 DB::raw('if(sub.balance is not null,"parent","main")as type'),
			 DB::raw('ifnull(sub.nn_chips,accounts.nn_chips)as nn_chips'),
			 DB::raw('ifnull(sub.junket_chips,accounts.junket_chips)as junket_chips'),
			 DB::raw('ifnull(sub.cash_chips,accounts.cash_chips)as cash_chips'),
			 DB::raw('ifnull(sub.cash_real,accounts.cash_real)as cash_real'),
			 DB::raw('ifnull(sub.balance,accounts.balance)as balance')])
			->join(DB::raw('(select parent_id as id_,sum(nn_chips)as nn_chips,sum(junket_chips)as junket_chips,
			  sum(cash_chips)as cash_chips,sum(cash_real)as cash_real,sum(balance)as balance
			  from accounts where deleted_at is null and parent_id is not null group by parent_id)as sub'),
			  'sub.id_','=','accounts.id','left')
			->whereNull('parent_id');
		
		$where='(company_id='.$company_id.' '.($company_id==1?'or company_id is null':'').')';
		$account=$account->whereRaw($where);
		if($search){
			$account = $account->whereRaw('(code LIKE "%'.$search.'%" or name LIKE "%'.$search.'%" or nature_type LIKE "%'.$search.'%") and '.$where);
		}
		
		if(!$sort_by){
			$sort_by='accounts.created_at';
		}
		$sort_type=$sort_type?$sort_type:'desc';
		
		$account = $account->orderBy($sort_by,$sort_type)->paginate(10);

        return response()->json(transformCollection($account), 200);
    }
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function parenting(Request $request)
    {
		$company_id = $request->input('company_id');
		$company_id = $company_id?$company_id:1;
        $accounts = DB::table("accounts")->whereNull('parent_id')->whereNull('deleted_at');
		$accounts = $accounts->whereRaw('(company_id='.$company_id.' '.($company_id==1?'or company_id is null':'').')');
		$accounts = $accounts->orderBy('name','asc')->get();

        return response()->json(['data' => $accounts], 200);
    }
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function children($id)
    {
		$company_id = $request->input('company_id');
		$company_id = $company_id?$company_id:1;
        $accounts = Account::where('parent_id','=',$id);
		$accounts = $company_id==1?$accounts->orWhereNull('company_id'):$accounts;
		$accounts = $accounts->orderBy('name','asc')->get();

        return response()->json(['data' => $accounts], 200);
    }
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bank()
    {
		$account_config = AccountConfig::first();
        $accounts = DB::table("accounts")->where('parent_id','=',$account_config->bank)->orderBy('name','asc')->get();

		return response()->json(['data' => $accounts], 200);
    }

	public function notBank()
    {
		$account_config = AccountConfig::first();
        $accounts = DB::table("accounts")
			->join(DB::raw('(select id as ide from accounts 
			  where deleted_at is null 
		      and parent_id is null
			  and id!='.$account_config['bank'].')as parent'),
		    'parent_id','=','ide','left')
			->join(DB::raw('(select distinct parent_id as parent_ide from accounts where parent_id is not null)as parent2'),
		    'parent_ide','=','id','left')
			->whereRaw('deleted_at is null and parent_ide is null
			 and ((parent_id is not null and ide is not null)or parent_id is null)')
			->orderBy('name','asc')->get();

        return response()->json(['data' => $accounts], 200);
    }
	
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $account = Account::find($id);
		$company_id = $request->input('company_id');
		$company_id = $company_id?$company_id:1;
		$where=' and (company_id='.$company_id.' '.($company_id==1?'or company_id is null':'').')';
		$child = DB::table(DB::raw('(
			select sum(nn_chips)as nn_chips,sum(junket_chips)as junket_chips,sum(cash_chips)as cash_chips,
			  sum(cash_real)as cash_real,sum(balance)as balance
			from accounts
			where parent_id='.$id.' and deleted_at is null
			group by parent_id
		)as sub'))->first();
		
		if($child){
			$account['disabled']=true;
			$account['nn_chips']=$child->nn_chips;
			$account['junket_chips']=$child->junket_chips;
			$account['cash_chips']=$child->cash_chips;
			$account['cash_real']=$child->cash_real;
			$account['balance']=$child->balance;
		}

        if($account != null){
            return response()->json([
                "message" => "success",
                "data" => $account
            ],200);
        }else {
            return response()->json(["message" => "Account not found!"],404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$company_id = $request->input('company_id');
		$company_id = $company_id?$company_id:1;
        $validator = Validator::make($request->all(), [
            'code' => 'required|unique:accounts,code,'.$request->input('id').',id,deleted_at,NULL,company_id,'.$company_id,
            'name' => 'required|unique:accounts,name,'.$request->input('id').',id,deleted_at,NULL,company_id,'.$company_id
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $data = $request->all();
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
			$cek = check_auth($user,'create accounts');

			if($cek['result']!=1){
				return error_unauthorized();
			}
			
            $account = Account::find($id);
			
			//kudu dideklarasi deui soalna bakal ngaset sorangan nilaina saengges diupdate
			$last_account['id']=$account['id'];
			$last_account['name']=$account['name'];
			$last_account['code']=$account['code'];
			$last_account['nature_type']=$account['nature_type'];
			$last_account['nn_chips_id']=$account['nn_chips_id'];
			$last_account['nn_chips']=$account['nn_chips'];
			$last_account['junket_chips_id']=$account['junket_chips_id'];
			$last_account['junket_chips']=$account['junket_chips'];
			$last_account['cash_chips_id']=$account['cash_chips_id'];
			$last_account['cash_chips']=$account['cash_chips'];
			$last_account['cash_real_id']=$account['cash_real_id'];
			$last_account['cash_real']=$account['cash_real'];
			$last_account['balance_id']=$account['balance_id'];
			$last_account['balance']=$account['balance'];
			
            if($account == null){
                return response()->json(['message' => 'Data not found'], 404);
            }else{
				$data['nn_chips']=isset($data['nn_chips'])?$data['nn_chips']:0;
				$data['junket_chips']=isset($data['junket_chips'])?$data['junket_chips']:0;
				$data['cash_chips']=isset($data['cash_chips'])?$data['cash_chips']:0;
				$data['cash_real']=isset($data['cash_real'])?$data['cash_real']:0;
				$data['balance']=$data['nn_chips']+$data['junket_chips']+$data['cash_chips']+$data['cash_real'];
                $log = $account->update($data);
				$this->journal($id,$last_account,$request);

				if(isset($data['is_genting2'])){
					AccountConfig::find(1)->update(['insurance'=>$id]);
				}
            }

            if($log){
                UserActivity::storeActivity(array(
                    "activity" => "update account for id: ".$id,
                    "user" => $user->id,
                    "menu" => "set up",
                    "ipaddress" => $request->ip()
                ));

                return response()->json([
                    "status" => "success",
                    "message" => "account has been updated!",
                    "data" => $account
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed update account!",
                    "data" => null
                ],403);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'create accounts');

		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $account = Account::find($id);
        if($account == null){
            return response()->json(['message' => 'Data not found'], 404);
        }else{
            if (Account::destroy($id)) {
				Account::where('parent_id','=',$id)->delete();
                UserActivity::storeActivity(array(
                    "activity" => "delete account for id: ".$id,
                    "user" => $user->id,
                    "menu" => "set up",
                    "ipaddress" => $request->ip()
                ));

                return response()->json([
                    "status" => "success",
                    "message" => "account has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete account!"
                ], 403);
            }
        }
    }
	
	public function getGentingTwo(Request $request) {
        $user = User::where('token', $request->header("X-Auth-Token"))->first();
		$cek = check_auth($user, 'read accounts');

		if($cek['result'] != 1 ){
			return error_unauthorized();
		}
		
		$account_config=AccountConfig::first();
		$account_genting2=$account_config['insurance']?$account_config['insurance']:0;
		$data=Account::find($account_genting2);
		if(!$data){
			$data=Account::where(['name'=>'Genting 2','company_id'=>2])->first();
		}

		return response()->json([
			"message" => "success",
			"data" => $data
		], 200);
	}
	
	public function getGentingTwoBalance(Request $request) {
        $user = User::where('token', $request->header("X-Auth-Token"))->first();
		$cek = check_auth($user, 'read accounts');

		if($cek['result'] != 1 ){
			return error_unauthorized();
		}
		
		$account_config=AccountConfig::first();
		$account_genting2=$account_config['insurance']?$account_config['insurance']:0;
		$data=Account::find($account_genting2);
		if(!$data){
			$data=Account::where(['name'=>'Genting 2','company_id'=>2])->first();
		}
		
		$datab=DB::table(DB::raw('(select sum(balance)as balance,sum(nn_chips)as nn_chips,
			sum(junket_chips)as junket_chips,sum(cash_chips)as cash_chips,sum(cash_real)as cash_real
			from(
				'.(isset($data->id)?'':'-- ').'select balance,nn_chips,junket_chips,cash_chips,cash_real from accounts where id='.(isset($data->id)?$data->id:0).'
				'.(isset($data->id)?'':'-- ').'union all
				select -sum(p.commission)as balance,-sum(if(p.type_chip="NN Chips",p.value,0))as nn_chips,
				 -sum(if(p.type_chip="Junket NN Chips",p.value,0))as junket_chips,-sum(if(p.type_chip="Cash Chips",p.value,0)+p.commission-p.value)as cash_chips,
				 0 as cash_real
				from purchases p
				join vendors v on v.id=p.vendor_id and (v.name="Genting 2" or v.name="Genting2") and v.deleted_at is null
				where p.deleted_at is null
				union all
				select sum(p.commission)as balance,sum(if(p.type_chip="NN Chips",p.value,0))as nn_chips,
				 sum(if(p.type_chip="Junket NN Chips",p.value,0))as junket_chips,sum(if(p.type_chip="Cash Chips",p.value,0)+p.commission-p.value)as cash_chips,
				 0 as cash_real
				from return_chips p
				join vendors v on v.id=p.vendor_id and (v.name="Genting 2" or v.name="Genting2") and v.deleted_at is null
				where p.deleted_at is null
				union all
				select sum(gl-if(agent_id is not null,(gl*if(status="Win Only",10,agent_shared)/100),0))as balance,
					0 as nn_chips, 0 as junket_chips,
					sum(gl-if(agent_id is not null,(gl*if(status="Win Only",10,agent_shared)/100),0))as cash_chips, 0 as cash_real
				from(
					select t.id,tr.id as detail_id,date(t.date)as date,concat(c.first_name," ",c.last_name)as customer_name,
					u.name as user_name,c.agent_id,tr.rolling,ifnull(t.commission,0)as commission,tr.room,tr.insurance,tr.odds,
					tr.agent_shared,tr.status,if(tr.scenario="1","Win","Lose")as scenario,
					if(tr.status="Win Only" and tr.scenario=1,
					 if(tr.room="Room 38",(tr.insurance*0.9),(if(tr.room="Maxims",(tr.insurance*0.7*0.9),(tr.insurance*0.8*0.9)))),
					 if(tr.room="Room 38",(tr.insurance*1),(if(tr.room="Maxims",(tr.insurance*0.7),(tr.insurance*0.8)))))as gl,
					if(tr.status="Win Only" and tr.scenario=0,0,
					 if(tr.room="Maxims",(tr.insurance*0.3),0))as pp,
					if(tr.status="Win Only" and tr.scenario=0,0,
					if(tr.room="Other Room",(tr.insurance*0.2),0))as other
					from transactions t
					join customers c on c.id=t.customer_id
					join users u on u.id = t.user_id
					join transaction_rollings tr on tr.transaction_id=t.id 
					where t.deleted_at is null
				)as sub
				union all
				select sum(gd.nn_chips+gd.junket_chips+gd.cash_chips+gd.cash_real)as balance,
				 sum(gd.nn_chips)as nn_chips,sum(gd.junket_chips)as junket_chips,sum(gd.cash_chips)as cash_chips,sum(gd.cash_real)as cash_real
				from gentingtwo_deposits gd
				where deleted_at is null
			)as sub)as sub'))->first();

		$id=null;
		if($data){
			if($data->nn_chips>0 || $data->junket_chips>0 || $data->cash_chips>0 || $data->cash_real>0){
				$id=$data->id;
			}
		}
			
		return response()->json([
			"message" => "success",
			"data" => $datab,
			"id" => $id
		], 200);
	}
}
