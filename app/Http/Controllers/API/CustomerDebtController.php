<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\UserActivity;
use Validator;
use App\CustomerDebt;
use App\JournalAccount;
use App\JournalAccountDetail;
use App\AccountConfig;
use App\UserHasAccount;
use DB;

class CustomerDebtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $date = $request->input('date');
        $sort_by = $request->input('sort_by');
        $sort_type = $request->input('sort_type');
		$is_past = $request->input('is_past');

		$customer_debts = CustomerDebt::select(['customer_debts.*',
			DB::raw('ifnull((select sum(pdd.amount)as amount
				from payment_debt_details pdd
				where pdd.customer_debt_id=customer_debts.id),0)as payment_amount')])
			->join('customers','customers.id','=','customer_debts.customer_id')
			->join('users','users.id','=','customer_debts.staff_id','left')
			->where('amount','<>',0);
			
		if($date){
			$customer_debts = $customer_debts->whereDate('date','=',$date);
		}
		
		if($is_past){
			$customer_debts = $customer_debts->where('is_past','=',1);
		}
		
		if(!$sort_by){
			$sort_by='customer_debts.created_at';
		}
		$sort_type=$sort_type?$sort_type:'desc';
		
		switch($sort_by){
			case 'customer':
				$sort_by='customers.first_name';
				break;
			case 'users':
				$sort_by='users.name';
				break;
		}
		
        $customer_debts = $customer_debts->orderBy($sort_by,$sort_type)->paginate();

        return response()->json(transformCollection($customer_debts), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|integer',
            'transaction_id' => 'integer',
            'date' => 'required|date',
            'due_date' => 'date',
            'amount' => 'required|numeric',
        ]);

		
        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else {
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $data = $request->all();
			if($data['amount']==0){
				return response()->json(["status"=>"error",
				"message" => "Amount Due is 0"], 422);
			}
            $customer_debt = CustomerDebt::create($data);
			
			if(!isset($customer_debt->is_past) || (isset($customer_debt->is_past) && $customer_debt->is_past==0)){
				$this->journal($customer_debt->id,$request);
			}
            UserActivity::storeActivity(array(
                "activity" => "add debt for id: ".$customer_debt->id,
                "user" => $user->id,
                "menu" => "transactions",
                "ipaddress" => $request->ip()
            ));
        }

        if($customer_debt){
            return response()->json([
                "status" => "success",
                "message" => "Customer debt has been saved!",
                "data" => $customer_debt
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save customer debt!",
                "data" => null
            ],403);
        }
    }

	public function journal($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = CustomerDebt::find($id);
		
		if($data['transaction_id']){
			return true;
		}
		
		DB::beginTransaction();
		
		try{
			$uha=UserHasAccount::where('user_id','=',$data['user_id'])->first();
			$account_config=AccountConfig::first();
			$journal=JournalAccount::where('activity','=','customer_debts')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>$data['date'],
				'user_id'=>$user['id'],
				'entity'=>'customer',
				'entity_id'=>$data['customer_id'],
				'activity'=>'customer_debts',
				'activity_id'=>$id,
				'remark'=>'Loan ('.$data['customer']['first_name'].' '.$data['customer']['last_name'].')'
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$data['amount']=isset($data['amount'])?$data['amount']:0;
				$data['interest_amount']=isset($data['interest_amount'])?$data['interest_amount']:0;
				$data['amount_due']=isset($data['amount_due'])?$data['amount_due']:0;
				
				$account_id=isset($uha['account_id'])?$uha['account_id']:$account_config['receivable'];
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_id,//ar
					'debit'=>$data['amount'],
					'credit'=>0,
					'entity'=>'customer',
					'entity_id'=>$data['customer_id']
				]);
				
				/*JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['loan'],//loan
					'debit'=>0,
					'credit'=>$data['interest_amount'],
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);*/
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$data['is_past']==1?$account_id:$account_config['cash_real'],
					'debit'=>0,
					'credit'=>$data['amount'],
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
			}
			
			DB::commit();
		}catch(\Exception $exc){
			// echo $exc;
			DB::rollback();
		}
			
		return true;
	}
	
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer_debt = CustomerDebt::find($id);
        if($customer_debt!=null){
            return response()->json([
                "message" => "success",
                "data" => $customer_debt,
            ],200);
        }else{
            return response()->json(["message" => "Customer debt not exists!"],404);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required|integer',
            'transaction_id' => 'integer',
            'date' => 'required|date',
            'due_date' => 'date',
            'amount' => 'required|numeric',
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else {
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $data = $request->all();

			if($data['amount']==0){
				return response()->json(["status"=>"error",
				"message" => "Amount Due is 0"], 422);
			}
			
            $customer_debt = CustomerDebt::find($id);
            if($customer_debt==null){
                return response()->json(["message" => "Customer debt not exists!"],404);
            }else{
                $customer_debt->update($data);
				if(!isset($customer_debt->is_past) || (isset($customer_debt->is_past) && $customer_debt->is_past==0)){
					$this->journal($id,$request);
				}
            }

            UserActivity::storeActivity(array(
                "activity" => "update debt for id: ".$id,
                "user" => $user->id,
                "menu" => "transactions",
                "ipaddress" => $request->ip()
            ));
        }

        if($customer_debt){
            return response()->json([
                "status" => "success",
                "message" => "Customer debt has been updated!",
                "data" => $customer_debt
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed update customer debt!",
                "data" => null
            ],403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $customer_debt = CustomerDebt::find($id);

        if($customer_debt == null){
            return response()->json(["message"=>"Customer debt not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();

        if (CustomerDebt::destroy($id)) {
			JournalAccount::where('activity','=','customer_debts')->where('activity_id','=',$id)->delete();
            UserActivity::storeActivity(array(
                "activity" => "delete customer debt for id: ".$id,
                "user" => $user->id,
                "menu" => "transactions",
                "ipaddress" => $request->ip()
            ));

            return response()->json([
                "status" => "success",
                "message" => "Customer debt has been deleted!"
            ], 200);
        } else {
            return response()->json([
                "status" => "error",
                "message" => "Failed delete customer debt!"
            ], 403);
        }
    }
}
