<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\UserHasAccount;
use App\UserActivity;
use Validator;
use App\JournalAccount;
use App\JournalAccountDetail;
use DB;

class UserHasAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_accounts = UserHasAccount::all();

        return response()->json(['data' => $user_accounts], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|unique:user_has_accounts',
            'account_id' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $data = $request->all();
        $log = UserHasAccount::create($data);

        UserActivity::storeActivity(array(
            "activity" => "add user link to account for id: ".$log->id,
            "user" => $user->id,
            "menu" => "set up",
            "ipaddress" => $request->ip()
        ));

        if($log){
            return response()->json([
                "status" => "success",
                "message" => "user link to account has been saved!",
                "data" => $log,
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save user link to account!",
                "data" => null
            ],403);
        }
    }
	
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_account = UserHasAccount::find($id);

        if($user_account != null){
            return response()->json([
                "message" => "success",
                "data" => $user_account
            ],200);
        }else {
            return response()->json(["message" => "Data not found!"],404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {	
		$data = $request->all();
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$user_account = UserHasAccount::where('user_id',$id)->first();
		if($user_account == null){
			return response()->json(['message' => 'Data not found'], 404);
		}else{
			$log = UserHasAccount::where('user_id',$id)->update(['account_id'=>$data['account_id']]);
		}

		if($log){
			UserActivity::storeActivity(array(
				"activity" => "update user link to account for id: ".$id,
				"user" => $user->id,
				"menu" => "set up",
				"ipaddress" => $request->ip()
			));

			return response()->json([
				"status" => "success",
				"message" => "user link to account has been updated!",
				"data" => $user_account
			],200);
		}else{
			return response()->json([
				"status" => "error",
				"message" => "Failed update user link to account!",
				"data" => null
			],403);
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $user_account = UserHasAccount::where('user_id',$id)->first();
        if($user_account == null){
            return response()->json(['message' => 'Data not found'], 404);
        }else{
            if (UserHasAccount::where('user_id',$id)->delete()) {
                UserActivity::storeActivity(array(
                    "activity" => "delete user link to account for id: ".$id,
                    "user" => $user->id,
                    "menu" => "set up",
                    "ipaddress" => $request->ip()
                ));

                return response()->json([
                    "status" => "success",
                    "message" => "user link to account has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete user link to account!"
                ], 403);
            }
        }
    }
}
