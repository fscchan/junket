<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\OpenBalance;
use App\UserActivity;
use Validator;
use App\JournalAccount;
use App\JournalAccountDetail;
use DB;

class OpenBalanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $balances = OpenBalance::all();

        return response()->json(['data' => $balances], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'manager_id' => 'required|unique:open_balances',
            'nn_chips' => 'required|numeric',
            'cash_chips' => 'required|numeric',
            'cash_real' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $data = $request->all();
        $log = OpenBalance::create($data);
		$this->journal($log->id,$request);

        UserActivity::storeActivity(array(
            "activity" => "add open balances for id: ".$log->id,
            "user" => $user->id,
            "menu" => "set up",
            "ipaddress" => $request->ip()
        ));

        if($log){
            return response()->json([
                "status" => "success",
                "message" => "Open balance has been saved!",
                "data" => $log,
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save open balance!",
                "data" => null
            ],403);
        }
    }

    /**
     * Journal Open Balance.
     *
     * @param  int  $id
     * @param  object  $request from Request Class
     * @return boolean
     */
	public function journal($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = OpenBalance::find($id);
		
		DB::beginTransaction();
		
		try{
			$journal=JournalAccount::where('activity','=','open_balance')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>$data['date'],
				'user_id'=>$user['id'],
				'entity'=>'manager',
				'entity_id'=>$data['manager_id'],
				'activity'=>'open_balance',
				'activity_id'=>$id,
				'remark'=>'Open Balance ('.$data['user']['name'].')'
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$journal->save();
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$data['nn_chips']=isset($data['nn_chips'])?$data['nn_chips']:0;
				$data['cash_chips']=isset($data['cash_chips'])?$data['cash_chips']:0;
				$data['cash_real']=isset($data['cash_real'])?$data['cash_real']:0;
				
				$amount=$data['nn_chips']+$data['cash_chips']+$data['cash_real'];
				if($amount==0){
					DB::rollback();
					return true;
				}
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>8,//opening balance asset
					'debit'=>0,
					'credit'=>$amount
				]);
				
				if($data['nn_chips']>0){
					JournalAccountDetail::create([
						'journal_id'=>$journal->id,
						'account_id'=>1,//nn chip
						'debit'=>$data['nn_chips'],
						'credit'=>0
					]);
				}
				
				if($data['cash_chips']>0){
					JournalAccountDetail::create([
						'journal_id'=>$journal->id,
						'account_id'=>2,//cash chip
						'debit'=>$data['cash_chips'],
						'credit'=>0
					]);
				}
				
				if($data['cash_real']>0){
					JournalAccountDetail::create([
						'journal_id'=>$journal->id,
						'account_id'=>3,//cash real
						'debit'=>$data['cash_real'],
						'credit'=>0
					]);
				}
			}
			
			DB::commit();
		}catch(\Exception $exc){
			// echo $exc;
			DB::rollback();
		}
			
		return true;
	}
	
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $balance = OpenBalance::find($id);

        if($balance != null){
            return response()->json([
                "message" => "success",
                "data" => $balance
            ],200);
        }else {
            return response()->json(["message" => "Balance not found!"],404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'manager_id' => 'required|unique:open_balances,manager_id,'.$id.',id',
            'nn_chips' => 'required|numeric',
            'cash_chips' => 'required|numeric',
            'cash_real' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $data = $request->all();
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $balance = OpenBalance::find($id);
            if($balance == null){
                return response()->json(['message' => 'Data not found'], 404);
            }else{
                $log = $balance->update($data);
				$this->journal($id,$request);
            }

            if($log){
                UserActivity::storeActivity(array(
                    "activity" => "update open balances for id: ".$id,
                    "user" => $user->id,
                    "menu" => "set up",
                    "ipaddress" => $request->ip()
                ));

                return response()->json([
                    "status" => "success",
                    "message" => "Open balance has been updated!",
                    "data" => $balance
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed update open balance!",
                    "data" => null
                ],403);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $balance = OpenBalance::find($id);
        if($balance == null){
            return response()->json(['message' => 'Data not found'], 404);
        }else{
            if (OpenBalance::destroy($id)) {
				JournalAccount::where('activity','=','open_balance')->where('activity_id','=',$id)->delete();
                UserActivity::storeActivity(array(
                    "activity" => "delete open balances for id: ".$id,
                    "user" => $user->id,
                    "menu" => "set up",
                    "ipaddress" => $request->ip()
                ));

                return response()->json([
                    "status" => "success",
                    "message" => "Open balance has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete open balance!"
                ], 403);
            }
        }
    }
}
