<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Transaction;
use App\TransactionExpense;
use Validator;
use App\UserActivity;
use App\JournalAccount;
use App\JournalAccountDetail;
use App\AccountConfig;
use DB;

class TransactionExpenseController extends Controller
{
	
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'transaction_id' => 'required',
            'date' => 'required',
            'amount' => 'required',
            'date' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{

            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'create transactions');
			$data = $request->all();
			
            if($cek['result']==1){
                $transaction = Transaction::find($data['transaction_id']);
                if($transaction==null){
                    return response()->json(["message" => "Transaction not exists!"],404);
                }else{
					$data['user_id']=$user->id;
                    $log = TransactionExpense::create($data);
					
					$this->journal($log->id,$request);
					
                    UserActivity::storeActivity(array(
                        "activity" => "add transaction expense for id: ".$log->id,
                        "user" => $user->id,
                        "menu" => "transactions",
                        "ipaddress" => $request->ip()
                    ));
                }
            }else{
                return error_unauthorized();
            }

            if($log){
                return response()->json([
                    "status" => "success",
                    "message" => "Expense has been saved!",
                    "data" => $log
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed save expense!",
                    "data" => null
                ],403);
            }
        }
    }

    /**
     * Journal Expense.
     *
     * @param  int  $id
     * @param  object  $request from Request Class
     * @return boolean
     */
	public function journal($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = TransactionExpense::select(['transaction_expenses.*','transactions.customer_id'])
			->join('transactions','transactions.id','=','transaction_expenses.transaction_id')->find($id);

		if(!$data){return false;}
		
		DB::beginTransaction();
		
		try{
			$account_config=AccountConfig::first();
			$journal=JournalAccount::where('activity','=','transaction_expense')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>$data['date'],
				'user_id'=>$user['id'],
				'entity'=>'user',
				'entity_id'=>$data['user_id'],
				'activity'=>'transaction_expense',
				'activity_id'=>$id,
				'remark'=>$data['description']
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$journal->save();
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$data['value']=isset($data['value'])?$data['value']:0;
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['expense'],//expense
					'debit'=>$data['amount'],
					'credit'=>0,
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_real'],//cash
					'debit'=>0,
					'credit'=>$data['amount'],
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
			}
			
			DB::commit();
		}catch(\Exception $exc){
			echo $exc;
			DB::rollback();
		}
			
		return true;
	}
	
    /**
     * Display expense by master id.
     * GET /api/transaction_expense/master/{id}
     *
     * @param string $token     The token for authentication
     * @return Response
     **/
	public function index(Request $request){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read transactions');
        if($cek['result']==1){
            $detail = TransactionExpense::where(['transaction_id'=>$request->input('id')])->get();
			
			return response()->json([
				"message" => "success",
				"data" => $detail
			],200);
        }else{
            return error_unauthorized();
        }
	}
	
    /**
     * Display expense by id.
     * GET /api/transaction_expense/{detail}
     *
     * @param string $token     The token for authentication
     * @return Response
     **/
    public function show(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read transactions');
        if($cek['result']==1){
            $detail = TransactionExpense::find($id);
            if($detail!=null){
                return response()->json([
                    "message" => "success",
                    "data" => $detail
                ],200);
            }else{
                return response()->json(["message" => "Expense not found!"],404);
            }
        }else{
            return error_unauthorized();
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'transaction_id' => 'required',
            'date' => 'required',
            'amount' => 'required',
            'date' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $user = User::where('token',$request->header("X-Auth-Token"))->first();
            $cek = check_auth($user,'update transactions');
            if($cek['result']==1){
                $detail = TransactionExpense::find($id);
                if($detail==null){
                    return response()->json(["message" => "Expense not exists!"],404);
                }else{
                    $data = $request->all();
                    $log = $detail->update($data);
					
					$this->journal($id,$request);
						
                    UserActivity::storeActivity(array(
                        "activity" => "update transactions expense for id: ".$detail->id,
                        "user" =>$user->id,
                        "menu" =>"transactions",
                        "ipaddress" => $request->ip()
                    ));
                }
            }else{
                return error_unauthorized();
            }

            if($log){
                return response()->json([
                    "status" => "success",
                    "message" => "Expense has been update!",
                    "data" => $detail
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed update expense!",
                    "data" => null
                ],403);
            }
        }
    }

    public function destroy(Request $request, $id)
    {
        $detail = TransactionExpense::find($id);

        if($detail == null){
            return response()->json(["message"=>"Expense not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete transactions');
        if($cek['result']==1) {
            if (TransactionExpense::destroy($id)) {
				JournalAccount::where('activity','=','transaction_expense')->where('activity_id','=',$id)->delete();
                UserActivity::storeActivity(array(
                    "activity" => "delete transaction expense for id: ".$id,
                    "user" => $user->id,
                    "menu" => "transactions",
                    "ipaddress" => $request->ip()
                ));
                return response()->json([
                    "status" => "success",
                    "message" => "Expense has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete expense!"
                ], 403);
            }
        }else{
            return error_unauthorized();
        }
    }
}
