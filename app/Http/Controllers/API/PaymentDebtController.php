<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\UserActivity;
use Validator;
use App\PaymentDebt;
use App\PaymentDebtDetail;
use App\JournalAccount;
use App\JournalAccountDetail;
use App\AccountConfig;
use App\PaymentMethod;
use App\Customer;
use App\Agent;
use App\Vendor;
use App\AgentCompany;
use App\CustomerDebt;
use App\AgentDebt;
use App\VendorDebt;
use App\AgentCompanyDebt;
use DB;

class PaymentDebtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $date = $request->input('date');
        $sort_by = $request->input('sort_by');
        $sort_type = $request->input('sort_type');

		$payment_debts = PaymentDebt::select(['payment_debts.*',
				DB::raw('(ifnull(agents.name,ifnull(vendors.name,ifnull(agent_companies.company_name,
				 concat(customers.first_name," ",ifnull(customers.last_name,""))))))as entity')])
			->join('agents','agents.id','=','payment_debts.agent_id','left')
			->join('vendors','vendors.id','=','payment_debts.vendor_id','left')
			->join('agent_companies','agent_companies.id','=','payment_debts.company_id','left')
			->join('customers','customers.id','=','payment_debts.customer_id','left')
			->join('users','users.id','=','payment_debts.user_id');
		
		if($date!=null){
			$payment_debts = $payment_debts->where('date', $date);
		}

		if(!$sort_by){
			$sort_by='payment_debts.created_at';
		}
		$sort_type=$sort_type?$sort_type:'desc';
		
		switch($sort_by){
			case 'customer':
				$sort_by='customers.first_name';
				break;
			case 'user':
				$sort_by='users.name';
				break;
		}
		
		if($sort_by != null){
			$payment_debts = $payment_debts->orderBy($sort_by,$sort_type)->paginate();
		}else{
			$payment_debts = $payment_debts->sortable(['created_at','desc'])->paginate();
		}

        return response()->json(transformCollection($payment_debts), 200);
    }
	
	/**
     * Get number.
     *
     * @return \Illuminate\Http\Response
	*/
	public function number(Request $request)
	{
		$user = User::where('token',$request->header('x-auth-token'))->first();
		$cek = check_auth($user,'read payment_debts');

		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$data=getAutoNumber('number','payment_debts','REC-'.date('my')."-",4);
		
		return [
			'status'=>'success',
			'data'=>$data
		];
	}
	
	/**
     * Get list debt by customer.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
	*/
	public function getSources(Request $request)
	{
		$user = User::where('token',$request->header('x-auth-token'))->first();
		$cek = check_auth($user,'read payment_debts');

		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		switch($request->input("source")) {
			case "1": {
				$data = Customer::all();
				foreach($data as $i => $o) {
					$data[$i]->label_name = $o->last_name . " " . $o->first_name;
				}
				break;
			}
			case "2": {
				$data = Agent::all();
				foreach($data as $i => $o) {
					$data[$i]->label_name = $o->name;
				}
				break;
			}
			case "3": {
				$data = Vendor::all();
				foreach($data as $i => $o) {
					$data[$i]->label_name = $o->name;
				}
				break;
			}
			case "4": {
				$data = AgentCompany::all();
				foreach($data as $i => $o) {
					$data[$i]->label_name = $o->company_name;
				}
				break;
			}
			default: {
				return [
					'status'=>'error',
					'message'=>'Data not found'
				];
			}
		}		

		return [
			'status'=>'success',
			'data'=>$data
		];
	}

	public function getDebts(Request $request,$id)
	{
		$user = User::where('token',$request->header('x-auth-token'))->first();
		$cek = check_auth($user,'read payment_debts');

		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		switch($request->input("source")) {
			case "1": {
				$data = PaymentDebtDetail::get_debt($id);
				break;
			}
			case "2": {
				$data = AgentDebt::where("agent_id", $id)->join("agents", "agents.id", "=", "agent_debts.agent_id")->get();
				break;
			}
			case "3": {
				$data = VendorDebt::where("vendor_id", $id)->join("vendors", "vendors.id", "=", "vendor_debts.vendor_id")->get();
				break;
			}
			case "4": {
				$data = AgentCompanyDebt::where("agent_company_id", $id)->join("agent_companies", "agent_companies.id", "=", "agent_company_debts.agent_company_id")->get();
				break;
			}
			default: {
				return [
					'status'=>'error',
					'message'=>'Data not found'
				];
			}
		}		

		return [
			'status'=>'success',
			'data'=>$data
		];
	}
	
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'create payment_debts');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $validator = Validator::make($request->all(), [
            'number' => 'unique:payment_debts'.($request->input('id')?(',number,'.$request->input('id').',id'):'').',deleted_at,NULL',
            'date' => 'required|date',
			'remark' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

		$data = $request->all();
		/* $payment_method=PaymentMethod::find($data['payment_method_id']);
		if($payment_method['is_bank']==1 && !isset($data['bank_account_id'])){
			return response()->json(["status"=>"error","message"=>"Please select bank account!"],422);
		}
		
		if($payment_method['is_bank']==0 && !isset($data['staff_id'])){
			return response()->json(["status"=>"error","message"=>"Please select staff!"],422);
		} */
		
		$data["customer_id"] = null;
		$data["agent_id"] = null;
		$data["vendor_id"] = null;
		$data["company_id"] = null;
		
		switch((int) $request->input("debt_source")) {
			case 1: {
				$data["customer_id"] = $request->input("customer_id");
				break;
			}
			case 2: {
				$data["agent_id"] = $request->input("customer_id");
				break;
			}
			case 3: {
				$data["vendor_id"] = $request->input("customer_id");
				break;
			}
			case 4: {
				$data["company_id"] = $request->input("customer_id");
				break;
			}
			default: {
				return response()->json(["status"=>"error","message"=>"Invalid debt source!"],422);
			}
		}
		
		if($data['action']=='create'){
			$payment_debt = PaymentDebt::create($data);
		}else if($data['action']=='update'){
			$payment_debt = PaymentDebt::find($data['id'])->update($data);
		}
		
		
		if($payment_debt){
			if($request->hasFile("image")) {
				$this->uploadImage($request, $payment_debt);
			}
		}
		
		$payment_debt_id=isset($payment_debt->id)?$payment_debt->id:$data['id'];
		
		UserActivity::storeActivity(array(
			"activity" => "add payment debt for id: ".($payment_debt_id),
			"user" => $user->id,
			"menu" => "transactions",
			"ipaddress" => $request->ip()
		));

        if($payment_debt){
			$this->journal($payment_debt_id,$request);
            return response()->json([
                "status" => "success",
                "message" => "Payment debt has been saved!",
                "data" => $payment_debt,
				"id" => ($payment_debt_id)
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save payment debt!",
                "data" => null
            ],403);
        }
    }
	
	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'read payment_debts');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $payment_debt = PaymentDebt::find($id);
        if($payment_debt!=null){
			$payment_debt['image_url'] = url("/public/") . $payment_debt->image_path . $payment_debt->image_name;
			$payment_debt['is_bank'] = $payment_debt->bank_account->parent_id == AccountConfig::first()->bank ? true : false;
			return response()->json([
                "message" => "success",
                "data" => $payment_debt,
            ],200);
        }else{
            return response()->json(["message" => "Payment debt not exists!"],404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'update payment_debts');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $validator = Validator::make($request->all(), [
            'number' => 'unique:payment_debts,number,'.$id.',id,deleted_at,NULL',
            'date' => 'required|date',
			'remark' => 'required'
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

		$data = $request->all();
		$payment_debt = PaymentDebt::find($id)->update($data);
		$this->journal($id,$request);
		
		UserActivity::storeActivity(array(
			"activity" => "update payment debt for id: ".$id,
			"user" => $user->id,
			"menu" => "payment_debts",
			"ipaddress" => $request->ip()
		));

        if($payment_debt){
            return response()->json([
                "status" => "success",
                "message" => "Payment debt has been saved!",
                "data" => $payment_debt
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save payment debt!",
                "data" => null
            ],403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'delete payment_debts');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		PaymentDebtDetail::where('payment_debt_id','=',$id)->delete();
        $payment_debt = PaymentDebt::find($id);

        if($payment_debt == null){
            return response()->json(["message"=>"Payment debt not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();

		PaymentDebtDetail::where('payment_debt_id','=',$id)->delete();
        if (PaymentDebt::destroy($id)) {
			JournalAccount::where('activity','=','payment_debt')->where('activity_id','=',$id)->delete();
            UserActivity::storeActivity(array(
                "activity" => "delete payment debt for id: ".$id,
                "user" => $user->id,
                "menu" => "payment_debts",
                "ipaddress" => $request->ip()
            ));

            return response()->json([
                "status" => "success",
                "message" => "Payment debt has been deleted!"
            ], 200);
        } else {
            return response()->json([
                "status" => "error",
                "message" => "Failed delete payment debt!"
            ], 403);
        }
    }
	
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDetail(Request $request,$id)
    {
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'create payment_debts');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $validator = Validator::make($request->all(), [
            'customer_debt_id' => 'required|integer',
            'amount' => 'required|numeric',
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

		$data = $request->all();
		
		if($data['amount']==0){
			return response()->json(["status"=>"success",
			"message" => "Payment Amount is 0"], 200);
		}
		
		$data['payment_debt_id']=$id;
		$payment_debt = PaymentDebtDetail::create($data);
		
		UserActivity::storeActivity(array(
			"activity" => "add payment debt detail for id: ".$id,
			"user" => $user->id,
			"menu" => "transactions",
			"ipaddress" => $request->ip()
		));

        if($payment_debt){
			$this->updatePaymentAmount($id,$request);
            return response()->json([
                "status" => "success",
                "message" => "Payment debt detail has been saved!",
                "data" => $payment_debt
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save payment debt detail!",
                "data" => null
            ],403);
        }
    }
	
	public function updatePaymentAmount($id,$request){
		$payment=PaymentDebtDetail::where('payment_debt_id','=',$id)->value('amount');
		if($payment!=0){
			$payment_debt=PaymentDebt::find($id)->update(['amount'=>$payment]);
			if($payment_debt){
				$this->journal($id,$request);
			}
		}
	}
	
    /**
     * Journal Payment Debt.
     *
     * @param  int  $id
     * @param  object  $request from Request Class
     * @return boolean
     */
	public function journal($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = PaymentDebt::select(['payment_debts.*',DB::raw('(ifnull(agents.name,ifnull(vendors.name,ifnull(agent_companies.company_name,
			 concat(customers.first_name," ",ifnull(customers.last_name,""))))))as entity')])
			->join('agents','agents.id','=','payment_debts.agent_id','left')
			->join('vendors','vendors.id','=','payment_debts.vendor_id','left')
			->join('agent_companies','agent_companies.id','=','payment_debts.company_id','left')
			->join('customers','customers.id','=','payment_debts.customer_id','left')
			->find($id);
		
		DB::beginTransaction();
		
		try{
			$account_config=AccountConfig::first();
			$account_id=$account_config['cash_real'];
			$entity='staff';
			$entity_id=$data['staff_id'];
			switch($data['payment_mode']){
				case 'NN Chip':
					$account_id=$account_config['nn_chips'];
				break;
				case 'Cash Chip':
					$account_id=$account_config['cash_chips'];
				break;
				case 'Junket NN Chip':
					$account_id=$account_config['junket_chips'];
				break;
			}
			if($data['payment_method']['is_bank']==1){
				$entity='account';
				$entity_id=$data['bank_account_id'];
				$account_id=isset($data['bank_account_id'])?$data['bank_account_id']:1;
			}
			
			$account_id=$data['bank_account_id'];
			$data['amount']=isset($data['amount'])?$data['amount']:0;
			
			$journal=JournalAccount::where('activity','=','payment_debt')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>$data['date'],
				'user_id'=>$user['id'],
				'entity'=>$entity,
				'entity_id'=>$entity_id,
				'activity'=>'payment_debt',
				'activity_id'=>$id,
				'remark'=>$data['entity'].' ('.$data['remark'].') '.number_format($data['amount'],2)
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal['remark']=$data['entity'].' ('.$data['remark'].') '.number_format($data['amount'],2);
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_real'],//ar
					'debit'=>$data['amount'],
					'credit'=>0,
					'entity'=>'customer',
					'entity_id'=>$data['customer_id']?$data['customer_id']:null
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_id,//ac
					'debit'=>0,
					'credit'=>$data['amount'],
					'entity'=>$entity,
					'entity_id'=>$entity_id
				]);
			}
			
			DB::commit();
		}catch(\Exception $exc){
			// echo $exc;
			DB::rollback();
		}
			
		return true;
	}
	
	public function showDetail(Request $request, $id)
	{
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'update payment_debts');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$data=PaymentDebtDetail::get_edit($id);
		
		return [
			'status'=>'success',
			'data'=>$data
		];
	}

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateDetail(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'update payment_debts');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $validator = Validator::make($request->all(), [
            'customer_debt_id' => 'required|integer',
            'amount' => 'required|numeric',
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }

		$data = $request->all();
		
		if($data['amount']==0){
			return response()->json(["status"=>"success",
			"message" => "Payment Amount is 0"], 200);
		}

		$payment_debt=PaymentDebtDetail::find($id)->update($data);
		
		UserActivity::storeActivity(array(
			"activity" => "update payment debt detail for id: ".$id,
			"user" => $user->id,
			"menu" => "payment_debts",
			"ipaddress" => $request->ip()
		));

        if($payment_debt){
			$this->updatePaymentAmount($payment_debt->payment_debt_id,$request);
            return response()->json([
                "status" => "success",
                "message" => "Payment debt detail has been saved!",
                "data" => $payment_debt
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save payment debt detail!",
                "data" => null
            ],403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyDetailPerMaster(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'delete payment_debts');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $payment_debt = PaymentDebtDetail::where('payment_debt_id','=',$id)->get();

        if($payment_debt == null){
            return response()->json(["message"=>"Payment debt detail not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();

        if (PaymentDebtDetail::where('payment_debt_id','=',$id)->delete()) {
			$this->updatePaymentAmount($id,$request);
            UserActivity::storeActivity(array(
                "activity" => "delete payment debt detail for master id: ".$id,
                "user" => $user->id,
                "menu" => "payment_debts",
                "ipaddress" => $request->ip()
            ));

            return response()->json([
                "status" => "success",
                "message" => "Payment debt detail has been deleted!"
            ], 200);
        } else {
            return response()->json([
                "status" => "error",
                "message" => "Failed delete payment debt detail!"
            ], 403);
        }
    }
	
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyDetail(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'delete payment_debts');
		
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $payment_debt = PaymentDebtDetail::find($id);
		$payment_id=$payment_debt['payment_debt_id'];

        if($payment_debt == null){
            return response()->json(["message"=>"Payment debt detail not exist"],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();

        if (PaymentDebtDetail::destroy($id)) {
			$this->updatePaymentAmount($payment_id,$request);
            UserActivity::storeActivity(array(
                "activity" => "delete payment debt detail for id: ".$id,
                "user" => $user->id,
                "menu" => "payment_debts",
                "ipaddress" => $request->ip()
            ));

            return response()->json([
                "status" => "success",
                "message" => "Payment debt detail has been deleted!"
            ], 200);
        } else {
            return response()->json([
                "status" => "error",
                "message" => "Failed delete payment debt detail!"
            ], 403);
        }
    }
	
    public function uploadImage($request, $data)
    {
        if($request->hasFile("image")){
            $file = array();
            $file["name"] = $data->id."_".$request->file("image")->getClientOriginalName();
            $file["size"] = $request->file("image")->getClientSize();
            $file["type"] = $request->file("image")->getMimeType();

            $arr_image = array(
                "image_name" => $file["name"],
                "image_size" => $file["size"],
                "image_ext" => $file["type"],
                "image_path" => ""
            );
            $image = $data->update($arr_image);

            if($image){
                $file["url"] = '/uploads/payment/'.$file["name"];
                $file["path"] = '/uploads/payment/';

                if($data->update(["image_path" => $file["path"]])){
                    $request->file("image")->move(public_path().$file["path"], $data->image_name);

                    return $image;
                }
            }
        }
    }
}
