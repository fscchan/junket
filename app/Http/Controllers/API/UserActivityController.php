<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\User;
use App\UserActivity;

class UserActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read user_activities');
        if($cek['result']==1){
            $activities = UserActivity::select('user_activities.*')
				->orderBy('created_at','desc')->paginate(25);
        }else{
            return error_unauthorized();
        }

        return response()->json(transformCollection($activities), 200);
    }

}
