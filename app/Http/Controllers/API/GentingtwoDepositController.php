<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GentingtwoDeposit;
use App\AccountConfig;
use App\JournalAccount;
use App\JournalAccountDetail;
use App\User;
use Validator;
use DB;
use App\UserActivity;

class GentingtwoDepositController extends Controller
{
	
    /**
     * Agent List
     * GET /api/deposit_genting2
     *
     * @param string $token | The token for authentication
     * @return Response
     **/
    public function index(Request $request){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read deposit_genting2');
        if($cek['result']!=1){
            return error_unauthorized();
		}
	
		$search = $request->input('search');
		$sort_by = $request->input('sort_by');
		$sort_type = $request->input('sort_type');
		
		$deposit_genting2 = GentingtwoDeposit::select(['gentingtwo_deposits.*'])
			->join('users','users.id','=','gentingtwo_deposits.user_id');
		
		
		if($search){
			$deposit_genting2 = $deposit_genting2->where('date','LIKE','%'.$search.'%')
				->orWhere('name','LIKE','%'.$search.'%')
				->orWhere('nn_chips','LIKE','%'.$search.'%')
				->orWhere('junket_chips','LIKE','%'.$search.'%')
				->orWhere('cash_chips','LIKE','%'.$search.'%')
				->orWhere('cash_real','LIKE','%'.$search.'%');
		}
		
		if($sort_by){
			$deposit_genting2 = $deposit_genting2->sortable([$sort_by => $sort_type]);
		}else{
			$deposit_genting2 = $deposit_genting2->sortable(['created_at','desc']);
		}
		
		$deposit_genting2 = $deposit_genting2->paginate(10);

        return response()->json(transformCollection($deposit_genting2), 200);
    }
	
    /**
     * Store Agent
     * POST /api/deposit_genting2
     *
     * @param string $token | The token for authentication
     * @param array $deposit_genting2_data | Data input agent
     * @return Response
     **/
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'date' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
				'status' => 'error',
				'message' => implode(' ',$validator->errors()->all())
				], 422);
        }
		
		$data = $request->all();
		$user = User::where('token',$request->header('X-Auth-Token'))->first();
		$data['user_id']=$user->id;
		$cek = check_auth($user,'create deposit_genting2');
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$deposit_genting2 = GentingtwoDeposit::create($data);
		$this->journal($deposit_genting2->id,$request);
		UserActivity::storeActivity(array(
			'activity' => 'add deposit genting 2 for id: '.$deposit_genting2->id,
			'user' => $user->id,
			'menu' => 'deposit_genting2',
			'ipaddress' => $request->ip()
		));
		
        if($deposit_genting2){
            return response()->json([
                'status' => 'success',
                'message' => 'deposit genting 2 has been saved!',
                'data' => $deposit_genting2
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Failed save deposit genting 2!',
                'data' => null
            ],403);
        }
    }
	
	public function journal($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = GentingtwoDeposit::find($id);
		
		DB::beginTransaction();
		
		try{
			$account_config=AccountConfig::first();
			$journal=JournalAccount::where('activity','=','deposit_genting2')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>$data['date'],
				'user_id'=>$user['id'],
				'entity'=>'user',
				'entity_id'=>$data['user_id'],
				'activity'=>'deposit_genting2',
				'activity_id'=>$id,
				'remark'=>'Deposit Genting 2 (Input by:'.$data['user']['name'].')',
				'company_id'=>2
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$data['nn_chips']=isset($data['nn_chips'])?$data['nn_chips']:0;
				$data['junket_chips']=isset($data['junket_chips'])?$data['junket_chips']:0;
				$data['cash_chips']=isset($data['cash_chips'])?$data['cash_chips']:0;
				$data['cash_real']=isset($data['cash_real'])?$data['cash_real']:0;
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['insurance'],//genting 2
					'debit'=>0,
					'credit'=>$data['nn_chips']+$data['junket_chips']+$data['cash_chips']+$data['cash_real'],
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['nn_chips'],//cash
					'debit'=>$data['nn_chips'],
					'credit'=>0,
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['junket_chips'],//cash
					'debit'=>$data['junket_chips'],
					'credit'=>0,
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_chips'],//cash
					'debit'=>$data['cash_chips'],
					'credit'=>0,
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_real'],//cash
					'debit'=>$data['cash_real'],
					'credit'=>0,
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
			}
			
			DB::commit();
		}catch(\Exception $exc){
			echo $exc;
			DB::rollback();
		}
			
		return true;
	}
	
    /**
     * Get Agent by ID
     * GET /api/deposit_genting2/{agent_id}
     *
     * @param string $token | The token for authentication
     * @return Response
     **/
    public function show(Request $request,$id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read deposit_genting2');
        if($cek['result']!=1){
            return error_unauthorized();
		}
		
		$deposit_genting2 = GentingtwoDeposit::find($id);
            
		if(!$deposit_genting2){
			return response()->json([
				'status' => 'error',
				'message' => 'deposit genting 2 not exists!'
			],404);
		}
		
		return response()->json([
			'status' => 'success',
			'data' => $deposit_genting2
		],200);
    }

    /**
     * Update Customer
     * PUT /api/customer/{agent_id}
     *
     * @param string $token | The token for authentication
     * @param integer $deposit_genting2_id | Agent id
     * @param array $deposit_genting2_data | Data update agent
     * @return Response
     **/
    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required'
        ]);

         if($validator->fails()){
            return response()->json([
				'status' => 'error',
				'message' => implode(' ',$validator->errors()->all())
				], 422);
        }
		
		$data = $request->all();
		$user = User::where('token',$request->header('X-Auth-Token'))->first();
		$data['user_id']=$user->id;
		$cek = check_auth($user,'update deposit_genting2');
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$deposit_genting2 = GentingtwoDeposit::find($id);
		if(!$deposit_genting2){
			return response()->json([
				'status' => 'error',
				'message' => 'deposit genting 2 not exists!'
			],404);
		}
		
		$deposit_genting2->update($data);
		$this->journal($id,$request);
		UserActivity::storeActivity(array(
			'activity' => 'update deposit genting 2 for id: '.$deposit_genting2->id,
			'user' => $user->id,
			'menu' => 'deposit_genting2',
			'ipaddress' => $request->ip()
		));
		
        if($deposit_genting2){
            return response()->json([
                'status' => 'success',
                'message' => 'deposit genting 2 has been updated!',
                'data' => $deposit_genting2
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Failed update deposit genting 2!',
                'data' => null
            ],403);
        }
    }

    /**
     * Delete Agent
     * DELETE /api/deposit_genting2/{agent_id}
     *
     * @param string $token | The token for authentication
     * @param integer $deposit_genting2_id | Agent id
     * @return Response
     **/
    public function destroy(Request $request,$id)
    {
        $deposit_genting2 = GentingtwoDeposit::find($id);

        if(!$deposit_genting2){
            return response()->json([
				'status'=>'error',
				'message'=>'deposit genting 2 not exist'
			],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete deposit_genting2');
        if($cek['result']!=1) {
            return error_unauthorized();
        }
		
		$deposit_genting2=GentingtwoDeposit::destroy($id);
		JournalAccount::where('activity','=','deposit_genting2')->where('activity_id','=',$id)->delete();
		UserActivity::storeActivity(array(
			'activity' => 'delete deposit genting 2 for id: '.$id,
			'user' =>$user->id,
			'menu' =>'deposit_genting2',
			'ipaddress' => $request->ip()
		));
		
		if($deposit_genting2) {
			return response()->json([
				'status' => 'success',
				'message' => 'deposit genting 2 has been deleted!'
			], 200);
		} else {
			return response()->json([
				'status' => 'error',
				'message' => 'Fail delete deposit genting 2!'
			], 403);
		}
    }

}
