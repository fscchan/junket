<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Validator;
use App\User;
use App\UserActivity;
use App\LockedVisitor;

class AuthController extends Controller
{
    /**
     * User Login Authentication
     * POST /api/users/login
     *
     * @param string  $username               User username
     * @param string  $password               User password
     * @return Response
     **/
    public function user_login(Request $request){
        $cek_locked = $this->checkLocked($request->ip());
        if(!$cek_locked){
            return response()->json([
                "status" => "error",
                "message" => "Your IP Address has been blocked!"
            ], 403);
        }

        $validator = Validator::make($request->all(),[
            "username" => "required",
            "password" => "required",
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $username = $request->input('username');
            $password = $request->input('password');

            $user = User::where(array("username"=>$username))->first();

            if($user == null){
                LockedVisitor::create(["ip_address" => $request->ip()]);
                return response()->json([
                    "status" => "error",
                    "message" => "The username that you've entered doesn't match any account."
                ], 403);
            } else {
                if(!password_verify($password, $user->password)){
                    LockedVisitor::create(["ip_address" => $request->ip()]);
                    return response()->json([
                        "status" => "error",
                        "message" => "The username and password you entered don't match."
                    ], 403);
                } else {
                    $token_string = md5(time().$user->name.$user->username.md5($request->header('User-Agent')));

                    if($user->token == null || $user->token == ""){
                        $user->token = $token_string;
                        $user->expired_at = date('Y-m-d H:i:s', strtotime('+1 hour', time()));
                        $user->save();
                    }else{
                        if((strtotime($user->expired_at) - time()) <= 0) {
                            $user->update([
                                "token" => $token_string,
                                "expired_at" => date('Y-m-d H:i:s', strtotime('+1 hour', time()))
                            ]);
                        }
                    }

                    UserActivity::storeActivity(array(
                        "activity" => "user login",
                        "user" =>$user->id,
                        "menu" =>"Auth",
                        "ipaddress" => $request->ip()
                    ));
                    LockedVisitor::where('ip_address', $request->ip())->delete();

                    return response()->json([
                        "status" => "success",
                        "message" => "Login Success",
                        "token" => $user->token,
                        "expired_at" => strtotime($user->expired_at),
                        "user" => [
                            "id" => $user->id,
                            "username" => $user->username,
                            "name" => $user->name,
                            "email" => $user->email,
                            "roles" => $user->roles()->first()->name
                        ],
                        "menus" => get_menus($user)
                    ], 200);
                }
            }
        }
    }

    /**
     * User Logout
     * POST /api/users/logout
     *
     * @param integer  $username               Id User
     * @return Response
     **/
    public function user_logout(Request $request){
        $id = $request->input("id");
        $user = User::find($id);
        $log = "";
        if($user!=null){
            $log = $user->update([
                    "token" => null,
                    "expired_at" => null
                ]);
        }

        if($log){
            UserActivity::storeActivity(array(
                "activity" => "user logout",
                "user" =>$user->id,
                "menu" =>"Auth",
                "ipaddress" => $request->ip()
            ));
            return response()->json([
                "status" => "success",
                "message" => "Log Out Success",
            ], 200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "log out failed!"
            ],403);
        }
    }

    public function checkLocked($ip)
    {
        $locked = LockedVisitor::where('ip_address', $ip)->count();
        if($locked >= 3){
            return false;
        }else{
            return true;
        }
    }
}
