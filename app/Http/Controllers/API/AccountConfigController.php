<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\AccountConfig;
use App\UserActivity;
use Validator;
use DB;

class AccountConfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = AccountConfig::orderBy('id','asc')->get();

        return response()->json(['data' => $accounts], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'create accounts');

		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $data = $request->all();
        $log = AccountConfig::create($data);
		// $this->journal($log->id,$request);

        UserActivity::storeActivity(array(
            "activity" => "create account config",
            "user" => $user->id,
            "menu" => "set up",
            "ipaddress" => $request->ip()
        ));

        if($log){
            return response()->json([
                "status" => "success",
                "message" => "account config has been saved!",
                "data" => $log,
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save account config!",
                "data" => null
            ],403);
        }
    }
	
	/**
     * All Account
     * GET /api/accounts
     *
     * @param string $token | The token for authentication
     * @param string $search | Searching value
     * @param string $sort_by | Sorting field
     * @param string $sort_type | Sorting asc, desc
     * @return Response
     **/
    public function all(Request $request){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read accounts');
        if($cek['result']!=1){
            return error_unauthorized();
		}
	
		$search = $request->input('search');
		$sort_by = $request->input('sort_by');
		$sort_type = $request->input('sort_type');
		
		$account = new AccountConfig();
		
		if($search){
		}
		
		if($sort_by){
			$account = $account->sortable([$sort_by => $sort_type]);
		}else{
			$account = $account->sortable(['created_at','desc']);
		}
		
		$account = $account->paginate(10);

        return response()->json(transformCollection($account), 200);
    }
	
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $account = AccountConfig::find($id);

        if($account != null){
            return response()->json([
                "message" => "success",
                "data" => $account
            ],200);
        }else {
            return response()->json(["message" => "Account not found!"],404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$data = $request->all();
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'create accounts');

		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$account = AccountConfig::find($id);
		if($account == null){
			return response()->json(['message' => 'Data not found'], 404);
		}else{
			$log = $account->update($data);
			// $this->journal($id,$request);
		}

		if($log){
			UserActivity::storeActivity(array(
				"activity" => "update config ".$id,
				"user" => $user->id,
				"menu" => "set up",
				"ipaddress" => $request->ip()
			));

			return response()->json([
				"status" => "success",
				"message" => "account config has been updated!",
				"data" => $account
			],200);
		}else{
			return response()->json([
				"status" => "error",
				"message" => "Failed update account config!",
				"data" => null
			],403);
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'create accounts');

		if($cek['result']!=1){
			return error_unauthorized();
		}
		
        $account = AccountConfig::find($id);
        if($account == null){
            return response()->json(['message' => 'Data not found'], 404);
        }else{
            if (AccountConfig::destroy($id)) {
				AccountConfig::where('parent_id','=',$id)->delete();
                UserActivity::storeActivity(array(
                    "activity" => "delete account config for id: ".$id,
                    "user" => $user->id,
                    "menu" => "set up",
                    "ipaddress" => $request->ip()
                ));

                return response()->json([
                    "status" => "success",
                    "message" => "account config has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete account config!"
                ], 403);
            }
        }
    }
}
