<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\AgentCompany;
use App\User;
use Validator;
use DB;
use App\UserActivity;

class AgentCompanyController extends Controller
{
	
    /**
     * Agent List
     * GET /api/agent_company
     *
     * @param string $token | The token for authentication
     * @return Response
     **/
    public function Index(){
        $agent_company_companies = AgentCompany::all();
        return response()->json([
            'message' => 'success',
            'data' => $agent_company_companies
        ],200);
    }
	
    /**
     * Store Agent
     * POST /api/agent_company
     *
     * @param string $token | The token for authentication
     * @param array $agent_company_data | Data input agent
     * @return Response
     **/
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'company_name' => 'required|unique:agent_companies,deleted_at,null'
        ]);

        if($validator->fails()){
            return response()->json([
				'status' => 'error',
				'message' => implode(' ',$validator->errors()->all())
				], 422);
        }
		
		$data = $request->all();
		$user = User::where('token',$request->header('X-Auth-Token'))->first();
		$cek = check_auth($user,'create agent_companies');
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$agent_company = AgentCompany::create($data);
		UserActivity::storeActivity(array(
			'activity' => 'add agent company for id: '.$agent_company->id,
			'user' => $user->id,
			'menu' => 'agent_companies',
			'ipaddress' => $request->ip()
		));
		
        if($agent_company){
            return response()->json([
                'status' => 'success',
                'message' => 'Agent company has been saved!',
                'data' => $agent_company
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Failed save agent company!',
                'data' => null
            ],403);
        }
    }

	/**
     * All Agent
     * GET /api/agent_company/all
     *
     * @param string $token | The token for authentication
     * @param string $search | Searching value
     * @param string $sort_by | Sorting field
     * @param string $sort_type | Sorting asc, desc
     * @return Response
     **/
    public function all(Request $request){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read agent_companies');
        if($cek['result']!=1){
            return error_unauthorized();
		}
	
		$search = $request->input('search');
		$sort_by = $request->input('sort_by');
		$sort_type = $request->input('sort_type');
		
		$agent_company = AgentCompany::select('agent_companies.*');
		
		if($search){
			$agent_company = $agent_company->where('company_name','LIKE','%'.$search.'%')
				->orWhere('pic','LIKE','%'.$search.'%')
				->orWhere('address','LIKE','%'.$search.'%')
				->orWhere('postal_code','LIKE','%'.$search.'%')
				->orWhere('company_contact','LIKE','%'.$search.'%')
				->orWhere('company_email','LIKE','%'.$search.'%');
		}
		
		if(!$sort_by){
			$sort_by='created_at';
		}
		$sort_type=$sort_type?$sort_type:'desc';
		
		$agent_company = $agent_company->orderBy($sort_by,$sort_type)->paginate(10);

        return response()->json(transformCollection($agent_company), 200);
    }
	
    /**
     * Get Agent by ID
     * GET /api/agent_company/{agent_id}
     *
     * @param string $token | The token for authentication
     * @return Response
     **/
    public function show(Request $request,$id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read agent_companies');
        if($cek['result']!=1){
            return error_unauthorized();
		}
		
		$agent_company = AgentCompany::find($id);
            
		if(!$agent_company){
			return response()->json([
				'status' => 'error',
				'message' => 'Agent company not exists!'
			],404);
		}
		
		return response()->json([
			'status' => 'success',
			'data' => $agent_company
		],200);
    }

    /**
     * Update Customer
     * PUT /api/customer/{agent_id}
     *
     * @param string $token | The token for authentication
     * @param integer $agent_company_id | Agent id
     * @param array $agent_company_data | Data update agent
     * @return Response
     **/
    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'company_name' => 'required|unique:agent_companies,company_name,'.$request->input('id').',id,deleted_at,NULL'
        ]);

         if($validator->fails()){
            return response()->json([
				'status' => 'error',
				'message' => implode(' ',$validator->errors()->all())
				], 422);
        }
		
		$data = $request->all();
		$user = User::where('token',$request->header('X-Auth-Token'))->first();
		$cek = check_auth($user,'update agent_companies');
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$agent_company = AgentCompany::find($id);
		if(!$agent_company){
			return response()->json([
				'status' => 'error',
				'message' => 'Agent company not exists!'
			],404);
		}
		
		$agent_company->update($data);
		UserActivity::storeActivity(array(
			'activity' => 'update agent company for id: '.$agent_company->id,
			'user' => $user->id,
			'menu' => 'agent_companies',
			'ipaddress' => $request->ip()
		));
		
        if($agent_company){
            return response()->json([
                'status' => 'success',
                'message' => 'Agent company has been updated!',
                'data' => $agent_company
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Failed update agent company!',
                'data' => null
            ],403);
        }
    }

    /**
     * Delete Agent
     * DELETE /api/agent_company/{agent_id}
     *
     * @param string $token | The token for authentication
     * @param integer $agent_company_id | Agent id
     * @return Response
     **/
    public function destroy(Request $request,$id)
    {
        $agent_company = AgentCompany::find($id);

        if(!$agent_company){
            return response()->json([
				'status'=>'error',
				'message'=>'Agent company not exist'
			],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete agent_companies');
        if($cek['result']!=1) {
            return error_unauthorized();
        }
		
		$agent_company=AgentCompany::destroy($id);
		UserActivity::storeActivity(array(
			'activity' => 'delete agent company for id: '.$id,
			'user' =>$user->id,
			'menu' =>'agent_companies',
			'ipaddress' => $request->ip()
		));
		
		if($agent_company) {
			return response()->json([
				'status' => 'success',
				'message' => 'Agent company has been deleted!'
			], 200);
		} else {
			return response()->json([
				'status' => 'error',
				'message' => 'Fail delete agent company!'
			], 403);
		}
    }

}
