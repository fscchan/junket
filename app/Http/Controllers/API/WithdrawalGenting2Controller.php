<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\WithdrawalGenting2;
use Validator;

class WithdrawalGenting2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $date = $request->input('date');
        $sort_by = $request->input('sort_by');
        $sort_type = $request->input('sort_type');
        $column = "date";

        $wg = WithdrawalGenting2::orderBy($column, $sort_type)->paginate(10);

        return response()->json(transformCollection($wg), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $data = $request->all();
            $log = WithdrawalGenting2::create($data);
        }

        if($log){
            return response()->json([
                "status" => "success",
                "message" => "Withdrawal Genting 2 has been saved!",
                "data" => $log
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed save Withdrawal Genting 2!",
                "data" => null
            ],403);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $purchase = WithdrawalGenting2::find($id);
        if($purchase!=null){
            return response()->json([
                "message" => "success",
                "data" => $purchase
            ],200);
        }else{
            return response()->json(["message" => "Tips Employee not exists!"],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
        ]);

        if($validator->fails()){
            return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
        }else{
            $data = $request->all();
            $log = WithdrawalGenting2::find($id);
            if($log == null){
                return response()->json(["message" => "Data not found"], 404);
            }else{
                $log = $log->update($data);
            }
        }

        if($log){
            return response()->json([
                "status" => "success",
                "message" => "Withdrawal Genting 2 has been update!",
                "data" => $log
            ],200);
        }else{
            return response()->json([
                "status" => "error",
                "message" => "Failed update Withdrawal Genting 2!",
                "data" => null
            ],403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $log = WithdrawalGenting2::find($id);
        if($log == null){
            return response()->json(["message" => "Data not found"], 404);
        }else{
            if ($log->delete()) {
                return response()->json([
                    "status" => "success",
                    "message" => "Withdrawal Genting 2 has been deleted!"
                ], 200);
            } else {
                return response()->json([
                    "status" => "error",
                    "message" => "Failed delete Withdrawal Genting 2!"
                ], 403);
            }
        }
    }
}
