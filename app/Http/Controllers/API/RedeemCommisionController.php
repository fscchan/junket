<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\RedeemCommision;
use App\AccountConfig;
use App\UserActivity;
use App\JournalAccount;
use App\JournalAccountDetail;
use Validator;
use DB;

class RedeemCommisionController extends Controller {
    public function index(Request $request) {
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'read redeem_commision');
		if($cek['result']==1){
			$redeems = RedeemCommision::select(['redeem_commision.*','transactions.transaction_no',
			   'users.username as name','customers.last_name','customers.first_name'])
			 ->join("transactions", "transactions.id", "=", "redeem_commision.trans_id")
			 ->join("users", "users.id", "=", "transactions.user_id")
			 ->join("customers", "customers.id", "=", "transactions.customer_id")
			 ->orderBy($request->input("sort_by"), $request->input("sort_type"));
			 
			if($request->input("filter")){
				$redeems->where("transaction_no", "like", $request->input("filter"). "%");
			}
			/* foreach ($redeems as $i => $o) {
				$redeems[$i]->status = (bool) $o->redeemed;
				$redeems[$i]->trans_no = $o->transaction()->first()->transaction_no;
				$customer = $o->transaction()->first()->customer()->first();
				$redeems[$i]->customer_name = $customer->last_name . " " . $customer->first_name;
				$redeems[$i]->employee_name = $o->transaction()->first()->user()->first()->name;
				$redeems[$i]->status = (
						(bool) $o->redeemed ?
						'<span class="label label-info">Redeeemed</span>' : 
						'<span class="label label-success">Open</span>'
				);
			} */
			$redeems = $redeems->paginate(10)->toArray();
			foreach($redeems["data"] as $i => $o) {
				$redeems["data"][$i]["redeem_status"] = ((bool) $o["redeemed"] ? '<span class="label label-info">Redeemed</span>' : '<span class="label label-success">Open</span>');
			}
			return $redeems;
		} else {
			return error_unauthorized();
		}
    }

    public function create() {
        //
    }

    public function store(Request $request) {
		$validator = Validator::make($request->all(), []);

		if($validator->fails()){
			return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
		} else {
			$user = User::where('token',$request->header("X-Auth-Token"))->first();
			$cek = check_auth($user,'create transactions');
			if($cek['result']==1){
				$redeem = RedeemCommision::create($request->all());
				if(UserActivity::storeActivity(["activity" => "add redeem commision for id: " . $redeem->id, "user" => $user->id, "menu" => "redeem", "ipaddress" => $request->ip()])){
					return response()->json([
						"status" => "success",
						"message" => "Vendor has been saved!",
						"data" => $redeem
					], 200);
				} else {
					return response()->json([
						"status" => "error",
						"message" => "Failed save vendor!",
						"data" => null
					], 403);
				}
			} else {
				return error_unauthorized();
			}
		}
    }

    public function show($id) {
        //
    }

    public function edit($id) {
        //
    }

    public function update(Request $request, $id) {
        //
    }

    public function destroy($id) {
        //
    }
	
	public function closing(Request $request,$id){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'update redeem_commision');
		if($cek['result']==1){
			$account_id=$request->input('account_id');
			$date=$request->input('redeem_date');
			
			$validator = Validator::make($request->all(), [
				'account_id' => 'required',
				'redeem_date' => 'required'
			]);
			if($validator->fails()){
				return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
			}

			$redeem=RedeemCommision::find($id);

            if($redeem == null){
                return response()->json(['message' => 'Data not found'], 404);
            }else{
				$data['account_id']=$account_id;
				$data['redeem_date']=$date;
				$data['redeemed']=1;
                $log = $redeem->update($data);
				$this->journal($id,$request);
            }

            if($log){
                UserActivity::storeActivity(array(
                    "activity" => "update redeem for id: ".$id,
                    "user" => $user->id,
                    "menu" => "redeem",
                    "ipaddress" => $request->ip()
                ));

                return response()->json([
                    "status" => "success",
                    "message" => "redeem has been updated!",
                    "data" => $redeem
                ],200);
            }else{
                return response()->json([
                    "status" => "error",
                    "message" => "Failed update redeem!",
                    "data" => null
                ],403);
            }
		} else {
			return error_unauthorized();
		}
	}
	
	public function journal($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$data = RedeemCommision::select(['redeem_commision.*','transactions.transaction_no',
			   'transactions.user_id','users.username as name','customers.last_name','customers.first_name'])
			 ->join("transactions", "transactions.id", "=", "redeem_commision.trans_id")
			 ->join("users", "users.id", "=", "transactions.user_id")
			 ->join("customers", "customers.id", "=", "transactions.customer_id")
			 ->find($id);
		
		DB::beginTransaction();
		
		try{
			$account_config=AccountConfig::first();
			$journal=JournalAccount::where('activity','=','redeem')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>$data['redeem_date'],
				'user_id'=>$user['id'],
				'entity'=>'user',
				'entity_id'=>$data['user_id'],
				'activity'=>'redeem',
				'activity_id'=>$id,
				'remark'=>'Redeem Commission ('.$data['first_name'].' '.$data['last_name'].')'
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				$data['amount']=isset($data['amount'])?$data['amount']:0;
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$data['account_id'],
					'debit'=>$data['amount'],
					'credit'=>0,
					'entity'=>'customer',
					'entity_id'=>$data['customer_id']
				]);
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_chips'],
					'debit'=>0,
					'credit'=>$data['amount'],
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);
			}
			
			DB::commit();
		}catch(\Exception $exc){
			echo $exc;
			DB::rollback();
		}
			
		return true;
	}
}
