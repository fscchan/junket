<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GentingtwoCommission;
use App\User;
use Validator;
use DB;
use App\UserActivity;

class GentingtwoCommissionController extends Controller
{
	
    /**
     * Agent List
     * GET /api/commission_genting2
     *
     * @param string $token | The token for authentication
     * @return Response
     **/
    public function Index(){
        $commission_genting2_companies = GentingtwoCommission::all();
        return response()->json([
            'message' => 'success',
            'data' => $commission_genting2_companies
        ],200);
    }
	
    /**
     * Store Agent
     * POST /api/commission_genting2
     *
     * @param string $token | The token for authentication
     * @param array $commission_genting2_data | Data input agent
     * @return Response
     **/
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'date' => 'required',
			'buy_from' => 'required',
			'type_chip' => 'required',
			'amount' => 'numeric',
			'commission' => 'numeric',
			'commission_payout' => 'numeric'
        ]);

        if($validator->fails()){
            return response()->json([
				'status' => 'error',
				'message' => implode(' ',$validator->errors()->all())
				], 422);
        }
		
		$data = $request->all();
		$user = User::where('token',$request->header('X-Auth-Token'))->first();
		$cek = check_auth($user,'create commission_genting2');
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$commission_genting2 = GentingtwoCommission::create($data);
		UserActivity::storeActivity(array(
			'activity' => 'add commission genting 2 for id: '.$commission_genting2->id,
			'user' => $user->id,
			'menu' => 'commission_genting2',
			'ipaddress' => $request->ip()
		));
		
        if($commission_genting2){
            return response()->json([
                'status' => 'success',
                'message' => 'commission genting 2 has been saved!',
                'data' => $commission_genting2
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Failed save commission genting 2!',
                'data' => null
            ],403);
        }
    }

	/**
     * All Agent
     * GET /api/commission_genting2/all
     *
     * @param string $token | The token for authentication
     * @param string $search | Searching value
     * @param string $sort_by | Sorting field
     * @param string $sort_type | Sorting asc, desc
     * @return Response
     **/
    public function all(Request $request){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read commission_genting2');
        if($cek['result']!=1){
            return error_unauthorized();
		}
	
		$search = $request->input('search');
		$sort_by = $request->input('sort_by');
		$sort_type = $request->input('sort_type');
		
		$commission_genting2 = GentingtwoCommission::select(['gentingtwo_commissions.*',
				DB::raw('if(gentingtwo_commissions.entity="agent",a.name,
				if(gentingtwo_commissions.entity="boss",u.name,""))as name')])
			->join('users as u','u.id','=','gentingtwo_commissions.entity_id','left')
			->join('agents as a','a.id','=','gentingtwo_commissions.entity_id','left');
		
		
		if($search){
			$commission_genting2 = $commission_genting2->where('buy_from','LIKE','%'.$search.'%')
				->orWhere('type_chips','LIKE','%'.$search.'%');
		}
		
		if($sort_by){
			$commission_genting2 = $commission_genting2->sortable([$sort_by => $sort_type]);
		}else{
			$commission_genting2 = $commission_genting2->sortable(['created_at','desc']);
		}
		
		$commission_genting2 = $commission_genting2->paginate(10);

        return response()->json(transformCollection($commission_genting2), 200);
    }
	
    /**
     * Get Agent by ID
     * GET /api/commission_genting2/{agent_id}
     *
     * @param string $token | The token for authentication
     * @return Response
     **/
    public function show(Request $request,$id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read commission_genting2');
        if($cek['result']!=1){
            return error_unauthorized();
		}
		
		$commission_genting2 = GentingtwoCommission::find($id);
            
		if(!$commission_genting2){
			return response()->json([
				'status' => 'error',
				'message' => 'commission genting 2 not exists!'
			],404);
		}
		
		return response()->json([
			'status' => 'success',
			'data' => $commission_genting2
		],200);
    }

    /**
     * Update Customer
     * PUT /api/customer/{agent_id}
     *
     * @param string $token | The token for authentication
     * @param integer $commission_genting2_id | Agent id
     * @param array $commission_genting2_data | Data update agent
     * @return Response
     **/
    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
			'buy_from' => 'required',
			'type_chip' => 'required',
			'amount' => 'numeric',
			'commission' => 'numeric',
			'commission_payout' => 'numeric'
        ]);

         if($validator->fails()){
            return response()->json([
				'status' => 'error',
				'message' => implode(' ',$validator->errors()->all())
				], 422);
        }
		
		$data = $request->all();
		$user = User::where('token',$request->header('X-Auth-Token'))->first();
		$cek = check_auth($user,'update commission_genting2');
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$commission_genting2 = GentingtwoCommission::find($id);
		if(!$commission_genting2){
			return response()->json([
				'status' => 'error',
				'message' => 'commission genting 2 not exists!'
			],404);
		}
		
		$commission_genting2->update($data);
		UserActivity::storeActivity(array(
			'activity' => 'update commission genting 2 for id: '.$commission_genting2->id,
			'user' => $user->id,
			'menu' => 'commission_genting2',
			'ipaddress' => $request->ip()
		));
		
        if($commission_genting2){
            return response()->json([
                'status' => 'success',
                'message' => 'commission genting 2 has been updated!',
                'data' => $commission_genting2
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Failed update commission genting 2!',
                'data' => null
            ],403);
        }
    }

    /**
     * Delete Agent
     * DELETE /api/commission_genting2/{agent_id}
     *
     * @param string $token | The token for authentication
     * @param integer $commission_genting2_id | Agent id
     * @return Response
     **/
    public function destroy(Request $request,$id)
    {
        $commission_genting2 = GentingtwoCommission::find($id);

        if(!$commission_genting2){
            return response()->json([
				'status'=>'error',
				'message'=>'commission genting 2 not exist'
			],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete commission_genting2');
        if($cek['result']!=1) {
            return error_unauthorized();
        }
		
		$commission_genting2=GentingtwoCommission::destroy($id);
		UserActivity::storeActivity(array(
			'activity' => 'delete commission genting 2 for id: '.$id,
			'user' =>$user->id,
			'menu' =>'commission_genting2',
			'ipaddress' => $request->ip()
		));
		
		if($commission_genting2) {
			return response()->json([
				'status' => 'success',
				'message' => 'commission genting 2 has been deleted!'
			], 200);
		} else {
			return response()->json([
				'status' => 'error',
				'message' => 'Fail delete commission genting 2!'
			], 403);
		}
    }

}
