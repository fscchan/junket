<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

use App\User;
use App\UserActivity;

use App\Vendor;

class VendorController extends Controller
{
	public function lists(){
		$vendors = Vendor::all();
		return response()->json([
			"data" => $vendors
		],200);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request) {
		$user = User::where("token",$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'read vendor');
		if($cek['result'] == 1){
			$vendors = Vendor::whereNull("deleted_at");
			if($request->input("filter")){
				$vendors->where("name", "like", $request->input("filter"). "%");
			}

			return response()->json(transformCollection($vendors->orderBy($request->input("sort_by"), $request->input("sort_type"))->paginate(10)), 200);
		} else {
			return error_unauthorized();
		}
	}

	public function show($id) {
		$vendor = Vendor::find($id);

		if($vendor == null) {
			return response()->json(["error" => "Vendor not found!"], 404);
		} else {
			return response()->json(["data" => $vendor], 200);
		}

	}
	public function create() {}

	public function edit() {}

	public function store(Request $request) {
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'chip' => 'required|in:NN,Junket'
		]);

		if($validator->fails()){
			return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
		} else {
			$user = User::where('token',$request->header("X-Auth-Token"))->first();
			$cek = check_auth($user,'create vendor');
			if($cek['result']==1){
				$vendor = Vendor::create($request->all());
				if(UserActivity::storeActivity(["activity" => "add vendor for id: " . $vendor->id, "user" => $user->id, "menu" => "vendor", "ipaddress" => $request->ip()])){
					return response()->json([
						"status" => "success",
						"message" => "Vendor has been saved!",
						"data" => $vendor
					], 200);
				} else {
					return response()->json([
						"status" => "error",
						"message" => "Failed save vendor!",
						"data" => null
					], 403);
				}
			} else {
				return error_unauthorized();
			}
		}
	}

	public function update(Request $request, $id) {
		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'chip' => 'required|in:NN,Junket'
		]);

		if($validator->fails()){
			return response()->json(["status"=>"error","message" => implode(' ',$validator->errors()->all())], 422);
		} else {
			$user = User::where('token',$request->header("X-Auth-Token"))->first();
			$cek = check_auth($user,'update vendor');
			if($cek['result']==1){
				$vendor = Vendor::find($id);
				if($vendor===null){
					return response()->json(["message" => "Vendor not exists!"],404);
				}else{
					$vendor->update($request->all());
					if(UserActivity::storeActivity(["activity" => "update vendor for id: ".$vendor->id,"user" =>$user->id,"menu" =>"vendor","ipaddress" => $request->ip()])){
						return response()->json([
							"status" => "success",
							"message" => "Vendor has been updated!",
							"data" => $vendor
						],200);
					}else{
						return response()->json([
							"status" => "error",
							"message" => "Failed update vendor!",
							"data" => null
						],403);
					}
				}
			} else {
				return error_unauthorized();
			}
		}
	}

	public function destroy(Request $request, $id) {
		$vendor = Vendor::find($id);

		if($vendor === null){
			return response()->json(["message" => "Vendor not exist"], 404);
		}

		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$cek = check_auth($user,'delete vendor');
		if($cek['result']==1) {
			UserActivity::storeActivity(array(
				"activity" => "delete vendor for id: ".$id,
				"user" => $user->id,
				"menu" => "vendor",
				"ipaddress" => $request->ip()
			));
			$vendor->deleted_at = date("Y-m-d H:i:s");
			$vendor->save();
			
			return response()->json([
				"status" => true
			],200);
		} else {
			return error_unauthorized();
		}
	}
}