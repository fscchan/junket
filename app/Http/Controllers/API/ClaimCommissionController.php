<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ClaimCommission;
use App\JournalAccount;
use App\JournalAccountDetail;
use App\AccountConfig;
use App\User;
use Validator;
use DB;
use App\UserActivity;

class ClaimCommissionController extends Controller
{
	
    public function Index(){
       return [];
    }
	
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'vendor_id' => 'required',
            'year' => 'required',
            'month' => 'required',
            'amount' => 'required|numeric',
            'account_id' => 'required'
        ]);

        if($validator->fails()){
            return response()->json([
				'status' => 'error',
				'message' => implode(' ',$validator->errors()->all())
				], 422);
        }
		
		$data = $request->all();
		$user = User::where('token',$request->header('X-Auth-Token'))->first();
		$cek = check_auth($user,'create claim_commission');
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$claim = ClaimCommission::create($data);
		$this->journal($claim->id,$request);
		UserActivity::storeActivity(array(
			'activity' => 'add purchase commision for id: '.$claim->id,
			'user' => $user->id,
			'menu' => 'claim_commission',
			'ipaddress' => $request->ip()
		));
		
        if($claim){
            return response()->json([
                'status' => 'success',
                'message' => 'Purchase commission has been saved!',
                'data' => $claim
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Failed save purchase commission!',
                'data' => null
            ],403);
        }
    }
	
    public function listHead(Request $request){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read claim_commission');
        if($cek['result']!=1){
            return error_unauthorized();
		}
	
		$sort_by = $request->input('sort_by');
		$sort_type = $request->input('sort_type');
		
		$claim = ClaimCommission::listHead($sort_by,$sort_type)->paginate(10);

        return response()->json(transformCollection($claim), 200);
    }
	
    public function listDetail(Request $request){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read claim_commission');
        if($cek['result']!=1){
            return error_unauthorized();
		}
	
		$year = $request->input('year');
		$month = $request->input('month');
		$sort_by = $request->input('sort_by');
		$sort_type = $request->input('sort_type');
		
		$claim = ClaimCommission::listDetail($year,$month,$sort_by,$sort_type)->paginate(10);

        return response()->json(transformCollection($claim), 200);
    }
	
    public function listAll(Request $request){
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read claim_commission');
        if($cek['result']!=1){
            return error_unauthorized();
		}
	
		$month = $request->input('month');
		$year = $request->input('year');
		$sort_by = $request->input('sort_by');
		$sort_type = $request->input('sort_type');
		
		$claim = ClaimCommission::select(['claim_commissions.*'])
			->join('vendors','vendors.id','=','claim_commissions.vendor_id','left')
			->join('accounts','accounts.id','=','claim_commissions.account_id','left')
			->orderBy($sort_by,$sort_type);
			
		$page=10;
		if($month && $year){
			$claim=$claim->where('year','=',$year)->where('month','=',$month);
			$page=100;
		}
		$claim=$claim->paginate($page);

        return response()->json(transformCollection($claim), 200);
    }
	
	function journal($id,$request){
		$user = User::where('token',$request->header("X-Auth-Token"))->first();
		$vendor = DB::table('vendors')->where('id','=',$request->input('vendor_id'))->first();
		$data['date']=$request->input('date');
		$data['account_id']=$request->input('account_id');
		
		DB::beginTransaction();
		
		try{
			// $uha=UserHasAccount::where('user_id','=',$data['user_id'])->first();
			$account_config=AccountConfig::first();
			$journal=JournalAccount::where('activity','=','purchase_commission')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>$data['date'],
				'user_id'=>$user['id'],
				'entity'=>'user',
				'entity_id'=>$user['id'],
				'activity'=>'purchase_commission',
				'activity_id'=>$id,
				'remark'=>'Purchase Commission ('.$user['name'].')'
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				// $data['value']=isset($data['value'])?$data['value']:0;
				$data['commission']=$request->input('amount')?$request->input('amount'):0;
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$data['account_id'],//cash purchase
					'debit'=>0,
					'credit'=>$data['commission'],
					'entity'=>'user',
					'entity_id'=>$user['id']
				]);
				
				/*JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_chips'],//cash purchase
					'debit'=>0,
					'credit'=>$data['value'],
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);*/
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_chips'],//commission
					'debit'=>$data['commission'],
					'credit'=>0,
					'entity'=>'user',
					'entity_id'=>$user['id']
				]);
				
				/*$account_id=$account_config['nn_chips'];
				switch($data['type_chip']){
					case 'NN Chip':
						$account_id=$account_config['nn_chips'];
					break;
					case 'Cash Chip':
						$account_id=$account_config['cash_chips'];
					break;
					case 'Junket NN Chip':
						$account_id=$account_config['junket_chips'];
					break;
				}
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_id,//chips
					'debit'=>$data['value'],
					'credit'=>0,
					'entity'=>'user',
					'entity_id'=>$data['user_id']
				]);*/
			}
			
			if($vendor->name!='Genting 2' && $vendor->name!='Genting2'){
				DB::commit();
				return true;
			}
			
			//journal genting 2
			$journal=JournalAccount::where('activity','=','purchase_commission_genting2')->where('activity_id','=',$id)->first();
			$number=getAutoNumber('je_num','journal_accounts','JE-'.date('mY')."-",4);
			$data_journal=[
				'je_num'=>$number,
				'date'=>$data['date'],
				'user_id'=>$user['id'],
				'entity'=>'user',
				'entity_id'=>$user['id'],
				'activity'=>'purchase_commission_genting2',
				'activity_id'=>$id,
				'remark'=>'Purchase Commission ('.$user['name'].')',
				'company_id'=>2
			];
			
			if(count($journal)==0){
				$journal=JournalAccount::create($data_journal);
			}else{
				$data_journal['je_num']=$journal->je_num;
				$journal->update($data_journal);
				JournalAccountDetail::where('journal_id','=',$journal->id)->delete();
			}
			
			if($journal){
				// $data['value']=isset($data['value'])?$data['value']:0;
				// $data['commission']=isset($data['commission'])?$data['commission']:0;
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['insurance'],//cash purchase
					'debit'=>$data['commission'],
					'credit'=>0,
					// 'entity'=>'user',
					// 'entity_id'=>$data['user_id']
				]);
				
				/*JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_chips'],//cash purchase
					'debit'=>$data['value'],
					'credit'=>0,
					// 'entity'=>'user',
					// 'entity_id'=>$data['user_id']
				]);*/
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_config['cash_chips'],//commission
					'debit'=>0,
					'credit'=>$data['commission'],
					// 'entity'=>'user',
					// 'entity_id'=>$data['user_id']
				]);
				
				/*$account_id=$account_config['nn_chips'];
				switch($data['type_chip']){
					case 'NN Chip':
						$account_id=$account_config['nn_chips'];
					break;
					case 'Cash Chip':
						$account_id=$account_config['cash_chips'];
					break;
					case 'Junket NN Chip':
						$account_id=$account_config['junket_chips'];
					break;
				}
				
				JournalAccountDetail::create([
					'journal_id'=>$journal->id,
					'account_id'=>$account_id,//chips
					'debit'=>0,
					'credit'=>$data['value'],
					// 'entity'=>'user',
					// 'entity_id'=>$data['user_id']
				]);*/
				
				DB::commit();
			}
		}catch(\Exception $exc){
			echo $exc;
			DB::rollback();
		}
			
		return true;
	}
	
    public function show(Request $request,$id)
    {
        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'read claim_commission');
        if($cek['result']!=1){
            return error_unauthorized();
		}
		
		$claim = ClaimCommission::find($id);
            
		if(!$claim){
			return response()->json([
				'status' => 'error',
				'message' => 'Agent not exists!'
			],404);
		}
		
		return response()->json([
			'status' => 'success',
			'data' => $claim
		],200);
    }
	
    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date',
            'vendor_id' => 'required',
            'year' => 'required',
            'month' => 'required',
            'amount' => 'required|numeric',
            'account_id' => 'required'
        ]);

         if($validator->fails()){
            return response()->json([
				'status' => 'error',
				'message' => implode(' ',$validator->errors()->all())
				], 422);
        }
		
		$data = $request->all();
		$user = User::where('token',$request->header('X-Auth-Token'))->first();
		$cek = check_auth($user,'update agents');
		if($cek['result']!=1){
			return error_unauthorized();
		}
		
		$claim = ClaimCommission::find($id);
		$claim->update($data);
		$this->journal($id,$request);
		UserActivity::storeActivity(array(
			'activity' => 'update purchase commision for id: '.$id,
			'user' => $user->id,
			'menu' => 'claim_commission',
			'ipaddress' => $request->ip()
		));
		
        if($claim){
            return response()->json([
                'status' => 'success',
                'message' => 'Purchase commission has been updated!',
                'data' => $claim
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Failed update purchase commission!',
                'data' => null
            ],403);
        }
    }
	
    public function destroy(Request $request,$id)
    {
        $claim = ClaimCommission::find($id);

        if(!$claim){
            return response()->json([
				'status'=>'error',
				'message'=>'Purchase Commission not exist'
			],404);
        }

        $user = User::where('token',$request->header("X-Auth-Token"))->first();
        $cek = check_auth($user,'delete claim_commission');
        if($cek['result']!=1) {
            return error_unauthorized();
        }
		
		$claim=ClaimCommission::destroy($id);
		JournalAccount::where('activity','=','purchase_commission')->where('activity_id','=',$id)->delete();
		JournalAccount::where('activity','=','purchase_commission_genting2')->where('activity_id','=',$id)->delete();
		UserActivity::storeActivity(array(
			'activity' => 'delete purchase commission for id: '.$id,
			'user' =>$user->id,
			'menu' =>'claim_commission',
			'ipaddress' => $request->ip()
		));
		
		if($claim) {
			return response()->json([
				'status' => 'success',
				'message' => 'Purchase Commission has been deleted!'
			], 200);
		} else {
			return response()->json([
				'status' => 'error',
				'message' => 'Failed delete purchase commission!'
			], 403);
		}
    }

}
