<?php

namespace App\Http\Controllers\Sync;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\SyncController;
use App\User;
use Spatie\Permission\Models\Role;
use Validator;
use App\UserActivity;
use DB;

class UserController extends SyncController
{
    public function pull(Request $request)
	{
		$last_sync=$request->input('date');
		$user_id=$request->input('user_id');
		$token=$request->input('token');
		
		$token_server=md5(date('Y-m-d').'_syahidmuhammadfadil');
		
		if($token!=$token_server)
		{
            return response()->json([
				'status'=>'error',
				'message'=>'Invalid token!'
			], 422);
		}
		
		$user=User::find($user_id);
		if(!$user)
		{
            return response()->json([
				'status'=>'error',
				'message'=>'User not found on server!'
			], 422);
		}
		
		$data['user']=User::where('created_at','>',$last_sync)->orWhere('updated_at','>',$last_sync)->get();
		$data['user_has_roles']=DB::table('user_has_roles')->get();
		$data['permissions']=DB::table('permissions')->where('created_at','>',$last_sync)->orWhere('updated_at','>',$last_sync)->get();
		$data['role_has_permissions']=DB::table('role_has_permissions')->get();
		
		return response()->json([
			'status'=>'success',
			'data'=>$data,
			'sync_local'=>$last_sync
		],200);
	}
}
