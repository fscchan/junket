<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentShare extends Model
{
    protected $fillable = ['agent_id', 'amount'];

    protected $with = ['agent'];

    public function agent(){
        return $this->belongsTo(Agent::class)->withTrashed();
    }
}
