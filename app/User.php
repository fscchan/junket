<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username','name', 'email', 'password', 'token', 'expired_at', 'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'token', 'expired_at', 'deleted_at'
    ];

    public function from_histories(){
        return $this->hasMany(ShiftHistory::class);
    }

    public function to_histories(){
        return $this->hasMany(ShiftHistory::class);
    }

    public function activities(){
        return $this->hasMany(UserActivity::class);
    }

    public function purchases() {
        return $this->hasMany(Purchase::class);
    }

    public function calendar_activities() {
        return $this->hasMany(Activity::class);
    }

    public function expenses() {
        return $this->hasMany(Expense::class);
    }

    public function transaction_banks() {
        return $this->hasMany(TransactionBank::class);
    }

    public function open_balance(){
        return $this->hasOne(OpenBalance::class);
    }

    public function boss_share(){
        return $this->hasOne(BossShare::class);
    }

    public function customer_debt(){
        return $this->hasMany(CustomerDebt::class);
    }

    public function tips_managements(){
        return $this->hasMany(TipsManagement::class);
    }

    public function withdrawal_genting2(){
        return $this->hasMany(WithdrawalGenting2::class);
    }

}
