<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class CustomerType extends Model
{
    use Sortable;

    protected $fillable = [
        'name'
    ];

    protected $hidden = ['created_at', 'updated_at'];
    protected $dates = ['created_at','updated_at'];

    public function customers(){
        return $this->hasMany(Customer::class);
    }

    public $sortable = ["name"];
}
