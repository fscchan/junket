<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;
use DB;

class Customer extends Model
{
    use SoftDeletes, Sortable;

    protected $fillable = [
        'customer_type_id',
        'title',
        'first_name',
        'last_name',
        'dob',
        'gender',
        'age',
        'address',
        'postal_code',
		'phone_code',
        'contact',
        'company_name',
        'background',
        'runner_remarks',
        'commission',
        'debt',
        'credit_term',
        'email',
		'grc_number',
		'check_in_commission',
		'group_flag',
		'agent_flag',
		'agent_id',
        //agung
        'address2',
        'postal_code2'
        //end
    ];

    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at','updated_at','deleted_at'];

    protected $with = ['customer_type','agent'];

    public function customer_type(){
        return $this->belongsTo(CustomerType::class);
    }
	
    public function agent(){
        return $this->belongsTo(Agent::class)->withTrashed();
    }

    public function customer_debts(){
        return $this->hasMany(CustomerDebt::class);
    }

    protected $sortable = [
        'id',
        'title',
        'first_name',
        'last_name',
        'dob',
        'gender',
        'age',
        'address',
        'contact',
        'company_name',
        'background',
        'runner_remarks',
        'commission',
        'debt',
        'credit_term',
        'email',
		'grc_number',
		'check_in_commission',
		'group_flag',
		'agent_flag',
		'agent_id'
    ];

    public static function historyTips($customer_id)
    {
        $histories = TransactionDetail::select(
            'transactions.id',
            'transactions.date',
            'transactions.transaction_no',
            'transactions.check_in_commision',
            DB::raw('SUM(transaction_details.rolling) as amount'))
            ->join('transactions', 'transactions.id', '=', 'transaction_details.transaction_id')
            ->join('customers', 'transactions.customer_id', '=', 'customers.id')
            ->where('transaction_details.flags', '=','role')
            ->where('customers.id', $customer_id)
            ->groupBy('transactions.date', 'transactions.id')
            ->orderBy('transactions.date', 'desc')->get();

        return $histories;
    }

    public static function debts($customer_id){
        $debts = DB::table(DB::raw("(select * from(
				select cd.date,ifnull(cd.amount_due,0)as amount, sum(ifnull(pdd.amount,0)) as check_in_commision
				FROM customer_debts cd
				LEFT JOIN payment_debt_details pdd on pdd.customer_debt_id = cd.id
				LEFT JOIN payment_debts pd on pd.id = pdd.payment_debt_id and pd.deleted_at is null
				LEFT JOIN customers c on c.id = cd.customer_id
				LEFT JOIN users u on u.id = pd.user_id
				where cd.deleted_at is null and c.id='".$customer_id."'
				group by cd.id,pd.id
				order by cd.date asc
			) as sub  where check_in_commision<amount)as sub"))->get();
        return $debts;
    }

}
