<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        "name",
        "address",
        "nn_chips",
        "cash_chips",
        "cash_real",
        "date"
    ];

    protected $hidden = ['updated_at'];
}
