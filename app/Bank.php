<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bank extends Model
{
	use SoftDeletes;
	
    protected $fillable = ["name", "description","account_id","balance"];

    protected $hidden = ['created_at', 'updated_at'];
	
	protected $with = ['account'];
	
	public function account(){
		return $this->belongsTo(Account::class);
	}

    protected $sortable = ['id', 'name','description','balance'];
}
