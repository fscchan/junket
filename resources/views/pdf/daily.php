<html>
<head>
<title>Daily Report</title>
</head>
<style>
	body{
		font-size: 11px;
	}
	table{
		border-collapse: collapse;
	}
	
	table tr td{
		padding: none;
	}
	
	td.padding5{
		padding: 5px;
	}
	
	table.padding5 tr td{
		padding: 5px;
	}
	
	table.center tr td{
		text-align: center
	}
	
	table tr.bottom-line td{
		border-bottom: 1px solid black;
	}
	table tr td.bottom-line{
		border-bottom: 1px solid black;
	}
	.red{
		color: red;
	}
</style>
<body>
	<table width="100%" border="1">
		<tr>
			<td>
				Date
			</td>
			<td>
				Credit Out
			</td>
			<td>
				Credit In
			</td>
			<td>
				Revenue
			</td>
			<td>
				Expense
			</td>
			<td>
				Revenue-Expense
			</td>
			<td>
				Cash On Hand
			</td>
			<td>
				NN Rolling
			</td>
			<td>
				Check In Commission
			</td>
			<td>
				Rolling Commission
			</td>
			<td>
				Net Profit
			</td>
		</tr>
		<?php
			$credit_out=0;
			$credit_in=0;
			$revenue=0;
			$expense=0;
			$net_revenue=0;
			$cash_on_hand=0;
			$nn_rolling=0;
			$check_in_commission=0;
			$rolling_commission=0;
			$net_profit=0;
			foreach($data as $row): ?>
			<tr>
				<td>
					<?php echo $row->date_; ?>
				</td>
				<td>
					<?php echo $row->credit_out; $credit_out+=$row->credit_out; ?>
				</td>
				<td>
					<?php echo $row->credit_in; $credit_in+=$row->credit_in; ?>
				</td>
				<td>
					<?php echo $row->revenue; $revenue+=$row->revenue; ?>
				</td>
				<td>
					<?php echo $row->expense; $expense+=$row->expense; ?>
				</td>
				<td>
					<?php echo $row->net_revenue; $net_revenue+=$row->net_revenue; ?>
				</td>
				<td>
					<?php echo $row->cash_on_hand; $cash_on_hand=$row->cash_on_hand; ?>
				</td>
				<td>
					<?php echo $row->nn_rolling; $nn_rolling+=$row->nn_rolling; ?>
				</td>
				<td>
					<?php echo $row->check_in_commission; $check_in_commission+=$row->check_in_commission; ?>
				</td>
				<td>
					<?php echo $row->rolling_commission; $rolling_commission+=$row->rolling_commission; ?>
				</td>
				<td>
					<?php echo $row->net_profit; $net_profit+=$row->net_profit; ?>
				</td>
			</tr>
		<?php endforeach; ?>
			<tr>
				<td>
					Total
				</td>
				<td>
					<?php echo $credit_out; ?>
				</td>
				<td>
					<?php echo $credit_in; ?>
				</td>
				<td>
					<?php echo $revenue; ?>
				</td>
				<td>
					<?php echo $expense; ?>
				</td>
				<td>
					<?php echo $net_revenue; ?>
				</td>
				<td>
				
				</td>
				<td>
					<?php echo $nn_rolling; ?>
				</td>
				<td>
					<?php echo $check_in_commission; ?>
				</td>
				<td>
					<?php echo $rolling_commission; ?>
				</td>
				<td>
					<?php echo $net_profit; ?>
				</td>
			</tr>
	</table>
</body>
</html>