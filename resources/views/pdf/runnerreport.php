<html>
<head><title>Runner Report</title></head>
<style>
	table{
		border-collapse: collapse;
	}
	
	table tr td{
		padding: none;
	}
	
	td.padding5{
		padding: 5px;
	}
	
	table.padding5 tr td{
		padding: 5px;
	}
	
	table.center tr td{
		text-align: center;
	}
	
	.center{
		text-align: center;
	}
	
	.bottom-line{
		border-bottom: 1px solid black;
	}
	
	table.padding5-top-bottom tr td{
		padding-top: 5px;
		padding-bottom: 5px;
	}
</style>
<body>
	<div style="margin:10px;">
		<table class="padding5" border="0" width="100%">
			<tr>
				<td width="10%">Name :</td>
				<td width="20%"><?php echo $user->name; ?></td>
				<td width="45%">&nbsp;</td>
				<td width="15%">Time In :</td>
				<td width="10%"><?php echo ($shift=='morning')?'8 am':'8 pm'; ?></td>
			</tr>
			<tr>
				<td>Date :</td>
				<td><?php echo date('d-m-Y',strtotime($date)); ?></td>
				<td>&nbsp;</td>
				<td>Time Out :</td>
				<td><?php echo ($shift=='morning')?'8 pm':'8 am'; ?></td>
			</tr>
		</table>
		
		<table class="padding5 center" border="1" width="100%">
			<tr>
				<td>Name</td>
				<td>Buy NN</td>
				<td>Sell NN</td>
				<td>Debt</td>
			</tr>
			<?php $buy=0; $sell=0; $debt=0; $open=$details[0]->nn_chips; foreach($record as $row): ?>
			<tr>
				<td><?php echo $row->name; ?></td>
				<td><?php echo number_format($row->balance_in,0);$buy+=$row->balance_in; ?></td>
				<td><?php echo number_format($row->balance_out,0);$sell+=$row->balance_out; ?></td>
				<td><?php echo number_format($row->debt,0);$debt+=$row->debt; ?></td>
			</tr>
			<?php endforeach; ?>
			<tr>
				<td>&nbsp;</td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td><?php echo number_format($buy,0); ?></td>
				<td><?php echo number_format($sell,0); ?></td>
				<td><?php echo number_format($debt,0); ?></td>
			</tr>
			<tr>
				<td style="text-align:left;">Balance</td>
				<td>C = <?php echo number_format($open_close->cash_chips,0); ?></td>
				<td>N = <?php echo number_format($open_close->chips_return,0); ?></td>
				<td></td>
			</tr>
		</table>
		
		<table width="100%" border="0" class="padding5">
			<tr>
				<td width="10%">
					Total = 
				</td>
				<td width="60%" style="border-bottom: 1px solid black;" align="center"><?php echo number_format($open_close->cash_chips+$open_close->chips_return,0); ?></td>
				<td width="30%">&nbsp;</td>
			</tr>
		</table>
	</div>
	
	<table width="100%" class="padding5-top-bottom">
		<tr>
			<td width="13%"></td>
			<td width="12%"></td>
			<td width="20%"></td>
			<td width="10%"></td>
			<td width="8%"></td>
			<td width="10%"></td>
			<td width="7%"></td>
			<td width="20%"></td>
		</tr>
		<tr>
			<td><b>Opening</b></td>
			<td>Cash Chip :</td>
			<td class="bottom-line center">&nbsp;</td>
			<td>NN Chip :</td>
			<td class="bottom-line center" colspan="2"><?php echo number_format($open_close->chips_out,0); ?></td>
			<!--td>Cash :</td>
			<td class="bottom-line center">&nbsp;</td-->
		</tr>
		<tr>
			<td><b>Close</b></td>
			<td>Cash Chip :</td>
			<td class="bottom-line center"><?php echo number_format($open_close->cash_chips,0); ?></td>
			<td>NN Chip :</td>
			<td class="bottom-line center" colspan="2"><?php echo number_format($open_close->chips_return,0); ?></td>
			<!--td>Cash :</td>
			<td class="bottom-line center"><?php //echo number_format($master->cash_real,0); ?></td-->
		</tr>
		<tr>
			<td colspan="8">&nbsp;</td>
		</tr>
		<?php foreach($transaction as $inde=>$row): ?>
		<tr>
			<td colspan="2"><?php echo $inde+1; ?>. Customer Name</td>
			<td class="bottom-line"><?php echo $row['customer']['first_name'].' '.$row['customer']['last_name']; ?></td>
			<td colspan="3">Check In (House of Group)</td>
			<td class="bottom-line" colspan="3"><?php echo number_format($row['commission'],2); ?> %</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp; Rolling Amount</td>
			<td class="bottom-line"><?php echo number_format($row['rolling'],0); ?></td>
			<td colspan="2">Handover From : <?php echo $row['user_from']['name']; ?></td>
			<td class="bottom-line" colspan="4" align="right">(<?php echo $row['status']==1?'Continue':'Settle'; ?>)</td>
		</tr>
		<?php endforeach; ?>
	</table>
</body>
</html>