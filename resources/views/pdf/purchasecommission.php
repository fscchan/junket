<html>
<head>
<title>Purchase Commission Report</title>
</head>
<style>
	body{
		font-size: 11px;
	}
	table{
		border-collapse: collapse;
	}
	
	table tr td{
		padding: none;
	}
	
	td.padding5{
		padding: 5px;
	}
	
	table.padding5 tr td{
		padding: 5px;
	}
	
	table.padding5 tr th{
		padding: 5px;
	}
	
	table.center tr td{
		text-align: center
	}
	
	table tr.bottom-line td{
		border-bottom: 1px solid black;
	}
	table tr td.bottom-line{
		border-bottom: 1px solid black;
	}
	.red{
		color: red;
	}
</style>
<body>
	<h3>Purchase Commission Report</h3>
	<table class="padding5" width="100%" border="1">
		<tr>
			<th align="center">No</th>
			<th>Customer</th>
			<th>From</th>
			<th>Date</th>
			<th>Purchase Commission</th>
		</tr>
		<?php foreach($data as $key=>$row): ?>
			<tr>
				<td align="center"><?php echo $key+1; ?></td>
				<td><?php echo $row->customer->first_name.' '.$row->customer->last_name; ?></td>
				<td><?php echo $row->vendor->name; ?></td>
				<td><?php echo date('d-m-Y',strtotime($row->date)); ?></td>
				<td align="right"><?php echo number_format($row->commission,2); ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
</body>
</html>