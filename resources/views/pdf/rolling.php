<html>
<head>
<title>Rolling Report</title>
</head>
<style>
	table{
		border-collapse: collapse;
	}
	
	table tr td{
		padding: none;
	}
	
	td.padding5{
		padding: 5px;
	}
	
	table.padding5 tr td{
		padding: 5px;
	}
	
	table.center tr td{
		text-align: center;
	}
</style>
<body>
	<table width="100%" border="1">
		<tr>
			<td style="padding:5px" valign="top" width="33%">
				Name : <?php echo $master->customer->title.' '.$master->customer->first_name.' '.$master->customer->last_name; ?>
			</td>
			<td style="padding:5px" valign="top" width="33%">
				Card ID :
			</td>
			<td style="padding:5px" valign="top" width="33%">
				Date : <br/><?php echo date('d-m-Y',strtotime($master->date)); ?>
			</td>
		</tr>
		<tr>
			<td colspan="3" style="border:none;">&nbsp;</td>
		</tr>
		<tr>
			<td>
				<table class="padding5 center" width="100%" border="1">
					<tr>
						<td width="50%">Rolling</td>
						<td width="50%">Total</td>
					</tr>
					<?php $totalrolling=0; for($idetail=0;$idetail<13;$idetail++): ?>
						<tr>
							<td><?php echo isset($details[$idetail]['rolling'])?(number_format($details[$idetail]['rolling'],0)):'&nbsp;'; $totalrolling+=$details[$idetail]['rolling']; ?></td>
							<td><?php echo isset($details[$idetail]['rolling'])?(number_format($totalrolling,0)):'&nbsp;'; ?></td>
						</tr>
					<?php endfor; ?>
				</table>
			</td>
			<td>
				<table class="padding5 center" width="100%" border="1">
					<tr>
						<td width="50%">Rolling</td>
						<td width="50%">Total</td>
					</tr>
					<?php $totalrolling=0; for($idetail=13;$idetail<26;$idetail++): ?>
						<tr>
							<td><?php echo isset($details[$idetail]['rolling'])?(number_format($details[$idetail]['rolling'],0)):'&nbsp;'; $totalrolling+=$details[$idetail]['rolling']; ?></td>
							<td><?php echo isset($details[$idetail]['rolling'])?(number_format($totalrolling,0)):'&nbsp;'; ?></td>
						</tr>
					<?php endfor; ?>
				</table>
			</td>
			<td>
				<table class="padding5 center" width="100%" border="1">
					<tr>
						<td width="50%">Rolling</td>
						<td width="50%">Total</td>
					</tr>
					<?php $totalrolling=0; for($idetail=26;$idetail<39;$idetail++): ?>
						<tr>
							<td><?php echo isset($details[$idetail]['rolling'])?(number_format($details[$idetail]['rolling'],0)):'&nbsp;'; $totalrolling+=$details[$idetail]['rolling']; ?></td>
							<td><?php echo isset($details[$idetail]['rolling'])?(number_format($totalrolling,0)):'&nbsp;'; ?></td>
						</tr>
					<?php endfor; ?>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>