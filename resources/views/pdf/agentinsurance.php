<html>
<head>
<title>Agent Insurance Report</title>
</head>
<style>
	body{
		font-size: 11px;
	}
	table{
		border-collapse: collapse;
	}
	
	table tr td{
		padding: none;
	}
	
	td.padding5{
		padding: 5px;
	}
	
	table.padding5 tr td{
		padding: 5px;
	}
	
	table.padding5 tr th{
		padding: 5px;
	}
	
	table.center tr td{
		text-align: center
	}
	
	table tr.bottom-line td{
		border-bottom: 1px solid black;
	}
	table tr td.bottom-line{
		border-bottom: 1px solid black;
	}
	.red{
		color: red;
	}
</style>
<body>
	<h3>Agent Insurance Report</h3>
	<table class="padding5" width="100%" border="1">
		<tr>
			<th>Date</th>
			<th>Customer Name</th>
			<th>Runner</th>
			<th>Insurance</th>
			<th>GL</th>
			<th>AG</th>
			<th>PP</th>
			<th>Other</th>
		</tr>
		<?php foreach($data as $row): ?>
			<tr>
				<td><?php echo date('d-m-Y',strtotime($row->date)); ?></td>
				<td><?php echo $row->customer_name; ?></td>
				<td><?php echo $row->username; ?></td>
				<td align="right"><?php echo number_format($row->insurance,2); ?></td>
				<td align="right"><?php echo number_format($row->gl,2); ?></td>
				<td align="right"><?php echo number_format($row->ag,2); ?></td>
				<td align="right"><?php echo number_format($row->pp,2); ?></td>
				<td align="right"><?php echo number_format($row->other,2); ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
</body>
</html>