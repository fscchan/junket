<html>
<head>
<title>Final Report</title>
</head>
<style>
	body{
		font-size: 11px;
	}
	table{
		border-collapse: collapse;
	}
	
	table tr td{
		padding: none;
	}
	
	td.padding5{
		padding: 5px;
	}
	
	table.padding5 tr td{
		padding: 5px;
	}
	
	table.center tr td{
		text-align: center
	}
	
	table tr.bottom-line td{
		border-bottom: 1px solid black;
	}
	table tr td.bottom-line{
		border-bottom: 1px solid black;
	}
	.red{
		color: red;
	}
</style>
<body>
	Date : <?php echo date('d-m-Y',strtotime($_GET['date'])); ?>
	<table width="100%">
		<tr>
			<td valign="top" width="60%">
				<table class="padding5" width="100%" border="1">
					<tr>
						<td>No</td>
						<td>Name</td>
						<td>Type</td>
						<td>Out</td>
						<td>In</td>
						<td>Balance Earn</td>
						<td colspan="2">Remark</td>
					</tr>
					<?php foreach($rolling as $key=>$values_rolling): ?>
					<tr>
						<td><?php echo $key+1; ?></td>
						<td><?php echo $values_rolling->name; ?></td>
						<td><?php echo $values_rolling->type; ?></td>
						<td align="right" class="red"><?php echo number_format($values_rolling->rolling,2); ?></td>
						<td align="right"><?php echo number_format($values_rolling->check_in_commision,2); ?></td>
						<td align="right"><?php echo number_format($values_rolling->closing_cash,2); ?></td>
						<td colspan="2"><?php echo $values_rolling->remarks; ?></td>
					</tr>
					<?php endforeach; ?>
					<tr>
						<td colspan="8" align="center">ROLLING</td>
					</tr>
					<tr>
						<td>No</td>
						<td>Name</td>
						<td>%</td>
						<td>Transaction</td>
						<td>Company Commission</td>
						<td>Spending Commission</td>
						<td>AD</td>
						<td>Remark</td>
					</tr>
					<?php $rolling_t=0; $total_commission_t=0; $closing_cash_t=0; foreach($rolling_trans as $key=>$values_rolling_t): ?>
					<tr>
						<td><?php echo $key+1; ?></td>
						<td><?php echo $values_rolling_t->name; ?></td>
						<td><?php echo $values_rolling_t->commission; ?></td>
						<td align="right" class="red"><?php echo number_format($values_rolling_t->rolling,2); $rolling_t+=$values_rolling_t->rolling; ?></td>
						<td align="right"><?php echo number_format($values_rolling_t->total_commission,2); $total_commission_t+=$values_rolling_t->total_commission; ?></td>
						<td align="right"><?php echo number_format($values_rolling_t->closing_cash,2); $closing_cash_t+=$values_rolling_t->closing_cash; ?></td>
						<td></td>
						<td><?php echo $values_rolling_t->remarks; ?></td>
					</tr>
					<?php endforeach; ?>
					<tr>
						<td></td>
						<td>Total</td>
						<td></td>
						<td align="right" class="red"><?php echo number_format($rolling_t,2); ?></td>
						<td align="right"><?php echo number_format($total_commission_t,2); ?></td>
						<td align="right"><?php echo number_format($closing_cash_t,2); ?></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td colspan="8" align="center">Expense</td>
					</tr>
					<tr>
						<td>No</td>
						<td colspan="4">Description</td>
						<td>Amount</td>
						<td colspan="2">Remark</td>
					</tr>
					<?php foreach($expense as $key=>$values_expense): ?>
					<tr>
						<td><?php echo $key+1; ?></td>
						<td colspan="4"><?php echo $values_expense->annotation; ?></td>
						<td align="right" class="red"><?php echo number_format($values_expense->amount,2); ?></td>
						<td colspan="2"></td>
					</tr>
					<?php endforeach; ?>
				</table>
			</td>
			<td>&nbsp;</td>
			<td valign="top">
				<table class="padding5">
					<tr class="bottom-line">
						<td colspan="2">Today Cash Report</td>
						<td></td>
						<td colspan="2">Today Cash Commission</td>
					</tr>
					<tr>
						<td>In</td>
						<td align="right"><?php echo number_format($balance[0]->debit,2); ?></td>
					</tr>
					<tr>
						<td>Out</td>
						<td class="red" align="right"><?php echo number_format($balance[0]->credit,2); ?></td>
					</tr>
					<tr>
						<td>Expense</td>
						<td class="red" align="right"><?php echo number_format($balance[0]->expense,2); ?></td>
					</tr>
					<tr>
						<td>Client Commission</td>
						<td class="red" align="right"><?php echo number_format($balance[0]->spending_fee,2); ?></td>
					</tr>
					<tr>
						<td>Today Balance</td>
						<td align="right" class="<?php echo $balance[0]->today_balance<0?'red':'' ?>"><?php $balance[0]->today_balance=($balance[0]->today_balance<0)?-$balance[0]->today_balance:$balance[0]->today_balance; echo number_format($balance[0]->today_balance,2); ?></td>
						<td></td>
						<td>Interest</td>
						<td align="right"><?php echo number_format($balance[0]->balance_earn,2); ?></td>
					</tr>
					<tr class="bottom-line">
						<td>Last Day</td>
						<td align="right" class="<?php echo $balance[0]->last_day_balance<0?'red':'' ?>"><?php $balance[0]->last_day_balance=($balance[0]->last_day_balance<0)?-$balance[0]->last_day_balance:$balance[0]->last_day_balance; echo number_format($balance[0]->last_day_balance,2); ?></td>
						<td></td>
						<td>Expense</td>
						<td align="right"><?php echo number_format($balance[0]->expense,2); ?></td>
					</tr>
					<tr class="bottom-line">
						<td>Current Balance</td>
						<td align="right" class="<?php echo $balance[0]->total_balance_in<0?'red':'' ?>"><?php $balance[0]->total_balance_in=($balance[0]->total_balance_in<0)?-$balance[0]->total_balance_in:$balance[0]->total_balance_in; echo number_format($balance[0]->total_balance_in,2); ?></td>
						<td></td>
						<td>Profit Loss</td>
						<td align="right" class="<?php echo $balance[0]->total_earn<0?'red':'' ?>"><?php $balance[0]->total_earn=($balance[0]->total_earn<0)?-$balance[0]->total_earn:$balance[0]->total_earn; echo number_format($balance[0]->total_earn,2); ?></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td class="bottom-line" colspan="2">Today Rolling Report</td>
						<td></td>
						<td class="bottom-line" colspan="2">Today total balance commission report</td>
					</tr>
					<tr>
						<td>Total Rolling</td>
						<td align="right"><?php echo number_format($commission[0]->transaction,2); ?></td>
					</tr>
					<tr>
						<td>Total Commission</td>
						<td align="right"><?php echo number_format($commission[0]->total_commission,2); ?></td>
					</tr>
					<tr>
						<td>Company Commission</td>
						<td align="right"><?php echo number_format($commission[0]->company_commission,2); ?></td>
						<td></td>
						<td>Profit Loss</td>
						<td align="right" class="red"><?php echo number_format($balance[0]->total_earn,2); ?></td>
					</tr>
					<tr>
						<td>Client Commission</td>
						<td align="right" class="red"><?php echo number_format($commission[0]->spending_commission,2); ?></td>
						<td></td>
						<td class="bottom-line">Rolling</td>
						<td align="right" class="bottom-line"><?php echo number_format($commission[0]->company_commission,2); ?></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td>Total Earning</td>
						<td align="right" class="red"><?php echo number_format($balance[0]->total_earn-$commission[0]->total_collection,2); ?></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>