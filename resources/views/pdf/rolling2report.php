<html>
<head>
<title>Rolling Report</title>
</head>
<style>
	body{
		font-size: 11px;
	}
	table{
		border-collapse: collapse;
	}
	
	table tr td{
		padding: none;
	}
	
	td.padding5{
		padding: 5px;
	}
	
	table.padding5 tr td{
		padding: 5px;
	}
	
	table.padding5 tr th{
		padding: 5px;
	}
	
	table.center tr td{
		text-align: center
	}
	
	table tr.bottom-line td{
		border-bottom: 1px solid black;
	}
	table tr td.bottom-line{
		border-bottom: 1px solid black;
	}
	.red{
		color: red;
	}
</style>
<body>
	<h3>Rolling 3 Report</h3>
	<table class="padding5" width="100%" border="1">
		<tr>
			<th>Date</th>
			<th>Runner</th>
			<th>Customer Name</th>
			<th>Bet</th>
			<th>Odds</th>
			<th>Insurance</th>
			<th>Scenario</th>
			<th>Room</th>
			<th>GL</th>
			<th>Agent</th>
			<th>PP</th>
			<th>Other</th>
			<th>Result</th>
		</tr>
		<?php foreach($data as $row): ?>
			<tr>
				<td><?php echo date('d-m-Y',strtotime($row->date)); ?></td>
				<td><?php echo $row->user_name; ?></td>
				<td><?php echo $row->customer_name; ?></td>
				<td align="right"><?php echo number_format($row->rolling,2); ?></td>
				<td align="right"><?php echo number_format($row->odds,2); ?></td>
				<td align="right"><?php echo number_format($row->insurance,2); ?></td>
				<td><?php echo $row->status; ?></td>
				<td><?php echo $row->room; ?></td>
				<td><?php echo number_format($row->gl,2); ?></td>
				<td><?php echo number_format($row->agent_share,2); ?></td>
				<td><?php echo number_format($row->pp,2); ?></td>
				<td><?php echo number_format($row->other,2); ?></td>
				<td><?php echo $row->scenario; ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
</body>
</html>