<html>
<head>
<title>Collection Report</title>
</head>
<style>
	table{
		border-collapse: collapse;
	}
	
	table tr td{
		padding: none;
	}
	
	td.padding5{
		padding: 5px;
	}
	
	table.padding5 tr td{
		padding: 5px;
	}
	
	table.center tr td{
		text-align: center
	}
</style>
<body>
	<table width="100%" border="1" class="padding5 center">
		<tr>
			<td style="border-right:none;text-align:left" colspan="5">
				Name : <?php echo $master->customer->title.' '.$master->customer->first_name.' '.$master->customer->last_name; ?>
			</td>
			<td style="border-left:none;" colspan="2">
				Date : <?php echo date('d-m-Y',strtotime($master->date)); ?>
			</td>
		</tr>
		<tr>
			<td width="14%">
				Date
			</td>
			<td width="14%">
				Time
			</td>
			<td width="14%">
				Taken
			</td>
			<td width="14%">
				Return Rolling
			</td>
			<td width="14%">
				Total
			</td>
			<td width="14%">
				Balance
			</td>
			<td width="14%">
				Sign
			</td>
		</tr>
		<?php $master_date=date('d-m-Y',strtotime($master->date)); $master_time=date('H:i:s',strtotime($master->date)); $total=0; ?>
		<tr>
			<td><?php echo $master_date; ?></td>
			<td><?php echo $master_time; ?></td>
			<td><?php echo number_format($master->nn_chips,0); $total+=$master->nn_chips; ?></td>
			<td>-</td>
			<td><?php echo number_format($total,0); ?></td>
			<td></td>
			<td></td>
		</tr>
		<?php $credit=0; foreach($details as $values_detail): ?>
			<tr>
				<td><?php echo $master_date; /*$master_date='II';*/ ?></td>
				<td><?php echo $values_detail->time_transaction; ?></td>
				<td><?php echo number_format($values_detail->rolling,0); $credit+=$values_detail->rolling; ?></td>
				<td>-</td>
				<td><?php echo number_format($credit,0); ?></td>
				<td></td>
				<td></td>
			</tr>
		<?php endforeach; ?>
			<tr>
				<td><?php echo date('d-m-Y',strtotime($master->closing_date)); /*$master_date='II';*/ ?></td>
				<td><?php echo date('H:i:s',strtotime($master->closing_date)); ?></td>
				<td>-</td>
				<td>
					<?php $closing=$master->closing_nn_chips+$master->closing_cash_chips+$master->closing_cash; 
						echo number_format($closing,0); ?>
				</td>
				<td><?php echo number_format(($closing-$credit),0); ?></td>
				<td></td>
				<td></td>
			</tr>
	</table>
</body>
</html>