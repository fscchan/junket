<html>
<head>
<title>Collection Report</title>
</head>
<style>
	table{
		border-collapse: collapse;
	}
	
	table tr td{
		padding: none;
	}
	
	td.padding5{
		padding: 5px;
	}
	
	table.padding5 tr td{
		padding: 5px;
	}
	
	table.center tr td{
		text-align: center
	}
</style>
<body>
	<h3><?php echo 'Collection Report '.($is_paid==0?'(Outstanding)':'(Settle)'); ?></h3>
	<table width="100%" border="1" class="padding5 center">
		<tr>
			<td width="5%">
				No
			</td>
			<td width="15%">
				Name
			</td>
			<td width="5%">
				Type
			</td>
			<td width="15%">
				Amount
			</td>
			<?php if($is_paid==1): ?>
			<td width="15%">
				Settle
			</td>
			<?php endif; ?>
			<td width="10%">
				Ins
			</td>
			<td width="10%">
				Handle
			</td>
			<td width="15%">
				Remark
			</td>
			<td width="8%">
				Date
			</td>
			<td width="8%">
				Payment <br/>Date
			</td>
		</tr>
		<?php foreach($data as $key=>$values_detail): ?>
			<tr>
				<td><?php echo $key+1; ?></td>
				<td><?php echo $values_detail->name; ?></td>
				<td><?php echo $values_detail->type; ?></td>
				<td align="right"><?php echo number_format($values_detail->amount_due,2); ?></td>
				<?php if($is_paid==1): ?>
				<td align="right"><?php echo number_format($values_detail->settle,2); ?></td>
				<?php endif; ?>
				<td align="right"><?php echo number_format($values_detail->ins,2); ?></td>
				<td><?php echo $values_detail->handle; ?></td>
				<td><?php echo $values_detail->remark; ?></td>
				<td><?php echo date('d M Y',strtotime($values_detail->date)); ?></td>
				<td><?php echo $values_detail->pay_date?date('d M Y',strtotime($values_detail->pay_date)):''; ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
</body>
</html>