<html>
<head>
<title>Final Customer</title>
</head>
<style>
	body{
		font-size: 11px;
	}
	table{
		border-collapse: collapse;
	}
	
	table tr td{
		padding: none;
	}
	
	td.padding5{
		padding: 5px;
	}
	
	table.padding5 tr td{
		padding: 5px;
	}
	
	table.padding5 tr th{
		padding: 5px;
	}
	
	table.center tr td{
		text-align: center
	}
	
	table tr.bottom-line td{
		border-bottom: 1px solid black;
	}
	table tr td.bottom-line{
		border-bottom: 1px solid black;
	}
	.red{
		color: red;
	}
</style>
<body>
	<h3>Customer Report</h3>
	<table class="padding5" width="100%" border="1">
		<tr>
			<th>Date</th>
			<th>GRC No</th>
			<th>Customer Name</th>
			<th>Check In %</th>
			<th>NN Chips <br/>Purchased</th>
			<th>Commission <br/>(NN Chips)</th>
			<th>Table Rolling <br/>(Cash Chips)</th>
			<th>Commission <br/>(Cash Chips)</th>
			<th>Total <br/>Commission Due</th>
		</tr>
		<?php foreach($data as $row): ?>
			<tr>
				<td><?php echo $row->date; ?></td>
				<td><?php echo $row->grc_number; ?></td>
				<td><?php echo $row->customer_name; ?></td>
				<td align="right"><?php echo number_format($row->check_in_commission,2); ?></td>
				<td align="right"><?php echo number_format($row->p_amount,2); ?></td>
				<td align="right"><?php echo number_format($row->check_in_commision,2); ?></td>
				<td align="right"><?php echo number_format($row->cash_chip,2); ?></td>
				<td align="right"><?php echo number_format($row->cash_chip_commission,2); ?></td>
				<td align="right"><?php echo number_format($row->total,2); ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
</body>
</html>