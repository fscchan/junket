<html>
<head>
<title>Rolling Report</title>
</head>
<style>
	body{
		font-size: 11px;
	}
	table{
		border-collapse: collapse;
	}
	
	table tr td{
		padding: none;
	}
	
	td.padding5{
		padding: 5px;
	}
	
	table.padding5 tr td{
		padding: 5px;
	}
	
	table.padding5 tr th{
		padding: 5px;
	}
	
	table.center tr td{
		text-align: center
	}
	
	table tr.bottom-line td{
		border-bottom: 1px solid black;
	}
	table tr td.bottom-line{
		border-bottom: 1px solid black;
	}
	.red{
		color: red;
	}
</style>
<body>
	<h3>Rolling Report 2</h3>
	<table class="padding5" width="100%" border="1">
		<tr>
			<th>Date</th>
			<th>Runner</th>
			<th>Customer Name</th>
			<th>Rolling</th>
		</tr>
		<?php foreach($data as $row): ?>
			<tr>
				<td><?php echo date('d-m-Y',strtotime($row->date)); ?></td>
				<td><?php echo $row->user_name; ?></td>
				<td><?php echo $row->customer_name; ?></td>
				<td align="right"><?php $total_rolling += $row->rolling;
				 echo number_format($row->rolling,2); ?></td>
			</tr>
		<?php endforeach; ?>
		<tr>
			<td>Total</td>
			<td></td>
			<td></td>
			<td><?php echo number_format($total_rolling,2); ?></td>
		</tr>
	</table>
</body>
</html>