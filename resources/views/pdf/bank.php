<html>
<head>
<title>Bank Statement Report</title>
</head>
<style>
	table{
		border-collapse: collapse;
	}
	
	table tr td{
		padding: none;
	}
	
	td.padding5{
		padding: 5px;
	}
	
	table.padding5 tr td{
		padding: 5px;
	}
	
	table.center tr td{
		text-align: center
	}
</style>
<body>
	<table width="100%" border="1" class="padding5 center">
		<tr>
			<td align="left" colspan="7">
				Start Date: <?php echo $start_date; ?>
				&emsp;
				End Date: <?php echo $start_date; ?>
				&emsp;
				Bank: <?php echo $bank['name']; ?>
			</td>
		</tr>
		<tr>
			<td width="15%">
				No
			</td>
			<td width="15%">
				Date
			</td>
			<td width="15%">
				Ref. No
			</td>
			<td width="15%">
				Pay To/Received From
			</td>
			<td width="15%">
				Debit
			</td>
			<td width="15%">
				Credit
			</td>
			<td width="15%">
				Balance
			</td>
		</tr>
		<?php foreach($data as $key=>$values_data): ?>
			<tr>
				<td><?php echo $key+1; ?></td>
				<td><?php echo date('d-m-Y',strtotime($values_data->date)); ?></td>
				<td><?php echo $values_data->ref_no; ?></td>
				<td><?php echo $values_data->pic; ?></td>
				<td align="right"><?php echo number_format($values_data->debit,2); ?></td>
				<td align="right"><?php echo number_format($values_data->credit,2); ?></td>
				<td align="right"><?php echo number_format($values_data->balance,2); ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
</body>
</html>