<!DOCTYPE html>
<html lang="en" ng-app="main">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>JUNKET SYSTEM</title>
    <link href="<?php echo asset('public/css/bootstrap.min.css'); ?>" rel="stylesheet">

	<link href="<?php echo asset('/public/css/adminlte/AdminLTE.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo asset('/public/css/adminlte/skins/_all-skins.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo asset('/public/css/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet">
	<link href="<?php echo asset('/public/css/datatables/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo asset('/public/css/jquery-ui.css'); ?>" rel="stylesheet">
	<link href="<?php echo asset('/public/css/angular-material.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo asset('/public/css/custom.css'); ?>" rel="stylesheet">

	<!-- Style - Calendar -->
	<link href="<?php echo asset('/public/js/lib/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min.css'); ?>" rel="stylesheet">
	<!-- <link href="<?php echo asset('/public/lib/scheduler/dhtmlxscheduler.css'); ?>" rel="stylesheet"> -->
	<link href="<?php echo asset('/public/lib/scheduler/dhtmlxscheduler-responsive.css'); ?>" rel="stylesheet">
	<link href="<?php echo asset('/public/lib/scheduler/dhtmlxscheduler-tools.css'); ?>" rel="stylesheet">
	<link href="<?php echo asset('/public/css/calendar/dhtmlxscheduler.css'); ?>" rel="stylesheet">

	<!-- Fonts -->
	<link href="<?php echo asset('public/fonts/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">

	<!-- Scripts -->
	<script src="<?php echo asset('/public/js/jquery-1.11.3.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/jquery-ui.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/scriptjs.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/admin-lte.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/datatables/jquery.dataTables.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/datatables/dataTables.bootstrap.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/datatables/dataTables.responsive.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/datatables/responsive.bootstrap.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/angular/angular.js'); ?>"></script>
	<!--script src="<?php echo asset('/public/js/angular/script.min.js'); ?>"></script-->
	<script src="<?php echo asset('/public/js/angular/angular-animate.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/angular/angular-aria.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/angular/angular-cookies.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/angular/angular-loader.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/angular/angular-messages.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/angular/angular-resource.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/angular/angular-route.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/angular/angular-sanitize.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/angular/angular-touch.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/ocLazyLoad.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/ng-file-upload-shim.js'); ?>"></script>
  	<script src="<?php echo asset('/public/js/ng-file-upload.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/datatables/angular-datatables.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/datepicker/date.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/angular-ui-bootstrap.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/angular-ui-bootstrap-modal.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/jquery.maskedinput.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/angular-chosen.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/angular-select2.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/angular-jquery-autocomplete.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/angular-material.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/js/international-phone-number.min.js'); ?>"></script>
	
	<script src="<?php echo asset('/public/js/mask.min.js'); ?>"></script>
	
	<script src="<?php echo asset('/public/js/main.js'); ?>"></script>

	<!-- Script - Calendar -->
	<script src="<?php echo asset('/public/js/lib/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.min.js'); ?>"></script>
	<script src="<?php echo asset('/public/lib/scheduler/dhtmlxscheduler.js'); ?>"></script>
	<script src="<?php echo asset('/public/lib/scheduler/dhtmlxscheduler-responsive.js'); ?>"></script>
	<!-- <script src="<?php echo asset('/public/js/dhtmlxScheduler/app.js'); ?>"></script> -->
	<script src="<?php echo asset('/public/js/dhtmlxScheduler/directive-calendar.js'); ?>"></script>
	<!-- <script src="<?php echo asset('/public/js/ext/dhtmlxscheduler_editors.js'); ?>" type="text/javascript" charset="utf-8"></script>
	<script src="<?php echo asset('/public/js/ext/dhtmlxscheduler_minical.js'); ?>" type="text/javascript" charset="utf-8"></script>
	<script src="<?php echo asset('/public/js/ext/dhtmlxscheduler_recurring.js'); ?>" type="text/javascript" charset="utf-8"></script> -->


	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini" ng-controller="templateController" ng-class="{'sidebar-collapse':user.length==0}">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="#/" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>J</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Junket</b> System</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="" id="sidebar-toggle" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu" ng-show="isLoggedin()">
                <a href="" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo asset('/public/img/placeholder.png'); ?>" class="user-image" alt="User Image">
                  <span class="hidden-xs">{{user.name}}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo asset('/public/img/placeholder.png'); ?>" class="img-circle" alt="User Image">
                    <p>
					{{user.name}}
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#/admin/change_password" class="btn btn-default btn-flat">Profile</a>
                      <a href="#/report/app/current_chips" class="btn btn-default btn-flat">Cash&Chips</a>
                    </div>
                    <div class="pull-right">
                      <a ng-click="logout()" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar" ng-show="isLoggedin()">
          <!-- Sidebar user panel -->
          <div class="user-panel">
			<div class="div-logo"><a href="#"><img height="50" src="<?php echo asset('/public/img/logo.png') ?>"></a></div>
            <div class="pull-left image">
              <!--img src="<?php echo asset('/public/img/placeholder.png'); ?>" class="img-circle" alt="User Image"-->
            </div>
            <div class="pull-left info">
              <p>{{user.name}}</p>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu" ng-show="isLoggedin()">
            <li class="header">MAIN NAVIGATION</li>
            <li ng-repeat="(key,menu) in menus" ng-class="{'treeview':menu.menu}" ng-show="menu.menu">
              <a href="">
                <i class="fa {{menu.icon}}"></i> <span>{{key | ucwords}}</span> <i ng-class="{'fa fa-angle-left pull-right':menu.menu}"></i>
              </a>
              <ul ng-class="{'treeview-menu':menu.menu}">
                <li ng-repeat="(keyChild,child) in menu.menu"><a ng-href="#/{{child.url}}"><i class="fa fa-circle-o"></i> {{(replace(child.display_name,'_',' ')) | ucwords}}</a></li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
		<section class="content" ng-view></section>
	  </div>

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; 2016 JUNKET System.</strong> All rights reserved.
      </footer>

      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div>
	<div class="div-dialog-modal" ng-show="pageloading">
		<div class="loading">
			<img width="50" src="public/img/myloader.gif">
		</div>
	</div>
</body>
</html>
