main.registerCtrl("lockedVisitorController",function($scope,$http){
    $scope.alert = "";
    $scope.success = "";
    $scope.page			= 1;

    $scope.lockedVisitorList=[];
    $scope.loadTable=function(){
        $http({
            method: "GET",
            url: "api/locked",
            headers:{
                "Content-Type":"application/json",
                "X-Auth-Token":$scope.token
            },
            params:{
                page: $scope.page
            }}).
        then(function(response){
            $scope.lockedVisitorList=response.data;
        }, function(response){
            $scope.alert=response.data.message;
        });
    }

    $scope.showPage=function(en){
        return (en>=$scope.lockedVisitorList.current_page && en<=$scope.lockedVisitorList.current_page+2) || (en<=$scope.lockedVisitorList.current_page && (en>=$scope.lockedVisitorList.current_page-2 || en>=$scope.lockedVisitorList.last_page-2));
    }

    $scope.isLastPage=function(){
        return $scope.lockedVisitorList.current_page==$scope.lockedVisitorList.last_page;
    }

    $scope.paging=function(page){
        $scope.page=page;
        $scope.loadTable();
    };

    $scope.delete = function(id) {
        $http({
            method: "delete",
            url: 'api/locked/'+id,
            headers: {
                "X-Auth-Token":$scope.token
            }
        }).
        then(function(response){
            $scope.success="Data Deleted!";
            $scope.loadTable();
        }, function(response){
            $scope.alert=response.data.message;
        });
    }

});