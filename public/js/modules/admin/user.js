main.registerCtrl("userController",function($scope,$location,$http,DTOptionsBuilder, DTColumnDefBuilder,$routeParams){
	$scope.alert = "";
	$scope.success = "";
	
	$scope.fd=[];
	$scope.add=function(){
		$location.path("admin/users/create");
	}
	
	$scope.rolesOption = [];
	$scope.getRoles = function() {
		$http({
			method: "get", 
			url: "api/role/get_roles", 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.rolesOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.save=function(){
		if(!$scope.fd.id){
 			$http({
				method: "post", 
				url: 'api/users/save', 
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				}, 
				params: $scope.fd
			}).
			then(function(response){
				$scope.success="Data Saved!";
				$scope.cancel();
			}, function(response){
				$scope.alert=response.data.message;
			});
		}
		
		if($scope.fd.id){
			$http({
				method: "put", 
				url: 'api/users/update', 
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				}, 
				params: $scope.fd
			}).
			then(function(response){
				$scope.success="Data Updated!";
				$scope.cancel();
			}, function(response){
				$scope.alert=response.data.message;
			});
		}
	}
		
	$scope.cancel=function(){
		$location.path("admin/users");
	}
	
    $scope.dtOptions = DTOptionsBuilder.newOptions();
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3).notSortable()
    ];
    $scope.dtInstance = {};

	$scope.listData = [];
	$scope.loadData = function() {
		$http({
			method: "get", 
			url: "api/users/get_users", 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.listData = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.edit=function(id){
		$location.path("admin/users/edit/"+id);
	}

	$scope.getEdit=function() {
		if(!$routeParams.id){
			return false;
		}
		$http({
			method: "get", 
			url: "api/users/"+$routeParams.id+"/get_user", 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.fd = response.data.data;
			$scope.fd.role = $scope.fd.roles;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.delete = function(id) {
		$http({
			method: "delete", 
			url: 'api/users/'+id+'/delete', 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.success="Data Deleted!";
			$scope.loadData();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.importing=function(){
		var fdi = new FormData();
		fdi.append('upload', $scope.upload);
		$http.post('api/users/import',fdi, {
			transformRequest: angular.identity,
			headers: {
				"X-Auth-Token":$scope.token,
				'Content-Type': undefined
			}
		})
		.success(function(response){
			$scope.success="Data Imported!"
		})
		.error(function(response){
			$scope.alert=response.data.message;
		});
	}
});