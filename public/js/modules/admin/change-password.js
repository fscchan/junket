main.registerCtrl("changePasswordController",function($scope,$http){
	$scope.alert="";
	$scope.success="";
	$scope.fd=[];
	$scope.fd.id=$scope.user.id;
	$scope.fd.username=$scope.user.username;
	$scope.resetPassword=function(){
		$http({
			method: "PUT",
			url: "api/users/reset_password",
			headers:{
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: $scope.fd
		}).
			then(function(response){
				$scope.success=response.data.message;
				$scope.alert="";
			}, function(response){
				$scope.alert=response.data.message;
				$scope.success="";
		});
	}

	$scope.changePassword=function(){
		$http({
			method: "PUT",
			url: "api/users/change_password/"+$scope.fd.id,
			headers:{
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: $scope.fd
		}).
			then(function(response){
				$scope.success=response.data.message;
				$scope.alert="";
			}, function(response){
				$scope.alert=response.data.message;
				$scope.success="";
		});
		return false;
	}

	$scope.showAlert=function(){
		return $scope.alert!='' && $scope.alert!='success';
	}

	$scope.showSuccess=function(){
		return $scope.success!='';
	}
});
