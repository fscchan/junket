main.registerCtrl("dashboardController",function($scope,$filter,$location,$http,DTOptionsBuilder, DTColumnDefBuilder){
    $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('bFilter', false).withOption("bLengthChange", false);
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0).notSortable(),
        DTColumnDefBuilder.newColumnDef(1).notSortable(),
        DTColumnDefBuilder.newColumnDef(2).notSortable()
    ];
    $scope.dtColumnDefs2 = [
        DTColumnDefBuilder.newColumnDef(0).notSortable(),
        DTColumnDefBuilder.newColumnDef(1).notSortable(),
        DTColumnDefBuilder.newColumnDef(2).notSortable(),
        DTColumnDefBuilder.newColumnDef(3).notSortable()
    ];
    $scope.dtInstance = {};

	$scope.balance=[];
	$scope.balance.chip=0;
	$scope.balance.rolling=0;
	$http({
		method: "get",
		url: "report/dashboard_balance?token="+$scope.token
	}).
	then(function(response){
		$scope.balance=response.data.data;
	},function(response){
        $scope.alert=response.message;
	});

    $scope.dateOptions = {
  		changeYear: true,
  		changeMonth: true,
  		dateFormat: "dd/M/yy",
  		yearRange: '1900:-0',
    };

    // date pick
	$scope.startDateDaily=[];
	$scope.endDateDaily=[];
	$scope.startDateWeekly=[];
	$scope.endDateWeekly=[];
	$scope.startDateMonthly=[];
	$scope.endDateMonthly=[];
	
    $scope.startDateDaily.date = 1;
    $scope.endDateDaily.date = new Date().getDate();
    $scope.startDateDaily.month = ("0" + (new Date().getMonth() + 1)).slice(-2);
    $scope.endDateDaily.month = $scope.startDateDaily.month;
    $scope.startDateDaily.year = new Date().getFullYear();
    $scope.endDateDaily.year = $scope.startDateDaily.year;

    $scope.startDateWeekly.date = 1;
    $scope.endDateWeekly.date = new Date().getDate();
    $scope.startDateWeekly.month = ("0" + (new Date().getMonth() + 1)).slice(-2);
    $scope.endDateWeekly.month = $scope.startDateDaily.month;
    $scope.startDateWeekly.year = new Date().getFullYear();
    $scope.endDateWeekly.year = $scope.startDateDaily.year;
	
    $scope.startDateMonthly.date = 1;
    $scope.endDateMonthly.date = 31;
    $scope.startDateMonthly.month = ("0" + (new Date().getMonth() + 1)).slice(-2);
    $scope.endDateMonthly.month = $scope.startDateDaily.month;
    $scope.startDateMonthly.year = new Date().getFullYear();
    $scope.endDateMonthly.year = $scope.startDateDaily.year;

    $scope.report = [];
    $scope.typeDateDaily = 'daily';
    $scope.typeDateWeekly = 'weekly';
    $scope.typeDateMonthly = 'monthly';

	$scope.dailyReport=[];
	$scope.dailyCustomerReport=[];
	$scope.weeklyReport=[];
	$scope.monthlyReport=[];
    $scope.getReport=function(typeDate, dateOne, dateTwo){
      if(typeDate == 'daily') {

        $http({method: "get", url: "report/dashboard_daily?token="+$scope.token+"&startdate="+dateOne+"&enddate="+dateTwo}).
        then(function(response){
          $scope.dailyReport = response.data.data;
          $scope.dailyCustomerReport = response.data.customer;
		  console.log($scope.dailyReport);
        }, function(response){
          $scope.alert=response.message;
        });

      } else if (typeDate == 'weekly') {
        $http({method: "get", url: "report/dashboard_weekly?token="+$scope.token+"&startdate="+dateOne+"&enddate="+dateTwo}).
        then(function(response){
          $scope.weeklyReport = response.data.data;
        }, function(response){
          $scope.alert=response.message;
        });
      } else if (typeDate == 'monthly') {
        $http({method: "get", url: "report/dashboard_monthly?token="+$scope.token+"&startdate="+dateOne+"&enddate="+dateTwo}).
        then(function(response){
          $scope.monthlyReport = response.data.data;
        }, function(response){
          $scope.alert=response.message;
        });
      }
  	}

    $scope.formDaily = function() {
		
      $scope.startDateDaily2 = $filter('date')($scope.startDateDaily.year+'-'+$scope.startDateDaily.month+'-'+$scope.startDateDaily.date, 'yyyy-MM-dd');
      $scope.endDateDaily2 = $filter('date')($scope.endDateDaily.year+'-'+$scope.endDateDaily.month+'-'+$scope.endDateDaily.date, 'yyyy-MM-dd');

      $scope.getReport($scope.typeDateDaily, $scope.startDateDaily2, $scope.endDateDaily2);
    }
    $scope.formWeekly = function() {

      $scope.startDateWeekly2 = $filter('date')($scope.startDateWeekly.year+'-'+$scope.startDateWeekly.month+'-'+$scope.startDateWeekly.date, 'yyyy-MM-dd');
      $scope.endDateWeekly2 = $filter('date')($scope.endDateWeekly.year+'-'+$scope.endDateWeekly.month+'-'+$scope.endDateWeekly.date, 'yyyy-MM-dd');

      $scope.getReport($scope.typeDateWeekly, $scope.startDateWeekly2, $scope.endDateWeekly2);
    }
    $scope.formMonthly = function() {

      $scope.startDateMonthly2 = $filter('date')($scope.startDateMonthly.year+'-'+$scope.startDateMonthly.month+'-'+$scope.startDateMonthly.date, 'yyyy-MM-dd');
      $scope.endDateMonthly2 = $filter('date')($scope.endDateMonthly.year+'-'+$scope.endDateMonthly.month+'-'+$scope.endDateMonthly.date, 'yyyy-MM-dd');

      $scope.getReport($scope.typeDateMonthly, $scope.startDateMonthly2, $scope.endDateMonthly2);
    }

});
