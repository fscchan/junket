main.registerCtrl("rolling2Controller", function($scope,$http,$routeParams) {
	id=$routeParams.id;
	$scope.master=[];
	$scope.open=[];
	$scope.rolling=0.00;
	$scope.insurance=0.00;
	$scope.status='win';
	$scope.room="";
	$scope.amount=0.00;
	$scope.odds=0.00;
	$scope.points=0.00;
	$scope.confirmModal=false;
	$scope.success="";
	$scope.nama_agent="";
	$scope.alert="";
	$scope.editing=false;
	$scope.status_customer = "";
	$scope.showgl = true;
	$scope.showagent = false;
	$scope.showpp = false;
	$scope.valuegl = 0;
	$scope.valueag = 0;
	$scope.valuepp = 0;
	$scope.persent = 0;
	$scope.shownego = false;
	$scope.valuenego = 0;

	$scope.getRolling=function(){
		$http({
			method: "get",
			url: "api/transaction/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.master = response.data.data;
			$scope.status = $scope.master.scenario;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.edit=function(){
		$scope.editing=true;
	}
	
	$scope.cancelEdit=function(){
		$scope.editing=false;
	}
	
	$scope.update=function(){
		$scope.loading=true;
		$http({
			method: "put",
			url: "api/transaction/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: $scope.master
		}).
		then(function(response){
			$scope.getRolling();
			$scope.loading=false;
			$scope.editing=false;
		}, function(response){
			$scope.alert=response.data.message;
			$scope.loading=false;
			$scope.editing=false;
		});
	}

	$scope.detail=[];
	$scope.getRollingDetail=function(){
		$http({
			method: "get",
			url: "api/rollings/by_transaction/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.detail = response.data.data;
			initTotal=0;
			for(i=0;i<$scope.detail.length;i++){
				initTotal+=parseFloat($scope.detail[i].amount);
				$scope.detail[i].total=initTotal;
			}
			if($scope.detail.length > 0){
				$scope.status = $scope.detail[0].status;
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	$scope.setinsurance = function(val){
		$scope.maxinsurance = $scope.rolling / $scope.odds;
		if($scope.insurance >= $scope.maxinsurance && $scope.insurance>0){
			$scope.insurance = $scope.maxinsurance;
		}else if($scope.insurance <= -$scope.maxinsurance && $scope.insurance<0){
			$scope.insurance = -$scope.maxinsurance;
		}
		
		if(val.keyCode == 13){
			$scope.addRolling();
		}
	}

	$scope.setinsuranceedit = function(ev){
		$scope.maxinsurance = $scope.selected.rolling / $scope.selected.odds;
		if($scope.selected.insurance >= $scope.maxinsurance && $scope.selected.insurance>0){
			$scope.selected.insurance = $scope.maxinsurance;
		}else if($scope.selected.insurance <= -$scope.maxinsurance && $scope.selected.insurance<0){
			$scope.selected.insurance = -$scope.maxinsurance;
		}
		
		if(ev.keyCode == 13){
			$scope.editRolling($scope.selected.id);
		}
	}

	$scope.addRolling=function(){
		$scope.loading=true;
		console.log($scope.room);
		$http({
			method: "post",
			url: "api/rollings/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			data: {
				rolling: $scope.rolling,
				insurance: $scope.insurance,
				status: $scope.status,
				scenario: parseFloat($scope.insurance)>0?1:0,
				amount: $scope.amount,
				room: $scope.room,
				total: 0,
				odds: $scope.odds,
				points: $scope.points
			}
		}).
		then(function(response){
			$scope.data=response.data.data;
			$scope.rolling = 0;
			$scope.insurance = 0;
			$scope.status = 'win';
			$scope.amount = 0;
			$scope.odds = 0;
			$scope.room = "";
			$scope.points=0;
			// $("#rolling").focus();
			$scope.getRollingDetail();
			$scope.loading=false;
		},function(response){
			$scope.alert=response.data.message;
			$scope.loading=false;
		});
	}

	$scope.updaterolling = function(detail_id){
		$scope.loading=true;
		$http({
			method: "put",
			url: "api/rollings/"+detail_id+"/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				rolling: $scope.viewselected.rolling,
				insurance: $scope.viewselected.insurance,
				status: $scope.viewselected.status,
				amount: $scope.viewselected.amount,
				room: $scope.viewselected.room,
				total: 0,
				odds: $scope.viewselected.odds,
				points: $scope.viewselected.points,
				scenario: parseFloat($scope.viewselected.insurance)>0?1:0,
				// scenario: $scope.viewselected.scenario,
				agent_shared: $scope.viewselected.agent_shared
			}
		}).
		then(function(response){
			$scope.data=response.data.data;
			$scope.getRollingDetail();
			$scope.loading=false;
		}, function(response){
			$scope.alert=response.data.message;
			$scope.loading=false;
		});
	}

	$scope.selected={};
	$scope.editForm=function(row){
		$scope.selected = angular.copy(row);
		console.log($scope.selected.room);
		setTimeout(function(){$("#selected_amount").focus()},1);
	}
	$scope.viewselected={};
	$scope.viewForm=function(row){
		$scope.viewselected = angular.copy(row);
		$scope.viewselected.negoagent = 0;
		$scope.valuegl = 0;
		$scope.valueag = 0;

		if($scope.master.customer.agent == null){
			$scope.shownego = false;
			$scope.showagent = false;
			$scope.showpp = false;
			if($scope.viewselected.room == "Room 38"){
				$scope.valuegl = $scope.viewselected.insurance;
			} else if($scope.viewselected.room == "Maxims"){
				$scope.showpp = true;
				$scope.valuegl = $scope.viewselected.insurance * 0.7;
				console.log($scope.valuegl);
				$scope.valuepp = $scope.viewselected.insurance * 0.3;
			} else if($scope.viewselected.room == "Other Room"){
				$scope.showpp = true;
				$scope.valuegl = $scope.viewselected.insurance * 0.8;
				$scope.valuepp = $scope.viewselected.insurance * 0.2;
			}
		}else if($scope.master.customer.agent.name != ""){
			console.log($scope.viewselected.status);
			$scope.showagent = true;
			$scope.showpp = false;
			if($scope.viewselected.status == "Win or Lose"){
				$scope.shownego = true;
				if($scope.viewselected.room == "Room 38"){
					$scope.persent = 100;
				} else if($scope.viewselected.room == "Maxims"){
					$scope.showpp = true;
					$scope.persent = 70;
					$scope.valuepp = $scope.viewselected.insurance * 0.3;
				} else if($scope.viewselected.room == "Other Room"){
					$scope.showpp = true;
					$scope.persent = 80;
					$scope.valuepp = $scope.viewselected.insurance * 0.2;
				}
				$scope.negoagent($scope.persent);
			} else if($scope.viewselected.status == "Win Only"){
				$scope.shownego = false;
				if($scope.viewselected.room == "Room 38"){
					$scope.valuegl = $scope.viewselected.insurance * (90 / 100);
					$scope.valueag = $scope.viewselected.insurance * (10 / 100);
				} else if($scope.viewselected.room == "Maxims"){
					$scope.showpp = true;
					$scope.hasil = $scope.viewselected.insurance * 0.7;
					$scope.valuegl = $scope.hasil * 0.9;
					$scope.valueag = $scope.hasil * 0.1;
					$scope.valuepp = $scope.viewselected.insurance * 0.3;
				} else if($scope.viewselected.room == "Other Room"){
					$scope.showpp = true;
					$scope.hasil = $scope.viewselected.insurance * 0.8;
					$scope.valuegl = $scope.hasil * 0.9;
					$scope.valueag = $scope.hasil * 0.1;
					$scope.valuepp = $scope.viewselected.insurance * 0.2;
				}
			}
		}
	}

	$scope.negoagent = function(persent){
		console.log(persent);
		$scope.hasil = $scope.viewselected.insurance * (persent / 100);
		$scope.persengl = 100 - $scope.viewselected.agent_shared;
		console.log($scope.hasil);
		$scope.valuegl = $scope.hasil * ($scope.persengl / 100);
		$scope.valueag = $scope.hasil * ($scope.viewselected.agent_shared / 100);
	}

	$scope.editRolling=function(detail_id){
		$scope.loading=true;
		$http({
			method: "put",
			url: "api/rollings/"+detail_id+"/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			data: {
				rolling: $scope.selected.rolling,
				insurance: $scope.selected.insurance,
				status: $scope.selected.status,
				amount: $scope.selected.amount,
				room: $scope.selected.room,
				total: 0,
				odds: $scope.selected.odds,
				points: $scope.selected.points
			}
		}).
		then(function(response){
			$scope.data=response.data.data;
			$scope.getRollingDetail();
			$scope.selected={};
			$scope.loading=false;
		},function(response){
			$scope.alert=response.data.message;
			$scope.loading=false;
		});
	}

	$scope.deleteRolling=function(detail_id){
		$http({
			method: "delete",
			url: 'api/rollings/'+detail_id+'/'+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.data = response.data.data;
			$scope.getRollingDetail();
			$scope.selected={};
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.getTemplate = function (row) {
		if (row.id === $scope.selected.id){
		  return 'edit';
		}else{
			return 'display';
		}
	}
});