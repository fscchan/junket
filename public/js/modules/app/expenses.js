main.registerCtrl("expensesController",function($filter,$scope,$location,$http,$routeParams){
	$scope.success="";
	$scope.alert = "";
	$scope.fd=[];

	$scope.add=function(){
		$location.path("app/expenses/create");
	}

	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd/M/yy",
		yearRange: '1900:-0',
    };

    $scope.initData = function() {
		$scope.fd.date = new Date();
		$scope.fd.amount = 0;
	};
	
	$scope.managerOption=[];
	$scope.getManager=function(){
		$http({
			method: "get",
			url: "api/users/4/find_by_role",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.managerOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.save = function(file) {
 		$scope.fd.user_id = $scope.user.id;
		console.log($scope.fd);
		var fd = new FormData();
		fd.append('image', file);
		$scope.fd.date = $filter('date')($scope.fd.date, 'yyyy-MM-dd')
		fd.append('date', $scope.fd.date);
		fd.append('amount', $scope.fd.amount);
		fd.append('annotation', $scope.fd.annotation);
		fd.append('user_id', $scope.fd.user_id);
		if($scope.fd.bank_id){
			fd.append('bank_id', $scope.fd.bank_id);
		}
		fd.append('manager_id', $scope.fd.manager_id);
		fd.append('shift',$scope.fd.shift);
 		if (!$scope.fd.id) {
			$http.post('api/expenses', fd, {
				transformRequest: angular.identity,
				headers: {
					"X-Auth-Token":$scope.token,
					'Content-Type': undefined
				}
			})

			.success(function(response){
				$scope.cancel();
			})

			.error(function(response){
				$scope.alert=response.data.message;
			});
			/*$http({method: "post", url: 'api/expenses', headers: {
					"Content-Type":undefined,
					"X-Auth-Token":$scope.token
				}, data: file
			}).
			then(function(response){
				$scope.dataTable = response.data.data;
				$scope.get_expenses();
			}, function(response){
				$scope.alert=response.data.message;
			});*/
 		}

		if($scope.fd.id){
 			fd.append('id', $scope.fd.id);
            $http.post('api/expenses/'+$scope.fd.id, fd, {
            	transformRequest: angular.identity,
                headers: {
                	"X-Auth-Token":$scope.token,
                	'Content-Type': undefined
                }
            })
			.success(function(response){
            	$scope.cancel();
            })

            .error(function(response){
            	$scope.alert=response.data.message;
            });
 		}
    }

	$scope.cancel = function() {
		$location.path("app/expenses");
	}

    $scope.sortType     = 'date'; // set the default sort type
	$scope.sortReverse  = true;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;

	$scope.dataTable = [];
	$scope.loadTable = function() {
		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}

		if ($scope.date) {
			$scope.date = $filter('date')($scope.date, 'yyyy-MM-dd');
		} else {
			$scope.date = '';
		}

		$http({method: "get", url: "api/expenses?sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&page="+$scope.page+"&date="+$scope.date, headers: {
				    "Content-Type":"application/json",
				    "X-Auth-Token":$scope.token
				   }
		}).
		then(function(response){
			$scope.dataTable = response.data;
			$scope.alert="";
			$scope.showModal = false;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.sort = function(sortType) {
		$scope.sortType = sortType;
		$scope.sortReverse = !$scope.sortReverse;
		$scope.loadTable();
	}

	$scope.showPage=function(en){
		return (en>=$scope.dataTable.current_page && en<=$scope.dataTable.current_page+2) || (en<=$scope.dataTable.current_page && (en>=$scope.dataTable.current_page-2 || en>=$scope.dataTable.last_page-2));
	}

	$scope.isLastPage=function(){
		return $scope.dataTable.current_page==$scope.dataTable.last_page;
	}

	$scope.paging=function(page){
		if ($scope.dataTable.last_page) {
			$scope.page=page;
			$scope.loadTable();
		}
	}

    $scope.show = function(id) {
		$location.path("app/expenses/show/"+id);
    }
	
	$scope.showed = function(){
		return $scope.fd.transaction || $routeParams.show;
	}
	
    $scope.edit = function(id) {
		$location.path("app/expenses/edit/"+id);
    }

	$scope.getById = function(id) {
		if(!id){
			id=$routeParams.id;
			if(!id){
				return false;
			}
		}

		$http({
			method: "get",
			url: "api/expenses/"+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			// response.data.data.bank_id = parseInt(response.data.data.bank_id);
			response.data.data.manager_id = parseInt(response.data.data.manager_id);
			$scope.fd = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

    $scope.viewImage = function(id) {
    	$scope.getById(id);
    }

	$scope.delete= function(id) {
		console.log(id);
		$http({
			method: "delete",
			url: 'api/expenses/'+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.success="Data deleted!";
			$scope.loadTable();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.bankOption=[];
		$scope.getPaymentMethod = function() {
		$http({
			method: "get",
			url: "api/banks	",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.bankOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
});
