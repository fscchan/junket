main.registerCtrl("paymentDebitController",function($filter,$scope,$location,$http,$routeParams,$httpParamSerializer){
	$scope.success="";
	$scope.alert="";
	$scope.fd=[];
	$scope.fd.debt_source=0;
	$scope.loading=false;
	$scope.debtSourceOptions = [
		{id: 1, label: "Customer"},
		{id: 2, label: "Agent"},
		{id: 3, label: "Vendor"},
		{id: 4, label: "Company"}
	];

	$scope.add=function(){
		$location.path("app/payment_debit/create");
	}

	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd/M/yy",
		yearRange: '1900:-0',
    };

	$scope.is_bank=0;
	
	$scope.initData=function(){
		$scope.fd.date=new Date();
		$scope.fd.user_id=$scope.user.id;
		$scope.fd.user=[];
		$scope.fd.user.name=$scope.user.name;
		$scope.fd.amount=0;
		$scope.fd.amount_due=0;
		$scope.getNumber();
	}
	
	$scope.getNumber=function(){
		if($routeParams.id){
			return false;
		}
		
		$http({
			method: "get",
			url: "api/payment_debts/number",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.fd.number = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.setMethod=function(){
		method=$scope.paymentMethodOption;
		loop1:for(inde=0;inde<method.length;inde++){
			if(method[inde].id==$scope.fd.payment_method_id){
				$scope.is_bank=method[inde].is_bank;
				switch(parseInt($scope.is_bank)){
					case 0:
						$scope.fd.bank_account_id=null;
						$scope.fd.payment_mode=$scope.fd.payment_mode?$scope.fd.payment_mode:'Cash';
						$scope.fd.staff_id=$scope.fd.staff_id?$scope.fd.staff_id:$scope.user.id;
						$http({
							method: "get",
							url: "api/account/not_bank",
							headers: {
								"Content-Type":"application/json",
								"X-Auth-Token":$scope.token
							}
						}).then(function(response){
							$scope.bankAccOption = response.data.data;
						}, function(response){
							$scope.alert=response.data.message;
						});
					break;
					case 1:
						$scope.fd.bank_account_id=$scope.fd.bank_account_id?$scope.fd.bank_account_id:($scope.bankAccOption[0]?$scope.bankAccOption[0].id:null);
						$scope.fd.staff_id=null;
						$scope.fd.payment_mode='';
						$http({
							method: "get",
							url: "api/account/bank",
							headers: {
								"Content-Type":"application/json",
								"X-Auth-Token":$scope.token
							}
						}).then(function(response){
							$scope.bankAccOption = response.data.data;
						}, function(response){
							$scope.alert=response.data.message;
						});
					break;
				}
				break loop1;
			}
		}
	}
	
	$scope.customerOption=[];
    $scope.getCustomer = function() {
		$http({
			method: "get",
			url: "api/customer/get_lists",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.customerOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.paymentMethodOption=[];
    $scope.getPaymentMethod = function() {
		$http({
			method: "get",
			url: "api/pay_methods",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.paymentMethodOption = response.data.data;
			if(!$scope.fd.payment_method_id){
				$scope.fd.payment_method_id=$scope.paymentMethodOption[0].id;
				$scope.setMethod();
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
		
	$scope.bankAccOption=[];
	$scope.getBankAcc = function() {
		$http({
			method: "get",
			url: "api/account/bank",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.bankAccOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.staffOption=[];
    $scope.getStaff = function() {
		$http({
			method: "get",
			url: "api/users/get_users",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.staffOption = response.data.data;
			$scope.setMethod();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.bankOption=[];
    $scope.getBank = function() {
		$http({
			method: "get",
			url: "api/banks",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.bankOption = response.data.data;
			$scope.setMethod();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.changeSource = function() {
		$scope.dataDetail=[];
		$http({
			method: "get",
			url: "api/payment_debts/debt_source",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {source: $scope.fd.debt_source}
		}).then(function(response){
			$scope.sourceOptions = response.data.data;
			$scope.getDebts();
		}, function(response){
			$scope.alert=response.data.message;
		});
	};
	
	$scope.select=0;
	$scope.dataDetail=[];
    $scope.getDebts = function() {
		$scope.dataDetail=[];
		$scope.select=0;
		$http({
			method: "get",
			url: "api/payment_debts/"+$scope.fd.customer_id+"/outstanding_debts",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {source: $scope.fd.debt_source}
		}).
		then(function(response){
			$scope.dataDetail = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.selecting=function(ind){
		$scope.select=ind;
	}
	
	$scope.keyAmountHead=function(ind){
		amountHead=parseFloat(angular.copy($scope.fd.amount));
		loop1:for(inde=0;inde<$scope.dataDetail.length;inde++){
			if(amountHead>=$scope.dataDetail[inde].amount_due){
				$scope.dataDetail[inde].amount=$scope.dataDetail[inde].amount_due;
				amountHead-=$scope.dataDetail[inde].amount_due;
			}else{
				$scope.dataDetail[inde].amount=amountHead;
				break loop1;
			}
		}
	}
	
	$scope.validPayment=function(ind){
		if($scope.dataDetail[ind].amount>$scope.dataDetail[ind].amount_due){
			$scope.dataDetail[ind].amount=$scope.dataDetail[ind].amount_due;
		}
		amountDue=0;
		amountPayment=0;
		for(inde=0;inde<$scope.dataDetail.length;inde++){
			amountDue+=parseFloat($scope.dataDetail[inde].amount_due);
			amountPayment+=parseFloat($scope.dataDetail[inde].amount);
		}
		$scope.fd.amount_due=amountDue;
		$scope.fd.amount=amountPayment;
		$scope.select=0;
		
		return false;
	}

	$scope.save = function() {
		/* if($scope.dataDetail.length==0){
			alert("Please add detail!");
			return false;
		} */
		
		console.log($scope.fd);
		console.log($scope.fd.id);
		
		var fd = new FormData();
		fd.append('number', $scope.fd.number);
		fd.append('date', $filter('date')($scope.fd.date, 'yyyy-MM-dd'));
		
		debt_source_='customer_id';
		// switch($scope.fd.debt_source){
			// case 1:
				// debt_source_='customer_id';
			// break;
			// case 2:
				// debt_source_='agent_id';
			// break;
			// case 3:
				// debt_source_='vendor_id';
			// break;
			// case 4:
				// debt_source_='company_id';
			// break;
		// }
		
		fd.append(debt_source_, $scope.fd.customer_id);
		fd.append('user_id', $scope.fd.user_id);
		fd.append('remark', ($scope.fd.remark?$scope.fd.remark:''));
		if($scope.fd.image){
			fd.append('image', $scope.fd.image);
		}
		fd.append('payment_method_id', $scope.fd.payment_method_id);
		fd.append('payment_mode', $scope.fd.payment_mode);
		if($scope.fd.staff_id){
			fd.append('staff_id', $scope.fd.staff_id);
		}
		if($scope.fd.bank_id){
			fd.append('bank_id', $scope.fd.bank_id);
		}
		if($scope.fd.bank_account_id){
			fd.append('bank_account_id', $scope.fd.bank_account_id);
		}
		fd.append('amount', $scope.fd.amount);
		fd.append('debt_source', $scope.fd.debt_source);
		
		// $scope.loading=true;
 		if (!$scope.fd.id) {
			fd.append('action','create');
 			/*
			$http({
				method: "post",
				url: 'api/payment_debts',
				transformRequest: angular.identity,
				headers: {
					"X-Auth-Token":$scope.token,
					'Content-Type': undefined
				},
				params: fd
			}).
			then(function(response){
				idDet=response.data.data.id;
				$scope.saveDetail(idDet);
			}, function(response){
				$scope.alert=response.data.message;
				$scope.loading=false;
			});
			*/
			
			$http.post('api/payment_debts', fd, {
				transformRequest: angular.identity,
				headers: {
					"X-Auth-Token":$scope.token,
					'Content-Type': undefined
				}
			}).success(function(response){
				idDet=response.data.id;
				$scope.saveDetail(idDet);
				// $scope.loading=false;
				if($scope.dataDetail.length==0){
					$location.path("app/payment_debit");
				}
			}).error(function(response){
				$scope.alert=response.data.message;
				// $scope.loading=false;
				$location.path("app/payment_debit");
			});
 		}

		if($scope.fd.id){
			fd.append('id', $scope.fd.id);
			fd.append('action','update');
 			/*
			$http({
				// method: "put",
				method: "post", //upload must use post
				url: 'api/payment_debts/'+$scope.fd.id,
				transformRequest: angular.identity,
				headers: {
					"X-Auth-Token":$scope.token,
					'Content-Type': undefined
				},
				params: fd
			}).
			then(function(response){
				$http({
					method: "delete",
					url: 'api/payment_debt_details/'+$scope.fd.id+"/delete",
					headers: {
						'Content-Type': 'application/json',
						"X-Auth-Token":$scope.token
					}
				}).
				then(function(response){
					$scope.saveDetail($scope.fd.id);
				}, function(response){
					$scope.alert=response.data.message;
					$scope.loading=false;
				});
			}, function(response){
				$scope.alert=response.data.message;
				$scope.loading=false;
			});
			*/
			
			$http.post('api/payment_debts', fd, {
			// $http.post('api/payment_debts/'+$scope.fd.id, fd, {
				transformRequest: angular.identity,
				headers: {
					"X-Auth-Token":$scope.token,
					'Content-Type': undefined
				},
				/*params: {
					"_method" : "PUT"
				}*/
			}).success(function(response){
				$http({
					method: "delete",
					url: 'api/payment_debt_details/'+$scope.fd.id+"/delete",
					headers: {
						'Content-Type': 'application/json',
						"X-Auth-Token":$scope.token
					}
				}).
				then(function(response){
					$scope.saveDetail($scope.fd.id);
				}, function(response){
					$scope.alert=response.data.message;
					// $scope.loading=false;
				});
				if($scope.dataDetail.length==0){
					$location.path("app/payment_debit");
				}
			}).error(function(response){
				$scope.alert=response.data.message;
				// $scope.loading=false;
				$location.path("app/payment_debit");
			});
 		}
    };
	
	$scope.saveDetail=function(idDet){
		for(inde=0;inde<$scope.dataDetail.length;inde++){
			$http({
				method: "post",
				url: 'api/payment_debt_details/'+idDet,
				headers: {
					'Content-Type': 'application/json',
					"X-Auth-Token":$scope.token
				},
				params: $scope.dataDetail[inde]
			}).
			then(function(response){
				if(inde==$scope.dataDetail.length){
					$scope.cancel();
					$scope.loading=false;
				}
			}, function(response){
				$scope.alert=response.data.message;
				$scope.loading=false;
			});
		}
	}

	$scope.cancel = function() {
		$location.path("app/payment_debit");
	};

    $scope.sortType     = 'date'; // set the default sort type
	$scope.sortReverse  = true;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;

	$scope.dataTable = [];
	$scope.loadData = function() {
		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}

		if ($scope.date) {
			$scope.date = $filter('date')($scope.date, 'yyyy-MM-dd');
		} else {
			$scope.date = '';
		}

		$http({
			method: "get",
			url: "api/payment_debts?sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&page="+$scope.page+"&date="+$scope.date,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.dataTable = response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.sort = function(sortType) {
		$scope.sortType = sortType;
		$scope.sortReverse = !$scope.sortReverse;
		$scope.loadData();
	}

	$scope.showPage=function(en){
		return (en>=$scope.dataTable.current_page && en<=$scope.dataTable.current_page+4) || (en<=$scope.dataTable.current_page && en>=$scope.dataTable.last_page-4);
	}

	$scope.isLastPage=function(){
		return $scope.dataTable.current_page==$scope.dataTable.last_page;
	}

	$scope.paging=function(page){
		if ($scope.dataTable.last_page) {
			$scope.page=page;
			$scope.loadData();
		}
	};

	$scope.edit=function(id){
		$location.path("app/payment_debit/edit/"+id);
	}

	$scope.getEdit = function() {
		if(!$routeParams.id){
			return false;
		}
		amountDue=0;
		amountPayment=0;
		$http({
			method: "get",
			url: "api/payment_debts/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			if(response.data.data.customer_id !== null && (response.data.data.agent_id === null && response.data.data.vendor_id === null && response.data.data.company_id === null)) {
				$scope.fd.debt_source = 1;
				$scope.fd.customer_id = parseInt(response.data.data.customer_id);
			} else if(response.data.data.customer_id === null && response.data.data.agent_id !== null && response.data.data.vendor_id === null && response.data.data.company_id === null) {
				$scope.fd.debt_source = 2;
				$scope.fd.customer_id = parseInt(response.data.data.agent_id);
			} else if(response.data.data.customer_id === null && response.data.data.agent_id === null && response.data.data.vendor_id !== null && response.data.data.company_id === null) {
				$scope.fd.debt_source = 3;
				$scope.fd.customer_id = parseInt(response.data.data.vendor_id);
			} else if(response.data.data.customer_id === null && response.data.data.agent_id === null && response.data.data.vendor_id === null && response.data.data.company_id === null) {
				$scope.fd.debt_source = 4;
				$scope.fd.customer_id = parseInt(response.data.data.company_id);
			}
			$http({
				method: "get",
				url: "api/payment_debts/debt_source",
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: {source: $scope.fd.debt_source}
			}).then(function(response){
				$scope.sourceOptions = response.data.data;
			}, function(response){
				$scope.alert=response.data.message;
			});
			$scope.fd.id = response.data.data.id;
			$scope.fd.number = response.data.data.number;
			$scope.fd.date = response.data.data.date;
			$scope.fd.user.name = response.data.data.user.name;
			$scope.fd.remark = response.data.data.remark;
			if(response.data.data.is_bank) {
				for(i = 0; i < $scope.paymentMethodOption; i++) {
					if($scope.paymentMethodOption[i].is_bank == 1) {
						$scope.fd.payment_method_id = $scope.paymentMethodOption[i].id;
					}
				}
				$scope.fd.bank_account_id=$scope.fd.bank_account_id?$scope.fd.bank_account_id:($scope.bankAccOption[0]?$scope.bankAccOption[0].id:null);
				$scope.fd.staff_id=null;
				$scope.fd.payment_mode='';
				$http({
					method: "get",
					url: "api/account/bank",
					headers: {
						"Content-Type":"application/json",
						"X-Auth-Token":$scope.token
					}
				}).then(function(response){
					$scope.bankAccOption = response.data.data;
				}, function(response){
					$scope.alert=response.data.message;
				});
			} else {
				for(i = 0; i < $scope.paymentMethodOption; i++) {
					if($scope.paymentMethodOption[i].is_bank == 0) {
						$scope.fd.payment_method_id = $scope.paymentMethodOption[i].id;
					}
				}
				$scope.fd.payment_mode=$scope.fd.payment_mode?$scope.fd.payment_mode:'Cash';
				$scope.fd.staff_id=$scope.fd.staff_id?$scope.fd.staff_id:$scope.user.id;
				$http({
					method: "get",
					url: "api/account/not_bank",
					headers: {
						"Content-Type":"application/json",
						"X-Auth-Token":$scope.token
					}
				}).then(function(response){
					$scope.bankAccOption = response.data.data;
				}, function(response){
					$scope.alert=response.data.message;
				});
			}
			$scope.fd.payment_method_id = response.data.data.payment_method_id?parseInt(response.data.data.payment_method_id):null;
			$scope.fd.payment_mode = response.data.data.payment_mode;
			$scope.fd.staff_id = response.data.data.staff_id?parseInt(response.data.data.staff_id):null;
			$scope.fd.bank_account_id = response.data.data.bank_account_id?parseInt(response.data.data.bank_account_id):null;
			$scope.fd.amount_due=response.data.data.amount_due?parseFloat(response.data.data.amount_due):0;
			$scope.fd.amount=response.data.data.amount?parseFloat(response.data.data.amount):0;
			// $scope.fd.amount_due=amountDue;
			// $scope.fd.amount=amountPayment;
		}, function(response){
			$scope.alert=response.data.message;
		});
		
		$http({
			method: "get",
			url: "api/payment_debt_details/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.dataDetail = response.data.data;
			for(inde=0;inde<$scope.dataDetail.length;inde++){
				amountDue+=parseFloat($scope.dataDetail[inde].amount_due);
				amountPayment+=parseFloat($scope.dataDetail[inde].amount);
			}
			$scope.fd.amount_due=(amountDue!=0?amountDue:$scope.fd.amount_due);
			$scope.fd.amount=(amountPayment!=0?amountPayment:$scope.fd.amount);
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.delete= function(id) {
		$http({
			method: "delete",
			url: 'api/payment_debts/'+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.success="Data deleted";
			$scope.loadData();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.showAlert=function(){
		return $scope.alert!="" && $scope.alert!="success";
	}
});
