main.registerCtrl("bankGenting2Controller",function($filter,$scope,$location,$http,$routeParams){
	$scope.success="";
	$scope.alert="";
	$scope.fd=[];

	$scope.add=function(){
		$location.path("app/bank_gentings2/create");
	}

	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd/M/yy",
		yearRange: '1900:-0',
    };

	$scope.initData=function(){
		$scope.fd.date=new Date();
		$scope.fd.amount=0;
	}

	$scope.paymentMethodOption=[];
    $scope.getPaymentMethod = function() {
		$http({
			method: "get",
			url: "api/pay_methods",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.paymentMethodOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.bankOption=[];
	$scope.getBank = function() {
		$http({
			method: "get",
			url: "api/banks",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.bankOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.save = function() {
 		$scope.fd.user_id = $scope.user.id;
 		if (!$scope.fd.id) {
 			$http({
				method: "post",
				url: 'api/bank_genting2',
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.cancel();
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}

		if($scope.fd.id){
 			$http({
				method: "put",
				url: 'api/bank_genting2/'+$scope.fd.id,
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.cancel();
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}
    };

	$scope.cancel = function() {
		$location.path("app/bank_gentings2");
	};

    $scope.sortType     = 'date'; // set the default sort type
	$scope.sortReverse  = true;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;

	$scope.dataTable = [];
	$scope.loadData = function() {
		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}

		if ($scope.date) {
			$scope.date = $filter('date')($scope.date, 'yyyy-MM-dd');
		} else {
			$scope.date = '';
		}

		$http({
			method: "get",
			url: "api/bank_genting2?sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&page="+$scope.page+"&search="+$scope.date,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.dataTable = response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.sort = function(sortType) {
		$scope.sortType = sortType;
		$scope.sortReverse = !$scope.sortReverse;
		$scope.loadData();
	}

	$scope.showPage=function(en){
		return (en>=$scope.dataTable.current_page && en<=$scope.dataTable.current_page+4) || (en<=$scope.dataTable.current_page && en>=$scope.dataTable.last_page-4);
	}

	$scope.isLastPage=function(){
		return $scope.dataTable.current_page==$scope.dataTable.last_page;
	}

	$scope.paging=function(page){
		if ($scope.dataTable.last_page) {
			$scope.page=page;
			$scope.loadData();
		}
	};

	$scope.edit=function(id){
		$location.path("app/bank_gentings2/edit/"+id);
	}

	$scope.getEdit = function() {
		if(!$routeParams.id){
			return false;
		}
		$http({
			method: "get",
			url: "api/bank_genting2/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			response.data.data.payment_method_id = parseInt(response.data.data.payment_method_id);
			response.data.data.bank_id = parseInt(response.data.data.bank_id);
			$scope.fd = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.delete= function(id) {

		$http({
			method: "delete",
			url: 'api/bank_genting2/'+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.success="Data deleted"
			$scope.loadData();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.showAlert=function(){
		return $scope.alert!="" && $scope.alert!="success";
	}
	
	$scope.showSuccess=function(){
		return $scope.success!="";
	}
});
