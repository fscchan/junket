main.registerCtrl("shiftTransferController",function($scope,$location,$http,DTOptionsBuilder, DTColumnDefBuilder,$routeParams,$filter){
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd/M/yy",
		yearRange: '1900:-0',
    };
    $scope.fd.date = new Date();
    $scope.fd.from_user_id = $scope.user.id;
	$scope.fd.nn_chips=0;
	$scope.fd.cash_chips=0;
	$scope.fd.cash_real=0;
	$scope.fd.shift_from = "Morning";
	$scope.fd.shift_to = "Morning";
    var today = new Date().toISOString().substr(0, 10).replace('T', ' ');

    $scope.shift_transfer = 0;
	$scope.alert = "";
	$scope.success = "";
	$scope.update_status = 0;

	$scope.entity='Manager';
	$scope.roleId=4;
	$scope.menu_name='shift_transfer';

	if($routeParams.entity=='runner'){
		$scope.entity='Runner';
		$scope.roleId=6;
		$scope.menu_name='shift_transfer_runner';
	}
	
	$scope.getEdit = function(id) {
		// if($scope.user.roles=='Administrator'){return false;}
		$http({
			method: "get",
			// url: "api/shift_history/all?menu_name="+$scope.menu_name+"&date="+today+"&sort_by=from_user&sort_type=desc",
			url: "api/shift_history/"+id+"/get",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			if (response.data.data) {
				$scope.fd = response.data.data;
				$scope.shift_transfer = 1;
				$scope.update_status = 0;
			} else {
				$scope.shift_transfer = 0;
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.get_manager = function() {
		$http({
			method: "get",
			url: "api/users/"+$scope.roleId+"/find_by_role",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.managerOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.shift_from = function() {
		/*if ($scope.fd.shift_from == "Morning") {
			$scope.fd.shift_to = "Night";
		} else if ($scope.fd.shift_from == "Night") {
			$scope.fd.shift_to = "Morning";
		}*/
	}

	$scope.shift_to = function() {
		/*if ($scope.fd.shift_to == "Morning") {
			$scope.fd.shift_from = "Night";
		} else if ($scope.fd.shift_to == "Night") {
			$scope.fd.shift_from = "Morning";
		}*/
	}

	$scope.save = function() {
		console.log($scope.fd);
 		$scope.alert ="";
 		$scope.form_data = $scope.fd;
		$scope.fd.menu_name = $scope.menu_name;
 		if ($scope.fd.id > 0) {
 			$http({
				method: "put",
				url: 'api/shift_history/'+$scope.fd.id+'/update',
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.success="Data updated!";
				$scope.shift_transfer = 0;
				$scope.reset();
				$scope.loadData();
			}, function(response){
				$scope.alert=response.data.message;
			});
 		} else {
 			$http({
				method: "post",
				url: 'api/shift_history/save',
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.success="Data saved!";
				$scope.shift_transfer = 0;
				$scope.reset();
				$scope.loadData();
			}, function(response){
				$scope.alert=response.data.message;
			});
		}
    };

    $scope.update_form = function() {
    	$scope.shift_transfer = 2;
    }

    $scope.reset=function(){
    	if ($scope.shift_transfer == 0 || $scope.shift_transfer==1) {
			$scope.shift_transfer=0;
    		$scope.fd = {};
    		$scope.fd.shift_from = "Morning";
    		$scope.fd.shift_to = "Morning";
	    	$scope.fd.date = new Date();
	    	$scope.fd.from_user_id = $scope.user.id;
			$scope.fd.nn_chips=0;
			$scope.fd.cash_chips=0;
			$scope.fd.cash_real=0;
    	} else {
    		$scope.shift_transfer = 1;
    	}
    }

	$scope.showAlert=function(){
		return $scope.alert!="" && $scope.alert!="success";
	}
	
	$scope.showSuccess=function(){
		return $scope.success!="";
	}
	
    $scope.sortType     = 'date'; // set the default sort type
	$scope.sortReverse  = true;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;
	
	$scope.listData = [];
	$scope.loadData = function() {
		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}

		$scope.date = $filter('date')(new Date(), 'yyyy-MM-dd');

		$http({
			method: "get", 
			url: "api/shift_history/all?menu_name="+$scope.menu_name+"&sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&page="+$scope.page, 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.listData = response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.sort = function(sortType) {
		$scope.sortType = sortType;
		$scope.sortReverse = !$scope.sortReverse;
		$scope.loadData();
	}

	$scope.showPage=function(en){
		return (en>=$scope.listData.current_page && en<=$scope.listData.current_page+2) || (en<=$scope.listData.current_page && (en>=$scope.listData.current_page-2 || en>=$scope.listData.last_page-2));
	}

	$scope.isLastPage=function(){
		return $scope.listData.current_page==$scope.listData.last_page;
	}

	$scope.paging=function(page){
		$scope.page=page;
		$scope.loadData();
	}
	
	$scope.delete = function(id) {
		$http({
			method: "delete", 
			url: 'api/shift_history/'+id+'/delete?menu_name='+$scope.menu_name, 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.success="Data deleted!";
			$scope.loadData();
			if($scope.fd.id==id){
				$scope.reset();
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
});
