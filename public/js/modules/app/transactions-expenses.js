main.registerCtrl("transactionExpenseController", function($scope,$http,$routeParams,$filter,$location) {
	$scope.loading=false;
	$scope.master={};
	$scope.success="";
	$scope.alert="";
	
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd-mm-yy",
		yearRange: '1900:-0',
    };
	
	$scope.date=$filter('date')(new Date(),'yyyy-MM-dd')
	
	$scope.getTransaction=function(){
		$http({
			method: "get",
			url: "api/transaction/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.shift = response.data.data.shift.toLowerCase();
			$scope.master = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.cancel=function(){
		$location.path("app/transactions");
	}

	$scope.showExpense=function(){
		return $scope.master.status==1;
	}
	
	$scope.detail=[];
	$scope.getExpense=function(){
		$http({
			method: "get",
			// url: "api/transaction_expense?id="+$routeParams.id,
			url: "api/expenses_transaction?id="+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.detail = response.data.data;
			initTotal=0;
			for(i=0;i<$scope.detail.length;i++){
				initTotal+=parseFloat($scope.detail[i].amount);
				$scope.detail[i].total=initTotal;
				$scope.detail[i].description=$scope.detail[i].annotation;
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.addExpense=function(){
		$scope.loading=true;
		$http({
			method: "post",
			// url: "api/transaction_expense",
			url: "api/expenses",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			data: {
				transaction_id: $routeParams.id,
				date: $scope.date,
				shift: $scope.shift,
				annotation: $scope.description,
				amount: $scope.amount
			}
		}).
		then(function(response){
			$scope.data=response.data.data;
			$scope.rolling=0;
			// $("#rolling").focus();
			$scope.getExpense();
			$scope.success="Data saved!";
			$scope.loading=false;
		},function(response){
			$scope.alert=response.data.message;
			$scope.loading=false;
		});
	}

	$scope.selected={};
	$scope.editForm=function(row){
		$scope.selected = angular.copy(row);
		setTimeout(function(){$("#selected_amount").focus()},1);
	}

	$scope.getTemplate = function (row) {
		if (row.id === $scope.selected.id){
		  return 'edit';
		}else{
			return 'display';
		}
	}

	$scope.editExpense=function(detail_id){
		$scope.loading=true;
		$http({
			method: "post",
			// url: "api/transaction_expense/"+detail_id,
			url: "api/expenses/"+detail_id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			data: {
				transaction_id: $routeParams.id,
				date: $scope.selected.date,
				shift: $scope.selected.shift,
				annotation: $scope.selected.description,
				amount: $scope.selected.amount
			}
		}).
		then(function(response){
			$scope.data=response.data.data;
			$scope.getExpense();
			$scope.selected={};
			$scope.success="Data updated!";
			$scope.loading=false;
		},function(response){
			$scope.alert=response.data.message;
			$scope.loading=false;
		});
	}

	$scope.deleteExpense=function(detail_id){
		$http({
			method: "delete",
			// url: 'api/transaction_expense/'+detail_id,
			url: 'api/expenses/'+detail_id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.data = response.data.data;
			$scope.getExpense();
			$scope.selected={};
			$scope.success="Data deleted!";
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
});