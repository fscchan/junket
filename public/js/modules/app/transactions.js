main.registerCtrl("tripController",function($scope,$location,$http,$filter,$log,$routeParams,DTOptionsBuilder, DTColumnDefBuilder){
	$scope.alert="";
	$scope.success="";
	$scope.fd=[];
	$scope.fda=[];
	$scope.loading=false;
	
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd-mm-yy",
		yearRange: '1900:-0',
    };
	
	$scope.assignDate=new Date();
	
	$scope.userOption=[];
	$scope.getUser=function(){
		$http({
			method: "get",
			url: "api/users/get_users",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.userOption=response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.assignFromData=[];
	$scope.params2=[];
	$scope.loadAssignFrom=function(){
		$scope.params2.assign_from=$scope.user.id;
		$scope.params2.date=$filter('date')($scope.assignDate,"yyyy-MM-dd");
		$scope.params2.status=1;
		
		$http({
			method: "get",
			url: "api/transactions",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: $scope.params2
		}).
		then(function(response){
			$scope.assignFromData = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.assignID=[];
	$scope.assigning=function(){
		if(!$scope.assign_to){
			$scope.alert="Please select Assign to!";
			return false;
		}
		inde=0;
		for(key=0;key<$scope.assignID.length;key++){
			if($scope.assignID[key]!=0){
				dataAssign={
					assign_from: $scope.user.id,
					assign_to: $scope.assign_to,
				};
				
				$http({
					method: "put",
					url: "api/transaction_assign/"+$scope.assignID[key],
					headers: {
						"Content-Type":"application/json",
						"X-Auth-Token":$scope.token
					},
					params: dataAssign
				}).
				then(function(response){
					$scope.success="Assigning is success!";
					inde++;
					if($scope.assignID.length==inde){
						$scope.loadAssignFrom();
						$scope.loadAssignTo();
						$scope.loadTable();
					}
				}, function(response){
					$scope.alert=response.data.message;
					inde++;
					if($scope.assignID.length==inde){
						$scope.loadAssignFrom();
						$scope.loadAssignTo();
						$scope.loadTable();
					}
				});
			}else{
				inde++;
				if($scope.assignID.length==inde){
					$scope.loadAssignFrom();
					$scope.loadAssignTo();
					$scope.loadTable();
				}
			}
		}
	}
	
	$scope.assignToData=[];
	$scope.params3=[];
	$scope.loadAssignTo=function(){
		$scope.params3.assign_to=$scope.assign_to;
		$scope.params3.date=$filter('date')($scope.assignDate,"yyyy-MM-dd");
		$scope.params3.status=1;
		
		$http({
			method: "get",
			url: "api/transactions",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: $scope.params3
		}).
		then(function(response){
			$scope.assignToData = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.assignID2=[];
	$scope.cancelAssigning=function(){
		inde=0;
		for(key=0;key<$scope.assignID2.length;key++){
			if($scope.assignID2[key]!=0){
				dataAssign={
					assign_to: $scope.user.id,
				};
				
				$http({
					method: "put",
					url: "api/transaction_assign/"+$scope.assignID2[key],
					headers: {
						"Content-Type":"application/json",
						"X-Auth-Token":$scope.token
					},
					params: dataAssign
				}).
				then(function(response){
					$scope.success="Assigning is success!";
					inde++;
					if($scope.assignID2.length==inde){
						$scope.loadAssignFrom();
						$scope.loadAssignTo();
						$scope.loadTable();
					}
				}, function(response){
					$scope.alert=response.data.message;
					inde++;
					if($scope.assignID2.length==inde){
						$scope.loadAssignFrom();
						$scope.loadAssignTo();
						$scope.loadTable();
					}
				});
			}else{
				inde++;
				if($scope.assignID2.length==inde){
					$scope.loadAssignFrom();
					$scope.loadAssignTo();
					$scope.loadTable();
				}
			}
		}
	}
	
	$scope.add=function(){
		$location.path("app/transactions/create");
	}

	$scope.initData=function(){
		if($routeParams.id){
			return false;
		}
        $scope.fd.user_id=$scope.user.id;
		$scope.date=new Date();
		$scope.time=new Date().toTimeString();
		$scope.fd.shift="morning";
		$scope.fd.nn_chips=0;
		$scope.fd.commission=0;
	}

	$scope.customerList=[];
	$scope.array=[];
	$scope.getCustomer=function(){
		$http({
			method: "get",
			url: "api/customer/get_lists",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.customerList = response.data.data;
			$scope.array=[];
			for(inde=0;inde<$scope.customerList.length;inde++){
				$scope.customerList[inde].display=$scope.customerList[inde].first_name+" "+($scope.customerList[inde].last_name?$scope.customerList[inde].last_name:"");
			}
			$scope.array=$scope.customerList;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
    // list of `state` value/display objects
    $scope.querySearch   = querySearch;
    $scope.selectedItemChange = selectedItemChange;
    $scope.searchTextChange   = searchTextChange;

    $scope.newState = newState;

    function newState(state) {
      alert("Sorry! You'll need to create a Constitution for " + state + " first!");
    }

    // ******************************
    // Internal methods
    // ******************************

    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
    function querySearch (query) {
      var results = query ? $scope.array.filter( createFilterFor(query) ) : $scope.array,
      // var results = query ? $scope.states.filter( createFilterFor(query) ) : $scope.states,
          deferred;
        return results;
    }

    function searchTextChange(text) {
      $log.info('Text changed to ' + text);
    }

    function selectedItemChange(item) {
      $log.info('Item changed to ' + JSON.stringify(item));
	  $scope.fd.customer=item;
	  $scope.getCommission();
    }
	
    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(state) {
        // return (state.value.indexOf(lowercaseQuery) === 0);
        return (angular.lowercase(state.display).indexOf(lowercaseQuery) != -1);
      };

    }
/*
	$scope.runnerOption=[];
	$scope.getRunner=function(){
		$http({
			method: "get",
			url: "api/users/"+$scope.user.id+"/find_by_role",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.runnerOption = response.data.data;
			console.log($scope.user.id);
			// if($scope.user.roles!="Administrator"){
				$scope.fd.user_id = $scope.user.id;
				$scope.fd.user_name = $scope.user.name;
			// }
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
*/
	$scope.getCommission=function(){
		$scope.fd.commission=parseFloat($scope.fd.customer.commission);
	}

	$scope.customer=[];
	$scope.save=function(){
		$scope.loading=true;
		// event.preventDefault();
		// tempDate=$scope.fd.date.getFullYear()+"-"+$scope.fd.date.getMonth()+"-"+$scope.fd.date.getDate();
		tempDate=$scope.fd.date;
		tempTime=($scope.time.split(" "))[0];
		$scope.fd.date=$filter('date')($scope.date,"yyyy-MM-dd")+" "+tempTime;

		console.log($scope.fd.date);

		$scope.fd.customer_id=(typeof $scope.fd.customer.id!=="undefined"?$scope.fd.customer.id:null);
		if(!$scope.fd.id){
			$http({
				method: "post", url: 'api/transaction',
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.success="Data saved!";
				$location.path("app/transactions");
			}, function(response){
				$scope.alert=response.data.message;
				$scope.fd.date=tempDate;
				$scope.time=tempTime;
				$scope.loading=false;
			});
		}

		if($scope.fd.id){
 			$http({
				method: "put",
				url: 'api/transaction/'+$scope.fd.id,
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.success="Data updated!";
				$location.path("app/transactions");
			}, function(response){
				$scope.alert=response.data.message;
				$scope.fd.date=tempDate;
				$scope.time=tempTime;
				$scope.loading=false;
			});
		}
	}

	$scope.cancel=function(){
		$location.path("app/transactions");
	}

    $scope.dtOptions = DTOptionsBuilder.newOptions();
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5),
        DTColumnDefBuilder.newColumnDef(6).notSortable()
    ];
    $scope.dtInstance = {};

	$scope.sortType     = 'date'; // set the default sort type
	$scope.sortReverse  = true;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;
	$scope.dataTable=[];
	$scope.params=[];
	$scope.loadTable=function(){
		$scope.params.page=$scope.page;
		$scope.params.sort_by=$scope.sortType;
		$scope.params.date=$scope.date?$filter('date')($scope.date,"yyyy-MM-dd"):'';
		$scope.sortAD='asc';
		if($scope.sortReverse==true){
			$scope.sortAD='desc';
		}
		$scope.params.sort_type=$scope.sortAD;
		
		$http({
			method: "get",
			url: "api/transactions",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: $scope.params
		}).
		then(function(response){
			$scope.dataTable = response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.showPage=function(en){
		return (en>=$scope.dataTable.current_page && en<=$scope.dataTable.current_page+2) || (en<=$scope.dataTable.current_page && (en>=$scope.dataTable.current_page-2 || en>=$scope.dataTable.last_page-2));
	}

	$scope.isLastPage=function(){
		return $scope.dataTable.current_page==$scope.dataTable.last_page;
	}

	$scope.paging=function(page){
		$scope.page=page;
		$scope.loadTable();
	}

	$scope.edit=function(id){
		$location.path("app/transactions/edit/"+id);
	}

	$scope.getEdit=function(){
		if(!$routeParams.id){
			return false;
		}
		$http({
			method: "get",
			url: "api/transaction/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.fd = response.data.data;
			$scope.time = ($scope.fd.date.split(" "))[1];
			$scope.date = ($scope.fd.date.split(" "))[0];
			$scope.customer = response.data.data.customer;
			$scope.fd.user_id=parseInt($scope.fd.user_id);
			$scope.fd.nn_chips = parseFloat($scope.fd.nn_chips);
			$scope.fd.commission = parseFloat($scope.fd.commission);
			$scope.searchText = $scope.customer.first_name+" "+($scope.customer.last_name?$scope.customer.last_name:"");
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.delete = function(id) {
		console.log(id);
		$http({
			method: "delete",
			url: 'api/transaction/'+id+'/delete',
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.success = "Data deleted!";
			$scope.loadTable();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.showAlert=function(){
		return $scope.alert!="" && $scope.alert!="success";
	}

	$scope.addCredit=function(id){
		$location.path("app/transactions/credit/"+id);
	}

	$scope.confirmModalrolling=false;
	$scope.confirmModalrolling3=false;
	$scope.tempRow=[];

	$scope.getOpenStatus=function(row){
		$scope.loading=true;
		$scope.tempRow=row;

		$http({
			method: "get",
			url: "api/transaction_detail/"+row.id+"/0",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				flags: "open"
			}
		}).
		then(function(response){
			$scope.open = response.data.data;
			if($scope.open.length==0){
				$scope.confirmModalrolling=true;
				$scope.loading=false;
			}else{
				$location.path("/app/transactions/rolling/"+row.id);
			}
		}, function(response){
			$scope.alert=response.data.message;
			$scope.loading=false;
		});
	}

	$scope.getOpenStatus1=function(row){
		$scope.loading=true;
		$scope.tempRow=row;

		$http({
			method: "get",
			url: "api/transaction_detail/"+row.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				flags: "open"
			}
		}).
		then(function(response){
			$scope.open = response.data.data;
			if($scope.open.length==0){
				$scope.confirmModalrolling3 = true;
				$scope.loading=false;
			}else{
				$location.path("/app/transactions/rolling/"+row.id+"/"+1);
			}
		}, function(response){
			$scope.alert = response.data.message;
			$scope.loading=false;
		});
	}
/*sanjaya table rolling*/

	$scope.openRolling=function(){
		$scope.loading=true;
		creditTrip=0;
		$http({
			method: "get",
			url: "api/transaction_detail/"+$scope.tempRow.id+"/0",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				flags: "credit"
			}
		}).
		then(function(response){
			creditData = response.data.data;
			for(indec=0;indec<creditData.length;indec++){
				creditTrip+=parseFloat(creditData[indec].rolling);
			}
			
			$http({
				method: "post",
				url: "api/transaction_detail/"+$scope.tempRow.id,
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				data: {
					rolling: (parseFloat($scope.tempRow.nn_chips)+parseFloat(creditTrip)),
					flags: "open",
					time_transaction: (new Date().getHours())+":"+(new Date().getMinutes()),
					type: 1
				}
			}).
			then(function(response){
				$scope.data=response.data.data;
				$location.path("/app/transactions/rolling/"+$scope.tempRow.id);
				$scope.loading=false;
			},function(response){
				$scope.alert=response.data.message;
				$scope.loading=false;
			});
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.openRolling3=function(){
		$scope.loading=true;
		creditTrip=0;
		$http({
			method: "get",
			url: "api/transaction_detail/"+$scope.tempRow.id+"/1",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				flags: "credit"
			}
		}).
		then(function(response){
			creditData = response.data.data;
			for(indec = 0; indec < creditData.length; indec++){
				creditTrip += parseFloat(creditData[indec].rolling);
			}

			$http({
				method: "post",
				url: "api/transaction_detail/"+$scope.tempRow.id,
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				data: {
					rolling: (parseFloat($scope.tempRow.nn_chips)+parseFloat(creditTrip)),
					flags: "open",
					time_transaction: (new Date().getHours())+":"+(new Date().getMinutes()),
					type: 1
				}
			}).
			then(function(response){
				$scope.data = response.data.data;
				$location.path("/app/transactions/rolling/"+$scope.tempRow.id+"/1");
				$scope.loading=false;
			},function(response){
				$scope.alert=response.data.message;
				$scope.loading=false;
			});
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.creditData=[];
	$scope.open=[];
	$scope.time_transaction=new Date();
	$scope.rolling=0;
	$scope.getOpen=function(){
		$http({
			method: "get",
			url: "api/transaction_detail/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				flags: "open"
			}
		}).
		then(function(response){
			$scope.open = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.getCreditStatus=function(){
		$scope.loading=true;

		if(!$routeParams.id){
			return false;
		}
		$http({
			method: "get",
			url: "api/transaction/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.master = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});

		$scope.getOpen();
		$scope.getCreditDetail();
	}

	$scope.showCredit=function(){
		return $scope.open.length==0;
	}

	$scope.getCreditDetail=function(){
		$http({
			method: "get",
			url: "api/transaction_detail/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				flags: "credit"
			}
		}).
		then(function(response){
			$scope.creditData = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.saveCredit=function(){
		if($scope.open.length>0){
			$scope.alert="Cannot add credit!";
			return false;
		}
		$http({
			method: "post",
			url: "api/transaction_detail/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			data: {
				rolling: $scope.rolling,
				flags: "credit",
				time_transaction: $filter('date')($scope.time_transaction, 'HH:mm:ss'),
				type: 0
			}
		}).
		then(function(response){
			$scope.data=response.data.data;
			$scope.rolling=0;
			$scope.time_transaction=new Date().toTimeString();
			// $("#rolling").focus();
			$scope.getCreditDetail();
		},function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.selected={};
	$scope.editFormCredit=function(row){
		$scope.selected = angular.copy(row);
		console.log($scope.selected);
		setTimeout(function(){$("#selected_rolling").focus()},1);
	}

	$scope.editCredit=function(detail_id){
		if($scope.open.length>0){
			$scope.selected={};
			$scope.alert="Cannot add credit!";
			return false;
		}

		$http({
			method: "put",
			url: "api/transaction_detail/"+detail_id+"/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			data: {
				rolling: $scope.selected.rolling,
				flags: $scope.selected.flags,
				time_transaction: $scope.selected.time_transaction,
				type: 0
			}
		}).
		then(function(response){
			$scope.data=response.data.data;
			$scope.getCreditDetail();
			$scope.time_transaction=new Date().toTimeString();
			$scope.selected={};
		},function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.deleteCredit=function(detail_id){
		if($scope.open.length>0){
			$scope.alert="Cannot delete credit!";
			return false;
		}

		$http({
			method: "delete",
			url: 'api/transaction/'+detail_id+'/'+$routeParams.id+'/delete',
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.data = response.data.data;
			$scope.getCreditDetail();
			$scope.selected={};
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.getTemplate = function (row) {
		if (row.id === $scope.selected.id){
		  return 'edit';
		}else{
			return 'display';
		}
	}

	$scope.closingModal=false;
	$scope.closingInput=false;
	$scope.closing_nn_chips=0;
	$scope.closing_cash_chips=0;
	$scope.closing_cash=0;
	$scope.closing_commission=0;
	newDate=new Date();
	$scope.closing_date=$filter('date')(newDate,'yyyy-MM-dd');
	$scope.closing_time=newDate.toTimeString();
	$scope.getClosingStatus = function (row){
		console.log(row);
		$scope.tempRow=angular.copy(row);
		$scope.tempRow.closing_date=row.closing_date?(row.closing_date.split(" "))[0]:$filter('date')(newDate,'yyyy-MM-dd');
		$scope.tempRow.closing_time=row.closing_date?(row.closing_date.split(" "))[1]:newDate.toTimeString();
		/*$scope.closing_nn_chips=row.closing_nn_chips?row.closing_nn_chips:0;
		$scope.closing_cash_chips=row.closing_cash_chips?row.closing_cash_chips:0;
		$scope.closing_cash=row.closing_cash?row.closing_cash:0;
		$scope.closing_commission=row.closing_commission?row.closing_commission:0;
		$scope.closing_date=row.closing_date?(row.closing_date.split(" "))[0]:$filter('date')(newDate,'yyyy-MM-dd');
		$scope.closing_time=row.closing_date?(row.closing_date.split(" "))[1]:newDate.toTimeString();*/
		$scope.closingModal=true;
		$scope.getSumRolling();
	}
	
	$scope.statusClose = function (){
		return tempRow.status==0 && (tempRow.user_id!=user.id || user.roles!='Manager');
	}

	$scope.showClosin=function(){
		return $scope.closingInput;
	}

	$scope.sumDebt=0;
	$scope.sumRolling=0;
	$scope.sumOpening=0;
	$scope.getSumRolling=function(){
		$scope.sumDebt=0;
		$scope.sumRolling=0;
		$scope.sumOpening=parseFloat($scope.tempRow.nn_chips);
		$http({
			method: "get",
			url: "api/transaction_detail/"+$scope.tempRow.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				flags: "role"
			}
		}).
		then(function(response){
			dataRolling=response.data.data;
			for(inde=0;inde<dataRolling.length;inde++){
				$scope.sumRolling+=parseFloat(dataRolling[inde].rolling);
			}

			$http({
				method: "get",
				url: "api/transaction_detail/"+$scope.tempRow.id,
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: {
					flags: "credit"
				}
			}).
			then(function(response){
				dataOpening=response.data.data;
				for(inde=0;inde<dataOpening.length;inde++){
					$scope.sumOpening+=parseFloat(dataOpening[inde].rolling);
				}

				$scope.getSumDebt();
			}, function(response){
				$scope.alert=response.data.message;
			});

		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.getSumDebt=function(){
		$scope.closing_commission=$scope.sumRolling*$scope.tempRow.commission/100;
		$scope.sumDebt=$scope.sumOpening-($scope.closing_nn_chips?$scope.closing_nn_chips:0)-($scope.closing_cash_chips?$scope.closing_cash_chips:0)-($scope.closing_cash?$scope.closing_cash:0);
	}

	$scope.alertClosing="";
	$scope.closingTrip=function(){
		closingDate=$filter('date')($scope.closing_date,'yyyy-MM-dd')+" "+$filter('date')($scope.closing_time,'HH:mm:ss');
		closingDate2=(closingDate.split(" "))[0]+" "+(closingDate.split(" "))[1];
		
		$scope.loading=true;
		$http({
			method: "get",
			url: "api/transaction_detail/"+$scope.tempRow.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				flags: "open"
			}
		}).
		then(function(response){
			$scope.openCheck = response.data.data;
			if($scope.openCheck.length==0){
				$scope.closingInput=false;
				$scope.alertClosing="This Trip not yet the rolling!";
				$scope.loading=false;
				return false;
			}

			$http({
				method: "put",
				url: "api/closing_trip/"+$scope.tempRow.id,
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				data: {
					closing_date: closingDate2,
					closing_cash_chips: $scope.closing_cash_chips,
					closing_nn_chips: $scope.closing_nn_chips,
					closing_cash: $scope.closing_cash,
					check_in_commision: $scope.closing_commission,
					debt: $scope.sumDebt
				}
			}).
			then(function(response){
				$scope.closing_cash_chips=0;
				$scope.closing_nn_chips=0;
				$scope.closing_cash=0;
				$scope.successNote=response.data.message;
				$scope.closingModal=false;
				$scope.closingInput=false;
				$scope.loading=false;
				$scope.loadTable();
				/*$http({
					method: "post",
					url: "api/customer_debts",
					headers: {
						"Content-Type":"application/json",
						"X-Auth-Token":$scope.token
					},
					params: {
						customer_id: $scope.tempRow.customer_id,
						staff_id: $scope.tempRow.user_id,
						transaction_id: $scope.tempRow.id,
						date: $scope.tempRow.date,
						amount: $scope.sumDebt,
						amount_due: $scope.sumDebt,
					}
				}).
				then(function(response){
					$scope.data=response.data.data;
					$scope.closing_cash_chips=0;
					$scope.closing_nn_chips=0;
					$scope.closing_cash=0;
					$scope.successNote=response.data.message;
					$scope.closingModal=false;
					$scope.closingInput=false;
					$scope.loading=false;
					$scope.loadTable();
				},function(response){
					$scope.alertClosing=response.data.message;
					$scope.closingInput=false;
					$scope.loading=false;
				});*/
			},function(response){
				$scope.alertClosing=response.data.message;
				$scope.closingInput=false;
				$scope.loading=false;
			});
		}, function(response){
			$scope.alert=response.data.message;
			$scope.loading=false;
		});

	}

	$scope.commissionModal=false;
	$scope.checkInCommission=false;
	$scope.alertCommission='';
	$scope.check_in_commision=0;
	$scope.checkCommission=function(row){
		$scope.tempRow=row;
		if($scope.tempRow.status==1){
			return false;
		}
		$scope.commissionModal=true;
		$scope.checkInCommission=false;
		$scope.alertCommission='';
		$scope.check_in_commision=row.check_in_commision?row.check_in_commision:0;
	}

	$scope.checkingInCommission=function(){
		$scope.loading=true;
		$http({
			method: "put",
			url: "api/check_in_commision/"+$scope.tempRow.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			data: {
				check_in_commision: $scope.check_in_commision
			}
		}).
		then(function(response){
			$scope.data=response.data.data;
			$scope.check_in_commision=0;
			$scope.successNote=response.data.message;
			$scope.commissionModal=false;
			$scope.checkInCommission=false;
			$scope.loading=false;
			$scope.loadTable();
		},function(response){
			$scope.commissionClosing=response.data.message;
			$scope.checkInCommission=false;
			$scope.loading=false;
		});
	}

	$scope.getTripReport=function(row){
		$scope.report_link = 'report/trip/'+row.id+'?token='+$scope.token;
		$scope.TripReportModal=true;
	}

	$scope.getRollingReport=function(row){
		$scope.report_link = 'report/rolling/'+row.id+'?token='+$scope.token;
		$scope.RollingReportModal=true;
	}
	
	$scope.getRolling2Report=function(row){
		$scope.report_link = 'report/rolling2/'+row.id+'?token='+$scope.token;
		$scope.Rolling2ReportModal=true;
	}

	$scope.getRunnerReport=function(row){
		$scope.report_link = 'report/open_trip/'+row.id+'?token='+$scope.token;
		$scope.RunnerReportModal=true;
	}

	$scope.showSuccess=function(){
		return $scope.successNote!="";
	}

	$scope.showAlertClosing=function(){
		return $scope.alertClosing!="";
	}

	$scope.checkStatus=function(val){
		if(val==1){
			return 'Open';
		}else{
			return 'Closed';
		}
	}
	
	$scope.alertC="";
	$scope.successC="";
	$scope.customerModal=false;
	$scope.showDialog=function(){
		$scope.customerModal=true;
	}
	
	$scope.custTypesOption = [];
	$scope.getCustTypes = function() {
		$http({
			method: "get",
			url: "api/customer/get_customer_types",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.custTypesOption = response.data.data;

		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.saveCustomer=function(){
		$http({
			method: "post",
			url: 'api/customer/save',
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: $scope.fdc
		}).
		then(function(response){
			$scope.customerModal=false;
			$scope.getCustomer();
		}, function(response){
			$scope.alertC=response.data.message;
		});
	}
	
	$scope.manageExpense=function(id){
		$location.path("app/transactions/expenses/"+id);
	}

	$scope.tipsManagement=function(row){
		$http({
			method: 'get',
			url: 'api/checkTips',
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {t: row.id, e: row.user.id}
		}).then(function(response){
			if(response.data.status) {
				$location.path("app/tips/create/" + row.id);
			} else {
				$location.path("app/tips/edit/" + response.data.goTo);
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	};
});
