main.registerCtrl("loanController",function($scope,$location,$http,DTOptionsBuilder, DTColumnDefBuilder,$routeParams,$filter){
	$scope.alert = "";
	$scope.success = "";
	$scope.fd=[];

	$scope.slug=(($routeParams.past && $routeParams.past=="past") || ($routeParams.id && $routeParams.id=="past"))?"past":"";
	$scope.is_past=$scope.slug=="past"?1:0;
	
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd/M/yy",
		yearRange: '1900:-0',
    };

	$scope.add=function(){
		$location.path("app/loan/create/"+$scope.slug);
	}

    $scope.initData = function() {
		$scope.fd.date = $filter('date')(new Date(), 'yyyy-MM-dd');
		$scope.fd.due_date = $filter('date')(new Date(), 'yyyy-MM-dd');
		$scope.fd.staff_id = $scope.user.id;
		$scope.fd.users = [];
		$scope.fd.users=$scope.user;
		$scope.fd.interest_type = '0';
		$scope.fd.is_past=$scope.is_past;
	};
	
	$scope.customerOption=[];
	$scope.getCustomer=function(){
		$http({
			method: "get",
			url: 'api/customer/get_lists',
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.customerOption=response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.typeInterest=function(){
		switch(parseInt($scope.fd.interest_type)){
			case 0:
				$scope.fd.interest_rate=0;
			break;
			case 1:
				$scope.fd.interest_amount=0;
			break;
		}
		$scope.calInterest();
	}
	
	$scope.calInterest=function(){
		if(parseFloat($scope.fd.interest_rate)>0){
			$scope.fd.interest_amount=($scope.fd.amount?$scope.fd.amount:0)*($scope.fd.interest_rate?$scope.fd.interest_rate:0)/100;
		}
		$scope.fd.amount_due=parseFloat(($scope.fd.amount?$scope.fd.amount:0))+parseFloat(($scope.fd.interest_amount?$scope.fd.interest_amount:0));
	}
	
	$scope.save=function() {
		$scope.fd.due_date=$filter('date')($scope.fd.due_date, 'yyyy-MM-dd')
		
 		if (!$scope.fd.id) {
 			$http({
				method: "post",
				url: 'api/customer_debts',
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.cancel();
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}

		if($scope.fd.id){
 			$http({
				method: "put",
				url: 'api/customer_debts/'+$scope.fd.id,
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.cancel();
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}
	}

	$scope.cancel=function(){
		$location.path("app/loan/"+$scope.slug);
	}

    $scope.dtOptions = DTOptionsBuilder.newOptions();
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5),
        DTColumnDefBuilder.newColumnDef(6),
        DTColumnDefBuilder.newColumnDef(7),
        DTColumnDefBuilder.newColumnDef(8),
        DTColumnDefBuilder.newColumnDef(9).notSortable()
    ];
    $scope.dtInstance = {};

	$scope.search='';
	
    $scope.sortType     = 'date'; // set the default sort type
	$scope.sortReverse  = false;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;

	$scope.listData = [];
	$scope.loadData=function() {

		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}
		
		if($scope.date){
			$scope.date=$filter('date')($scope.date, 'yyyy-MM-dd');
		}else{
			$scope.date='';
		}

		$http({
			method: "get",
			url: "api/customer_debts?sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method
			 +"&page="+$scope.page+"&date="+$scope.date+"&is_past="+$scope.slug,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.listData = response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.sort = function(sortType) {
		$scope.sortType = sortType;
		$scope.sortReverse = !$scope.sortReverse;
		$scope.loadData();
	}

	$scope.showPage=function(en){
		return (en>=$scope.listData.current_page && en<=$scope.listData.current_page+2) || (en<=$scope.listData.current_page && (en>=$scope.listData.current_page-2 || en>=$scope.listData.last_page-2));
	}

	$scope.isLastPage=function(){
		return $scope.listData.current_page==$scope.listData.last_page;
	}

	$scope.paging=function(page){
		if ($scope.listData.last_page>0) {
			$scope.page=page;
			$scope.loadData();
		}
	}
	
    $scope.edit=function(id) {
		$location.path("app/loan/edit/"+id+"/"+$scope.slug);
    }
	
    $scope.show=function(id) {
		$location.path("app/loan/show/"+id+"/"+$scope.slug);
    }

	$scope.showStatus=false;
	$scope.getEdit=function() {
		if(!$routeParams.id){
			return false;
		}

		if($routeParams.show){
			if($routeParams.show!="show"){
				return false;
			}
			$scope.showStatus=true;
		}

		$http({
			method: "get",
			url: "api/customer_debts/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			response.data.data.customer_id = parseInt(response.data.data.customer_id);
			response.data.data.interest_type = response.data.data.interest_type.toString();
			$scope.fd = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.delete=function(id) {
		$http({
			method: "delete",
			url: 'api/customer_debts/'+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.success="Data deleted!";
			$scope.loadData();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
});
