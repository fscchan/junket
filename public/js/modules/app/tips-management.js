main.registerCtrl("TipsManagementController",function($filter,$scope,$location,$http,$routeParams,DTOptionsBuilder, DTColumnDefBuilder){
    $scope.success="";
    $scope.alert = "";
	$scope.editMod = ($routeParams.id || $routeParams.tid);
    $scope.fd=[];
    $scope.fd.customer=[];
    $scope.amount = [];
	
	$scope.date=new Date();

    $scope.add=function(){
        $location.path("app/tips/create");
    }

    $scope.dateOptions = {
        changeYear: true,
        changeMonth: true,
        dateFormat: "dd/M/yy",
        yearRange: '1900:-0',
    }

    $scope.initData=function(){
        $scope.fd.trans_date=$filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.fd.value=0;
        $http({
			method: "get",
			url: "api/transactions",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {status: 1}
		}).then(function(response){
			$scope.openTrans = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
        $http({
			method: "get",
			url: "api/customer/get_lists",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).then(function(response){
			$scope.customers = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
        $http({
			method: "get",
			url: "api/users/6/find_by_role",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).then(function(response){
			$scope.runners = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
		if($routeParams.tid){
			$http({
				method: "get",
				url: "api/transaction/" + $routeParams.tid,
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				}
			}).
			then(function(response){
				$scope.fd.trans_id = {id: response.data.data.id};
				$scope.fd.customer_id = {id: response.data.data.customer.id};
				$scope.fd.employee_id = {id: response.data.data.user.id};
			}, function(response){
				$scope.alert=response.data.message;
			});
		}
    }

    $scope.save = function() {
		$scope.fd.trans_date=$filter('date')($scope.fd.trans_date, 'yyyy-MM-dd');
        if (!$routeParams.id) {
            $scope.valuesave = [];
			$scope.valuesave.customer_id = $scope.fd.customer_id.id;
			$scope.valuesave.trans_id = $scope.fd.trans_id.id;
			$scope.valuesave.trans_date = $scope.fd.trans_date;
			$scope.valuesave.employee_id = $scope.fd.employee_id.id;
			$scope.valuesave.amount = $scope.fd.amount;
			$http({
				method: "post",
				url: 'api/tips',
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.valuesave
			}).
			then(function(response){
				setTimeout(function() { 
					$location.path("app/tips"); 
				}, 50); 						
			}, function(response){
				$scope.alert=response.data.message;
			});
			$scope.valuesave = [];
            $scope.success="Data saved!";
        }

        if($routeParams.id){
            $scope.valuesave = [];
			$scope.valuesave.customer_id = $scope.fd.customer_id.id;
			$scope.valuesave.trans_id = $scope.fd.trans_id.id;
			$scope.valuesave.trans_date = $scope.fd.trans_date;
			$scope.valuesave.employee_id = $scope.fd.employee_id.id;
			$scope.valuesave.amount = $scope.fd.amount;
            $http({
                method: "put",
                url: 'api/tips/'+$routeParams.id,
                headers: {
                    "Content-Type":"application/json",
                    "X-Auth-Token":$scope.token
                },
                params: $scope.valuesave
            }).
            then(function(response){
                $scope.success="Data updated!";
                $location.path("app/tips");
            }, function(response){
                $scope.alert=response.data.message;
            });
        }
        $location.path("app/tips");
    }

    $scope.cancel = function() {
        $location.path("app/tips");
    }

    $scope.sortType     = 'date'; // set the default sort type
    $scope.sortReverse  = true;  // set the default sort order
    $scope.searchList   = '';     // set the default search/filter term
    $scope.page			= 1;

    $scope.dtOptions = DTOptionsBuilder.newOptions();
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4).notSortable()
    ];
    $scope.dtInstance = {};

    $scope.dataTable = [];

    $scope.loadData = function() {
        if ($scope.sortReverse == true) {
            $scope.sort_method = 'desc';
        } else {
            $scope.sort_method = 'asc';
        }

        if ($scope.date) {
            $scope.date = $filter('date')($scope.date, 'yyyy-MM-dd');
        } else {
            $scope.date = '';
        }

        $http({
            method: "get",
            url: "api/tips?sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&page="+$scope.page+"&date="+$scope.date,
            headers: {
                "Content-Type":"application/json",
                "X-Auth-Token":$scope.token
            }
        }).
        then(function(response){
            $scope.dataTable = response.data;
			$scope.dataTable.totalTips = 0;
			angular.forEach(response.data.data, function(o, i) {
				$scope.dataTable.totalTips += parseFloat(o.amount);
			});
        }, function(response){
            $scope.alert=response.data.message;
        });
    }

    $scope.sort = function(sortType) {
        $scope.sortType = sortType;
        $scope.sortReverse = !$scope.sortReverse;
        $scope.loadData();
    }

    $scope.showPage=function(en){
        return (en>=$scope.dataTable.current_page && en<=$scope.dataTable.current_page+2) || (en<=$scope.dataTable.current_page && (en>=$scope.dataTable.current_page-2 || en>=$scope.dataTable.last_page-2));
    }

    $scope.isLastPage=function(){
        return $scope.dataTable.current_page==$scope.dataTable.last_page;
    }

    $scope.paging=function(page){
        if ($scope.dataTable.last_page>0) {
            $scope.page=page;
            $scope.loadData();
        }
    }

    $scope.edit=function(id){
        $location.path("app/tips/edit/"+id);
    }

    $scope.getEdit = function() {
		if(!$routeParams.id){
            return false;
        }
        $http({
            method: "get",
            url: "api/tips/"+$routeParams.id,
            headers: {
                "Content-Type":"application/json",
                "X-Auth-Token":$scope.token
            }
        }).
        then(function(response){
            $scope.fd.trans_id = {id: parseInt(response.data.data.trans_id)};
            $scope.fd.customer_id = {id: parseInt(response.data.data.customer_id)};
            $scope.fd.employee_id = {id: parseInt(response.data.data.employee_id)};
            $scope.fd.trans_date = response.data.data.trans_date;
            $scope.fd.amount = parseInt(response.data.data.amount);
        }, function(response){
            $scope.alert=response.data.message;
        });
    }

    $scope.delete = function(id) {
        $http({method: "delete", url: 'api/tips/'+id, headers: {
            "Content-Type":"application/json",
            "X-Auth-Token":$scope.token
        }
        }).
        then(function(response){
            $scope.success="Data deleted!";
            $scope.loadData();
        }, function(response){
            $scope.alert=response.data.message;
        });
    }

	$scope.getTrans = function() {
		$http({
			method: "get",
			url: "api/transaction/" + $scope.fd.trans_id.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.fd.customer_id = {id: response.data.data.customer.id};
			$scope.fd.employee_id = {id: response.data.data.user.id};
		}, function(response){
			$scope.alert=response.data.message;
		});
	};
	
	$scope.showAlert=function(){
        return $scope.alert!="" && $scope.alert!="success";
    }
});
