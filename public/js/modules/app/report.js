main.registerCtrl("chipRunnerReportController",function($scope,$location,$http,DTOptionsBuilder, DTColumnDefBuilder){

});

main.registerCtrl("byTripReportController",function($scope,$location,$http){

});

main.registerCtrl("collectionController",function($scope,$http,$filter){
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd-mm-yy",
		yearRange: '1900:-0',
    };
	
	$scope.start_date=$filter('date')(new Date(), 'yyyy-MM-01');
	$scope.end_date=$filter('date')(new Date(), 'yyyy-MM-dd');
	
	$scope.start_date2=$filter('date')(new Date(), 'yyyy-MM-01');
	$scope.end_date2=$filter('date')(new Date(), 'yyyy-MM-dd');
	
	$scope.tab = 1;
	$scope.setTab = function(newTab){
		$scope.tab = newTab;
	};

	$scope.isSet = function(tabNum){
		return $scope.tab === tabNum;
	};
	
	$scope.listData=[];
	$scope.loadTable=function(isPaid){
		startDate=$scope.start_date;
		endDate=$scope.end_date;
		if(isPaid==1){
			startDate=$scope.start_date2;
			endDate=$scope.end_date2;
		}
		$http({
			method: "get",
			url: "report/get_collection?is_paid="+isPaid,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				token: $scope.token,
				start_date: startDate,
				end_date: endDate,
			}
		}).
		then(function(response){
			if(isPaid==1){
				$scope.listData2 = response.data.data;
			}else{
				$scope.listData = response.data.data;
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.reportModal=false;
	$scope.printReport=function(isPaid){
		startDate=$scope.start_date;
		endDate=$scope.end_date;
		if(isPaid==1){
			startDate=$scope.start_date2;
			endDate=$scope.end_date2;
		}
		$scope.report_link = 'report/collection/'+$scope.customer_id+'?is_paid='+isPaid+'&token='+$scope.token+"&start_date="+$filter('date')(startDate,'yyyy-MM-dd')+"&end_date="+$filter('date')(endDate,'yyyy-MM-dd');
		$scope.reportModal=true;
	}
});

main.registerCtrl("finalController",function($scope,$http,$filter){
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd-mm-yy",
		yearRange: '1900:-0',
    };
	
	$scope.date=$filter('date')(new Date(), 'yyyy-MM-dd');
	$scope.reportModal=false;
	$scope.printReport=function(){
		$scope.report_link = 'report/final?token='+$scope.token+"&date="+$filter('date')($scope.date,'yyyy-MM-dd');
		$scope.reportModal=true;
	}
});

main.registerCtrl("managerController",function($scope,$http,$filter){
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd-mm-yy",
		yearRange: '1900:-0',
    };
	
	$scope.headName='';
	$scope.date=$filter('date')(new Date(), 'yyyy-MM-dd');
	/*$scope.listData=[
		{"name":"test","details":[{"in":1},{"in":2}],"ceil":2},
		{"name":"testing","details":[{"in":3}],"ceil":1}
	];*/
	$scope.listData=[];
	
	console.log($scope.listData);
	$scope.loadTable=function(){
		$scope.headName='';
		$http({
			method: "get",
			url: "report/get_daily_manager",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				token: $scope.token,
				date: $scope.date,
			}
		}).
		then(function(response){
			$scope.listData = response.data.data;
			$scope.listData = $scope.getDebitCredit($scope.listData);
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.getDebitCredit=function(dataDC){
		debit=[];credit=[];
		for(inde=0;inde<dataDC.length;inde++){
			dataDC[inde].debit=[];dataDC[inde].credit=[];dataDC[inde].balance=[];
			for(inde2=0;inde2<dataDC[inde].ceil;inde2++){
				debit[inde2]=0;credit[inde2]=0;
				for(inde3=0;inde3<19;inde3++){
					if(typeof dataDC[inde].details[inde3]!=="undefined" && inde3>0){
						debit[inde2]+=parseFloat(dataDC[inde].details[inde3].balancein);
						credit[inde2]+=parseFloat(dataDC[inde].details[inde3].balanceout);
					}
				}
				dataDC[inde].debit[inde2]=debit[inde2];
				dataDC[inde].credit[inde2]=credit[inde2];
				dataDC[inde].balance[inde2]=parseFloat(debit[inde2])-parseFloat(credit[inde2]);
			}
		}
		
		return dataDC;
	}
	
	$scope.getTemplate=function(valuesData){
		if(valuesData.name!=$scope.headName){
			$scope.headName=$scope.name;
			return 'head';
		}else{
			return 'detail';
		}
	}
	
	$scope.reportModal=false;
	$scope.printReport=function(){
		$scope.report_link = 'report/daily_manager?token='+$scope.token+"&date="+$filter('date')($scope.date,'yyyy-MM-dd');
		$scope.reportModal=true;
	}
	
	$scope.exportReport=function(){
		window.open('report/daily_manager?token='+$scope.token+"&date="+$filter('date')($scope.date,'yyyy-MM-dd')+"&report=excel");
	}
});

main.registerCtrl("bankRController",function($scope,$http,$filter){
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd-mm-yy",
		yearRange: '1900:-0',
    };
	
	$scope.start_date=$filter('date')(new Date(), 'yyyy-MM-01');
	$scope.end_date=$filter('date')(new Date(), 'yyyy-MM-dd');
	$scope.bankOption=[];
	$scope.getBank=function(){
		$http({
			method: "get", 
			url: "api/account/bank",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).then(function(response){
			$scope.bankOption = response.data.data;
			$scope.bank_id = response.data.data[0].id;
			$scope.loadData();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.listData=[];
	$scope.loadData=function(){
		$http({
			method: "get", 
			url: "report/get_bank_statement",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				token: $scope.token,
				start_date: $filter('date')($scope.start_date,'yyyy-MM-dd'),
				end_date: $filter('date')($scope.end_date,'yyyy-MM-dd'),
				bank_id: $scope.bank_id
			}
		}).then(function(response){
			$scope.listData=response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
		
	}
	
	$scope.reportModal=false;
	$scope.printReport=function(){
		$scope.report_link = 'report/bank_statement?token='+$scope.token+"&start_date="+$filter('date')($scope.start_date,'yyyy-MM-dd')+"&end_date="+$filter('date')($scope.end_date,'yyyy-MM-dd')+'&bank_id='+$scope.bank_id;
		$scope.reportModal=true;
	}
});

main.registerCtrl("dailyController",function($scope,$http,$filter){
	$scope.month=((new Date().getMonth())+1).toString();
	$scope.year=new Date().getFullYear();
	$scope.reportModal=false;
	$scope.printReport=function(){
		$scope.report_link = 'report/daily?token='+$scope.token+"&month="+$scope.month+"&year="+$scope.year;
		window.open($scope.report_link);
	}
});

main.registerCtrl("customerReportController",function($scope,$http,$filter){
	newDate=new Date();
	$scope.startMonth=(newDate.getMonth()+1).toString();
	$scope.startYear=newDate.getFullYear();
	$scope.endMonth=(newDate.getMonth()+1).toString();
	$scope.endYear=newDate.getFullYear();
	
	
	$scope.listData=[];
	$scope.loadData=function(){
		dateStart=$scope.startYear+"-"+$scope.startMonth+"-01";
		dateEnd=$scope.endYear+"-"+$scope.endMonth+"-31";
		$http({
			method: "get", 
			url: "report/final_customer_show",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				token: $scope.token,
				start_date: dateStart,
				end_date: dateEnd
			}
		}).then(function(response){
			$scope.listData=response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.reportModal=false;
	$scope.printReport=function(){
		dateStart=$scope.startYear+"-"+$scope.startMonth+"-01";
		dateEnd=$scope.endYear+"-"+$scope.endMonth+"-31";
		$scope.report_link = 'report/final_customer_pdf?token='+$scope.token+"&start_date="+dateStart+"&end_date="+dateEnd;
		$scope.reportModal=true;
	}
});

main.registerCtrl("rollingReportController",function($scope,$http,$filter){
	$scope.tab = 1;
	$scope.setTab = function(newTab){
		$scope.tab = newTab;
	};

	$scope.isSet = function(tabNum){
		return $scope.tab === tabNum;
	};
	
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd-mm-yy",
		yearRange: '1900:-0',
    };
	
	newDate=new Date();
	$scope.start_date=$filter('date')(newDate, 'yyyy-MM-01');
	$scope.end_date=$filter('date')(newDate, 'yyyy-MM-dd');
	
	$scope.listData=[];
	$scope.loadData=function(){
		$http({
			method: "get", 
			url: "report/rolling_show",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				token: $scope.token,
				start_date: $filter('date')($scope.start_date,'yyyy-MM-dd'),
				end_date: $filter('date')($scope.end_date,'yyyy-MM-dd')
			}
		}).then(function(response){
			$scope.listData=response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.reportModal=false;
	$scope.printReport=function(){
		$scope.report_link = 'report/rolling_pdf?token='+$scope.token+"&start_date="+$filter('date')($scope.start_date,'yyyy-MM-dd')+"&end_date="+$filter('date')($scope.end_date,'yyyy-MM-dd');
		$scope.reportModal=true;
	}
	
	$scope.start_date2=$filter('date')(newDate, 'yyyy-MM-01');
	$scope.end_date2=$filter('date')(newDate, 'yyyy-MM-dd');
	
	$scope.listData2=[];
	$scope.loadData2=function(){
		$http({
			method: "get", 
			url: "report/rolling2_show",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				token: $scope.token,
				start_date: $filter('date')($scope.start_date2,'yyyy-MM-dd'),
				end_date: $filter('date')($scope.end_date2,'yyyy-MM-dd')
			}
		}).then(function(response){
			$scope.listData2=response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.printReport2=function(){
		$scope.report_link = 'report/rolling2_pdf?token='+$scope.token+"&start_date="+$filter('date')($scope.start_date2,'yyyy-MM-dd')+"&end_date="+$filter('date')($scope.end_date2,'yyyy-MM-dd');
		$scope.reportModal=true;
	}
	
	$scope.start_date3=$filter('date')(newDate, 'yyyy-MM-01');
	$scope.end_date3=$filter('date')(newDate, 'yyyy-MM-dd');
	
	$scope.listData3=[];
	$scope.loadData3=function(){
		$http({
			method: "get", 
			url: "report/rolling3_show",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				token: $scope.token,
				start_date: $filter('date')($scope.start_date3,'yyyy-MM-dd'),
				end_date: $filter('date')($scope.end_date3,'yyyy-MM-dd')
			}
		}).then(function(response){
			$scope.listData3=response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.printReport3=function(){
		$scope.report_link = 'report/rolling3_pdf?token='+$scope.token+"&start_date="+$filter('date')($scope.start_date3,'yyyy-MM-dd')+"&end_date="+$filter('date')($scope.end_date3,'yyyy-MM-dd');
		$scope.reportModal=true;
	}
});

main.registerCtrl("endDayController",function($scope,$http,$filter){
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd-mm-yy",
		yearRange: '1900:-0',
    };
	
	newDate=new Date();
	$scope.start_date=$filter('date')(newDate, 'yyyy-MM-01');
	$scope.end_date=$filter('date')(newDate, 'yyyy-MM-dd');
	
	$scope.accountOption=[];
	$scope.getAccount=function(){
		$http({
			method: "get", 
			url: "api/account",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).then(function(response){
			$scope.accountOption=response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.accountModal=false;
	
	$scope.detailShow=false;
	$scope.listData=[];
	$scope.listDataTemp=[];
	$scope.total=0;
	$scope.loadData=function(){
		$scope.detailShow=false;
		$http({
			method: "get", 
			url: "report/balance_account_show",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				token: $scope.token,
				start_date: $filter('date')($scope.start_date,'yyyy-MM-dd'),
				end_date: $filter('date')($scope.end_date,'yyyy-MM-dd')
			}
		}).then(function(response){
			$scope.listData=response.data;
			$scope.total=0;
			for(inde=0;inde<$scope.listData.length;inde++){
				$scope.total+=parseFloat($scope.listData[inde].balance);
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.loadDataClick=function(idAcc){
		$scope.listDataTemp=$scope.listData;
		$http({
			method: "get", 
			url: "report/journal_show",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				token: $scope.token,
				start_date: $filter('date')($scope.start_date,'yyyy-MM-dd'),
				end_date: $filter('date')($scope.end_date,'yyyy-MM-dd'),
				idi: idAcc
			}
		}).then(function(response){
			$scope.listData=response.data;
			$scope.total=0;
			for(inde=0;inde<$scope.listData.length;inde++){
				$scope.total+=parseFloat($scope.listData[inde].balance);
			}
			$scope.detailShow=true;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.closeDetail=function(){
		$scope.detailShow=false;
		$scope.listData=$scope.listDataTemp;
		$scope.total=0;
		for(inde=0;inde<$scope.listData.length;inde++){
			$scope.total+=parseFloat($scope.listData[inde].balance);
		}
	}
	
	$scope.listData2=[];
	$scope.listDataDetail=[];
	$scope.accounts=[];
	$scope.loadData2=function(){
		$scope.listDataDetail=[];
		$scope.accountModal=false;
		idi='0';
		for(key in $scope.accounts){
			if($scope.accounts[key]==true){
				idi+=','+key;
			}
		}
		$http({
			method: "get", 
			url: "report/journal_show",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				token: $scope.token,
				start_date: $filter('date')($scope.start_date,'yyyy-MM-dd'),
				end_date: $filter('date')($scope.end_date,'yyyy-MM-dd'),
				idi: idi
			}
		}).then(function(response){
			$scope.listData2=response.data;
			idBeda=0;indeHead=0;iHeadDetail=0;
			for(inde=0;inde<$scope.listData2.length;inde++){
				if(idBeda!=$scope.listData2[inde].id){
					$scope.listDataDetail[indeHead]=$scope.listData2[inde];
					idBeda=$scope.listData2[inde].id;
					iHeadDetail=indeHead;
					$scope.listDataDetail[iHeadDetail].detail=[];
					$scope.listDataDetail[iHeadDetail].total=0;
					indeHead++;
				}
				if(idBeda==$scope.listData2[inde].id){
					$scope.listDataDetail[iHeadDetail].detail.push($scope.listData2[inde]);
					$scope.listDataDetail[iHeadDetail].total+=parseFloat($scope.listData2[inde].balance);
				}
			}
			console.log($scope.listDataDetail);
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.printReport=function(){
		idi='0';
		for(key in $scope.accounts){
			if($scope.accounts[key]==true){
				idi+=','+key;
			}
		}
		$scope.report_link = 'report/balance_account_export?token='+$scope.token+"&start_date="
			+$filter('date')($scope.start_date,'yyyy-MM-dd')+"&end_date="+$filter('date')($scope.end_date,'yyyy-MM-dd')+"&idi="+idi;
		window.open($scope.report_link);
		$scope.accountModal=false;
	}
});

main.registerCtrl("runnerController",function($scope,$http,$filter){
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd-mm-yy",
		yearRange: '1900:-0',
    };
	
	newDate=new Date();
	$scope.start_date=$filter('date')(newDate, 'yyyy-MM-01');
	$scope.end_date=$filter('date')(newDate, 'yyyy-MM-dd');
	
	$scope.listData=[];
	$scope.loadData=function(){
		$http({
			method: "get", 
			url: "report/runner_show",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				token: $scope.token,
				start_date: $filter('date')($scope.start_date,'yyyy-MM-dd'),
				end_date: $filter('date')($scope.end_date,'yyyy-MM-dd')
			}
		}).then(function(response){
			$scope.listData=response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.reportModal=false;
	$scope.printReport=function(user_id,date,shift){
		$scope.report_link = 'report/runner_pdf?token='+$scope.token+"&user_id="+user_id+"&date="+date+"&shift="+shift;
		$scope.reportModal=true;
	}
});

main.registerCtrl("currentChipController",function($scope,$http,$filter){
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd-mm-yy",
		yearRange: '1900:-0',
    };
	
	newDate=new Date();
	newDate.setDate(newDate.getDate()-5);
	$scope.start_date=$filter('date')(newDate, 'yyyy-MM-dd');
	newDate.setDate(newDate.getDate()+5);
	$scope.end_date=$filter('date')(newDate, 'yyyy-MM-dd');
	
	$scope.listData=[];
	$scope.loadData=function(){
		$http({
			method: "get", 
			url: "api/asset_on_hand",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				token: $scope.token,
				start_date: $filter('date')($scope.start_date,'yyyy-MM-dd'),
				end_date: $filter('date')($scope.end_date,'yyyy-MM-dd')
			}
		}).then(function(response){
			$scope.listData=response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
});

main.registerCtrl("finalEndController",function($scope,$http,$filter){
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd-mm-yy",
		yearRange: '1900:-0',
    };
	
	$scope.start_date=$filter('date')(new Date(), 'yyyy-MM-01');
	$scope.end_date=$filter('date')(new Date(), 'yyyy-MM-dd');
	$scope.reportModal=false;
	$scope.total=0;
	$scope.listData=[];
	$scope.loadData=function(){
		$scope.detailShow=false;
		$http({
			method: "get", 
			url: "report/balance_account_show",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				token: $scope.token,
				start_date: $filter('date')($scope.start_date,'yyyy-MM-dd'),
				end_date: $filter('date')($scope.end_date,'yyyy-MM-dd')
			}
		}).then(function(response){
			$scope.listData=response.data;
			$scope.total=0;
			for(inde=0;inde<$scope.listData.length;inde++){
				$scope.total+=parseFloat($scope.listData[inde].balance);
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.listData2=[];
	$scope.loadData2=function(){
		$scope.detailShow=false;
		$http({
			method: "get", 
			url: "report/final_show",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				token: $scope.token,
				start_date: $filter('date')($scope.start_date,'yyyy-MM-dd'),
				date: $filter('date')($scope.end_date,'yyyy-MM-dd')
			}
		}).then(function(response){
			$scope.listData2=response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
});

main.registerCtrl("agentInsuranceController",function($scope,$http,$filter){
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd-mm-yy",
		yearRange: '1900:-0',
    };
	
	$scope.start_date=$filter('date')(new Date(), 'yyyy-MM-01');
	$scope.end_date=$filter('date')(new Date(), 'yyyy-MM-dd');
	
	$scope.listData=[];
	$scope.loadData=function(){
		$http({
			method: "get", 
			url: "report/agent_insurance_show",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				token: $scope.token,
				start_date: $filter('date')($scope.start_date,'yyyy-MM-dd'),
				end_date: $filter('date')($scope.end_date,'yyyy-MM-dd')
			}
		}).then(function(response){
			$scope.listData=response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.reportModal=false;
	$scope.printReport=function(){
		$scope.report_link = 'report/agent_insurance_pdf?token='+$scope.token+"&start_date="+$filter('date')($scope.start_date,'yyyy-MM-dd')+"&end_date="+$filter('date')($scope.end_date,'yyyy-MM-dd');
		$scope.reportModal=true;
	}
});

main.registerCtrl("purchaseCommissionController",function($scope,$http,$filter){
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd-mm-yy",
		yearRange: '1900:-0',
    };
	
	$scope.start_date=$filter('date')(new Date(), 'yyyy-MM-01');
	$scope.end_date=$filter('date')(new Date(), 'yyyy-MM-dd');
	
	$scope.vendorOption=[];
	$scope.getVendor=function(){
		$http({
			method: "get", 
			url: "api/vendors",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).then(function(response){
			$scope.vendorOption=response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.listData=[];
	$scope.loadData=function(){
		$http({
			method: "get", 
			url: "report/purchase_commission_show",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			},
			params: {
				token: $scope.token,
				start_date: $filter('date')($scope.start_date,'yyyy-MM-dd'),
				end_date: $filter('date')($scope.end_date,'yyyy-MM-dd'),
				vendor_id: ($scope.vendor_id?$scope.vendor_id:'')
			}
		}).then(function(response){
			$scope.listData=response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.reportModal=false;
	$scope.printReport=function(){
		$scope.report_link = 'report/purchase_commission_pdf?token='+$scope.token+"&vendor_id="+($scope.vendor_id?$scope.vendor_id:'')+"&start_date="+$filter('date')($scope.start_date,'yyyy-MM-dd')+"&end_date="+$filter('date')($scope.end_date,'yyyy-MM-dd');
		$scope.reportModal=true;
	}
});