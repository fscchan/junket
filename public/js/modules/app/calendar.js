main.registerCtrl('calendarActivityController', function($filter,$scope,$location,$http) {

	$scope.dateNow = new Date();
	$scope.currentDate = $filter('date')($scope.dateNow, 'yyyy-MM-'+01);
	$scope.events = [];
	$scope.alert="";
	$scope.success="";

	$scope.get_activity = function() {
		$http({method: "get", url: 'api/activities?date='+$scope.currentDate, headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){

			angular.forEach(response.data, function(value, key) {

				for(var i = 0; i < value.length; i++) {
					$scope.events.push({
						id:value[i].id,
						user_id:value[i].user_id,
						username:$scope.user.name,
						text:value[i].notes,
						start_date: new Date(value[i].start),
						end_date: new Date(value[i].end),
					 	title: value[i].title,
					 	place: value[i].place,
					});
				}

			});

		}, function(response){
			alert(response.data.message);
		});
	}

	$scope.get_activity();

  $scope.scheduler = { date : new Date() };

	scheduler.attachEvent("onEventSave",function(id,ev,is_new){

			$scope.start_date = $filter('date')(ev.start_date, 'yyyy-MM-dd HH:mm:ss');
			$scope.end_date = $filter('date')(ev.end_date, 'yyyy-MM-dd HH:mm:ss');

		  if (!ev.text) {
        alert("Text must not be empty");
        return false;
	    } else if (!ev.title) {
        alert("Title must not be empty");
        return false;
	    } else if (!ev.place) {
        alert("Place must not be empty");
        return false;
	    } else if(is_new != null) {

					$scope.fca = {
						title : ev.title,
						start : $scope.start_date.replace(new RegExp("\\+","g"),' '),
						end 	: $scope.end_date.replace(new RegExp("\\+","g"),' '),
						place : ev.place,
						notes : ev.text
					};

					$http({method: "post", url: 'api/activities', headers: {
							"Content-Type":"application/json",
							"X-Auth-Token":$scope.token
						}, params: $scope.fca
					}).
					then(function(response){
						$scope.success="Data saved!";
						$scope.get_activity();
					}, function(response){
						alert(response.data.message);
					});

			} else if(is_new == null) {

					$scope.fca = {
						title : ev.title,
						start : $scope.start_date.replace(new RegExp("\\+","g"),' '),
						end 	: $scope.end_date.replace(new RegExp("\\+","g"),' '),
						place : ev.place,
						notes : ev.text
					};

					$http({method: "put", url: 'api/activities/'+id, headers: {
							"Content-Type":"application/json",
							"X-Auth-Token":$scope.token
						}, params: $scope.fca
					}).
					then(function(response){
						$scope.success="Data updated!";
						$scope.get_activity();
					}, function(response){
						alert(response.data.message);
					});

			}
			$scope.get_activity();
	    return true;
	});

	scheduler.attachEvent("onEventChanged", function(id,ev){

			$scope.start_date = $filter('date')(ev.start_date, 'yyyy-MM-dd HH:mm:ss');
			$scope.end_date = $filter('date')(ev.end_date, 'yyyy-MM-dd HH:mm:ss');

			$scope.fca = {
				title : ev.title,
				start : $scope.start_date.replace(new RegExp("\\+","g"),' '),
				end 	: $scope.end_date.replace(new RegExp("\\+","g"),' '),
				place : ev.location,
				notes : ev.text
			};

			$http({method: "put", url: 'api/activities/'+id, headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				}, params: $scope.fca
			}).
			then(function(response){
				$scope.success="Data updated!";
				$scope.get_activity();
			}, function(response){
				alert(response.data.message);
			});

			$scope.get_activity();
	});
	
	scheduler.attachEvent("onEventDeleted", function(id){
		$http({method: "delete", url: 'api/activities/'+id, headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.success="Data deleted!";
			$scope.get_activity();
		}, function(response){
			alert(response.data.message);
		});
	});

	$scope.showAlert=function(){
		return $scope.alert!="" && $scope.alert!="success";
	}
	
	$scope.showSuccess=function(){
		return $scope.success!="";
	}
	
});
