main.registerCtrl("WithdrawalGenting2Controller",function($filter,$scope,$location,$http,$routeParams,DTOptionsBuilder, DTColumnDefBuilder){
    $scope.success="";
    $scope.alert = "";
    $scope.fd=[];
    $scope.fd.customer=[];
    $scope.typemember = true;
    $scope.add=function(){
        $location.path("app/wg2/create");
    };

    $scope.dateOptions = {
        changeYear: true,
        changeMonth: true,
        dateFormat: "dd/M/yy",
        yearRange: '1900:-0',
    };

    $scope.initData=function(){
        $scope.fd.date=$filter('date')(new Date(), 'yyyy-MM-dd');
        $scope.fd.value=0;
    };

    $scope.entityOption=[];
    $scope.allEntity = [];
    $scope.getEntity = function() {
        $http({
            method: "get",
            url: "api/account",
            headers: {
                "Content-Type":"application/json",
                "X-Auth-Token":$scope.token
            }
        }).
        then(function(response){
            $scope.allEntity['account'] = response.data.data;
            $scope.setEntity();
        }, function(response){
            $scope.alert=response.data.message;
        });
    };

    $scope.setEntity = function(){
        $scope.entityOption = $scope.allEntity['account'];
    }

	$scope.wg=[];
	$scope.wg.date=$filter('date')(new Date(), 'yyyy-MM-dd');
    $scope.addwg = function() {
        $scope.datasave = [];
        $scope.datasave.user_id = $scope.user.id;
        $scope.datasave.date = $scope.wg.date;
        $scope.datasave.type_chip = "Only Cash";
        $scope.datasave.amount = $scope.wg.amount;
        $scope.datasave.account_id = $scope.wg.account_id;
        $http({
            method: "post",
            url: 'api/wg2',
            headers: {
                "Content-Type":"application/json",
                "X-Auth-Token":$scope.token
            },
            params: $scope.datasave
        }).
        then(function(response){
            $scope.success="Data saved!";
            $scope.loadData();
        }, function(response){
            $scope.alert=response.data.message;
        });
    };

    $scope.editwg = function(id){
        $http({
            method: "put",
            url: 'api/wg2/'+id,
            headers: {
                "Content-Type":"application/json",
                "X-Auth-Token":$scope.token
            },
            params: $scope.selected
        }).
        then(function(response){
            $scope.success="Data updated!";
            $scope.loadData();
            $scope.selected =[];
        }, function(response){
            $scope.alert=response.data.message;
        });
    }

    // $scope.selectboss = function(){
    //     $scope.fd.agent_id = "";
    // };

    // $scope.selectagent = function(){
    //     $scope.fd.boss_id = "";
    // };

    // $scope.cancel = function() {
    //     $location.path("app/wg2");
    // };

    $scope.sortType     = 'date'; // set the default sort type
    $scope.sortReverse  = true;  // set the default sort order
    $scope.searchList   = '';     // set the default search/filter term
    $scope.page			= 1;

    $scope.dtOptions = DTOptionsBuilder.newOptions();
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4).notSortable()
    ];
    $scope.dtInstance = {};

    $scope.dataTable = [];

    $scope.loadData = function() {
        if ($scope.sortReverse == true) {
            $scope.sort_method = 'desc';
        } else {
            $scope.sort_method = 'asc';
        }

        if ($scope.date) {
            $scope.date = $filter('date')($scope.date, 'yyyy-MM-dd');
        } else {
            $scope.date = '';
        }

        $http({
            method: "get",
            url: "api/wg2?sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&page="+$scope.page+"&date="+$scope.date,
            headers: {
                "Content-Type":"application/json",
                "X-Auth-Token":$scope.token
            }
        }).
        then(function(response){
            $scope.dataTable = response.data;
        }, function(response){
            $scope.alert=response.data.message;
        });
    };

    $scope.sort = function(sortType) {
        $scope.sortType = sortType;
        $scope.sortReverse = !$scope.sortReverse;
        $scope.loadData();
    };

    $scope.showPage=function(en){
        return (en>=$scope.dataTable.current_page && en<=$scope.dataTable.current_page+2) || (en<=$scope.dataTable.current_page && (en>=$scope.dataTable.current_page-2 || en>=$scope.dataTable.last_page-2));
    };

    $scope.isLastPage=function(){
        return $scope.dataTable.current_page==$scope.dataTable.last_page;
    };

    $scope.paging=function(page){
        if ($scope.dataTable.last_page>0) {
            $scope.page=page;
            $scope.loadData();
        }
    };

    $scope.selected={};
    $scope.editForm = function(row){
        $scope.selected = angular.copy(row);
		$scope.selected.account_id = parseInt(($scope.selected.account_id?$scope.selected.account_id:0));
        setTimeout(function(){$("#selected_amount").focus()},1);
    }

    $scope.getTemplate = function (row) {
        if (row.id === $scope.selected.id){
            return 'edit';
        }else{
            return 'display';
        }
    }
    // $scope.getedit = function() {
    //     if(!$routeParams.id){
    //         return false;
    //     }
    //     $http({
    //         method: "get",
    //         url: "api/wg2/"+$routeParams.id,
    //         headers: {
    //             "Content-Type":"application/json",
    //             "X-Auth-Token":$scope.token
    //         }
    //     }).
    //     then(function(response){
    //         response.data.data.payment_method_id = parseInt(response.data.data.payment_method_id);
    //         response.data.data.vendor_id = parseInt(response.data.data.vendor_id);
    //         response.data.data.customer_id = parseInt(response.data.data.customer_id);
    //         $scope.fd = response.data.data;
    //         $scope.fd.member_id = response.data.data.agent_id | response.data.data.boss_id;
    //         $scope.fd.member_id = parseInt($scope.fd.member_id);
    //         $scope.setEntity();
    //     }, function(response){
    //         $scope.alert=response.data.message;
    //     });
    // };

    $scope.delete = function(id) {
        console.log(id);
        $http({method: "delete", url: 'api/wg2/'+id, headers: {
            "Content-Type":"application/json",
            "X-Auth-Token":$scope.token
        }
        }).
        then(function(response){
            $scope.success="Data deleted!";
            $scope.loadData();
        }, function(response){
            $scope.alert=response.data.message;
        });
    };

    $scope.showAlert=function(){
        return $scope.alert!="" && $scope.alert!="success";
    };

});
