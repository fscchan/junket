main.registerCtrl("claimCommissionController",function($filter,$scope,$location,$http,$routeParams,DTOptionsBuilder, DTColumnDefBuilder){
	$scope.success="";
	$scope.alert = "";
	$scope.fd=[];
	$scope.fd.year=new Date().getFullYear();
	$scope.fd.month=new Date().getMonth();

	$scope.add=function(year,month){
		$location.path("app/claim_commission/create");
	}

	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd/M/yy",
		yearRange: '1900:-0',
    }

	$scope.initData=function(){
		$scope.fd.date=$filter('date')(new Date(), 'yyyy-MM-dd');
		$scope.fd.value=0;
	}
	
	$scope.monthOption=[
		{id:1, name: "January"},
		{id:2, name: "Febuary"},
		{id:3, name: "March"},
		{id:4, name: "April"},
		{id:5, name: "May"},
		{id:6, name: "June"},
		{id:7, name: "July"},
		{id:8, name: "August"},
		{id:9, name: "September"},
		{id:10, name: "October"},
		{id:11, name: "November"},
		{id:12, name: "December"}
	];

	$scope.vendorOption=[];
	$scope.getVendor = function() {
		$http({
			method: "get",
			url: "api/vendors",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.vendorOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.accountOption=[];
	$scope.getAccount = function() {
		$http({
			method: "get",
			url: "api/account",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		    }
		}).
		then(function(response){
			$scope.accountOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.save = function() {
 		$scope.fd.user_id = $scope.user.id;
 		if (!$scope.fd.id) {
 			$http({
				method: "post",
				url: 'api/claim_commission',
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.success="Data saved!";
				$scope.cancel();
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}

		if($scope.fd.id){
 			$http({
				method: "put",
				url: 'api/claim_commission/'+$scope.fd.id,
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.success="Data updated!";
				$scope.cancel();
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}
	}

	$scope.cancel = function() {
		$location.path("app/claim_commission");
	}

    $scope.sortType     = 'date'; // set the default sort type
	$scope.sortReverse  = true;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;

    $scope.dtOptions = DTOptionsBuilder.newOptions();
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5),
        DTColumnDefBuilder.newColumnDef(6).notSortable()
    ];
    $scope.dtInstance = {};

	$scope.dataTable = [];

	$scope.loadData = function() {
		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}

		if ($scope.date) {
			$scope.date = $filter('date')($scope.date, 'yyyy-MM-dd');
		} else {
			$scope.date = '';
		}

		$http({
			method: "get",
			url: "api/claim_commission/all?sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&page="+$scope.page+"&date="+$scope.date,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.dataTable = response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.listDetail = [];

	$scope.loadDetail = function() {
		if($scope.fd.year.toString().length<4){
			return false;
		}
		
		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}

		if ($scope.date) {
			$scope.date = $filter('date')($scope.date, 'yyyy-MM-dd');
		} else {
			$scope.date = '';
		}

		$http({
			method: "get",
			url: "api/claim_commission/detail?sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&page="+$scope.page
				+"&year="+$scope.fd.year+"&month="+$scope.fd.month,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.listDetail = response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.sort = function(sortType) {
		$scope.sortType = sortType;
		$scope.sortReverse = !$scope.sortReverse;
		$scope.loadData();
	}

	$scope.showPage=function(en){
		return (en>=$scope.dataTable.current_page && en<=$scope.dataTable.current_page+2) || (en<=$scope.dataTable.current_page && (en>=$scope.dataTable.current_page-2 || en>=$scope.dataTable.last_page-2));
	}

	$scope.isLastPage=function(){
		return $scope.dataTable.current_page==$scope.dataTable.last_page;
	}

	$scope.paging=function(page){
		if ($scope.dataTable.last_page>0) {
			$scope.page=page;
			$scope.loadData();
		}
	}

	$scope.edit=function(id){
		$location.path("app/claim_commission/edit/"+id);
	}

	$scope.getEdit = function() {
		if(!$routeParams.id){
			return false;
		}
		$http({
			method: "get",
			url: "api/claim_commission/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		    }
		}).
		then(function(response){
			response.data.data.payment_method_id = response.data.data.payment_method_id?parseInt(response.data.data.payment_method_id):null;
			response.data.data.vendor_id = parseInt(response.data.data.vendor_id);
			response.data.data.account_id = parseInt(response.data.data.account_id);
			response.data.data.customer_id = response.data.data.customer_id?parseInt(response.data.data.customer_id):null;
			$scope.fd = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.delete = function(id) {
		console.log(id);
		$http({method: "delete", url: 'api/claim_commission/'+id, headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.success="Data deleted!";
			$scope.loadData();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.showAlert=function(){
			return $scope.alert!="" && $scope.alert!="success";
	}
});
