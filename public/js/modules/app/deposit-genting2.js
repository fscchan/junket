main.registerCtrl("depositGenting2Controller",function($scope,$location,$http,DTOptionsBuilder, DTColumnDefBuilder,$routeParams){
	$scope.alert = "";
	$scope.success = "";
	$scope.fd=[];
	
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd/M/yy",
		yearRange: '1900:-0',
    };

	$scope.add=function(){
		$location.path("app/deposit_gentings2/create");
	}

    $scope.initData = function() {
		$scope.fd.date = new Date();
		$scope.fd.user_id = $scope.user.id;
		$scope.fd.username = $scope.user.name;
	};
	
	$scope.allowDeposit=false;
	$scope.setAllowDeposit=function(){
		$http({
			method: "get",
			url: "api/account/genting",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			data=response.data.data;
			$scope.allowDeposit=(data.nn_chips>0 || data.junket_chips>0 || data.cash_chips>0 
			  || data.cash_real>0 || data.balance>0)?true:false;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	$scope.setAllowDeposit();
	
	$scope.save=function() {
 		if (!$scope.fd.id) {
 			$http({
				method: "post",
				url: 'api/deposit_genting2',
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.cancel();
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}

		if($scope.fd.id){
 			$http({
				method: "put",
				url: 'api/deposit_genting2/'+$scope.fd.id,
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.cancel();
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}
	}

	$scope.cancel=function(){
		$location.path("app/deposit_gentings2");
	}

    $scope.dtOptions = DTOptionsBuilder.newOptions();
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5),
        DTColumnDefBuilder.newColumnDef(6),
        DTColumnDefBuilder.newColumnDef(7),
        DTColumnDefBuilder.newColumnDef(8),
        DTColumnDefBuilder.newColumnDef(9).notSortable()
    ];
    $scope.dtInstance = {};

	$scope.search='';
	
    $scope.sortType     = 'date'; // set the default sort type
	$scope.sortReverse  = false;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;

	$scope.listData = [];
	$scope.loadData=function() {

		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}

		$http({
			method: "get",
			url: "api/deposit_genting2?sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&page="+$scope.page+"&search="+$scope.search,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.listData = response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.sort = function(sortType) {
		$scope.sortType = sortType;
		$scope.sortReverse = !$scope.sortReverse;
		$scope.loadData();
	}

	$scope.showPage=function(en){
		return (en>=$scope.listData.current_page && en<=$scope.listData.current_page+2) || (en<=$scope.listData.current_page && (en>=$scope.listData.current_page-2 || en>=$scope.listData.last_page-2));
	}

	$scope.isLastPage=function(){
		return $scope.listData.current_page==$scope.listData.last_page;
	}

	$scope.paging=function(page){
		if ($scope.listData.last_page>0) {
			$scope.page=page;
			$scope.loadData();
		}
	}
	
    $scope.edit=function(id) {
		$location.path("app/deposit_gentings2/edit/"+id);
    }

	$scope.showStatus=false;
	$scope.getEdit=function() {
		if(!$routeParams.id){
			return false;
		}

		if($routeParams.show){
			$scope.showStatus=true;
		}

		$http({
			method: "get",
			url: "api/deposit_genting2/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.fd = response.data.data;
			$scope.fd.user_id = $scope.user.id;
			$scope.fd.username = $scope.user.name;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.delete=function(id) {
		$http({
			method: "delete",
			url: 'api/deposit_genting2/'+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.success="Data deleted!";
			$scope.loadData();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
});
