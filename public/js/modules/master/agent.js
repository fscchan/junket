main.registerCtrl("agentController",function($scope,$location,$http,DTOptionsBuilder, DTColumnDefBuilder,$routeParams){
	$scope.alert = "";
	$scope.success = "";
	$scope.fd=[];

	$scope.add=function(){
		$location.path("master/agents/create");
	}

	$scope.fd.phone_code='+60';
	$scope.phoneCodeOption=[];
	$scope.getPhoneCode=function(){
		$http.get('public/data/phone.json', {}).then(function(response){$scope.phoneCodeOption=response.data.countries}, function(response){});
	}
	
	$scope.agentCompanyOption=[];
	$scope.getAgentCompany=function(){
		$http({
			method: "get",
			url: "api/agent_company",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.agentCompanyOption = response.data.data;

		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.save=function() {
 		if (!$scope.fd.id) {
 			$http({
				method: "post",
				url: 'api/agent',
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.cancel();
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}

		if($scope.fd.id){
 			$http({
				method: "put",
				url: 'api/agent/'+$scope.fd.id,
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.cancel();
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}
	}

	$scope.cancel=function(){
		$location.path("master/agents");
	}

    $scope.dtOptions = DTOptionsBuilder.newOptions();
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5),
        DTColumnDefBuilder.newColumnDef(6),
        DTColumnDefBuilder.newColumnDef(7),
        DTColumnDefBuilder.newColumnDef(8),
        DTColumnDefBuilder.newColumnDef(9).notSortable()
    ];
    $scope.dtInstance = {};

	$scope.search='';
	
    $scope.sortType     = 'name'; // set the default sort type
	$scope.sortReverse  = false;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;

	$scope.listData = [];
	$scope.loadData=function() {

		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}

		$http({
			method: "get",
			url: "api/agent/all?sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&page="+$scope.page+"&search="+$scope.search,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.listData = response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.sort = function(sortType) {
		$scope.sortType = sortType;
		$scope.sortReverse = !$scope.sortReverse;
		$scope.loadData();
	}

	$scope.showPage=function(en){
		return (en>=$scope.listData.current_page && en<=$scope.listData.current_page+2) || (en<=$scope.listData.current_page && (en>=$scope.listData.current_page-2 || en>=$scope.listData.last_page-2));
	}

	$scope.isLastPage=function(){
		return $scope.listData.current_page==$scope.listData.last_page;
	}

	$scope.paging=function(page){
		if ($scope.listData.last_page>0) {
			$scope.page=page;
			$scope.loadData();
		}
	}
	
    $scope.edit=function(id) {
		$location.path("master/agents/edit/"+id);
    }

	$scope.showStatus=false;
	$scope.getEdit=function() {
		if(!$routeParams.id){
			return false;
		}

		if($routeParams.show){
			$scope.showStatus=true;
		}

		$http({
			method: "get",
			url: "api/agent/"+$routeParams.id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.fd = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.delete=function(id) {
		$http({
			method: "delete",
			url: 'api/agent/'+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.success="Data deleted!";
			$scope.loadData();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
});
