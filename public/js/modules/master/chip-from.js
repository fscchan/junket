main.registerCtrl("chipFromRunnerController",function($scope,$filter,$location,$http,$routeParams,DTOptionsBuilder, DTColumnDefBuilder){
	$scope.alert = "";
	$scope.success="";
	$scope.fd=[];
	$scope.permissionCreate = false;

	$scope.checkbutton=function() {
		$http({
			method: "get",
			url: "api/users/checkPermission?action=create&menu=chip_from_runner_or_assist",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.permissionCreate = response.data.permission;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.add=function(){
      $location.path("master/chip_from_runner/create");
	}

	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd/M/yy",
		yearRange: '1900:-0',
	};
	
	$scope.initData=function(){
		$scope.fd.date=$filter('date')(new Date(), 'yyyy-MM-dd');
		$scope.fd.nn_chips=0;
		$scope.fd.cash_chips=0;
		$scope.fd.cash_real=0;
		$scope.fd.nn_chips_out=0;
		$scope.fd.shift_from="Morning";
		$scope.fd.remark="";
	}

	$scope.radioFrom = function() {
		if ($scope.fd.radio_from == "runner") {
			$scope.getRunner(6);
		} else if ($scope.fd.radio_from == "assistant") {
			$scope.getRunner(5);
		}
	}
	
	$scope.outstandingData=[];
	$scope.loadOutstanding=function() {
		$scope.fd.nn_chips_out=0;
		if($routeParams.id){
			return false;
		}
		$http({
			method: "get", 
			url: "api/transaction_current?user_id="+$scope.fd.from_user_id, 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		    }
		}).
		then(function(response){
			$scope.outstandingData = response.data.data;
			for(inde=0;inde<$scope.outstandingData.length;inde++){
				$scope.outstandingData[inde].closing_nn_chips=$scope.outstandingData[inde].closing_nn_chips?
				 parseFloat($scope.outstandingData[inde].closing_nn_chips):0;
				$scope.outstandingData[inde].closing_cash_chips=$scope.outstandingData[inde].closing_cash_chips?
				 parseFloat($scope.outstandingData[inde].closing_cash_chips):0;
				$scope.fd.nn_chips_out+=parseFloat($scope.outstandingData[inde].credit)
				 -parseFloat($scope.outstandingData[inde].closing_nn_chips)
				 -parseFloat($scope.outstandingData[inde].closing_cash_chips);
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
		
		$http({
			method: "get", 
			url: "api/shift_history/current?menu_name=sell_chip&user_id="+$scope.fd.from_user_id, 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		    }
		}).
		then(function(response){
			$scope.fd.balance = response.data.data?response.data.data:0;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.runnerOption=[];
	$scope.getRunner = function(id) {
		$http({
			method: "get", 
			url: "api/users/"+id+"/find_by_role", 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		    }
		}).
		then(function(response){
			$scope.runnerOption = response.data.data;
			if(!$scope.fd.from_user_id){
				$scope.fd.from_user_id = $scope.user.id;
				if($scope.user.roles!='Administrator'){
					$scope.loadOutstanding();
				}
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.userOption = [];
	$scope.getManager = function() {
		$http({
			method: "get", 
			url: "api/users/4/find_by_role", 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.managerOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.save = function() {
		if($scope.fd.shift_from=='Morning'){
			$scope.fd.shift_to = 'Night';
		}else{
			$scope.fd.shift_to = 'Morning';
		}
		$scope.fd.menu_name = "chip_from_runner_or_assist";

		if (!$scope.fd.id) {
			$http({
				method: "post", 
				url: 'api/shift_history/save', 
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				}, 
				params: $scope.fd
			}).
			then(function(response){
				$scope.success="Data saved!";
				$scope.cancel();
			}, function(response){
				$scope.alert=response.data.message;
			});
		}
		
		if($scope.fd.id){
			$http({
				method: "put", 
				url: 'api/shift_history/'+$scope.fd.id+'/update', 
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				}, 
				params: $scope.fd
			}).
			then(function(response){
				$scope.success="Data updated!";
				$scope.cancel();
			}, function(response){
				$scope.alert=response.data.message;

			});
		}
	}

	$scope.cancel = function() {
		$location.path("master/chip_from_runner");
	}
	
    $scope.sortType     = 'date'; // set the default sort type
	$scope.sortReverse  = true;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;

	$scope.dataTable = [];
	$scope.loadData = function() {

		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}

		if ($scope.searchDate) {
			$scope.searchDate = $filter('date')($scope.searchDate, 'yyyy-MM-dd');
		} else {
			$scope.searchDate = '';
		}

		$http({
			method: "get", 
			url: "api/shift_history/all?menu_name=chip_from_runner_or_assist&sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&page="+$scope.page+"&date="+$scope.searchDate, 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.dataTable = response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.sort = function(sortType) {
		$scope.sortType = sortType;
		$scope.sortReverse = !$scope.sortReverse;
		$scope.loadData();
	}

	$scope.showPage=function(en){
		return (en>=$scope.dataTable.current_page && en<=$scope.dataTable.current_page+2) || (en<=$scope.dataTable.current_page && (en>=$scope.dataTable.current_page-2 || en>=$scope.dataTable.last_page-2));
	}

	$scope.isLastPage=function(){
		return $scope.dataTable.current_page==$scope.dataTable.last_page;
	}

	$scope.paging=function(page){
		if ($scope.dataTable.last_page>0) {
			$scope.page=page;
			$scope.loadData();
		}
	}
	
	$scope.edit=function(id){
		$location.path("master/chip_from_runner/edit/"+id);
	}

	$scope.getEdit = function() {
		if(!$routeParams.id){
			return false;
		}
		
		$http({
			method: "get", 
			url: "api/shift_history/"+$routeParams.id+"/get", headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		    }
		}).
		then(function(response){
			$scope.fd = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.approval = function(id) {
		$http({
			method: "put", 
			url: 'api/shift_history/'+id+'/accepted', 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, 
			params: {
				menu_name:"chip_from_runner_or_assist"
			}
		}).
		then(function(response){
			$scope.dataTable = response.data.data;
			$scope.loadData();
			$scope.success="Chip Accepted!";
		}, function(response){
			$scope.alert=response.data.message;
			$scope.loadData();
		});
	}

	$scope.deleteShift = function(id) {
		console.log(id);
		$http({method: "delete", url: 'api/shift_history/'+id+'/delete?menu_name=chip_from_runner_or_assist', headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.dataTable = response.data.data;
			$scope.get_shift_histories();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.statusEdit = false;
	$scope.addcustTypes = function() {
		$scope.statusEdit = false;
		$scope.fd = [];
	}

	$scope.showAlert=function(){
		return $scope.alert!="" && $scope.alert!="success";
	}
});
