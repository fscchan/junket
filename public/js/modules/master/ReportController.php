<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Transaction;
use App\TransactionDetail;
use App\TransactionRolling;
use App\ShiftHistory;
use App\Customer;
use App\TransactionBank;
use App\PaymentMethod;
use App\Bank;
use App\Expense;
use App\Purchase;
use App\AccountConfig;
use App\UserHasAccount;
use Validator;
use App\UserActivity;
use View;
use DB;

require_once base_path().'/vendor/mpdf/mpdf/mpdf.php';

class ReportController extends Controller
{

    public function showRollingPerTransaction(Request $request, $id)
    {
		$token = $request->input('token');
        $user = User::where('token',$token)->first();
        $cek = check_auth($user,'read transactions');
        if($cek['result']==1){
			$master = Transaction::find($id);

			$details = TransactionDetail::where('transaction_id', $id)
				->where('flags', '=', 'role')
				->get();

			$mpdf = new \mPDF();
			$html = View::make('pdf.rolling',['master'=>$master,'details'=>$details])->render();
			// exit($html);
			$mpdf->WriteHTML($html);
			$mpdf->Output();
			exit;
		}else{
            return error_unauthorized();
		}
    }

    public function showRolling2PerTransaction(Request $request, $id)
    {
		$token = $request->input('token');
        $user = User::where('token',$token)->first();
        $cek = check_auth($user,'read transactions');
        if($cek['result']==1){
			$master = Transaction::find($id);

			$details = TransactionRolling::where('transaction_id', $id)->get();

			$mpdf = new \mPDF();
			$html = View::make('pdf.rolling2',['master'=>$master,'details'=>$details])->render();
			$mpdf->WriteHTML($html);
			$mpdf->Output();
			exit;
			// echo $html;
			exit;
		}else{
            return error_unauthorized();
		}
    }

    public function showCollectionPerTransaction(Request $request, $id)
    {
		$token = $request->input('token');
        $user = User::where('token',$token)->first();
        $cek = check_auth($user,'read transactions');
        if($cek['result']==1){
			$master = Transaction::find($id);

			$details = TransactionDetail::where('transaction_id', $id)
				->where('flags','=','credit')
				->orderBy('flags','asc')
				->get();
				
			$mpdf = new \mPDF();
			$html = View::make('pdf.collection',['master'=>$master,'details'=>$details])->render();
			// exit($html);
			$mpdf->WriteHTML($html);
			$mpdf->Output();
			exit;
		}else{
            return error_unauthorized();
		}
    }

	public function showRunnerPerShift(Request $request,$id)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read transactions');
		if($cek['result']==1){
			$master = Transaction::find($id);

			$details = DB::table(DB::raw('(select u.name, sh.nn_chips, "buy" as pos, sh.created_at
				from shift_histories sh
				join users u on u.id=sh.from_user_id
				where sh.to_user_id="'.$master->user_id.'" and sh.date="'.date('Y-m-d',strtotime($master->date)).'" and status=1
				union all
				select u.name, sh.nn_chips, "sell" as pos, sh.created_at
				from shift_histories sh
				join users u on u.id=sh.to_user_id
				where sh.from_user_id="'.$master->user_id.'" and sh.date="'.date('Y-m-d',strtotime($master->date)).'" and sh.updated_at>"'.date('Y-m-d',strtotime($master->date.($master->shift=='Night'?' +1 days':''))).'"
				)as sub order by created_at'))->get();

			$mpdf = new \mPDF();
			$html = View::make('pdf.runner',['master'=>$master,'details'=>$details])->render();
			// exit($html);
			$mpdf->WriteHTML($html);
			$mpdf->Output();
			exit;
		}else{
			return error_unauthorized();
		}
	}

	public function showCollectionPerCustomer(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read transactions');

		$start_date = $request->input('start_date');
		$start_date = date('Y-m-d',strtotime($start_date));
		$end_date = $request->input('end_date');
		$end_date = date('Y-m-d',strtotime($end_date));
		if($cek['result']==1){
			$data = DB::table(DB::raw("(select concat(c.last_name,' ',c.first_name) as name,
					if(cd.transaction_id,'R','') as type, cd.amount_due, sum(ifnull(pdd.amount,0)) as settle,
					ifnull(cd.interest_amount,0) as ins, u.name as handle, pd.remark as remark, cd.date, pd.date as pay_date
				FROM customer_debts cd
				LEFT JOIN payment_debt_details pdd on pdd.customer_debt_id = cd.id
				LEFT JOIN payment_debts pd on pd.id = pdd.payment_debt_id
				JOIN customers c on c.id = cd.customer_id
				LEFT JOIN users u on u.id = pd.user_id
				where cd.deleted_at is null 
				 and (pdd.customer_debt_id = cd.id and pd.date >= '".$start_date."' and  pd.date <= '".$end_date."' and pd.deleted_at is null)
				 or (pdd.customer_debt_id is null and cd.date >= '".$start_date."' and  cd.date <= '".$end_date."')
				group by cd.id,pd.id
				order by cd.date asc
			) as sub"))->get();
			$data=['data'=>$data];

			return $data;
		}else{
			return error_unauthorized();
		}
	}

	public function reportCollectionPerCustomer(Request $request,$id)
	{
		// return $request;
		$data=$this->showCollectionPerCustomer($request,$id);
		// return $data['data'];

		$mpdf = new \mPDF();
		// $mpdf->AddPage('L', 'A4');
		$html = View::make('pdf.collectioncustomer',$data)->render();
		// exit($html);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}

	public function showFinalReport(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read transactions');

		$start_date = $request->input('start_date');
		$start_date = $start_date ? $start_date : '0000-00-00';
		$date = $request->input('date');
		if($cek['result']==1){
			$rolling = DB::table(DB::raw('(
				select name,type,sum(rolling)as rolling,sum(check_in_commision)as check_in_commision,
				 sum(closing_cash)as closing_cash,remarks from(
					select concat(ifnull(c.first_name," ")," ",ifnull(c.last_name," "))as name," " as type,
					 sum(cd.amount)as rolling,0 as check_in_commision,sum(cd.interest_amount)as closing_cash,
					 u.username as remarks
					from customer_debts cd
					join users u on u.id=cd.staff_id
					join customers c on c.id=cd.customer_id
					where date(cd.date)="'.$date.'" and c.deleted_at is null and cd.deleted_at is null 
					 and cd.transaction_id is null 
					group by c.id,u.username
					union all
					select concat(ifnull(c.first_name," ")," ",ifnull(c.last_name," "))as name," " as type,
					 sum(pdd.amount)as rolling,0 as check_in_commision,0 as closing_cash,
					 ifnull(u.username,a.name)as remarks
					from payment_debts pd
					join payment_debt_details pdd on pdd.payment_debt_id=pd.id
					left join users u on u.id=pd.staff_id
					left join accounts a on a.id=pd.bank_account_id
					join customer_debts cd on cd.id=pdd.customer_debt_id
					join customers c on c.id=cd.customer_id
					where date(cd.date)="'.$date.'" and c.deleted_at is null and cd.deleted_at is null 
					 and pd.deleted_at is null and cd.transaction_id is null 
					group by c.id,ifnull(u.username,a.name)
					union all
					select concat(ifnull(c.first_name," "),"R",ifnull(c.last_name," "))as name," " as type,
					 sum(cd.amount)as rolling,0 as check_in_commision,0 as closing_cash,u.username as remarks
					from customer_debts cd
					join users u on u.id=cd.staff_id
					join customers c on c.id=cd.customer_id
					where date(cd.date)="'.$date.'" and c.deleted_at is null and cd.deleted_at is null 
					 and cd.transaction_id is not null 
					group by c.id,u.username
					union all
					select concat(ifnull(c.first_name," "),"R",ifnull(c.last_name," "))as name," " as type,
					 sum(pdd.amount)as rolling,0 as check_in_commision,0 as closing_cash,ifnull(u.username,a.name)as remarks
					from payment_debts pd
					join payment_debt_details pdd on pdd.payment_debt_id=pd.id
					left join users u on u.id=pd.staff_id
					left join accounts a on a.id=pd.bank_account_id
					join customer_debts cd on cd.id=pdd.customer_debt_id
					join customers c on c.id=cd.customer_id
					where date(cd.date)="'.$date.'" and c.deleted_at is null and cd.deleted_at is null 
					 and pd.deleted_at is null and cd.transaction_id is not null 
					group by c.id,ifnull(u.username,a.name)
					union all
					select concat(ifnull(c.first_name," "),"J",ifnull(c.last_name," "))as name," " as type,
					 sum(cd.amount)as rolling,0 as check_in_commision,0 as closing_cash,u.username as remarks
					from customer_debts cd
					join users u on u.id=cd.staff_id
					join customers c on c.id=cd.customer_id
					where date(cd.date)="'.$date.'" and c.deleted_at is null and cd.deleted_at is null 
					 and c.agent_id is not null and cd.transaction_id is not null 
					group by c.id,u.username
					union all
					select concat(ifnull(c.first_name," "),"J",ifnull(c.last_name," "))as name," " as type,
					 sum(pdd.amount)as rolling,0 as check_in_commision,0 as closing_cash,ifnull(u.username,a.name)as remarks
					from payment_debts pd
					join payment_debt_details pdd on pdd.payment_debt_id=pd.id
					left join users u on u.id=pd.staff_id
					left join accounts a on a.id=pd.bank_account_id
					join customer_debts cd on cd.id=pdd.customer_debt_id
					join customers c on c.id=cd.customer_id
					where date(cd.date)="'.$date.'" and c.deleted_at is null and cd.deleted_at is null 
					 and c.agent_id is not null and pd.deleted_at is null and cd.transaction_id is not null 
					group by c.id,ifnull(u.username,a.name)
				)as sub group by name,type,remarks)as sub'))->get();

			$rolling_trans = DB::table(DB::raw('(select concat(ifnull(c.first_name,"")," ",ifnull(c.last_name,""))as name,
				t.commission,sum(td.rolling)*(if(1-t.commission>0,1-t.commission,0)) as total_commission,sum(td.rolling)as rolling,
				sum(td.rolling)*ifnull(t.commission,0)as closing_cash,u.username as remarks
				from transactions t
				join transaction_details td on td.transaction_id=t.id
				join customers c on c.id=t.customer_id
				join users u on u.id=t.user_id
				where (td.flags="role" or td.flags="open") and td.type=0 and date(t.date)="'.$date.'" and t.deleted_at is null 
				group by c.id order by c.first_name)as sub'))->get();

			$expense = Expense::where('date','=',$date)->get();

			$last_day=date('Y-m-d',strtotime($date.' -1 days'));
				
			$account_config=AccountConfig::first();
			$balance= DB::table(DB::raw('(select ifnull(debit,0)as debit,ifnull(credit,0)as credit,ifnull(expense,0)as expense,
					ifnull(spending_fee,0)as spending_fee,
					(ifnull(debit_last,0)-ifnull(credit_last,0)-ifnull(expense_last,0)-ifnull(spending_fee_last,0))as last_day_balance,
					(ifnull(debit,0)-ifnull(credit,0)-ifnull(expense,0)-ifnull(spending_fee,0))as today_balance,
					(ifnull(debit,0)-ifnull(credit,0)-ifnull(expense,0)-ifnull(spending_fee,0))-
					(ifnull(debit_last,0)-ifnull(credit_last,0)-ifnull(expense_last,0)-ifnull(spending_fee_last,0))as total_balance_in,
					ifnull(balance_earn,0)as balance_earn,ifnull(balance_earn,0)-ifnull(expense,0) as total_earn 
				from 
					(
					select sum(if(date="'.$date.'",amount,0))as debit,sum(if(date="'.$date.'",interest_amount,0))as balance_earn,
					 sum(if(date="'.$last_day.'",amount,0))as debit_last,sum(if(date="'.$last_day.'",interest_amount,0))as balance_earn_last
					from customer_debts
					where deleted_at is null and (date="'.$date.'" or date="'.$last_day.'")
					)as sub,
					(
					select sum(if(pd.date="'.$date.'",pdd.amount,0))as credit,sum(if(pd.date="'.$last_day.'",pdd.amount,0))as credit_last
					from payment_debts pd
					join payment_debt_details pdd on pdd.payment_debt_id=pd.id
					join customer_debts cd on cd.id=pdd.customer_debt_id
					where pd.deleted_at is null and (pd.date="'.$date.'" or pd.date="'.$last_day.'") and cd.deleted_at is null
					)as sub2,
					(
					select sum(if(date="'.$date.'",amount,0))as expense,sum(if(date="'.$last_day.'",amount,0))as expense_last
					from expenses
					where deleted_at is not null and (date="'.$date.'" or date="'.$last_day.'")
					)as sub3,
					(
					select sum(if(date(t.date)="'.$date.'",td.rolling*ifnull(t.commission,0),0))as spending_fee,
					 sum(if(date(t.date)="'.$last_day.'",td.rolling*ifnull(t.commission,0),0))as spending_fee_last
					from transactions t
					join transaction_details td on td.transaction_id=t.id
					join customers c on c.id=t.customer_id
					join users u on u.id=t.user_id
					where ( td.flags="open" or td.flags="role")and td.type=0 
					 and (date(t.date)="'.$date.'" or date(t.date)="'.$last_day.'") and t.deleted_at is null 
					)as sub4
				)as sub'))->get();
				
			$commission = DB::table(DB::raw('(select *, company_commission+spending_commission as total_commission,0 as collection_commission, company_commission as total_collection from
				(select sum(td.rolling)as transaction 
				from transaction_details td 
				join transactions t on t.id=td.transaction_id 
				where (td.flags="open" or td.flags="role") and td.type=0 and date(t.date)="'.$date.'" and t.deleted_at is null
				)as sub1,
				(select sum(td.rolling*ifnull(t.commission,0))as spending_commission 
				from transactions t 
				join transaction_details td on t.id=td.transaction_id 
				where (td.flags="open" or td.flags="role") and td.type=0 and date(date)="'.$date.'" and deleted_at is null
				)as sub2,
				(select sum(td.rolling*(if(1-t.commission>0,1-t.commission,0)))as company_commission 
				from transactions t 
				join transaction_details td on t.id=td.transaction_id 
				where (td.flags="open" or td.flags="role") and td.type=0 and date(date)="'.$date.'" and deleted_at is null
				)as sub3,
				(select sum(interest_amount)as balance_earn
				from customer_debts
				where deleted_at is null
				)as sub4)as sub'))->get();

			$data =['rolling'=>$rolling,'rolling_trans'=>$rolling_trans,'expense'=>$expense,'balance'=>$balance,'commission'=>$commission];
			
			return $data;
		}else{
			return error_unauthorized();
		}
	}

	public function printFinalReport(Request $request)
	{
		// return $request;
		$data=$this->showFinalReport($request);
		// return $data;

		$mpdf = new \mPDF();
		// $mpdf->AddPage('L', 'A4');
		$html = View::make('pdf.final',$data)->render();
		// exit($html);
		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}

	public function showDailyManager(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read transactions');

		$date=$request->input('date');
		$date=isset($date)?date('Y-m-d',strtotime($date)):'';
		$account_config=AccountConfig::first();
		if($cek['result']==1){
			$data=DB::table(DB::raw('(
			select distinct id,name from(
				select distinct u.id,u.name
				from users u
				join user_has_roles uhr on uhr.user_id=u.id and uhr.role_id=4
				join shift_histories sh on sh.from_user_id=u.id or sh.to_user_id=u.id
				where sh.deleted_at is null and date(sh.date)="'.$date.'" and sh.menu_name!="shift_transfer"
				union all
				select distinct u.id,u.name
				from users u
				join user_has_roles uhr on uhr.user_id=u.id and uhr.role_id=4
				join transaction_details td on td.user_id=u.id
				join transactions t on t.id=td.transaction_id
				where t.deleted_at is null and date(t.date)="'.$date.'" 
				 and (td.flags="1credit" or td.flags="credit")
				union all
				select distinct u.id,u.name
				from users u
				join user_has_roles uhr on uhr.user_id=u.id and uhr.role_id=4
				join transactions t on t.user_id=u.id
				where t.deleted_at is null and date(t.date)="'.$date.'" and t.status=0
			)as sub)as sub'))->get();

			foreach($data as $key=>$values_data){
				$data[$key]->details=DB::table(DB::raw('(
					select "start" as name,ifnull(sub.balance,ifnull(sub2.balance,0))as balancein, 0 balanceout,"" as remark
					from users u
					left join (
						select u.id as user_id, sum(ifnull(sh.nn_chips,0)+ifnull(sh.cash_chips,0)+ifnull(sh.cash_real,0))as balance
						from shift_histories sh
						join users u on u.id=sh.to_user_id
						where (date(sh.date)<="'.$date.'" and sh.shift_from="night" or date(sh.date)<"'.$date.'") and sh.deleted_at is null 
						 and sh.to_user_id="'.$values_data->id.'" and sh.menu_name="shift_transfer"
					)as sub on sub.user_id=u.id
					join (
						select sum(balance)as balance
						from (
							select aw.id,ifnull(aw2.balance,aw.balance)as balance
							from accounts aw
							left join (
								select parent_id,sum(balance)as balance
								from accounts 
								where parent_id is not null and deleted_at is not null
								group by parent_id
							)as aw2 on aw2.parent_id=aw.id 
						)as a
						join user_has_accounts uha on uha.account_id=a.id 
						where uha.user_id="'.$values_data->id.'"
					)as sub2
					where u.id="'.$values_data->id.'"
					union all
					select u.name,0 as balancein, (ifnull(sh.nn_chips,0)+ifnull(sh.cash_chips,0)+ifnull(sh.cash_real,0))as balanceout,sh.remark
					from shift_histories sh
					join users u on u.id=sh.to_user_id
					where date(sh.date)="'.$date.'" and sh.deleted_at is null 
					 and sh.from_user_id="'.$values_data->id.'" and sh.menu_name!="shift_transfer"
					union all
					select u.name,(ifnull(sh.nn_chips,0)+ifnull(sh.cash_chips,0)+ifnull(sh.cash_real,0))as balancein,0 as balanceout,sh.remark
					from shift_histories sh
					join users u on u.id=sh.from_user_id
					where date(sh.date)="'.$date.'" and sh.deleted_at is null 
					 and sh.to_user_id="'.$values_data->id.'" and sh.menu_name!="shift_transfer"
					union all
					select concat(c.first_name," ",ifnull(c.last_name,""))as name,0 balancein,sum(td.rolling)as balanceout,t.remarks
					from users u
					join transaction_details td on td.user_id=u.id
					join transactions t on t.id=td.transaction_id
					join customers c on c.id=t.customer_id
					where t.deleted_at is null and date(t.date)="'.$date.'" and td.user_id="'.$values_data->id.'"
					 and (td.flags="1credit" or td.flags="credit")
					group by t.id
					union all
					select concat(c.first_name," ",ifnull(c.last_name,""))as name,
					 sum(ifnull(t.closing_nn_chips,0)+ifnull(t.closing_cash_chips,0)+ifnull(t.closing_cash,0))as balancein,0 as balanceout,t.remarks
					from users u
					join transactions t on t.user_id=u.id
					join customers c on c.id=t.customer_id
					where t.deleted_at is null and date(t.date)="'.$date.'" and t.user_id="'.$values_data->id.'" and t.status=0
					group by t.id
				)as sub'))->get();

				$data[$key]->ceil=ceil(count($data[$key]->details)/19);
			}

			$data=['data'=>$data];

			return $data;
		}else{
			return error_unauthorized();
		}
	}

	public function reportDailyManager(Request $request)
	{
		// return $request;
		$data=$this->showDailyManager($request);
		$data['date']=date('d-m-Y',strtotime($request->input('date')));
		// exit(json_encode($data));

		$mpdf = new \mPDF();
		$mpdf->AddPage('L', 'A4');
		$html = View::make('pdf.manager',$data)->render();
		// exit($html);

		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}

	public function dashboardBalance(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		if(count($user)>0){
			$date=date('Y-m').'-01';
			$account_config=AccountConfig::first();
			$data=DB::table(DB::raw('(select sum(if(account_id='.$account_config['nn_chips'].',
				((if(debit>0,debit,if(credit<0,-credit,0)))-(if(credit>0,credit,if(debit<0,-debit,0)))),0))as nn_chips,
				sum(if(account_id='.$account_config['junket_chips'].',
				((if(debit>0,debit,if(credit<0,-credit,0)))-(if(credit>0,credit,if(debit<0,-debit,0)))),0))as junket_chips,
				sum(if(account_id='.$account_config['cash_chips'].',
				((if(debit>0,debit,if(credit<0,-credit,0)))-(if(credit>0,credit,if(debit<0,-debit,0)))),0))as cash_chips,
				sum(if(account_id='.$account_config['cash_real'].',
				((if(debit>0,debit,if(credit<0,-credit,0)))-(if(credit>0,credit,if(debit<0,-debit,0)))),0))as cash_real,
				ifnull(
					(select sum(td.rolling)as rolling 
					from transaction_details td 
					join transactions t on t.id=td.transaction_id 
					where t.date>="'.$date.'" and t.deleted_at is null and td.flags="role"),0
				)as rolling
				from journal_accounts ja
				join journal_account_details jad on jad.journal_id=ja.id
				join accounts a on a.id=jad.account_id
				where ja.deleted_at is null
				 and (a.id='.$account_config['nn_chips'].' or a.id='.$account_config['junket_chips'].'
				 or a.id='.$account_config['cash_chips'].' or a.id='.$account_config['cash_real'].')
				 and ja.id not in (select distinct jaw.id
					from journal_accounts jaw
					join journal_account_details jadw on jadw.journal_id=jaw.id
					join accounts aw on aw.parent_id=jadw.account_id or aw.id=jadw.account_id
					where (aw.parent_id=jadw.account_id and jaw.activity="account")
					union all
					select distinct jaw.id
					from journal_accounts jaw
					join journal_account_details jadw on jadw.journal_id=jaw.id
					join accounts aw on aw.id=jadw.account_id
					left join accounts aw2 on aw2.id=aw.parent_id
					where (aw.parent_id is null and aw.deleted_at is not null)
					 or (aw2.id=aw.parent_id and aw2.deleted_at is not null 
					   and aw.id not in ('.$account_config['nn_chips'].','.$account_config['junket_chips'].',
					   '.$account_config['cash_chips'].','.$account_config['cash_real'].',
					   '.$account_config['open_balance_asset'].')))
				)as sub'))->first();
			
			return ['data'=>$data];
		}else{
			return error_unauthorized();
		}
	}

	public function dashboardDaily(Request $request)
	{
		// dd($request->input('token'));
		// die();
		$startdate = $request->input('startdate');
		$enddate = $request->input('enddate');

		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read transactions');
		$data=array();
		if($cek['result']==1){

			$startTime = strtotime($startdate);
			$endTime = strtotime($enddate);
			$reportData = [];
			$log = [];
			$i = 0;
  				// Loop between timestamps, 24 hours at a time
			for ( $i = $startTime; $i <= $endTime; $i = $i + 86400 ) {

				$thisDate = date( 'Y-m-d', $i );
				$account_config=AccountConfig::first();
				$log[]=DB::table(DB::raw('(select "'.$thisDate.'" as date,
						ifnull(sum(if(jad.account_id='.$account_config['nn_chips'].',
						((if(debit>0,debit,if(credit<0,-credit,0)))-(if(credit>0,credit,if(debit<0,-debit,0)))),0)),0)as nn_chips,
						ifnull(sum(if(jad.account_id='.$account_config['junket_chips'].',
						((if(debit>0,debit,if(credit<0,-credit,0)))-(if(credit>0,credit,if(debit<0,-debit,0)))),0)),0)as cash_chips,
						ifnull(sum(if(jad.account_id='.$account_config['cash_chips'].',
						((if(debit>0,debit,if(credit<0,-credit,0)))-(if(credit>0,credit,if(debit<0,-debit,0)))),0)),0)as cash_chips,
						ifnull(sum(if(jad.account_id='.$account_config['cash_real'].',
						((if(debit>0,debit,if(credit<0,-credit,0)))-(if(credit>0,credit,if(debit<0,-debit,0)))),0)),0)as cash_real
					from journal_accounts ja
					join journal_account_details jad on jad.journal_id=ja.id
					where ja.date="'.$thisDate.'" and ja.deleted_at is null 
					and (jad.account_id='.$account_config['nn_chips'].' 
						or jad.account_id='.$account_config['junket_chips'].' 
						or jad.account_id='.$account_config['cash_chips'].' 
						or jad.account_id='.$account_config['cash_real'].')
					and ja.id not in (select distinct jaw.id
						from journal_accounts jaw
						join journal_account_details jadw on jadw.journal_id=jaw.id
						join accounts aw on aw.parent_id=jadw.account_id or aw.id=jadw.account_id
						where (aw.parent_id=jadw.account_id and jaw.activity="account")
						union all
						select distinct jaw.id
						from journal_accounts jaw
						join journal_account_details jadw on jadw.journal_id=jaw.id
						join accounts aw on aw.id=jadw.account_id
						left join accounts aw2 on aw2.id=aw.parent_id
						where (aw.parent_id is null and aw.deleted_at is not null)
						 or (aw2.id=aw.parent_id and aw2.deleted_at is not null 
						   and aw.id not in ('.$account_config['nn_chips'].','.$account_config['junket_chips'].',
						   '.$account_config['cash_chips'].','.$account_config['cash_real'].',
						   '.$account_config['open_balance_asset'].')))
					)as sub'))->first();
				
			}
			$log_customer=DB::table(DB::raw('(
				select concat(c.first_name," ",c.last_name)as customer_name,
					sum(if(td.flags="1credit" or td.flags="credit",td.rolling,0))as credit,
					sum(if(td.flags="role",td.rolling,0))as rolling
				from customers c
				join transactions t on t.customer_id=c.id
				join transaction_details td on td.transaction_id=t.id
				where DATE(t.date) >= "'.$startdate.'" and DATE(t.date) <= "'.$enddate.'" and t.deleted_at is null
				GROUP BY c.id
			)as sub'))->get();
		} else{
			return error_unauthorized();
		}

		if($log){
			return response()->json([
				"status" => "success",
				"data" => $log,
				"customer" => $log_customer
			],200);
		}else{
			return response()->json([
				"status" => "error",
				"data" => null,
				"customer" => null
			],403);
		}

	}

	public function dashboardWeekly(Request $request)
	{
			// dd($request->input('token'));
			// die();
			$startdate = $request->input('startdate');
			$enddate = $request->input('enddate');

			$token = $request->input('token');
			$user = User::where('token',$token)->first();
			$cek = check_auth($user,'read transactions');
			if($cek['result']==1){

  				$startTime = date('Y-m-d',strtotime($startdate));
  				$endTime = date('Y-m-d',strtotime($enddate));
				
				$log=DB::table(DB::raw('(select date,sum(chip)as chip,sum(rolling)as rolling
					from(select concat("Week ",(FLOOR((DAYOFMONTH(p.date)-1)/7)+1),", ",MONTHNAME(p.date)," ",YEAR(p.date))as date,p.value as chip,0 as rolling
					from purchases p where p.deleted_at is null and p.date>="'.$startTime.'" and p.date<="'.$endTime.'"
					union all
					select concat("Week ",(FLOOR((DAYOFMONTH(sh.date)-1)/7)+1),", ",MONTHNAME(sh.date)," ",YEAR(sh.date))as date,-sh.nn_chips as chip,0 as rolling
					from shift_histories sh where sh.deleted_at is null and sh.date>="'.$startTime.'" and sh.date<="'.$endTime.'"
					union all
					select concat("Week ",(FLOOR((DAYOFMONTH(t.date)-1)/7)+1),", ",MONTHNAME(t.date)," ",YEAR(t.date))as date,-td.rolling as chip,0 as rolling
					from transaction_details td
					join transactions t on t.id=td.transaction_id 
					where t.deleted_at is null and td.flags="credit" and t.date>="'.$startTime.'" and t.date<="'.$endTime.'"
					union all
					select concat("Week ",(FLOOR((DAYOFMONTH(t.date)-1)/7)+1),", ",MONTHNAME(t.date)," ",YEAR(t.date))as date,
					ifnull(t.closing_nn_chips,0)+ifnull(t.closing_cash_chips,0)+ifnull(t.closing_cash,0) as chip,0 as rolling
					from transactions t
					where t.deleted_at is null and t.date>="'.$startTime.'" and t.date<="'.$endTime.'"
					union all
					select concat("Week ",(FLOOR((DAYOFMONTH(t.date)-1)/7)+1),", ",MONTHNAME(t.date)," ",YEAR(t.date))as date,0 as chip,td.rolling as rolling
					from transaction_details td
					join transactions t on t.id=td.transaction_id 
					where t.deleted_at is null and td.flags="role" and t.date>="'.$startTime.'" and t.date<="'.$endTime.'")as sub group by date)as sub'))->get();
			} else{
				return error_unauthorized();
			}

      if($log){
          return response()->json([
              "status" => "success",
              "data" => $log
          ],200);
      }else{
          return response()->json([
              "status" => "error",
              "data" => null
          ],403);
      }

	}

	public function dashboardMonthly(Request $request)
	{

			$startdate = $request->input('startdate');
			$enddate = $request->input('enddate');

			$token = $request->input('token');
			$user = User::where('token',$token)->first();
			$cek = check_auth($user,'read transactions');
			if($cek['result']==1){



  				$startMonth = date('m', strtotime($startdate));
  				$endMonth = date('m', strtotime($enddate));
  				$startYear = date('Y', strtotime($startdate));
  				$endYear = date('Y', strtotime($enddate));

          $month = '';
          $year = '';
          $log = [];

          // Loop by year
  				for($year = $startYear; $year <= $endYear; $year++) {

            if($year != $endYear) {
              $endMonth = 12;
            } else {
              $endMonth = date('m', strtotime($enddate));
            }

            // Loop by month
            for($month = $startMonth; $month <= $endMonth; $month++) {

              $firstDate  = date('Y-m-d', strtotime($year.'-'.$month.'-'.'01'));
              $secondDate  = date('Y-m-d', strtotime($year.'-'.$month.'-'.'31'));

              $dateTransaction = DB::table('transactions')->whereBetween('date', array( $firstDate, $secondDate ))->whereNull('deleted_at')->value('date');
              $purchaseValue = DB::table('purchases')->whereBetween('date', array( $firstDate, $secondDate ))->whereNull('deleted_at')->sum('value');
              $shiftHistoryValue = DB::table('shift_histories')->whereBetween('date', array( $firstDate, $secondDate ))->whereNull('deleted_at')->sum('nn_chips');
			  $transactionCredit = DB::table('transaction_details')->join('transactions', 'transactions.id', '=', 'transaction_details.transaction_id')->whereDate('transactions.date', '=', $thisDate)->where('transaction_details.flags','=','credit')->whereNull('transactions.deleted_at')->sum('transaction_details.rolling');
			  $transactionClosingNNC = DB::table('transactions')->whereDate('date', '=', $thisDate)->whereNull('deleted_at')->sum('closing_nn_chips');
			  $transactionClosingCC = DB::table('transactions')->whereDate('date', '=', $thisDate)->whereNull('deleted_at')->sum('closing_cash_chips');
			  $transactionClosingC = DB::table('transactions')->whereDate('date', '=', $thisDate)->whereNull('deleted_at')->sum('closing_cash');
              $transactions = DB::table('transaction_details')->join('transactions', 'transactions.id', '=', 'transaction_details.transaction_id')->whereBetween('date', array( $firstDate, $secondDate ))->where('transaction_details.flags','=','role')->whereNull('transactions.deleted_at')->sum('transaction_details.rolling');

              // if($dateTransaction) {
                $log[] = array(
                          'date' => date('F Y', strtotime($firstDate)),
                          'chip' => (($purchaseValue?$purchaseValue:0) - ($shiftHistoryValue?$shiftHistoryValue:0) - ($transactionCredit?$transactionCredit:0) + ($transactionClosingNNC?$transactionClosingNNC:0) + ($transactionClosingNNC?$transactionClosingCC:0) + ($transactionClosingNNC?$transactionClosingC:0)),
                          'rolling' => $transactions
                      );
    					// }

            }

            if($year != $endYear){
              $startMonth = 01;
            }

				  }

			} else{
					return error_unauthorized();
			}

      if($log){
          return response()->json([
              "status" => "success",
              "data" => $log
          ],200);
      }else{
          return response()->json([
              "status" => "error",
              "data" => null
          ],403);
      }

	}

	public function dashboardMonthly2(Request $request){
		$data=DB::table(DB::raw('(select month_year as date,sum(chip)as chip,sum(rolling)as rolling
			from(select concat(monthname(p.date)," ",year(p.date))as month_year,sum(p.value)as chip,0 as rolling
			from purchases p where p.deleted_at is null group by month(p.date),year(p.date)
			union all
			select concat(monthname(sh.date)," ",year(sh.date))as month_year,-sum(sh.nn_chips)as chip,0 as rolling
			from shift_histories sh where sh.deleted_at is null group by month(sh.date),year(sh.date)
			union all
			select concat(monthname(t.date)," ",year(t.date))as month_year,0 as chip,sum(td.rolling)as rolling
			from transaction_details td
			join transactions t on t.id=td.transaction_id
			where t.deleted_at is null
			group by month(t.date),year(t.date))as sub group by month_year)as sub'))->get();
	}

    public function dashboardBalanceReport(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read transactions');
        $log = [];

        if($cek['result']==1){

			$purchaseValue = DB::table('purchases')->sum('value');
			$shiftHistoryValue = DB::table('shift_histories')->sum('nn_chips');
			$transactions = DB::table('transaction_details')->join('transactions', 'transactions.id', '=', 'transaction_details.transaction_id')->sum('transaction_details.rolling');

			$log = array(
				'balanceChip' => ($purchaseValue - $shiftHistoryValue),
				'balanceRolling' => $transactions
			);

		} else{
			return error_unauthorized();
		}

      if($data){
          return response()->json([
              "status" => "success",
              "data" => $data
          ],200);
      }else{
          return response()->json([
              "status" => "error",
              "data" => null
          ],403);
      }

	}

	public function showBankStatement(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read transaction_banks');

		$bank_id=$request->input('bank_id');
		$start_date=$request->input('start_date');
		$start_date=isset($start_date)?date('Y-m-d',strtotime($start_date)):'';
		$end_date=$request->input('end_date');
		$end_date=isset($end_date)?date('Y-m-d',strtotime($end_date)):'';
		if($cek['result']==1){
			$data=DB::table(DB::raw('(select "B/F" as ref_no, "'.$start_date.'" as date,"" as pic,"0000-00-00" as created_at, 0 as debit, 0 as credit,
				(select ifnull(sum(if(credit_debit="IN",amount,if(credit_debit="OUT",-amount,0))),0)as balance from transaction_banks where bank_id="'.$bank_id.'" and date<"'.$start_date.'" and deleted_at is null)as balance
				union all
				select tb.ref_no,tb.date,tb.pic,tb.created_at,
				if(tb.credit_debit="IN",amount,0)as debit,if(tb.credit_debit="OUT",amount,0)as credit,
				if(tb.credit_debit="IN",@i:=@i+amount,if(tb.credit_debit="OUT",@i:=@i-amount,@i:=@i))as balance
				from transaction_banks tb
				join (select @i:=0)as init
				where tb.bank_account_id="'.$bank_id.'" and tb.date>="'.$start_date.'" and tb.date<="'.$end_date.'" and tb.deleted_at is null)as sub order by date asc, created_at asc'))->get();

			$data=['data'=>$data];

			return $data;
		}else{
			return error_unauthorized();
		}
	}

	public function reportBankStatement(Request $request)
	{
		// return $request;
		$data=$this->showBankStatement($request);
		// exit(json_encode($data));
		$data['bank']=Bank::find($request->input('bank_id'));
		$data['start_date']=date('d-m-Y',strtotime($request->input('start_date')));
		$data['end_date']=date('d-m-Y',strtotime($request->input('end_date')));

		$mpdf = new \mPDF();
		$mpdf->AddPage('L', 'A4');
		$html = View::make('pdf.bank',$data)->render();
		// exit($html);

		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}

	public function dailyReport(Request $request)
	{
		$token = $request->input('token');
		$month = $request->input('month');
		$year = $request->input('year');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read daily_report');
		if($cek['result']==1){
			$month=isset($month)?$month:date('m');
			$year=isset($year)?$year:date('Y');
			$monthname=date('M',strtotime($year.'-'.$month.'-01'));
			
			$lastday=(int)date('t',strtotime($year.'-'.$month.'-01'));
			
			$query='(select sub.init,concat(sub.init," ","'.$monthname.'"," ",'.$year.')as date_,
				ifnull(ci.amount,0)as credit_in,ifnull(transactions.credit_out,0)as credit_out,
				ifnull(expense,0)as expense,ifnull(revenue,0)as revenue,
				(ifnull(revenue,0)-ifnull(expense,0))as net_revenue,@i:=@i+ifnull(cash,0) as cash_on_hand,
				ifnull(transactions.nn_rolling,0)as nn_rolling,ifnull(ci.commission,0)as check_in_commission,
				ifnull(transactions.rolling_commission,0)as rolling_commission,ifnull(net_profit,0)as net_profit
				from (select 1 as init ';
				
			for($inde=2;$inde<=$lastday;$inde++){
				$query.='union select '.$inde.' ';
			}
				
			$account_config=AccountConfig::first();
			$query.=')as sub
				join (
					select @i:=ifnull((select sum(jad.debit-jad.credit)as balance
					from journal_accounts ja 
					join journal_account_details jad on jad.journal_id=ja.id
					join accounts a on a.id=jad.account_id
					where ja.date<"'.$year.'-'.$month.'-01" and ja.deleted_at is null 
					and jad.account_id='.$account_config['cash_real'].'),0)
				)as cash_balance
				left join (
					select init,sum(if(account_id='.$account_config['cash_real'].',if(nature_type="debit",debit-credit,credit-debit),0))as cash,
					sum(if(account_id='.$account_config['expense'].' or account_id='.$account_config['expense_bank'].',
						if(nature_type="debit",debit-credit,credit-debit),0))as expense,
					sum(if(account_id='.$account_config['income'].',if(nature_type="debit",debit-credit,credit-debit),0))as revenue
					from (
						select day(ja.date)as init,sum(if(jad.debit>0,jad.debit,if(jad.credit<0,-jad.credit,0)))as debit,
						 sum(if(jad.credit>0,jad.credit,if(jad.debit<0,-jad.debit,0)))as credit,jad.account_id,a.nature_type
						from journal_accounts ja 
						join journal_account_details jad on jad.journal_id=ja.id
						join accounts a on a.id=jad.account_id
						where month(ja.date)='.$month.' and year(ja.date)='.$year.' 
						and ja.deleted_at is null
						and (jad.account_id='.$account_config['cash_real'].' or jad.account_id='.$account_config['expense'].' 
						or jad.account_id='.$account_config['expense_bank'].' or jad.account_id='.$account_config['income'].')
						group by jad.account_id,ja.date
					)as sub
					group by init
				)journal on journal.init=sub.init 
				left join (
					select init,sum(if(nature_type="debit",debit-credit,credit-debit))as net_profit
					from (
						select day(ja.date)as init,sum(if(jad.debit>0,jad.debit,if(jad.credit<0,-jad.credit,0)))as debit,
						sum(if(jad.credit>0,jad.credit,if(jad.debit<0,-jad.debit,0)))as credit,jad.account_id,a.nature_type
						from journal_accounts ja 
						join journal_account_details jad on jad.journal_id=ja.id
						join accounts a on a.id=jad.account_id
						where month(ja.date)='.$month.' and year(ja.date)='.$year.' 
						and ja.deleted_at is null
						and (jad.account_id!='.$account_config['nn_chips'].' and jad.account_id!='.$account_config['cash_chips'].' and jad.account_id!='.$account_config['junket_chips'].' and jad.account_id!='.$account_config['open_balance_asset'].')
						and ja.id not in (select distinct jaw.id
							from journal_accounts jaw
							join accounts aw on aw.parent_id=jaw.activity_id
							where jaw.activity="account" and aw.parent_id is not null)
						group by ja.date, a.id
					)as sub
					group by init
				)journal2 on journal2.init=sub.init 
				left join (
					select day(p.date)as init,sum(p.value)as amount,
						sum(p.value*ifnull(c.check_in_commission,0)/100)as commission
					from purchases p
					left join customers c on c.id=p.customer_id
					where month(p.date)='.$month.' and year(p.date)='.$year.' and p.deleted_at is null
					group by p.date
				)as ci on ci.init=sub.init
				left join (
					select init,date,sum(ifnull(credit_out,0))as credit_out,
						sum(ifnull(nn_rolling,0))as nn_rolling,sum(ifnull(check_in_commision,0))as rolling_commission
					from(
						select day(t.date)as init,t.date,
							sum(if(td.flags="1credit"||td.flags="credit",td.rolling,0))as credit_out,
							sum(if(td.flags="role",td.rolling,0))as nn_rolling,
							t.check_in_commision
						from transactions t
						join transaction_details td on td.transaction_id=t.id
						where month(t.date)='.$month.' and year(t.date)='.$year.' and deleted_at is null
						group by t.id
					)as sub
					group by init
				)as transactions on transactions.init=sub.init
			order by sub.init asc
			)as sub';
			
			$get_data=DB::table(DB::raw($query))->get();
			// return $get_data;
			$data['data']=$get_data;
			
			$html = View::make('pdf.daily',$data)->render();
			\header('Content-Type: application/vnd.ms-excel; charset=utf-8');
			\header('Content-Disposition: attachment; filename=daily-'.$month.'-'.$year.'.xls');
			\header('Expires: 0');
			\header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
			\header('Cache-Control: private',false);
			echo $html;
		}else{
			return error_unauthorized();
		}
	}

	public function finalCustomerShow(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read customer_report');

		$start_date=$request->input('start_date');
		$start_date=isset($start_date)?date('Y-m-d',strtotime($start_date)):'';
		$end_date=$request->input('end_date');
		$end_date=isset($end_date)?date('Y-m-d',strtotime($end_date)):'';

		if($cek['result']==1){
			$data=DB::table(DB::raw('(select date_format(date,"%M %Y")as date,
					c.grc_number,concat(c.first_name," ",c.last_name)as customer_name,c.check_in_commission,
					sum(p_amount)as p_amount,sum(check_in_commision)as check_in_commision,sum(cash_chip)as cash_chip,
					sum(cash_chip_commission)as cash_chip_commission,sum(check_in_commision+cash_chip_commission)as total
				from customers c
				join (
					select p.date,p.customer_id,sum(p.value)as p_amount,0 as check_in_commision,0 as cash_chip,
						0 as cash_chip_commission
					from purchases p
					where p.deleted_at is null and p.date>="'.$start_date.'" and p.date<="'.$end_date.'"
					group by p.id
					union all
					select t.date,t.customer_id,0 as p_amount,ifnull(t.check_in_commision,0)as check_in_commision,
						sum(ifnull(rt.cash_chip,0)*1.2/100)as cash_chip,sum(ifnull(rt.cash_chip,0)*1.2/100)as cash_chip_commission
					from transactions t
					left join rolling_tables rt on rt.transaction_id=t.id
					where t.deleted_at is null and date(t.date)>="'.$start_date.'" and date(t.date)<="'.$end_date.'"
					group by t.id
				)as sub on sub.customer_id=c.id
				group by month(date),year(date),c.id
			)as sub'))->get();

			$data=['data'=>$data];

			return $data;
		}else{
			return error_unauthorized();
		}
	}

	public function finalCustomerPdf(Request $request)
	{
		// return $request;
		$data=$this->finalCustomerShow($request);
		// exit(json_encode($data));
		$data['start_date']=date('d-m-Y',strtotime($request->input('start_date')));
		$data['end_date']=date('d-m-Y',strtotime($request->input('end_date')));

		$mpdf = new \mPDF();
		$mpdf->AddPage('L', 'A4');
		$html = View::make('pdf.customer',$data)->render();
		// exit($html);

		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}

	public function rollingShow(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read rolling_report');

		$start_date=$request->input('start_date');
		$start_date=isset($start_date)?date('Y-m-d',strtotime($start_date)):'';
		$end_date=$request->input('end_date');
		$end_date=isset($end_date)?date('Y-m-d',strtotime($end_date)):'';

		if($cek['result']==1){
			$data=DB::table(DB::raw('(select date(t.date)as date,concat(c.first_name," ",c.last_name)as customer_name,sum(td.rolling)as rolling, u.name as name,sum(rolling) as total_rollings,
					ifnull(t.check_in_commision,0)as check_in_commision , sum(check_in_commision) as total_commission
				from transactions t
				join customers c on c.id=t.customer_id
				join transaction_details td on td.transaction_id=t.id 
				join users u on u.id = td.user_id
				where date(t.date)>="'.$start_date.'" and date(t.date)<="'.$end_date.'" and t.deleted_at is null and (td.flags="role" or td.flags="open") and td.type=0
				group by c.id,t.date
			)as sub'))->get();

			$data=['data'=>$data];

			return $data;
		}else{
			return error_unauthorized();
		}
	}

	public function rollingPdf(Request $request)
	{
		// return $request;
		$data=$this->rollingShow($request);
		// exit(json_encode($data));
		$data['start_date']=date('d-m-Y',strtotime($request->input('start_date')));
		$data['end_date']=date('d-m-Y',strtotime($request->input('end_date')));

		$mpdf = new \mPDF();
		// $mpdf->AddPage('L', 'A4');
		$html = View::make('pdf.rollingreport',$data)->render();
		// exit($html);

		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}
	
	public function rolling2Show(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read rolling_report');

		$start_date=$request->input('start_date');
		$start_date=isset($start_date)?date('Y-m-d',strtotime($start_date)):'';
		$end_date=$request->input('end_date');
		$end_date=isset($end_date)?date('Y-m-d',strtotime($end_date)):'';

		if($cek['result']==1){
			$data=DB::table(DB::raw('(select id,detail_id,date,user_name,customer_name,rolling,
					insurance,odds,commission,room,gl,agent_share,status,pp,other,scenario
					from(
						select id,detail_id,date,customer_name,user_name,rolling,commission,room,insurance,odds,
						gl-(if(agent_id is not null,(gl*if(status="Win Only",10,agent_shared)/100),0))as gl,
						if(agent_id is not null,(gl*if(status="Win Only",10,agent_shared)/100),0)as agent_share,
						status,ifnull(pp,0)as pp,ifnull(other,0)as other,scenario
						from(
							select t.id,tr.id as detail_id,date(t.date)as date,concat(c.first_name," ",c.last_name)as customer_name,
							u.name as user_name,c.agent_id,tr.rolling,ifnull(t.commission,0)as commission,tr.room,tr.insurance,tr.odds,
							tr.agent_shared,tr.status,if(tr.scenario="1","Win","Lose")as scenario,
							if(tr.status="Win Only" and tr.scenario=1,
							 if(tr.room="Room 38",(tr.insurance*0.9),(if(tr.room="Maxims",(tr.insurance*0.7*0.9),(tr.insurance*0.8*0.9)))),
							 if(tr.room="Room 38",(tr.insurance*1),(if(tr.room="Maxims",(tr.insurance*0.7),(tr.insurance*0.8)))))as gl,
							if(tr.status="Win Only" and tr.scenario=0,0,
							 if(tr.room="Maxims",(tr.insurance*0.3),0))as pp,
							if(tr.status="Win Only" and tr.scenario=0,0,
							if(tr.room="Other Room",(tr.insurance*0.2),0))as other
							from transactions t
							join customers c on c.id=t.customer_id
							join users u on u.id = t.user_id
							join transaction_rollings tr on tr.transaction_id=t.id 
							where date(t.date)>="'.$start_date.'" and date(t.date)<="'.$end_date.'"
						)as sub
					)as sub
				)as sub'))->get();

			$data=['data'=>$data];

			return $data;
		}else{
			return error_unauthorized();
		}
	}

	public function rolling2Pdf(Request $request)
	{
		// return $request;
		$data = $this->rolling2Show($request);
		// exit(json_encode($data));
		$data['start_date']=date('d-m-Y',strtotime($request->input('start_date')));
		$data['end_date']=date('d-m-Y',strtotime($request->input('end_date')));

		$mpdf = new \mPDF();
		// $mpdf->AddPage('L', 'A4');
		$html = View::make('pdf.rolling2report',$data)->render();
		// exit($html);

		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}
	
	public function rolling3Show(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read rolling_report');

		$start_date=$request->input('start_date');
		$start_date=isset($start_date)?date('Y-m-d',strtotime($start_date)):'';
		$end_date=$request->input('end_date');
		$end_date=isset($end_date)?date('Y-m-d',strtotime($end_date)):'';

		if($cek['result']==1){
			$data=DB::table(DB::raw('(select date(t.date)as date,concat(c.first_name," ",c.last_name)as customer_name,sum(td.rolling)as rolling,u.name as user_name,
					ifnull(t.check_in_commision,0)as check_in_commision
				from transactions t
				join customers c on c.id=t.customer_id
				join users u on u.id = t.user_id
				join transaction_details td on td.transaction_id=t.id 
				where date(t.date)>="'.$start_date.'" and date(t.date)<="'.$end_date.'" and t.deleted_at is null and (td.flags="role" or td.flags="open") and td.type=1
				group by c.id,t.date
			)as sub'))->get();

			$data=['data'=>$data];

			return $data;
		}else{
			return error_unauthorized();
		}
	}

	public function rolling3Pdf(Request $request)
	{
		// return $request;
		$data=$this->rolling3Show($request);
		// exit(json_encode($data));
		$data['start_date']=date('d-m-Y',strtotime($request->input('start_date')));
		$data['end_date']=date('d-m-Y',strtotime($request->input('end_date')));

		$mpdf = new \mPDF();
		// $mpdf->AddPage('L', 'A4');
		$html = View::make('pdf.rolling3report',$data)->render();
		// exit($html);

		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}
	
	public function balanceGenting2(Request $request){
		$data=DB::table(DB::raw('(select sum(nn_chips)as nn_chips,
				sum(junket_chips)as junket_chips,
				sum(cash_chips)as cash_chips,
				sum(cash_real)as cash_real
			from(
				select nn_chips,junket_chips,cash_chips,cash_real
				from open_balance_agents
				union all
				select nn_chips,junket_chips,cash_chips,cash_real
				from open_balance_boss
				union all
				select if(type_chip="NN Chips",commission_payout,0)as nn_chips,
					if(type_chip="Junket NN Chips",commission_payout,0)as junket_chips,
					if(type_chip="Cash Chips",commission_payout,0)as cash_chips,
					if(type_chip="Cash Real",commission_payout,0)as cash_real
				from gentingtwo_commissions
				where deleted_at is null
				union all
				select if(type_chip="NN Chips",-amount,0)as nn_chips,
					if(type_chip="Junket NN Chips",-amount,0)as junket_chips,
					if(type_chip="Cash Chips",-amount,0)as cash_chips,
					if(type_chip="Cash Real",-amount,0)as cash_real
				from withdrawal_genting_2
			)as sub
		)as sub'))->first();
		
		echo json_encode($data);
	}	
	
	public function balanceAccountShow(Request $request){
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read end_day');

		$start_date=$request->input('start_date');
		$start_date=isset($start_date)?date('Y-m-d',strtotime($start_date)):'';
		$end_date=$request->input('end_date');
		$end_date=isset($end_date)?date('Y-m-d',strtotime($end_date)):'';

		$account_config=AccountConfig::first();
		if($cek['result']==1){
			$data=DB::table(DB::raw('(select 
				a.id, a.name, a.name as name2, at.name as account_type, 
				sum(jad.credit-jad.debit)as balance,max(ja.date)as date 
				from journal_accounts ja
				join (
					select 
						jad2.id,jad2.journal_id,ifnull(a2.parent_id,a2.id)as account_id,
						if(jad2.debit>0,jad2.debit,if(jad2.credit<0,-jad2.credit,0))as debit,
						if(jad2.credit>0,jad2.credit,if(jad2.debit<0,-jad2.debit,0))as credit,
						jad2.created_at,jad2.updated_at,jad2.entity,jad2.entity_id
					from journal_accounts ja2
					join journal_account_details jad2 on jad2.journal_id=ja2.id
					join accounts a2 on a2.id=jad2.account_id
					left join accounts a3 on a3.id=a2.parent_id
					where a2.deleted_at is null and ja2.date<="'.$end_date.'" and ja2.deleted_at is null
					and (a2.parent_id is null or (a3.id=a2.parent_id and a3.deleted_at is null))
					and ja2.id not in (
						select distinct jaw.id
						from journal_accounts jaw
						join accounts aw on aw.parent_id=jaw.activity_id
						where jaw.activity="account" and aw.parent_id is not null
					)
				)as jad on jad.journal_id=ja.id
				join accounts a on a.id=jad.account_id
				join (
					select distinct account_id from(
					select ifnull(a2.parent_id,a2.id)as account_id
					from journal_accounts ja2
					join journal_account_details jad2 on jad2.journal_id=ja2.id
					join accounts a2 on a2.id=jad2.account_id
					left join accounts a3 on a3.id=a2.parent_id
					where ja2.date>="'.$start_date.'" and ja2.date<="'.$end_date.'"
					and ja2.deleted_at is null and a2.deleted_at is null
					and (a2.parent_id is null or (a3.id=a2.parent_id and a3.deleted_at is null))
					and a2.id!='.$account_config['nn_chips'].'
					and a2.id!='.$account_config['cash_chips'].' 
					and a2.id!='.$account_config['junket_chips'].'
					and a2.id!='.$account_config['cash_real'].'
					and a2.id!='.$account_config['open_balance_asset'].'
					'.($user->roles[0]->id==2?'':'--').' and a2.id!='.$account_config['insurance'].'
					)as sub
				)as sub on sub.account_id=a.id
				left join account_types at on at.id=a.account_type_id
				where a.parent_id is null
				group by a.id
				order by a.name
			)as sub'))->get();
			
			return $data;
		}else{
			return error_unauthorized();
		}
	}
	
	public function journalShow(Request $request){
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read end_day');

		$start_date=$request->input('start_date');
		$start_date=isset($start_date)?date('Y-m-d',strtotime($start_date)):'';
		$end_date=$request->input('end_date');
		$end_date=isset($end_date)?date('Y-m-d',strtotime($end_date)):'';
		$id=$request->input('idi');
 
		$account_config=AccountConfig::first();
		if($cek['result']==1){
			$data=DB::table(DB::raw('(
				select 
				sub_row.urut,0 as inde,jad.parent_id as id,jad.account_id as id2,concat(a.name)as name,at.name as account_type,
				concat(a.name,sub_row.remark)as name2,
				(ifnull(if(sub_row.urut=0,sub.balance,sub.balance2),0))as balance,sub_row.date
				from (
					select distinct jad2.account_id,ifnull(a2.parent_id,a2.id)as parent_id
					from journal_accounts ja2
					join journal_account_details jad2 on jad2.journal_id=ja2.id
					join accounts a2 on a2.id=jad2.account_id
					where ja2.date>="'.$start_date.'" and ja2.date<="'.$end_date.'" and a2.deleted_at is null and ja2.deleted_at is null 
					 and (a2.id in ('.$id.') or (a2.parent_id is not null and a2.parent_id in ('.$id.')))
					 and ja2.id not in (select distinct jaw.id
						from journal_accounts jaw
						join accounts aw on aw.parent_id=jaw.activity_id
						where jaw.activity="account" and aw.parent_id is not null)
				)as jad
				join accounts a on a.id=jad.account_id
				join (
					select 0 as urut," B/F " as remark,"'.$start_date.'" as date
					union all
					select 2 as urut," Total " as remark,"'.$end_date.'" as date
				)as sub_row
				left join account_types at on at.id=a.account_type_id
				left join (
					select jad2.account_id,
					sum(if(ja2.date<"'.$start_date.'",if(jad2.credit>0,jad2.credit,if(jad2.debit<0,-jad2.debit,0))-
					if(jad2.debit>0,jad2.debit,if(jad2.credit<0,-jad2.credit,0)),0))as balance,
					sum(if(jad2.credit>0,jad2.credit,if(jad2.debit<0,-jad2.debit,0))-
					if(jad2.debit>0,jad2.debit,if(jad2.credit<0,-jad2.credit,0)))as balance2
					from journal_accounts ja2
					join journal_account_details jad2 on jad2.journal_id=ja2.id
					join accounts a2 on a2.id=jad2.account_id
					where ja2.date<="'.$end_date.'" and a2.deleted_at is null and ja2.deleted_at is null 
					 and (a2.id in ('.$id.') or (a2.parent_id is not null and a2.parent_id in ('.$id.')))
					 and ja2.id not in (select distinct jaw.id
						from journal_accounts jaw
						join accounts aw on aw.parent_id=jaw.activity_id
						where jaw.activity="account" and aw.parent_id is not null)
					group by jad2.account_id
				)as sub on sub.account_id=a.id
				union all
				select 1 as urut,inde,id,id2,name,account_type,name2,balance,date
				from(
					select distinct inde,id,id2,name,account_type,name2,balance,date
					from(
						select jad.id as inde,a.id,a.id as id2,a.name,at.name as account_type,ja.remark as name2,
						 if(jad.credit>0,jad.credit,if(jad.debit<0,-jad.debit,0))-
						 if(jad.debit>0,jad.debit,if(jad.credit<0,-jad.credit,0))as balance, ja.date
						from journal_accounts ja
						join journal_account_details jad on jad.journal_id=ja.id
						join accounts a on a.id=jad.account_id
						left join account_types at on at.id=a.account_type_id
						where ja.date>="'.$start_date.'" and ja.date<="'.$end_date.'" and a.deleted_at is null and ja.deleted_at is null 
						 and a.id in ('.$id.') and (jad.debit-jad.credit)<>0
						 and ja.id not in (select distinct jaw.id
						  from journal_accounts jaw
						  join accounts aw on aw.parent_id=jaw.activity_id
						  where jaw.activity="account" and aw.parent_id is not null)
						union all
						select jad.id as inde,a.parent_id as id,a.id as id2,a.name,at.name as account_type,ja.remark as name2,
						 if(jad.credit>0,jad.credit,if(jad.debit<0,-jad.debit,0))-
						 if(jad.debit>0,jad.debit,if(jad.credit<0,-jad.credit,0))as balance, ja.date
						from journal_accounts ja
						join journal_account_details jad on jad.journal_id=ja.id
						join accounts a on a.id=jad.account_id
						left join account_types at on at.id=a.account_type_id
						where ja.date>="'.$start_date.'" and ja.date<="'.$end_date.'" and a.deleted_at is null and ja.deleted_at is null 
						 and a.parent_id in ('.$id.') and (jad.debit-jad.credit)<>0
						 and ja.id not in (select distinct jaw.id
						  from journal_accounts jaw
						  join accounts aw on aw.parent_id=jaw.activity_id
						  where jaw.activity="account" and aw.parent_id is not null)
					)as sub
				)as sub
			)as sub order by id,id2,urut,inde,date'))->get();
			
			return $data;
		}else{
			return error_unauthorized();
		}
	}

	public function balanceAccountExport(Request $request)
	{
		// return $request;
		$data['data']=$this->balanceAccountShow($request);
		$data['journal']=$this->journalShow($request);
		// exit(json_encode($data));
		$data['start_date']=date('d-m-Y',strtotime($request->input('start_date')));
		$data['end_date']=date('d-m-Y',strtotime($request->input('end_date')));

		$mpdf = new \mPDF();
		// $mpdf->AddPage('L', 'A4');
		$html = View::make('pdf.balanceaccount',$data)->render();
		// exit($html);

		\header('Content-Type: application/vnd.ms-excel; charset=utf-8');
		\header('Content-Disposition: attachment; filename=end_day_report_'.$data['end_date'].'.xls');
		\header('Expires: 0');
		\header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		\header('Cache-Control: private',false);
		
		echo $html;
	}
	
	public function runnerShow(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read runner_report');

		$start_date=$request->input('start_date');
		$start_date=isset($start_date)?date('Y-m-d',strtotime($start_date)):'';
		$end_date=$request->input('end_date');
		$end_date=isset($end_date)?date('Y-m-d',strtotime($end_date)):'';

		if($cek['result']==1){
			$data=DB::table(DB::raw('(select co.user_id,co.date,u.name,co.shift,co.chips_out,ifnull(cr.chips_return,0)as chips_return
				from users u
				join user_has_roles uhr on uhr.user_id=u.id
				join (
					select sh.date, sh.to_user_id as user_id, sh.shift_to as shift, sum(sh.nn_chips)as chips_out
					from shift_histories sh
					where sh.deleted_at is null and sh.status=1 and sh.date>="'.$start_date.'" and sh.date<="'.$end_date.'"
					group by sh.date,sh.to_user_id,sh.shift_to
				)as co on co.user_id=u.id
				left join (
					select sh.date, sh.from_user_id as user_id, sh.shift_from as shift, sum(sh.nn_chips)as chips_return,
						sum(sh.cash_chips)as cash_chips,sum(cash_real)as cash_real
					from shift_histories sh
					where sh.deleted_at is null and sh.date>="'.$start_date.'" and sh.date<="'.$end_date.'"
					group by sh.date,sh.to_user_id,sh.shift_from
				)as cr on cr.user_id=co.user_id and cr.date=co.date
				where uhr.role_id=6
			)as sub'))->get();

			$data=['data'=>$data];

			return $data;
		}else{
			return error_unauthorized();
		}
	}

	public function runnerPdf(Request $request)
	{
		$user_id=$request->input('user_id');
		$date=$request->input('date');
		$shift=$request->input('shift');
		
		// return $request;
		$data['user']=User::find($user_id);
		$data['record']=DB::table(DB::raw('(select sh.date, u.name, sum(sh.nn_chips)as balance_in, 0 as balance_out, 0 as debt, sh.to_user_id as user_id, sh.shift_to as shift
			from shift_histories sh
			join users u on u.id=sh.from_user_id
			where sh.deleted_at is null and sh.status=1 and sh.date="'.$date.'" and sh.to_user_id="'.$user_id.'" -- and sh.shift_to="'.$shift.'"
			group by sh.id
			union all 
			select cd.date, concat(c.first_name," ",c.last_name)as name, 0 as balance_in, 0 as balance_out, sum(cd.amount)as debt, cd.staff_id as user_id, sh.shift
			from customer_debts cd
			join customers c on c.id=cd.customer_id
			join (
				select distinct sh2.date, sh2.shift_to as shift, sh2.to_user_id as user_id
				from shift_histories sh2
				where sh2.deleted_at is null and sh2.date="'.$date.'" and sh2.to_user_id="'.$user_id.'" -- and sh2.shift_to="'.$shift.'"
			)as sh on sh.user_id=cd.staff_id and sh.date=cd.date
			where cd.deleted_at is null and cd.staff_id="'.$user_id.'" and cd.date="'.$date.'"
			group by cd.id
			union all
			select date(t.date)as date, concat(c.first_name," ",c.last_name)as name, 0 as balance_in, sum(td.rolling)as balance_out, 0 as debt, td.user_id, t.shift
			from transaction_details td
			join transactions t on t.id=td.transaction_id
			join customers c on c.id=t.customer_id
			where t.deleted_at is null and (td.flags="1credit" or td.flags="credit") and date(t.date)="'.$date.'" and t.user_id="'.$user_id.'" -- and t.shift="'.$shift.'"
			group by t.id
			union all
			select date(t.date)as date, concat(c.first_name," ",c.last_name)as name, 0 as balance_in, sum(td.rolling)as balance_out, 0 as debt, td.user_id, t.shift
			from transaction_details td
			join transactions t on t.id=td.transaction_id
			join customers c on c.id=t.customer_id
			where t.deleted_at is null and (td.flags="role") and date(t.date)="'.$date.'" and t.user_id="'.$user_id.'" -- and t.shift="'.$shift.'"
			group by t.id
			union all
			select date(t.date)as date, concat(c.first_name," ",c.last_name)as name, t.closing_nn_chips as balance_in, 0 as balance_out, 0 as debt, td.user_id, t.shift
			from transaction_details td
			join transactions t on t.id=td.transaction_id
			join customers c on c.id=t.customer_id
			where t.deleted_at is null and date(t.date)="'.$date.'" and t.user_id="'.$user_id.'" -- and t.shift="'.$shift.'"
			group by t.id
			union all
			select pd.date, concat(c.first_name," ",c.last_name)as name, 0 as balance_in, 0 as balance_out, -sum(pd.amount)as debt, pd.user_id, sh.shift
			from payment_debts pd
			join customers c on c.id=pd.customer_id
			join (
				select distinct sh2.date, sh2.shift_to as shift, sh2.to_user_id as user_id
				from shift_histories sh2
				where sh2.deleted_at is null and sh2.date="'.$date.'" and sh2.to_user_id="'.$user_id.'" -- and  sh2.shift_to="'.$shift.'"
			)as sh on sh.user_id=pd.user_id and sh.date=pd.date
			where pd.date="'.$date.'" and pd.deleted_at is null
			group by pd.id
			-- union all
			-- select sh.date, u.name, 0 as balance_in, sum(sh.nn_chips)as balance_out, 0 as debt, sh.from_user_id, sh.shift_from as shift
			-- from shift_histories sh
			-- join users u on u.id=sh.to_user_id
			-- where sh.deleted_at is null and sh.date="'.$date.'" and sh.from_user_id="'.$user_id.'" -- and sh.shift_from="'.$shift.'"
			-- group by sh.date,sh.shift_from,sh.from_user_id
		)as sub'))->get();
		
		$data['open_close']=DB::table(DB::raw('(select co.user_id,co.date,u.name,co.shift,co.chips_out,
			 co.chips_out-cr.credit-cr.rolling+cr.chips_return as chips_return,cr.cash_chips+cr.rolling as cash_chips,cr.cash_real
			from users u
			join user_has_roles uhr on uhr.user_id=u.id
			join (
				select sh.date, sh.to_user_id as user_id, sh.shift_to as shift, sum(sh.nn_chips)as chips_out
				from shift_histories sh
				where sh.deleted_at is null and sh.status=1 and sh.date="'.$date.'" and sh.to_user_id="'.$user_id.'" -- and sh.shift_to="'.$shift.'"
			)as co on co.user_id=u.id
			left join (
				select user_id,sum(credit)as credit,sum(chips_return)as chips_return,sum(rolling)as rolling,
				 sum(cash_chips)as cash_chips,sum(cash_real)as cash_real
				from (
					select t.user_id,t.nn_chips+sum(ifnull(if(td.flags="credit",td.rolling,0),0))as credit,ifnull(t.closing_nn_chips,0)as chips_return,
					 sum(ifnull(if(td.flags="role",td.rolling,0),0))as rolling,ifnull(t.closing_cash_chips,0)as cash_chips,ifnull(t.closing_cash,0)as cash_real
					from transactions t
					left join transaction_details td on td.transaction_id=t.id and (td.flags="credit" or td.flags="role")
					where t.deleted_at is null and date(t.date)="'.$date.'" and t.user_id="'.$user_id.'"
					group by t.id
				)as sub
			)as cr on cr.user_id=u.id
			where u.id="'.$user_id.'"
			)as sub'))->first();
			
		$data['transaction']=Transaction::select(['transactions.*',DB::raw('sum(transaction_details.rolling)as rolling')])
			->join('transaction_details','transaction_details.transaction_id','=','transactions.id','left')
			->join('users','users.id','=','transactions.assign_from','left')
			->whereDate('transactions.date','=',$date)
			->where('transactions.user_id','=',$user_id)
			//->where('transactions.shift','=',$shift)
			->where('transaction_details.flags','=','role')
			->groupBy('transactions.id')
			->get();
		
		// exit(json_encode($data));
		
		$data['date']=date('d-m-Y',strtotime($date));
		$data['shift']=$shift;

		$mpdf = new \mPDF();
		$mpdf->AddPage('L', 'A4');
		$html = View::make('pdf.runnerreport',$data)->render();
		// exit($html);

		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}
	
	public function assetOnHand(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		//$cek = check_auth($user,'read transactions');
		if(!$user){
			return error_unauthorized();
		}
		
		$start_date=$request->input('start_date');
		$start_date=isset($start_date)?date('Y-m-d',strtotime($start_date)):'';
		$end_date=$request->input('end_date');
		$end_date=isset($end_date)?date('Y-m-d',strtotime($end_date)):'';
		
		$account_config=AccountConfig::first();
		$data=DB::table(DB::raw('(select date,remark,created_at,nn_chips,cash_chips,cash_real,
			@balance_nn_chips:=@balance_nn_chips+nn_chips as balance_nn_chips,
			@balance_cash_chips:=@balance_cash_chips+cash_chips as balance_cash_chips,
			@balance_cash_real:=@balance_cash_real+cash_real as balance_cash_real
			from(
				select "'.$start_date.'" as date,"B/F" as remark,"0000-00-00 00:00:00" as created_at,
				ifnull(sum(if(jad.account_id='.$account_config['nn_chips'].',debit-credit,0)),0)as nn_chips,
				ifnull(sum(if(jad.account_id='.$account_config['cash_chips'].',debit-credit,0)),0)as cash_chips,
				ifnull(sum(if(jad.account_id='.$account_config['cash_real'].',debit-credit,0)),0)as cash_real
				from journal_account_details jad
				join journal_accounts ja on ja.id=jad.journal_id
				where ja.date<"'.$start_date.'" and ja.deleted_at is null
				and jad.entity="user" and jad.entity_id='.$user->id.' 
				and (jad.account_id='.$account_config['nn_chips'].' or 
					jad.account_id='.$account_config['cash_chips'].' or 
					jad.account_id='.$account_config['cash_real'].') 
				and (ja.activity="transfer_chip" or ja.activity="purchase" or
					ja.activity="return_chip" or ja.activity="open_trip" or
					ja.activity="closing_trip" or ja.activity="customer_debts" or
					ja.activity="expense" or ja.activity="payment" or
					ja.activity="payment_debt")
				union all
				select ja.date,ja.remark,ja.created_at,
				if(jad.account_id='.$account_config['nn_chips'].',debit-credit,0)as nn_chips,
				if(jad.account_id='.$account_config['cash_chips'].',debit-credit,0)as cash_chips,
				if(jad.account_id='.$account_config['cash_real'].',debit-credit,0)as cash_real
				from journal_account_details jad
				join journal_accounts ja on ja.id=jad.journal_id
				where ja.date>="'.$start_date.'" and ja.date<="'.$end_date.'"
				and ja.deleted_at is null
				and jad.entity="user" and jad.entity_id='.$user->id.' 
				and (jad.account_id='.$account_config['nn_chips'].' or 
					jad.account_id='.$account_config['cash_chips'].' or 
					jad.account_id='.$account_config['cash_real'].') 
				and (ja.activity="transfer_chip" or ja.activity="purchase" or
					ja.activity="return_chip" or ja.activity="open_trip" or
					ja.activity="closing_trip" or ja.activity="customer_debts" or
					ja.activity="expense" or ja.activity="payment" or
					ja.activity="payment_debt")
			)as sub
			cross join (select @balance_nn_chips:=0)as b1
			cross join (select @balance_cash_chips:=0)as b2
			cross join (select @balance_cash_real:=0)as b3
			order by date asc,created_at asc
			)as sub order by date desc,created_at desc'))->get();
			
		$summary=DB::table(DB::raw('(select 
				ifnull(sum(if(jad.account_id='.$account_config['nn_chips'].',debit-credit,0)),0)as nn_chips,
				ifnull(sum(if(jad.account_id='.$account_config['cash_chips'].',debit-credit,0)),0)as cash_chips,
				ifnull(sum(if(jad.account_id='.$account_config['cash_real'].',debit-credit,0)),0)as cash_real
				from journal_account_details jad
				join journal_accounts ja on ja.id=jad.journal_id
				where ja.date<="'.$end_date.'" and ja.deleted_at is null
				and jad.entity="user" and jad.entity_id='.$user->id.' 
				and (jad.account_id='.$account_config['nn_chips'].' or 
					jad.account_id='.$account_config['cash_chips'].' or 
					jad.account_id='.$account_config['cash_real'].') 
				and (ja.activity="transfer_chip" or ja.activity="purchase" or
					ja.activity="return_chip" or ja.activity="open_trip" or
					ja.activity="closing_trip" or ja.activity="customer_debts" or
					ja.activity="expense" or ja.activity="payment" or
					ja.activity="payment_debt")
			)as sub'))->first();
		
		$data=['data'=>$data,'summary'=>$summary];
		
		return $data;
	}
	
	public function managerBalance(Request $request)
	{
		$token = $request->input('token');
		$user_id = $request->input('user_id');
		$user = User::where('token',$token)->first();
		if(!$user){
			return error_unauthorized();
		}
		
		$account_user=0;
		$user_has_account=UserHasAccount::where('user_id','=',$user_id)->first();
		if($user_has_account){
			$account_user=$user_has_account['account_id'];
		}
		$account_config=AccountConfig::first();
		$data=DB::table(DB::raw('(
			select (ifnull(sub.balance,0)+ifnull(je.balance,0)+ifnull(sub2.balance,0))as balance from
			(select balance
				from (
					select aw.id,ifnull(aw2.balance,aw.balance)as balance
					from accounts aw
					left join (
						select parent_id,sum(balance)as balance
						from accounts 
						where parent_id is not null and deleted_at is not null
						group by parent_id
					)as aw2 on aw2.parent_id=aw.id 
				)as a
				join user_has_accounts uha on uha.account_id=a.id 
				where uha.user_id='.$user_id.'
			)as sub
			left join('.($account_user==0?'select 0 as balance':('select sum(if(a.nature_type="debit",
				if(jad.debit>0,jad.debit,if(jad.credit<0,-jad.credit,0))-
				if(jad.credit>0,jad.credit,if(jad.debit<0,-jad.debit,0)),
				if(jad.credit>0,jad.credit,if(jad.debit<0,-jad.debit,0))-
				if(jad.debit>0,jad.debit,if(jad.credit<0,-jad.credit,0))))as balance
				from journal_accounts ja
				join journal_account_details jad on jad.journal_id=ja.id and jad.account_id='.$account_user.'
				join (select * from accounts where id='.$account_user.') a on a.id=jad.account_id
				where ja.deleted_at is null and ja.activity is null')).')as je on 1=1
			left join(select sum(if(a.nature_type="debit",if(jad.debit>0,jad.debit,if(jad.credit<0,-jad.credit,0))-
				if(jad.credit>0,jad.credit,if(jad.debit<0,-jad.debit,0)),
				if(jad.credit>0,jad.credit,if(jad.debit<0,-jad.debit,0))-
				if(jad.debit>0,jad.debit,if(jad.credit<0,-jad.credit,0))))as balance
				from journal_account_details jad
				join journal_accounts ja on ja.id=jad.journal_id
				join accounts a on a.id=jad.account_id
				left join user_has_accounts uha on uha.account_id=a.id and uha.user_id='.$user_id.'
				where ja.deleted_at is null
				and ((jad.entity="user" and jad.entity_id='.$user_id.' 
					 and (jad.account_id='.$account_config['nn_chips'].' or
						jad.account_id='.$account_config['junket_chips'].' or
						jad.account_id='.$account_config['cash_chips'].' or
						jad.account_id='.$account_config['cash_real'].')
					 and (ja.activity="transfer_chip" or ja.activity="purchase" or
						ja.activity="return_chip" or ja.activity="open_trip" or
						ja.activity="closing_trip" or ja.activity="customer_debts" or
						ja.activity="expense" or ja.activity="purchases_from_customer")))
				and ja.id not in (select distinct jaw.id
					from journal_accounts jaw
					join journal_account_details jadw on jadw.journal_id=jaw.id
					join accounts aw on aw.parent_id=jadw.account_id or aw.id=jadw.account_id
					where (aw.parent_id=jadw.account_id and jaw.activity="account")
					union all
					select distinct jaw.id
					from journal_accounts jaw
					join journal_account_details jadw on jadw.journal_id=jaw.id
					join accounts aw on aw.id=jadw.account_id
					left join accounts aw2 on aw2.id=aw.parent_id
					where (aw.parent_id is null and aw.deleted_at is not null)
					 or (aw2.id=aw.parent_id and aw2.deleted_at is not null 
					   and aw.id not in ('.$account_config['nn_chips'].','.$account_config['junket_chips'].',
					   '.$account_config['cash_chips'].','.$account_config['cash_real'].',
					   '.$account_config['open_balance_asset'].')))
			)as sub2 on 1=1
			left join(select (ifnull(sh.nn_chips,0)+ifnull(sh.cash_chips,0)+ifnull(sh.cash_real,0))as balance
				from shift_histories sh
				join users u on u.id=sh.to_user_id
				where sh.deleted_at is null and sh.to_user_id="'.$user_id.'" and sh.menu_name="shift_transfer"
				order by sh.date,sh.created_at,sh.id limit 1
			)as sub3 on 1=1
		)as subquery'))->first();
		return ['data'=>$data];
	}
	
	public function agentInsuranceShow(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read agent_insurance');

		$start_date=$request->input('start_date');
		$start_date=isset($start_date)?date('Y-m-d',strtotime($start_date)):'';
		$end_date=$request->input('end_date');
		$end_date=isset($end_date)?date('Y-m-d',strtotime($end_date)):'';

		if($cek['result']==1){
			$data=DB::table(DB::raw('(
				select id,detail_id,date,customer_name,user_name,rolling,commission,room,insurance,
				gl-(if(agent_id is not null,(gl*if(status="Win Only",10,agent_shared)/100),0))as gl,
				if(agent_id is not null,(gl*if(status="Win Only",10,agent_shared)/100),0)as agent_share,
				ifnull(pp,0)as pp,ifnull(other,0)as other,scenario
				from(
					select t.id,tr.id as detail_id,date(t.date)as date,concat(c.first_name," ",c.last_name)as customer_name,
					u.name as user_name,c.agent_id,tr.rolling,ifnull(t.commission,0)as commission,tr.room,tr.insurance,
					tr.agent_shared,tr.status,if(tr.scenario=1,"win","lose")as scenario,
					if(tr.status="Win Only" and tr.scenario=1,
					 if(tr.room="Room 38",(tr.insurance*0.9),(if(tr.room="Maxims",(tr.insurance*0.7*0.9),(tr.insurance*0.8*0.9)))),
					 if(tr.room="Room 38",(tr.insurance*1),(if(tr.room="Maxims",(tr.insurance*0.7),(tr.insurance*0.8)))))as gl,
					if(tr.status="Win Only" and tr.scenario=0,0,
					 if(tr.room="Maxims",(tr.insurance*0.3),0))as pp,
					if(tr.status="Win Only" and tr.scenario=0,0,
					if(tr.room="Other Room",(tr.insurance*0.2),0))as other
					from transactions t
					join customers c on c.id=t.customer_id
					join users u on u.id = t.user_id
					join transaction_rollings tr on tr.transaction_id=t.id 
					where date(t.date)>="'.$start_date.'" and date(t.date)<="'.$end_date.'"
				)as sub
			)as sub'))->get();
			$data=['data'=>$data];

			return $data;
		}else{
			return error_unauthorized();
		}
	}
	
	public function agentInsurancePdf(Request $request)
	{
		$data=$this->agentInsuranceShow($request);
		
		$data['start_date']=date('d-m-Y',strtotime($start_date));
		$data['end_date']=date('d-m-Y',strtotime($end_date));

		$mpdf = new \mPDF();
		$mpdf->AddPage('L', 'A4');
		$html = View::make('pdf.agentinsurance',$data)->render();
		// exit($html);

		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}
	
	public function purchaseCommissionShow(Request $request)
	{
		$token = $request->input('token');
		$user = User::where('token',$token)->first();
		$cek = check_auth($user,'read purchase_commission');

		$vendor_id=$request->input('vendor_id');
		$start_date=$request->input('start_date');
		$start_date=isset($start_date)?date('Y-m-d',strtotime($start_date)):'';
		$end_date=$request->input('end_date');
		$end_date=isset($end_date)?date('Y-m-d',strtotime($end_date)):'';

		if($cek['result']==1){
			$data=Purchase::where('date','>=',$start_date)
				->where('date','<=',$end_date);
			
			if($vendor_id){
				$data=$data->where('vendor_id','=',$vendor_id);
			}
			
			$data=$data->get();

			$data=['data'=>$data];

			return $data;
		}else{
			return error_unauthorized();
		}
	}
	
	public function purchaseCommissionPdf(Request $request)
	{
		$data=$this->purchaseCommissionShow($request);
		
		$data['start_date']=date('d-m-Y',strtotime($start_date));
		$data['end_date']=date('d-m-Y',strtotime($end_date));

		$mpdf = new \mPDF();
		$mpdf->AddPage('L', 'A4');
		$html = View::make('pdf.purchasecommission',$data)->render();
		// exit($html);

		$mpdf->WriteHTML($html);
		$mpdf->Output();
		exit;
	}
}
