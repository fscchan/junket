main.registerCtrl("userHasAccountController",function($scope,$filter,$location,$http,DTOptionsBuilder, DTColumnDefBuilder,$routeParams){

	$scope.alert='';
	$scope.success='';
	$scope.fd = [];

	$scope.userOption=[];
	$scope.getUserOption = function() {
		$http({
			method: "get",
			url: "api/users/4/find_by_role",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.userOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.accountOption=[];
	$scope.getAccountOption = function() {
		$http({
			method: "get",
			url: "api/account",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.accountOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.listData = [];
	$scope.getData = function() {
		$http({
			method: "get", 
			url: "api/user_has_accounts", 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.alert = "";
			$scope.listData = response.data.data;

		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.form = function() {
		$http({
			method: "post", 
			url: 'api/user_has_accounts', 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, 
			params: $scope.fd
		}).
		then(function(response){
			$scope.getData();
			$scope.success="Data saved!";
			$scope.fd=[];
		}, function(response){
			$scope.alert=response.data.message;

		});
	};

	$scope.selected=[];
	$scope.edit=function(row){
		$scope.selected = angular.copy(row);
		$scope.selected.account_id=parseInt($scope.selectd.account_id);
		setTimeout(function(){$("#selected_account_id").focus()},1);
	}
	
	$scope.getTemplate=function(row) {
		if (row.user_id === $scope.selected.user_id){
			return 'edit';
		}else{
			return 'display';
		}
	}
	
	$scope.editForm = function(id) {
		$http({
			method: "put", 
			url: 'api/user_has_accounts/'+id, 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, 
			params: $scope.selected
		}).
		then(function(response){
			$scope.getData();
			$scope.selected =[];
			$scope.success="Data updated!";
		}, function(response){
			$scope.alert=response.data.message;

		});
	};
	
	$scope.delete = function(id) {
		$http({
			method: "delete", 
			url: 'api/user_has_accounts/'+id, 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.getData();
			$scope.selected =[];
			$scope.success="Data deleted!";
		}, function(response){
			$scope.alert=response.data.message;

		});
	};
	
});
