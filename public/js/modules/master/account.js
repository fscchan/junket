main.registerCtrl("accountController",function($scope,$location,$http,DTOptionsBuilder, DTColumnDefBuilder,$routeParams,$filter){
	$scope.alert = "";
	$scope.success = "";
	$scope.fd=[];
	$scope.isGenting = ($routeParams.genting == "genting" || $routeParams.id == "genting") ? true : false;
	
	if($routeParams.genting && $routeParams.genting != "genting"){
		$location.path("master/accounts/genting");
	}
	
	$scope.company_id=($routeParams.genting && $routeParams.genting=='genting')?2:1;

	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd/M/yy",
		yearRange: '1900:-0',
	};
	
	$scope.add=function(){
		$location.path("master/accounts/create"+($scope.company_id==2?"/genting":""));
	}
	
	$scope.accountConfig=[];
	$scope.initData=function(){
		$scope.fd.balance_date=new Date();
		$http({
			method: "get",
			url: "api/account/number?company_id="+$scope.company_id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			if(!$scope.fd.code){
				$scope.fd.code = response.data.data;
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
		
		$http({
			method: "get",
			url: "api/account_config?company_id="+$scope.company_id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.accountConfig = response.data.data[0];
			$scope.fd.nn_chips_id=parseInt($scope.accountConfig.nn_chips);
			$scope.fd.junket_chips_id=parseInt($scope.accountConfig.junket_chips);
			$scope.fd.cash_chips_id=parseInt($scope.accountConfig.cash_chips);
			$scope.fd.cash_real_id=parseInt($scope.accountConfig.cash_real);
			$scope.fd.balance_id=parseInt($scope.accountConfig.open_balance_asset);
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.parentOption=[];
	$scope.getParent=function(){
		$http({
			method: "get",
			url: "api/account/parent?company_id="+$scope.company_id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.parentOption = response.data.data;
			for(var i = 0; i < $scope.parentOption.length; i++) {
				$scope.parentOption[i].id = parseInt($scope.parentOption[i].id);
			}
			$scope.setParent();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.accountTypeOption=[];
	$scope.getAccountType=function(){
		$http({
			method: "get",
			url: "api/account/type",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.accountTypeOption = response.data.data;
			if(!$scope.fd.account_type_id){
				$scope.fd.account_type_id = response.data.data[0].id;
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.balancing=function(){
		nn_chips=$scope.fd.nn_chips?parseFloat($scope.fd.nn_chips):0;
		junket_chips=$scope.fd.junket_chips?parseFloat($scope.fd.junket_chips):0;
		cash_chips=$scope.fd.cash_chips?parseFloat($scope.fd.cash_chips):0;
		cash_real=$scope.fd.cash_real?parseFloat($scope.fd.cash_real):0;
		$scope.fd.balance=nn_chips+junket_chips+cash_chips+cash_real;
	}
	
	$scope.accountOption=[];
	$scope.getAccount=function(){
		$http({
			method: "get",
			url: "api/account?company_id="+$scope.company_id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.accountOption = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.accountConfig=[];
	$scope.getAccountConfig=function(){
		$http({
			method: "get",
			url: "api/account_config?company_id="+$scope.company_id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.fd = response.data.data[0];
			$scope.fd.nn_chips=parseInt($scope.fd.nn_chips);
			$scope.fd.junket_chips=parseInt($scope.fd.junket_chips);
			$scope.fd.cash_chips=parseInt($scope.fd.cash_chips);
			$scope.fd.cash_real=parseInt($scope.fd.cash_real);
			$scope.fd.bank=parseInt($scope.fd.bank);
			$scope.fd.open_balance_asset=parseInt($scope.fd.open_balance_asset);
			$scope.fd.receivable=parseInt($scope.fd.receivable);
			$scope.fd.commission=parseInt($scope.fd.commission);
			$scope.fd.loan=parseInt($scope.fd.loan);
			$scope.fd.insurance=parseInt($scope.fd.insurance);
			$scope.fd.income=parseInt($scope.fd.income);
			$scope.fd.expense=parseInt($scope.fd.expense);
			$scope.fd.expense_morning=parseInt($scope.fd.expense_morning);
			$scope.fd.expense_night=parseInt($scope.fd.expense_night);
			$scope.fd.expense_bank=parseInt($scope.fd.expense_bank);
			$scope.fd.tips=parseInt($scope.fd.tips);
			$scope.fd.king_win=parseInt($scope.fd.king_win);
			$scope.fd.runner=parseInt($scope.fd.runner);
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.saveConfig=function() {
		$scope.fd.company_id=$scope.company_id;
		
 		if (!$scope.fd.id) {
 			$http({
				method: "post",
				url: 'api/account_config',
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.success="Account is Configured!";
				$scope.fd=response.data.data;
				$scope.fd.nn_chips=parseInt($scope.fd.nn_chips);
				$scope.fd.junket_chips=parseInt($scope.fd.junket_chips);
				$scope.fd.cash_chips=parseInt($scope.fd.cash_chips);
				$scope.fd.cash_real=parseInt($scope.fd.cash_real);
				$scope.fd.bank=parseInt($scope.fd.bank);
				$scope.fd.open_balance_asset=parseInt($scope.fd.open_balance_asset);
				$scope.fd.receivable=parseInt($scope.fd.receivable);
				$scope.fd.commission=parseInt($scope.fd.commission);
				$scope.fd.loan=parseInt($scope.fd.loan);
				$scope.fd.insurance=parseInt($scope.fd.insurance);
				$scope.fd.income=parseInt($scope.fd.income);
				$scope.fd.expense=parseInt($scope.fd.expense);
				$scope.fd.expense_morning=parseInt($scope.fd.expense_morning);
				$scope.fd.expense_night=parseInt($scope.fd.expense_night);
				$scope.fd.expense_bank=parseInt($scope.fd.expense_bank);
				$scope.fd.tips=parseInt($scope.fd.tips);
				$scope.fd.king_win=parseInt($scope.fd.king_win);
				$scope.fd.runner=parseInt($scope.fd.runner);
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}

		if($scope.fd.id){
 			$http({
				method: "put",
				url: 'api/account_config/'+$scope.fd.id,
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.success="Account is Configured!";
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}
	}

	$scope.is_parent=false;
	
	$scope.setParent=function(){
		$scope.is_parent=false;
		parenting=$scope.parentOption;
		loop1:for(inde=0;inde<parenting.length;inde++){
			if(parenting[inde].id==$scope.fd.parent_id){
				$scope.is_parent=true;
				$scope.fd.account_type_id=parenting[inde].account_type_id;
				$scope.fd.nature_type=parenting[inde].nature_type;
				break loop1;
			}
		}
	}
	
	$scope.save=function() {
		$scope.balancing();
		$scope.fd.company_id=$scope.company_id;
		// tempDate=$scope.fd.balance_date;
		$scope.fd.balance_date=$filter('date')($scope.fd.balance_date, 'yyyy-MM-dd');
 		if (!$scope.fd.id) {
 			$http({
				method: "post",
				url: 'api/account',
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.cancel();
			}, function(response){
				// $scope.fd.balance_date=tempDate;
				$scope.alert=response.data.message;
			});
 		}

		if($scope.fd.id){
 			$http({
				method: "put",
				url: 'api/account/'+$scope.fd.id,
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				},
				params: $scope.fd
			}).
			then(function(response){
				$scope.cancel();
			}, function(response){
				// $scope.fd.balance_date=tempDate;
				$scope.alert=response.data.message;
			});
 		}
	}

	$scope.cancel=function(){
		$location.path("master/accounts"+($scope.company_id==2?"/genting":""));
	}

    $scope.dtOptions = DTOptionsBuilder.newOptions();
    $scope.dtColumnDefs = [
        DTColumnDefBuilder.newColumnDef(0),
        DTColumnDefBuilder.newColumnDef(1),
        DTColumnDefBuilder.newColumnDef(2),
        DTColumnDefBuilder.newColumnDef(3),
        DTColumnDefBuilder.newColumnDef(4),
        DTColumnDefBuilder.newColumnDef(5),
        DTColumnDefBuilder.newColumnDef(6),
        DTColumnDefBuilder.newColumnDef(7),
        DTColumnDefBuilder.newColumnDef(8),
        DTColumnDefBuilder.newColumnDef(9).notSortable()
    ];
    $scope.dtInstance = {};

	$scope.search='';
	
    $scope.sortType     = 'name'; // set the default sort type
	$scope.sortReverse  = false;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;

	$scope.listData = [];
	$scope.config=false;
	$scope.loadData=function() {

		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}
		
		$http({
			method: "get",
			url: ($scope.isGenting ? "api/account/genting_balance" : "api/account/all?company_id="+$scope.company_id+"&sort_by="+$scope.sortType
			 +"&sort_type="+$scope.sort_method+"&page="+$scope.page+"&search="+$scope.search),
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.listData = response.data;
			if($scope.isGenting){
				data=$scope.listData.data;
				$scope.config=(data.nn_chips>0 || data.junket_chips>0 || data.cash_chips>0 
				  || data.cash_real>0 || data.balance>0)?(response.data.id?true:false):false;
				  console.log($scope.config);
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.sort = function(sortType) {
		$scope.sortType = sortType;
		$scope.sortReverse = !$scope.sortReverse;
		$scope.loadData();
	}

	$scope.showPage=function(en){
		return (en>=$scope.listData.current_page && en<=$scope.listData.current_page+2) || (en<=$scope.listData.current_page && (en>=$scope.listData.current_page-2 || en>=$scope.listData.last_page-2));
	}

	$scope.isLastPage=function(){
		return $scope.listData.current_page==$scope.listData.last_page;
	}

	$scope.paging=function(page){
		if ($scope.listData.last_page>0) {
			$scope.page=page;
			$scope.loadData();
		}
	}
	
    $scope.edit=function(id) {
		$location.path("master/accounts/edit/"+id+($scope.company_id==2?"/genting":""));
    }

	$scope.showStatus=false;
	$scope.getEdit=function() {
		if(!$routeParams.id){
			return false;
		}

		if($routeParams.show){
			if($routeParams.show!="show"){
				return false;
			}
			$scope.showStatus=true;
		}
		
		$http({
			method: "get",
			url: ($scope.isGenting ? "api/account/genting" : "api/account/" + $routeParams.id),
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			if(response.data.data==null){
				$scope.fd.balance_date=new Date();
				$scope.fd.is_genting2=$scope.isGenting?true:false;
				$scope.fd.name="Genting 2";
				$scope.fd.code=(new Date().getTime()).toString();
				$scope.fd.nature_type="credit";
				return false;
			}
			
			$scope.fd = response.data.data;
			
			$scope.fd.is_genting2=$scope.isGenting?true:false;
			$scope.fd.balance_date=$scope.isGenting?new Date():$scope.fd.balance_date;
			
			$scope.fd.parent_id=$scope.fd.parent_id?parseInt($scope.fd.parent_id):null;
			$scope.fd.nn_chips_id=parseInt($scope.accountConfig.nn_chips);
			$scope.fd.junket_chips_id=parseInt($scope.accountConfig.junket_chips);
			$scope.fd.cash_chips_id=parseInt($scope.accountConfig.cash_chips);
			$scope.fd.cash_real_id=parseInt($scope.accountConfig.cash_real);
			$scope.fd.balance_id=parseInt($scope.accountConfig.open_balance_asset);
			$scope.setParent();
			$scope.balancing();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.delete=function(id) {
		$http({
			method: "delete",
			url: 'api/account/'+id,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.success="Data deleted!";
			$scope.loadData();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
});
