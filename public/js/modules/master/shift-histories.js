main.registerCtrl("shiftHistoriesController",function($scope,$filter,$location,$http,$routeParams,DTOptionsBuilder, DTColumnDefBuilder){
	$scope.alert = "";
	$scope.success = "";
	$scope.fd=[];
	$scope.add=function(){
		$location.path("master/shift_histories/create");
	}
	
	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd/M/yy",
		yearRange: '1900:-0',
    };
	
	$scope.initData=function(){
		$scope.fd.date=new Date();
		$scope.fd.nn_chips=0;
		$scope.fd.cash_chips=0;
		$scope.fd.cash_real=0;
	}

	$scope.managerOption = [];
	$scope.getManager = function() {
		$http({method: "get", url: "api/users/4/find_by_role", headers: {
				    "Content-Type":"application/json",
				    "X-Auth-Token":$scope.token
				   }
		}).
		then(function(response){
			$scope.managerOption = response.data.data;

		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.shift_from = function() {
		if ($scope.fd.shift_from == "Morning") {
			$scope.fd.shift_to = "Night";
		} else if ($scope.fd.shift_from == "Night") {
			$scope.fd.shift_to = "Morning";
		}
	}

	$scope.shift_to = function() {
		if ($scope.fd.shift_to == "Morning") {
			$scope.fd.shift_from = "Night";
		} else if ($scope.fd.shift_to == "Night") {
			$scope.fd.shift_from = "Morning";
		}
	}
	
	$scope.save = function() {
 		console.log($scope.fd);
		$scope.fd.menu_name="shift_histories";
		
		if($scope.fd.from_user_id==$scope.fd.to_user_id){
			$scope.alert="From Manager cannot be same with to manager!";
			return false;
		}
		
 		if (!$scope.fd.id) {
 			$http({
				method: "post", 
				url: 'api/shift_history/save', 
				headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				}, 
				params: $scope.fd
			}).
			then(function(response){
				$scope.success="Data Saved!";
				$location.path("master/shift_histories");
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}
		
		if($scope.fd.id){
 			$http({
				method: "put", 
				url: 'api/shift_history/'+$scope.fd.id+'/update', headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				}, 
				params: $scope.fd
			}).
			then(function(response){
				$scope.success="Data Updated!";
				$location.path("master/shift_histories");
			}, function(response){
				$scope.alert=response.data.message;
			});
 		}
    };
	
	$scope.cancel = function() {
		$location.path("master/shift_histories");
	};
	
    $scope.sortType     = 'date'; // set the default sort type
	$scope.sortReverse  = true;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;
	
	$scope.listData = [];
	$scope.loadData = function() {
		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}

		if ($scope.date) {
			$scope.date = $filter('date')($scope.date, 'yyyy-MM-dd');
		} else {
			$scope.date = '';
		}

		$http({
			method: "get", 
			url: "api/shift_history/all?menu_name=shift_histories&sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&page="+$scope.page+"&date="+$scope.date, 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.listData = response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.sort = function(sortType) {
		$scope.sortType = sortType;
		$scope.sortReverse = !$scope.sortReverse;
		$scope.loadData();
	}

	$scope.showPage=function(en){
		return (en>=$scope.listData.current_page && en<=$scope.listData.current_page+2) || (en<=$scope.listData.current_page && (en>=$scope.listData.current_page-2 || en>=$scope.listData.last_page-2));
	}

	$scope.isLastPage=function(){
		return $scope.listData.current_page==$scope.listData.last_page;
	}

	$scope.paging=function(page){
		$scope.page=page;
		$scope.loadData();
	}
	
	$scope.edit=function(id){
		$location.path("master/shift_histories/edit/"+id);
	}
	
	$scope.getEdit = function() {
		if(!$routeParams.id){
			return false;
		}
		$http({
			method: "get", 
			url: "api/shift_history/"+$routeParams.id+"/get", 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.fd = response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.delete = function(id) {
		console.log(id);
		$http({
			method: "delete", 
			url: 'api/shift_history/'+id+'/delete', 
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.success="Data deleted!";
			$scope.loadData();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
});