main.registerCtrl("sellChipController",function($scope,$filter,$location,$http,$routeParams,DTOptionsBuilder, DTColumnDefBuilder){
	$scope.success="";
	$scope.alert="";
	$scope.fd=[];
	$scope.listDetail=[];
    $scope.permissionCreate = false;
	$scope.deleteArray = [];

	$scope.checkbutton=function() {
		$http({
			method: "get",
			url: "api/users/checkPermission?action=create&menu=sell_chip",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.permissionCreate = response.data.permission;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.add=function(){
		$location.path("master/sell_chip/create");
	}

	$scope.initData=function(){
		$scope.fd.date=new Date();
		$scope.fd.shift_from="Morning";
		$scope.fd.radio_to="runner";
		$scope.fd.nn_chips=0;
		$scope.fd.junket_chips=0;
		$scope.fd.remark="";
		$scope.listDetail=[];
		$scope.getUser();
	}

	$scope.userOption = [];
	$scope.getManager = function() {
		$http({
			method: "get",
			url: "api/users/4/find_by_role",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			// console.log(response.data.data[0].name);
			$scope.managerOption = response.data.data;
			if($scope.user.roles!="Administrator"){
				$scope.fd.from_user_id = $scope.user.id;
				$scope.getDataGroup();
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.listUser=[];
	$scope.getUser=function(){
		$http({
			method: "get",
			url: "api/users/5/find_by_role",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.listUser[4]=response.data.data;
			$scope.radio_to();
		}, function(response){
			$scope.alert=response.data.message;
		});

		$http({
			method: "get",
			url: "api/users/6/find_by_role",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.listUser[6]=response.data.data;
			$scope.radio_to();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.runnerOption=[];
	$scope.radio_to = function() {
		if ($scope.fd.radio_to == "assistant") {
			$scope.runnerOption=$scope.listUser[4];
		} else if ($scope.fd.radio_to == "runner") {
			$scope.runnerOption=$scope.listUser[6];
		}
	}

	$scope.getDataGroup=function(){
		if(typeof $scope.fd.date==="undefined" || typeof $scope.fd.from_user_id==="undefined" || typeof $scope.fd.shift_from==="undefined"){
			return false;
		}

		$scope.listDetail=[];
		$http({
			method: "get",
			url: "api/sell_chips/"+$filter('date')($scope.fd.date, 'yyyy-MM-dd')+"/"+$scope.fd.from_user_id+"/"+$scope.fd.shift_from,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.listDetail=response.data.data;
			$scope.checkDataGroup(0);
		}, function(response){
			$scope.alert=response.data.message;
		});
		
		$scope.fd.balance=0;
		$http({
			method: "get",
			url: "api/manager_balance?user_id="+$scope.fd.from_user_id+"&token="+$scope.token,
		}).
		then(function(response){
			$scope.fd.balance=response.data.data.balance?parseFloat(response.data.data.balance):0;
		}, function(response){
			
		});
	}

	$scope.checkDataGroup=function(indeks){
		if(indeks==$scope.listDetail.length){
			$scope.inc=indeks;
			return false;
		}
		statusNext=0;
		loop1:
		for(inde=0;inde<$scope.listUser[4].length;inde++){
			if($scope.listUser[4][inde].id==$scope.listDetail[indeks].to_user_id){
				$scope.listDetail[indeks].inc=indeks;
				$scope.listDetail[indeks].to_user=$scope.listUser[4][inde].name;
				$scope.listDetail[indeks].radio_to="assistant";
				statusNext=1;
				break loop1;
			}
		}

		if(statusNext==0){
			loop2:
			for(inde=0;inde<$scope.listUser[6].length;inde++){
				if($scope.listUser[6][inde].id==$scope.listDetail[indeks].to_user_id){
					$scope.listDetail[indeks].inc=indeks;
					$scope.listDetail[indeks].to_user=$scope.listUser[6][inde].name;
					$scope.listDetail[indeks].radio_to="runner";
					statusNext=1;
					break loop2;
				}
			}
			$scope.checkDataGroup(indeks+1);
		}else{
			$scope.checkDataGroup(indeks+1);
		}
	}

	$scope.inc=0;
	$scope.addDetail=function(){
		if(!$scope.fd.to_user_id){
			alert("Please select person buy!");
			return false;
		}

		user="";
		for(inde=0;inde<$scope.runnerOption.length;inde++){
			if($scope.runnerOption[inde].id==$scope.fd.to_user_id){
				user=$scope.runnerOption[inde].name;
			}
		}

		detail={
			inc: $scope.inc,
			id: 0,
			to_user_id: $scope.fd.to_user_id,
			to_user: user,
			radio_to: $scope.fd.radio_to,
			nn_chips: $scope.fd.nn_chips,
			junket_chips: $scope.fd.junket_chips,
			remark: $scope.fd.remark
		};

		$scope.fd.nn_chips=0;
		$scope.fd.junket_chips=0;
		$scope.listDetail.push(detail);
		console.log($scope.listDetail);
		$scope.inc++;
	}

	$scope.runnerOption2=[];
	$scope.radio_to2 = function() {
		if ($scope.selected.radio_to == "assistant") {
			$scope.runnerOption2=$scope.listUser[4];
			if($scope.listUser[4] != undefined) {
				$scope.selected.to_user_id = $scope.check_user_id;
			}
		} else if ($scope.selected.radio_to == "runner") {
			$scope.runnerOption2=$scope.listUser[6];
			if($scope.listUser[6] != undefined) {
				$scope.selected.to_user_id = $scope.check_user_id;
			}
		}
	}


	$scope.check_user_id = 0;
	$scope.editForm=function(row){
		$scope.selected = angular.copy(row);
		if(row.radio_to=="assistant"){
			$scope.check_user_id = parseFloat(row.to_user_id);
			$scope.selected.radio_to="assistant";
		}else if(row.radio_to=="runner"){
			$scope.selected.radio_to="runner";
			$scope.check_user_id = parseFloat(row.to_user_id);
		}
		$scope.radio_to2();
		setTimeout(function(){$("#selected_nn_chips").focus()},1);
		$scope.selected.nn_chips = parseFloat(row.nn_chips, 2);
		$scope.selected.junket_chips = parseFloat(row.junket_chips, 2);
	}

	$scope.editDetail=function(inde){
		$scope.listDetail[inde]=$scope.selected;
		$scope.selected={};
		setTimeout(function(){$("#nn_chips").focus()},1);
	}

	$scope.deleteId = null;
	$scope.modalDelete = false;
	$scope.deleteModal=function(inde) {
		console.log(inde);
		$scope.deleteId = inde;
		$scope.modalDelete = true;
	}

	$scope.deleteDetail=function(){

		if($scope.listDetail[$scope.deleteId].id){
			$scope.deleteArray.push($scope.listDetail[$scope.deleteId].id);
		}

		$scope.listDetail.splice($scope.deleteId,1);

		$scope.selected={};
		$scope.modalDelete = false;
		$scope.deleteId = null;
	}

	$scope.selected={};
	$scope.getTemplate = function (row) {
		if (row.inc === $scope.selected.inc){
			return 'edit';
		}else{
			return 'display';
		}
	}

	$scope.showDetail=function(){
		return (typeof $scope.fd.from_user_id!=="undefined");
	}
	$scope.save =function(){
		for(inde = 0; inde < $scope.deleteArray.length; inde++) {
			console.log($scope.deleteArray[inde]);

			$http({method: "delete", url: 'api/shift_history/'+$scope.deleteArray[inde]+'/delete?menu_name=sell_chip', headers: {
					"Content-Type":"application/json",
					"X-Auth-Token":$scope.token
				}
			}).
			then(function(response){
			}, function(response){
				$scope.alert=response.data.message;
			});

			if (inde==$scope.deleteArray.length-1){
				console.log('delete');
				$scope.save_();
			}
		}
		// return false;
		if ($scope.deleteArray.length==0){
			$scope.save_();
		}
	}
	$scope.save_=function(){
		$scope.success = '';
		for(inde2=0;inde2<$scope.listDetail.length;inde2++){

			params={
				date: $filter('date')($scope.fd.date, 'yyyy-MM-dd'),
				from_user_id: $scope.fd.from_user_id,
				shift_from: $scope.fd.shift_from,
				to_user_id: $scope.listDetail[inde2].to_user_id,
				shift_to: ($scope.fd.shift_from=='Morning'?'Night':'Morning'),
				nn_chips: $scope.listDetail[inde2].nn_chips,
				junket_chips: $scope.listDetail[inde2].junket_chips,
				cash_chips: 0,
				cash_real: 0,
				menu_name: "sell_chip",
				remark: $scope.listDetail[inde2].remark
			};
			
			if($scope.listDetail[inde2].id==0){
				$http({
					method: "post",
					url: 'api/shift_history/save',
					headers: {
						"Content-Type":"application/json",
						"X-Auth-Token":$scope.token
					},
					params: params
				}).
				then(function(response){
					// console.log(response.data.message);
					$scope.success = response.data.message;
				}, function(response){
					$scope.alert=response.data.message;
				});
			}

			if($scope.listDetail[inde2].id!=0){
				$http({
					method: "put",
					url: 'api/shift_history/'+$scope.listDetail[inde2].id+'/update',
					headers: {
						"Content-Type":"application/json",
						"X-Auth-Token":$scope.token
					},
					params: params
				}).
				then(function(response){
					// console.log(response.data.message);
					$scope.success = response.data.message;
				}, function(response){
					$scope.alert=response.data.message;
				});
			}
		}

		if(inde2==$scope.listDetail.length && ($scope.alert!="" || $scope.alert!="success")){
			$location.path("master/sell_chip");
		}
	}

	$scope.cancel=function(){
		$location.path("master/sell_chip");
	}

	$scope.dateOptions = {
		changeYear: true,
		changeMonth: true,
		dateFormat: "dd/M/yy",
		yearRange: '1900:-0',
  	};

	$scope.search='';
	
  	$scope.sortType     = 'date'; // set the default sort type
	$scope.sortReverse  = true;  // set the default sort order
	$scope.searchList   = '';     // set the default search/filter term
	$scope.page			= 1;

	$scope.dataTable = [];
	$scope.subData=[];
	$scope.loadData = function(date) {
		$scope.subData=[];
		if ($scope.sortReverse == true) {
			$scope.sort_method = 'desc';
		} else {
			$scope.sort_method = 'asc';
		}

		/*if ($scope.searchDate) {
			$scope.searchDate = $filter('date')($scope.searchDate, 'yyyy-MM-dd');
		} else {
		    $scope.searchDate = $filter('date')(new Date(), 'yyyy-MM-dd'); 
	        //$scope.searchDate = '';
		}*/

		$http({
			method: "get",
			url: "api/shift_history/all?menu_name=sell_chip&sort_by="+$scope.sortType+"&sort_type="+$scope.sort_method+"&search="+$scope.search,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, params: date
		}).
		then(function(response){
			$scope.dataTable = response.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.edit=function(id){
		$location.path("master/sell_chip/edit/"+id);
	}

	$scope.showEdit = '';
	$scope.getEdit = function() {
		if(!$routeParams.id){
			return false;
		}

		$http({method: "get",
			url: "api/shift_history/"+$routeParams.id+"/get",
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.fd = response.data.data;
			$scope.showEdit = 'edit';
			$scope.fd.nn_chips = parseFloat(response.data.data.nn_chips, 2);
			$scope.fd.junket_chips = parseFloat(response.data.data.junket_chips, 2);
			$scope.getDataGroup();
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.approval = function(id) {
		$http({method: "put", url: 'api/shift_history/'+id+'/accepted', headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}, params: {
				menu_name: "sell_chip"
			}
		}).
		then(function(response){
			$scope.dataTable = response.data.data;
			$scope.loadData($scope.currentDate);
			$scope.success="Chip Accepted!";
		}, function(response){
			$scope.alert=response.data.message;
		    $scope.loadData($scope.currentDate);
		});
	}

	$scope.delete = function(row) {
		$scope.listDetail=[];
		$http({
			method: "get",
			url: "api/sell_chips/"+$filter('date')(row.date, 'yyyy-MM-dd')+"/"+row.from_user_id+"/"+row.shift_from,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.listDetail=response.data.data;
			for(inde=0;inde<$scope.listDetail.length;inde++){
				$scope.deleteShift($scope.listDetail[inde].id);
				if(inde==$scope.listDetail.length){
					$scope.success="Data saved!";
					$scope.loadData();
				}
			}
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
	
	$scope.deleteShift = function(id) {
		console.log(id);
		$http({method: "delete", url: 'api/shift_history/'+id+'/delete?menu_name=sell_chip', headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
			}
		}).
		then(function(response){
			$scope.dataTable = response.data.data;
			// $scope.loadData($scope.currentDate);
		}, function(response){
			$scope.alert=response.data.message;
		});
	}

	$scope.sort = function(sortType) {
		$scope.sortType = sortType;
		$scope.sortReverse = !$scope.sortReverse;
		$scope.loadData($scope.currentDate);
	}

	$scope.showPage=function(en){
		return (en>=$scope.dataTable.current_page && en<=$scope.dataTable.current_page+2) || (en<=$scope.dataTable.current_page && (en>=$scope.dataTable.current_page-2 || en>=$scope.dataTable.last_page-2));
	}

	$scope.isLastPage=function(){
		return $scope.dataTable.current_page==$scope.dataTable.last_page;
	}

	$scope.paging=function(page){
		if ($scope.dataTable.last_page>0) {
			$scope.page=page;
			$scope.loadData($scope.currentDate);
		}
	};
	
	$scope.getDetail=function(inde){
		if(typeof $scope.subData[inde]!=="undefined"){return false;}
		dataSellChip=$scope.dataTable.data[inde];
		$http({
			method: "get",
			url: "api/sell_chips/"+$filter('date')(dataSellChip.date, 'yyyy-MM-dd')+"/"+dataSellChip.from_user_id+"/"+dataSellChip.shift_from,
			headers: {
				"Content-Type":"application/json",
				"X-Auth-Token":$scope.token
		   }
		}).
		then(function(response){
			$scope.subData[inde]=response.data.data;
		}, function(response){
			$scope.alert=response.data.message;
		});
	}
});