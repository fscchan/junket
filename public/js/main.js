var main=angular.module("main", ["ngAnimate", "ngAria", "ngCookies","ngMessages", "ngResource",
	"ngRoute", "ngSanitize","ngTouch","datatables","ui.date","ui.mask","ui.bootstrap.modal",
	"ui.bootstrap","mwl.calendar", "ngFileUpload","localytics.directives","oc.lazyLoad",
	"rt.select2","angular-jquery-autocomplete","ngMaterial"]);

	main.config(function($routeProvider, $locationProvider, $httpProvider, $controllerProvider, $ocLazyLoadProvider){
		$ocLazyLoadProvider.config({
			loadedModules: ['main'], modules: [
				{
					name: 'test',
					files: ['public/js/modules/test.js']
				}
			]
		});

		main.registerCtrl = $controllerProvider.register;
		main.resolveScriptDeps = function(dependencies){
		  return function($q,$rootScope){
			var deferred = $q.defer();
			$script(dependencies, function() {
			  // all dependencies have now been loaded by $script.js so resolve the promise
			  $rootScope.$apply(function()
			  {
				deferred.resolve();
			  });
			});

			return deferred.promise;
		  }
		};

		$httpProvider.interceptors.push('responseObserver');
		/* other configuration, like routing */

		$routeProvider.
			when("/dashboard", {
				templateUrl:"public/modules/dashboard.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/dashboard.js'])}
			}).

			when('/login', {templateUrl:"public/modules/login.html"}).
			when('/403', {templateUrl:"public/modules/403.html"}).

			/* without popup */
			//admin/user
			when("/admin/users",{
				templateUrl:"public/modules/admin/user_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/admin/user.js'])}
			}).
			when("/admin/users/create",{
				templateUrl:"public/modules/admin/user_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/admin/user.js'])}
			}).
			when("/admin/users/edit/:id?",{
				templateUrl:"public/modules/admin/user_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/admin/user.js'])}
			}).

			when("/admin/roles",{
				templateUrl:"public/modules/admin/role_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/admin/roles.js'])}
			}).
			when("/admin/user_activities",{
				templateUrl:"public/modules/admin/user_activity_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/admin/user-activity.js'])}
			}).
			when("/admin/blocked_visitors",{
				templateUrl:"public/modules/admin/blocked_visitors_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/admin/blocked-visitors.js'])}
			}).
			when("/admin/change_password",{
				templateUrl:"public/modules/admin/change_password.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/admin/change-password.js'])}
			}).

			//master
			when("/master/set_up",{
				templateUrl:"public/modules/master/setup.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/setup.js'])}
			}).
			when("/master/set_up/:menu",{
				templateUrl:"public/modules/master/bank.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/setup.js'])}
			}).

			//master/customer
			when("/master/customers",{
				templateUrl:"public/modules/master/customer_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/customer.js'])}
			}).
			when("/master/customers/create",{
				templateUrl:"public/modules/master/customer_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/customer.js'])}
			}).
			when("/master/customers/edit/:id?",{
				templateUrl:"public/modules/master/customer_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/customer.js'])}
			}).
			when("/master/customers/:show/:id?",{
				templateUrl:function(params){
					if(params.show=="show"){
						return "public/modules/master/customer_form.html";
					}else{
						return "public/modules/master/customer_view.html";
					}
				},
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/customer.js'])}
			}).

			//master/agents
			when("/master/agents",{
				templateUrl:"public/modules/master/agent_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/agent.js'])}
			}).
			when("/master/agents/create",{
				templateUrl:"public/modules/master/agent_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/agent.js'])}
			}).
			when("/master/agents/edit/:id?",{
				templateUrl:"public/modules/master/agent_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/agent.js'])}
			}).
			
			//master/agent_companies
			when("/master/agent_companies",{
				templateUrl:"public/modules/master/agent_company_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/agent-company.js'])}
			}).
			when("/master/agent_companies/create",{
				templateUrl:"public/modules/master/agent_company_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/agent-company.js'])}
			}).
			when("/master/agent_companies/edit/:id?",{
				templateUrl:"public/modules/master/agent_company_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/agent-company.js'])}
			}).
			
			//master/accounts
			when("/master/accounts",{
				templateUrl:"public/modules/master/account_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/account.js'])}
			}).
			when("/master/accounts/create",{
				templateUrl:"public/modules/master/account_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/account.js'])}
			}).
			when("/master/accounts/edit/:id",{
				templateUrl:"public/modules/master/account_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/account.js'])}
			}).
			
			//master/accounts2
			when("/master/accounts/:genting",{
				templateUrl:"public/modules/master/account_genting_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/account.js'])}
			}).
			when("/master/accounts/create/:genting",{
				templateUrl:"public/modules/master/account_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/account.js'])}
			}).
			when("/master/accounts/edit/:id/:genting",{
				templateUrl:"public/modules/master/account_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/account.js'])}
			}).
			
			//master/journals
			when("/master/journals",{
				templateUrl:"public/modules/master/journal_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/journal.js'])}
			}).
			when("/master/journals/create",{
				templateUrl:"public/modules/master/journal_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/journal.js'])}
			}).
			when("/master/journals/edit/:id?",{
				templateUrl:"public/modules/master/journal_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/journal.js'])}
			}).
			when("/master/journals/:show/:id",{
				templateUrl:"public/modules/master/journal_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/journal.js'])}
			}).
			
			//master/journals2
			when("/master/journals/:genting",{
				templateUrl:"public/modules/master/journal_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/journal.js'])}
			}).
			when("/master/journals/create/:genting",{
				templateUrl:"public/modules/master/journal_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/journal.js'])}
			}).
			when("/master/journals/edit/:id?/:genting",{
				templateUrl:"public/modules/master/journal_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/journal.js'])}
			}).
			when("/master/journals/:show/:id/:genting",{
				templateUrl:"public/modules/master/journal_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/journal.js'])}
			}).
			
			//master/user_has_accouunt
			when("/master/user_has_account",{
				templateUrl:"public/modules/master/user_has_account_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/user-has-account.js'])}
			}).

			//master/chip_from_runner_or_assist
			when("/master/chip_from_runner",{
				templateUrl:"public/modules/master/chip_from_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/chip-from.js'])}
			}).
			when("/master/chip_from_runner/create",{
				templateUrl:"public/modules/master/chip_from_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/chip-from.js'])}
			}).
			when("/master/chip_from_runner/edit/:id?",{
				templateUrl:"public/modules/master/chip_from_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/chip-from.js'])}
			}).
			//master/shift_histories
			when("/master/shift_histories",{
				templateUrl:"public/modules/master/shift_histories_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/shift-histories.js'])}
			}).
			when("/master/shift_histories/create",{
				templateUrl:"public/modules/master/shift_histories_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/shift-histories.js'])}
			}).
			when("/master/shift_histories/edit/:id?",{
				templateUrl:"public/modules/master/shift_histories_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/shift-histories.js'])}
			}).
			//master/sell_chip
			when("/master/sell_chip",{
				templateUrl:"public/modules/master/sell_chip_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/sell-chip.js'])}
			}).
			when("/master/sell_chip/create",{
				templateUrl:"public/modules/master/sell_chip_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/sell-chip.js'])}
			}).
			when("/master/sell_chip/edit/:id?",{
				templateUrl:"public/modules/master/sell_chip_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/master/sell-chip.js'])}
			}).

			//app
			//app/transactions
			when("/app/transactions",{
				templateUrl:"public/modules/app/transaction_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/transactions.js'])}
			}).
			when("/app/transactions/create",{
				templateUrl:"public/modules/app/transaction_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/transactions.js'])}
			}).
			when("/app/transactions/edit/:id?",{
				templateUrl:"public/modules/app/transaction_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/transactions.js'])}
			}).
			when("/app/transactions/expenses/:id?",{
				templateUrl:"public/modules/app/transaction_expense.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/transactions-expenses.js'])}
			}).
			when("/app/transactions/credit/:id?",{
				templateUrl:"public/modules/app/transaction_credit.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/transactions.js'])}
			}).
			when("/app/transactions/rolling/:id?/:type?",{
				templateUrl:"public/modules/app/transaction_rolling.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/rolling.js'])}
			}).
			when("/app/transactions/rolling2/:id?",{
				templateUrl:"public/modules/app/transaction_rolling2.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/rolling2.js'])}
			}).
		    when("/app/transactions/table_rolling/:id?",{
				templateUrl:"public/modules/app/transaction_table_rolling_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/table_rolling.js'])}
		    }).
		    when("/app/transactions/table_rolling/create/:id?",{
				templateUrl:"public/modules/app/transaction_table_rolling_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/table_rolling.js'])}
		    }).
		    when("/app/transactions/table_rolling/edit/:detailid?",{
				templateUrl:"public/modules/app/transaction_table_rolling_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/table_rolling.js'])}
		    }).

			//app/purchases
			when("/app/purchases",{
				templateUrl:"public/modules/app/purchases_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/purchases.js'])}
			}).
			when("/app/purchases/create",{
				templateUrl:"public/modules/app/purchases_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/purchases.js'])}
			}).
			when("/app/purchases/edit/:id?",{
				templateUrl:"public/modules/app/purchases_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/purchases.js'])}
			}).

			//app/purchases
			when("/app/claim_commission",{
				templateUrl:"public/modules/app/claim_commission_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/claim-commission.js'])}
			}).
			when("/app/claim_commission/create",{
				templateUrl:"public/modules/app/claim_commission_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/claim-commission.js'])}
			}).
			when("/app/claim_commission/edit/:id?",{
				templateUrl:"public/modules/app/claim_commission_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/claim-commission.js'])}
			}).

			//app/purchases_from_customer
			when("/app/purchases_from_customer",{
				templateUrl:"public/modules/app/purchases_from_customer_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/purchases-from-customer.js'])}
			}).
			when("/app/purchases_from_customer/create",{
				templateUrl:"public/modules/app/purchases_from_customer_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/purchases-from-customer.js'])}
			}).
			when("/app/purchases_from_customer/edit/:id?",{
				templateUrl:"public/modules/app/purchases_from_customer_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/purchases-from-customer.js'])}
			}).
			
			//app/return_chips
			when("/app/return_chips",{
				templateUrl:"public/modules/app/return_chip_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/return-chips.js'])}
			}).
			when("/app/return_chips/create",{
				templateUrl:"public/modules/app/return_chip_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/return-chips.js'])}
			}).
			when("/app/return_chips/edit/:id?",{
				templateUrl:"public/modules/app/return_chip_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/return-chips.js'])}
			}).
			
			// app/commission_genting2
			when("/app/commission_gentings2",{
				templateUrl:"public/modules/app/commission_genting2_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/commission-genting2.js'])}
			}).
			when("/app/commission_gentings2/create",{
				templateUrl:"public/modules/app/commission_genting2_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/commission-genting2.js'])}
			}).
			when("/app/commission_gentings2/edit/:id?",{
				templateUrl:"public/modules/app/commission_genting2_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/commission-genting2.js'])}
			}).
			
			// app/deposit_genting2
			when("/app/deposit_gentings2",{
				templateUrl:"public/modules/app/deposit_genting2_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/deposit-genting2.js'])}
			}).
			when("/app/deposit_gentings2/create",{
				templateUrl:"public/modules/app/deposit_genting2_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/deposit-genting2.js'])}
			}).
			when("/app/deposit_gentings2/edit/:id?",{
				templateUrl:"public/modules/app/deposit_genting2_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/deposit-genting2.js'])}
			}).
			
			//app/loan_management
			when("/app/loan",{
				templateUrl:"public/modules/app/loan_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/loan.js'])}
			}).
			when("/app/loan/create",{
				templateUrl:"public/modules/app/loan_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/loan.js'])}
			}).
			when("/app/loan/edit/:id",{
				templateUrl:"public/modules/app/loan_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/loan.js'])}
			}).
			when("/app/loan/:show/:id",{
				templateUrl:"public/modules/app/loan_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/loan.js'])}
			}).
			
			//app/loan_management/past
			when("/app/loan/:past",{
				templateUrl:"public/modules/app/loan_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/loan.js'])}
			}).
			when("/app/loan/create/:past",{
				templateUrl:"public/modules/app/loan_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/loan.js'])}
			}).
			when("/app/loan/edit/:id/:past",{
				templateUrl:"public/modules/app/loan_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/loan.js'])}
			}).

			when("/app/workin",{
				templateUrl:"public/modules/app/workin_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/workin.js'])}
			}).

			when("/app/shift_transfer/:entity?",{
				templateUrl:"public/modules/app/shift_transfer_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/shift-transfer.js'])}
			}).

			when("/app/calendar_activity",{
				templateUrl:"public/modules/app/calendar_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/calendar.js'])}
			}).

			// Tips Management
			when("/app/tips",{
				templateUrl:"public/modules/app/tips_management_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/tips-management.js'])}
			}).
			when("/app/tips/create/:tid?",{
				templateUrl:"public/modules/app/tips_management_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/tips-management.js'])}
			}).
			when("/app/tips/edit/:id?",{
				templateUrl:"public/modules/app/tips_management_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/tips-management.js'])}
			}).

			// Withdrawal Genting 2
			when("/app/wg2",{
				templateUrl:"public/modules/app/withdrawal_genting_2_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/withdrawal-genting2.js'])}
			}).
			when("/app/wg2/create",{
				templateUrl:"public/modules/app/withdrawal_genting_2_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/withdrawal-genting2.js'])}
			}).
			when("/app/wg2/edit/:id?",{
				templateUrl:"public/modules/app/withdrawal_genting_2_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/withdrawal-genting2.js'])}
			}).


			//app/expenses
			when("/app/expenses",{
				templateUrl:"public/modules/app/expenses_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/expenses.js'])}
			}).
			when("/app/expenses/create",{
				templateUrl:"public/modules/app/expenses_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/expenses.js'])}
			}).
			when("/app/expenses/edit/:id?",{
				templateUrl:"public/modules/app/expenses_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/expenses.js'])}
			}).
			when("/app/expenses/:show/:id?",{
				templateUrl:"public/modules/app/expenses_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/expenses.js'])}
			}).


			//app/vendor
			when("/app/vendor",{
				templateUrl:"public/modules/app/vendor_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/vendor.js'])}
			}).
			when("/app/vendor/create",{
				templateUrl:"public/modules/app/vendor_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/vendor.js'])}
			}).
			when("/app/vendor/edit/:id?",{
				templateUrl:"public/modules/app/vendor_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/vendor.js'])}
			}).

			//app/bank
			when("/app/bank",{
				templateUrl:"public/modules/app/bank_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/bank.js'])}
			}).
			when("/app/bank/create",{
				templateUrl:"public/modules/app/bank_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/bank.js'])}
			}).
			when("/app/bank/edit/:id?",{
				templateUrl:"public/modules/app/bank_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/bank.js'])}
			}).

			//app/payment_debit
			when("/app/payment_debit",{
				templateUrl:"public/modules/app/payment_debit_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/payment-debit.js'])}
			}).
			when("/app/payment_debit/create",{
				templateUrl:"public/modules/app/payment_debit_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/payment-debit.js'])}
			}).
			when("/app/payment_debit/edit/:id?",{
				templateUrl:"public/modules/app/payment_debit_form.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/payment-debit.js'])}
			}).

			//app/redeem_commision
			when("/app/redeem_commision",{
				templateUrl:"public/modules/app/redeem_commision_view.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/redeem_commision.js'])}
			}).

			//app/bank_genting2
			// when("/app/bank_gentings2",{
			// 	templateUrl:"public/modules/app/bank_genting2_view.html",
			// 	resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/bank-genting2.js'])}
			// }).
			// when("/app/bank_gentings2/create",{
			// 	templateUrl:"public/modules/app/bank_genting2_form.html",
			// 	resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/bank-genting2.js'])}
			// }).
			// when("/app/bank_gentings2/edit/:id?",{
			// 	templateUrl:"public/modules/app/bank_genting2_form.html",
			// 	resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/bank-genting2.js'])}
			// }).
			
			//report
			when("/report/app/collection",{
				templateUrl:"public/modules/report/collection.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/report.js'])}
			}).
			when("/report/app/final",{
				templateUrl:"public/modules/report/final.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/report.js'])}
			}).
			when("/report/app/manager",{
				templateUrl:"public/modules/report/manager.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/report.js'])}
			}).
			when("/report/app/bank",{
				templateUrl:"public/modules/report/bank.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/report.js'])}
			}).
			when("/report/app/daily",{
				templateUrl:"public/modules/report/daily.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/report.js'])}
			}).
			when("/report/app/customer",{
				templateUrl:"public/modules/report/customer.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/report.js'])}
			}).
			when("/report/app/rolling",{
				templateUrl:"public/modules/report/rolling.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/report.js'])}
			}).
			when("/report/app/end_day",{
				templateUrl:"public/modules/report/endday.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/report.js'])}
			}).
			when("/report/app/runner",{
				templateUrl:"public/modules/report/runner.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/report.js'])}
			}).
			when("/report/app/current_chips",{
				templateUrl:"public/modules/report/current_chip.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/report.js'])}
			}).
			when("/report/app/compare_final_end",{
				templateUrl:"public/modules/report/compare_final_end.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/report.js'])}
			}).
			when("/report/app/agent_insurance",{
				templateUrl:"public/modules/report/agent_insurance.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/report.js'])}
			}).
			when("/report/app/purchase_commission",{
				templateUrl:"public/modules/report/purchase_commission.html",
				resolve: {deps: main.resolveScriptDeps(['public/js/modules/app/report.js'])}
			}).
			/* end without popup*/

			when("/test",{
				templateUrl:"public/modules/test.html",
				controller: "testController",
				resolve: {
					loadModule: ['$ocLazyLoad', function ($ocLazyLoad) {
						return $ocLazyLoad.load('test');
					}]
				}
			}).

			when('/admin/:category/:action?/:id?', {
				templateUrl: function (params) {
					var allowedParams = ['category', 'action', 'id'];
					var paramVals = ['admin'];
					for (var key in params) {
						if (allowedParams.indexOf(key) !== -1) {
							paramVals.push(params[key]);
						}
					}
					return paramVals.join('/');
				},
				resolve: {deps: main.resolveScriptDeps(['public/js/backend/admin.js'])}
			}).
			when('/master/:category/:action?/:id?', {
				templateUrl: function (params) {
					var allowedParams = ['category', 'action', 'id'];
					var paramVals = ['master'];
					for (var key in params) {
						if (allowedParams.indexOf(key) !== -1) {
							paramVals.push(params[key]);
						}
					}
					return paramVals.join('/');
				},
				resolve: {deps: main.resolveScriptDeps(['public/js/backend/master.js'])}
			}).
			when('/app/:category/:action?/:id?', {
				templateUrl: function (params) {
					var allowedParams = ['category', 'action', 'id'];
					var paramVals = ['app'];
					for (var key in params) {
						if (allowedParams.indexOf(key) !== -1) {
							paramVals.push(params[key]);
						}
					}
					return paramVals.join('/');
				},
				resolve: {deps: main.resolveScriptDeps(['public/js/backend/app.js'])}
			}).
			when('/report/:category/:action?/:id?', {
				templateUrl: function (params) {
					var allowedParams = ['category', 'action', 'id'];
					var paramVals = ['report'];
					for (var key in params) {
						if (allowedParams.indexOf(key) !== -1) {
							paramVals.push(params[key]);
						}
					}
					return paramVals.join('/');
				},
				resolve: {deps: main.resolveScriptDeps(['public/js/backend/report.js'])}
			}).
			// otherwise({redirectTo:'/home'});
			otherwise({redirectTo:'/dashboard'});

			// $locationProvider.html5Mode(true);
	}).

	controller("templateController",function($scope,$location,$http,$filter,$rootScope){
		$scope.menus=[];
		$scope.user=[];
		$scope.alert="";
		$scope.token="";
		getAuth=function(){
			
			if (typeof(localStorage) == "undefined") {
				$location.path("login");
				console.log("Browser not support local storage!");
				return false;
			}

			var dataAuth=localStorage.junketData;
			if(!dataAuth){
				$scope.menus=[];
				$scope.user=[];
				$location.path("login");
				return false;
			}
			
			dataAuth=JSON.parse(dataAuth);
			
			// if(new Date()>new Date(dataAuth.expired_at*1000) || new Date()>new Date(dataAuth.stay_expired)){
			if(new Date()>new Date(dataAuth.expired_at*1000)){
				$scope.menus=[];
				$scope.user=[];
				$location.path("login");
				localStorage.removeItem("junketData");
				$scope.alert="Login session is expired!";
				return false;
			}

			$scope.menus=dataAuth.menus;
			$scope.user=dataAuth.user;
			$scope.token=dataAuth.token;
			
			dataAuth.stay_expired=new Date().getTime()+5*60000;
			
			localStorage.junketData = JSON.stringify(dataAuth);
		}
		getAuth();

		$scope.isLoggedin=function(){
			var banyak = 0;
			for(var key in $scope.user){banyak++;}
			return banyak>0;
		}

		$scope.fd=[];

		$scope.login=function(){
			// event.preventDefault();
			$http({method: "POST", url: "api/users/login", params: $scope.fd}).
				then(function(response){
					$scope.status = response.status;
					$scope.data = response.data;
					$scope.data.stay_expired = new Date().getTime()+5*60000;
					localStorage.junketData = JSON.stringify($scope.data);
					getAuth();
					$location.path("dashboard");
					$scope.fd=[];
					$scope.alert="";
					// $location.path("home");
				}, function(response){
				  	$scope.alert=response.data.message;
			});
			return false;
		}

		$scope.showAlert=function(){
			return $scope.alert!="" && $scope.alert!="success";
		}

		$scope.logout=function(){
		  $http({method: "POST", url: "api/users/logout", params: {id:$scope.user.id}});
		  localStorage.removeItem("junketData");
		  getAuth();
		  $location.path("login");
		}

		$scope.redirectAuth=function(){
			if($scope.isLoggedin()==true){
				// $location.path("home");
				$location.path("dashboard");
			}
			if(getAuth() == false) {

				localStorage.removeItem("junketData");
				getAuth();
				$location.path("login");

			}
		}

		$scope.replace=function(string,searchString,replaceString){
			regexString=new RegExp(searchString,"g");
			string=string.replace(regexString,replaceString);
			return string;
		}

		$scope.range = function(min, max, step) {
			step = step || 1;
			var input = [];
			for (var i = min; i <= max; i += step) {
				input.push(i);
			}
			return input;
		}

		$scope.formatDateTime=function(sourceDate){
			dateTime=new Date(sourceDate);
			return $filter('date')(dateTime, 'dd-MM-yyyy HH:mm:ss');
		}

		$scope.formatDate=function(sourceDate){
			dateTime=new Date(sourceDate);
			return $filter('date')(dateTime, 'dd-MM-yyyy');
		}

		$scope.formatTime=function(sourceDate){
			dateTime=new Date(sourceDate);
			return $filter('date')(dateTime, 'HH:mm:ss');
		}

		$scope.toString=function(numb){
			return numb.toString();
		}

		$scope.parseInt=function(str){
			return parseInt(str);
		}

		$scope.$on('$locationChangeSuccess',function(){
			getAuth();
		});
	}).

	filter("ucwords", function () {
		return function (input){
			if(input) { //when input is defined the apply filter
			   input = input.toLowerCase().replace(/\b[a-z]/g, function(letter) {
				  return letter.toUpperCase();
			   });
			}
			return input;
		}
	}).

	filter("shiftMorningNight", function(){
		return function (input){
			if(typeof input!=='string'){
				return input;
			}
			switch(input.toLowerCase()){
				case 'morning':
					input = '8am to 8pm';
				break;
				case 'night':
					input = '8pm to 8am';
				break;
			}
			return input;
		}
	}).

	filter("formatCurrency", function($filter){
		return function (input){
			return $filter('number')(input,2);
		}
	}).
	
	directive('currencyInput', function($filter, $browser) {
		return {
			require: 'ngModel',
			link: function($scope, $element, $attrs, ngModelCtrl) {
				var listener = function() {
					var value = $element.val().replace(/,/g, '');
					if(value=='-'){return false;}
					checkComma=value.split('.');
					countComma=0;
					if(typeof checkComma[1] !=='undefined' && checkComma[1].length>0){
						countComma=checkComma[1].length;
						if(countComma>2){
							countComma=2;
						}
					}
					$element.val($filter('number')(value, countComma))
				}
				
				// This runs when we update the text field
				ngModelCtrl.$parsers.push(function(viewValue) {
					return viewValue.replace(/,/g, '');
				})
				
				// This runs when the model gets updated on the scope directly and keeps our view in sync
				ngModelCtrl.$render = function() {
					var value2 = ngModelCtrl.$viewValue;
					if(value2=='-'){return false;}
					var value2 = value2?value2:'0';
					checkComma2=value2.split('.');
					countComma2=0;
					if(typeof checkComma2[1] !=='undefined' && checkComma2[1].length>0){
						countComma2=checkComma2[1].length;
						if(countComma2>2){
							countComma2=2;
						}
					}
					$element.val($filter('number')(ngModelCtrl.$viewValue, countComma2))
				}
				
				$element.bind('change', listener)
				$element.bind('keydown', function(event) {
				// $element.bind('blur', function(event) {
					var key = event.keyCode
					// If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
					// This lets us support copy and paste too
					if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40) || key==190) 
						return 
					$browser.defer(listener) // Have to do this or changes don't get picked up properly
				})
				
				$element.bind('paste cut', function() {
					$browser.defer(listener)  
				})
			}
			
		}
	}).

	directive('stringToNumber', function() {
	  return {
		require: 'ngModel',
		link: function(scope, element, attrs, ngModel) {
		  ngModel.$parsers.push(function(value) {
			return '' + value;
		  });
		  ngModel.$formatters.push(function(value) {
			return parseFloat(value);
		  });
		}
	  };
	}).
	
	directive("minim", function () {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, element, attributes, ngModel) {
                ngModel.$validators.minim = function (modelValue) {
                    if (!isNaN(modelValue) && modelValue !== "" && attributes.minim !== "")
                        return parseFloat(modelValue) >= attributes.minim;
                    else
                        return true;
                }
            }
        };
    }).

	directive("maxim", function () {
		return {
			restrict: "A",
			require: "ngModel",
			link: function (scope, element, attributes, ngModel) {
				ngModel.$validators.maxim = function (modelValue) {
					if (!isNaN(modelValue) && modelValue !== "" && attributes.maxim !== "")
						return parseFloat(modelValue) <= attributes.maxim;
					else
						return true;
				}
			}
		};
	}).
	
	factory('responseObserver', function responseObserver($q, $window,$location,$rootScope) {
		return {
			'responseError': function(errorResponse) {
				$rootScope.pageloading=false;
				console.log(errorResponse);
				if(errorResponse.data.message=="Sorry! We can't found any matching user by given token"){
				  localStorage.removeItem("junketData");
				  $location.path("login");
				}
				return $q.reject(errorResponse);
			},
			'request': function(request) {
				$rootScope.pageloading=true;
				if($location.url()=="/login"){
					return request;
				}
				
				if (typeof(localStorage) == "undefined") {
					$location.path("login");
					console.log("Browser not support local storage!");
					return request;
				}

				var dataAuth=localStorage.junketData;
				if(!dataAuth){
					$location.path("login");
					return request;
				}
				
				dataAuth=JSON.parse(dataAuth);
				
				// if(new Date()>new Date(dataAuth.expired_at*1000) || new Date()>new Date(dataAuth.stay_expired)){
				if(new Date()>new Date(dataAuth.expired_at*1000)){
					$location.path("login");
					localStorage.removeItem("junketData");
					return request;
				}
				
				dataAuth.stay_expired=new Date().getTime()+5*60000;
				
				localStorage.junketData = JSON.stringify(dataAuth);
				
				return request;
			},
			'response': function(response){
				$rootScope.pageloading=false;
				return response;
			}
		};
	});

	// window.onbeforeunload = function() {
		// localStorage.removeItem("junketData");
	// };

	window.unload = function() {
		localStorage.removeItem("junketData");
	};