main.directive('dhxScheduler', function() {
  return {

    restrict: 'A',
    scope: false,
    transclude: true,
    template:'<div class="dhx_cal_navline" ng-transclude></div><div class="dhx_cal_header"></div><div class="dhx_cal_data"></div>',



    link:function ($scope, $element, $attrs, $controller){
      //default state of the scheduler
      if (!$scope.scheduler)
        $scope.scheduler = {};
      $scope.scheduler.mode = $scope.scheduler.mode || "month";
      $scope.scheduler.date = $scope.scheduler.date || new Date();

      scheduler.config.buttons_left = ["dhx_save_btn", "dhx_cancel_btn"];
      scheduler.config.buttons_right = ["dhx_delete_btn"];

      //default lightbox definition
      scheduler.config.lightbox.sections=[
          {name:"username",height:40, map_to:"username", type:"textarea" , default_value:$scope.user.name, focus:true},
          {name:"title",height:40, map_to:"title", type:"textarea", focus:true},
          {name:"description", height:80, map_to:"text", type:"textarea"},
          {name:"place",height:40, map_to:"place", type:"textarea"},
          {name:"time", height:72, type:"time", map_to:"auto"},
      ];
      scheduler.locale.labels['section_username'] = "User by";
      scheduler.locale.labels['section_title'] = "Title";
      scheduler.locale.labels['section_place'] = "Place";

      scheduler.attachEvent("onLightbox", function(){
         var section = scheduler.formSection("username");
        //  console.log('fskfjsfeklj');
         section.control.disabled = true;

      });

      scheduler.attachEvent("onTemplatesReady", function(){
          scheduler.templates.event_text=function(start,end,event){
              return "<b>" + event.title + "</b><br><i>" + event.place + "</i>";
          }
      });

      // scheduler.attachEvent("onMouseMove", function (id, ev, html){
      //     console.log(ev);
      // });

      scheduler.attachEvent("onClick", function (id, e){
        scheduler._on_dbl_click(e);
        return false;
      });

      // scheduler.templates.month_date_class = function(start,end,event){
      //
      //   console.log(event.color);
      // };

      scheduler.attachEvent("onEmptyClick", function (date, e){
        scheduler._on_dbl_click(e);
        return false;
      });

      scheduler.templates.event_bar_text = function(start,end,event){
          var title = event.title?event.title:'';

          if (event.place) {
            var place = " - " + event.place;
          } else {
            place = "";
          }

          return title + place;
      };

      // regular header height for Terrace or Flat skin
      var navbarHeight = 59,
          // regular header for Glossy or Standart skin
          navbarClassicHeight = 23,
          // height for Terrace and Flat in Mobile mode
          navbarMobileHeight = 130,
          // height for Glossy and Standart in Mobile mode
          navbarClassicMobileHeight = 140,

          // load QuickInfo if mobile browser detected
          loadQuickInfo = true,

          // navbar label formats for Regular and Mobile modes
          scaleDate = "%F, %D %d",
          scaleDateMobile = "%D %d";
      // we’ll need to handle Glossy and Standart(classic) skins
      // a bit differently from Terrace and Flat
      var classic = { "glossy": true, "classic": true };

      function setSizes(navHeight, navHeightClassicSkin, scaleDate) {
          scheduler.xy.nav_height = navHeight;

          if (classic[scheduler.skin]) {
              scheduler.xy.nav_height = navHeightClassicSkin;
          }

          scheduler.templates.week_scale_date = function (date) {
              return scheduler.date.date_to_str(scaleDate)(date);
          };
      }

      scheduler.attachEvent("onBeforeViewChange", function setNavbarHeight() {
          /* set sizes based on screen size */
          if (typeof scheduler !== "undefined") {

              if (window.innerWidth >= 768) {
                  setSizes(navbarHeight, navbarClassicHeight, scaleDate);
              } else {
                  setSizes(navbarMobileHeight, navbarClassicMobileHeight, scaleDateMobile);
              }
          }
          return true;
      });

      scheduler.attachEvent("onSchedulerResize", function () {
          scheduler.setCurrentView();
      });

      scheduler.templates.week_scale_date = function (date) {
          return scheduler.date.date_to_str("%D %d")(date);
      };

      //watch data collection, reload on changes
      $scope.$watch($attrs.data, function(collection){
        scheduler.clearAll();
        scheduler.parse(collection, "json");
      }, true);

      //mode or date
      $scope.$watch(function(){
        return $scope.scheduler.mode + $scope.scheduler.date.toString();
      }, function(nv, ov) {
        var mode = scheduler.getState();
        if (nv.date != mode.date || nv.mode != mode.mode)
          scheduler.setCurrentView($scope.scheduler.date, $scope.scheduler.mode);
      }, true);

      //size of scheduler
      $scope.$watch(function() {
        return $element[0].offsetWidth + "." + $element[0].offsetHeight;
      }, function() {
        scheduler.setCurrentView();
      });

      //styling for dhtmlx scheduler
      $element.addClass("dhx_cal_container");

      //init scheduler
      scheduler.init($element[0], $scope.scheduler.date, $scope.scheduler.mode);
    }
  }
});

main.directive('dhxTemplate', ['$filter', function($filter){
  scheduler.aFilter = $filter;

  return {
    restrict: 'AE',
    terminal:true,

    link:function($scope, $element, $attrs, $controller){
      $element[0].style.display = 'none';

      var template = $element[0].innerHTML;
      template = template.replace(/[\r\n]/g,"").replace(/"/g, "\\\"").replace(/\{\{event\.([^\}]+)\}\}/g, function(match, prop){
        if (prop.indexOf(" ") != -1){
          var parts = prop.split(" ");
          return "\"+scheduler.aFilter('"+(parts[1]).trim()+"')(event."+(parts[0]).trim()+")+\"";
        }
        return '"+event.'+prop+'+"';
      });
      var templateFunc = Function('sd','ed','event', 'return "'+template+'"');
      scheduler.templates[$attrs.dhxTemplate] = templateFunc;
    }
  };
}]);
