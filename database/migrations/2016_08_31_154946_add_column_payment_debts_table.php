<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPaymentDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_debts',function(Blueprint $table){
			$table->integer('bank_id')->unsigned()->nullable();
			$table->text('remark')->default('');
			
			$table->foreign('bank_id')
				->references('id')
				->on('banks');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_debts',function(Blueprint $table){
			$table->dropForeign(['bank_id']);
			$table->dropColumn(['bank_id','remark']);
		});
    }
}
