<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_expenses',function(Blueprint $table){
			$table->increments('id');
			$table->integer('transaction_id')->unsigned();
			$table->date('date');
			$table->text('description');
			$table->double('amount')->default(0);
			$table->integer('user_id')->unsigned();
			$table->timestamps();
			
			$table->foreign('transaction_id')->on('transactions')->references('id')->onDelete('cascade');
			$table->foreign('user_id')->on('users')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaction_expenses');
    }
}
