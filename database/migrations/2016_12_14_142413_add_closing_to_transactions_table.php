<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClosingToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions',function(Blueprint $table){
			$table->integer('closing_by')->unsigned()->nullable();
			$table->datetime('closing_date')->default('0000-00-00 00:00:00');
			
			$table->foreign('closing_by')->on('users')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions',function(Blueprint $table){
			$table->dropForeign(['closing_by']);
			$table->dropColumn(['closing_by','closing_date']);
		});
    }
}
