<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAssign2ToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions',function(Blueprint $table){
			$table->integer('assign_from_old')->unsigned()->nullable();
			
			$table->foreign('assign_from_old')->on('users')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions',function(Blueprint $table){
			$table->dropForeign(['assign_from_old']);
			$table->dropColumn('assign_from_old');
		});
    }
}
