<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToGentingtwoDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gentingtwo_deposits',function(Blueprint $table){
			$table->text('remark');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gentingtwo_deposits',function(Blueprint $table){
			$table->dropColumn(['remark']);
		});
    }
}
