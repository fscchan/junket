<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountToReturnChipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('return_chips',function(Blueprint $table){
			$table->integer('account_id')->unsigned()->nullable();
			
			$table->foreign('account_id')->on('accounts')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('return_chips',function(Blueprint $table){
			$table->dropForeign(['account_id']);
			$table->dropColumn(['account_id']);
		});
    }
}
