<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_commissions',function(Blueprint $table){
			$table->increments('id');
			$table->date('date');
			$table->integer('vendor_id')->unsigned();
			$table->integer('year');
			$table->integer('month');
			$table->double('amount')->default(0);
			$table->integer('account_id')->unsigned();
			$table->text('remark');
			$table->timestamps();
			$table->softDeletes();
			
			$table->foreign('vendor_id')->on('vendors')->references('id')->onDelete('cascade');
			$table->foreign('account_id')->on('accounts')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('claim_commissions');
    }
}
