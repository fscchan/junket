<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_types',function(Blueprint $table){
			$table->increments('id');
			$table->string('name',50);
			$table->integer('parent_id')->unsigned()->nullable();
			$table->string('group_account_type',25);
			$table->timestamps();
			$table->softDeletes();
			
			$table->foreign('parent_id')->on('account_types')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('account_types');
    }
}
