<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGentingtwoCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gentingtwo_commissions',function(Blueprint $table){
			$table->increments('id');
			$table->date('date');
			$table->string('buy_from');
			$table->string('type_chip',25);
			$table->double('amount')->default(0);
			$table->double('commission')->default(0);
			$table->double('commission_payout')->default(0);
			$table->integer('user_id')->unsigned();
			
			$table->timestamps();
			$table->softDeletes();
			
			$table->foreign('user_id')->on('users')->references('id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gentingtwo_commissions');
    }
}
