<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToRedeemCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('redeem_commision',function(Blueprint $table){
			$table->date('redeem_date')->nullable();
			$table->integer('account_id')->unsigned()->nullable();
			
			$table->foreign('account_id')->on('accounts')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('redeem_commision',function(Blueprint $table){
			$table->dropForeign(['account_id']);
			$table->dropColumn(['redeem_date','account_id']);
		});
    }
}
