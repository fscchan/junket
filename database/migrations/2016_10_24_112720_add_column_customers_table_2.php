<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCustomersTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('customers', function (Blueprint $table) {
            $table->string('grc_number')->nullable();
            $table->double('check_in_commission')->default(0);
            $table->string('group_flag')->nullable();
            $table->string('agent_flag')->nullable();
            $table->integer('agent_id')->unsigned()->nullable();
			
			$table->foreign('agent_id')->references('id')->on('agents')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
			$table->dropForeign(['agent_id']);
            $table->dropColumn(['grc_number','check_in_commission','group_flag','agent_flag','agent_id']);			
        });
    }
}
