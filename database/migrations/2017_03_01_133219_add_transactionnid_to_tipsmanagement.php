<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionnidToTipsmanagement extends Migration {
    public function up() {
		Schema::table("tipsmanagement", function (Blueprint $table) {
			$table->integer("trans_id")->unsigned()->default(0)->after("id");
		});
    }

    public function down() {
        Schema::table("tipsmanagement", function (Blueprint $table) {
            $table->dropColumn("trans_id");
        });
    }
}
