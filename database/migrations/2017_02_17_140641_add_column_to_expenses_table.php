<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses',function(Blueprint $table){
			$table->integer('transaction_id')->unsigned()->nullable();
			$table->foreign('transaction_id')->on('transactions')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses',function(Blueprint $table){
			$table->dropForeign(['transaction_id']);
			$table->dropColumn('transaction_id');
		});
    }
}
