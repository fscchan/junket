<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDebtDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_debt_details',function(Blueprint $table){
			$table->increments('id');
			$table->integer('payment_debt_id')->unsigned();
			$table->integer('customer_debt_id')->unsigned();
			$table->double('amount')->default(0);
			
			$table->foreign('payment_debt_id')
				->references('id')
				->on('payment_debts')
				->onDelete('cascade');
				
			$table->foreign('customer_debt_id')
				->references('id')
				->on('customer_debts')
				->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payment_debt_details');
    }
}
