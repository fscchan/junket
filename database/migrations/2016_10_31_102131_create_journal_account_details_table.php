<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalAccountDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journal_account_details',function(Blueprint $table){
			$table->increments('id');
			$table->integer('journal_id')->unsigned();
			$table->integer('account_id')->unsigned();
			$table->double('debit')->default(0);
			$table->double('credit')->default(0);
			$table->timestamps();
			
			$table->foreign('journal_id')->on('journal_accounts')->references('id')->onDelete('cascade');
			$table->foreign('account_id')->on('accounts')->references('id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('journal_account_details');
    }
}
