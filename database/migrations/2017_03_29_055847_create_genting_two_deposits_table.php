<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGentingTwoDepositsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gentingtwo_deposits',function(Blueprint $table){
			$table->increments('id');
			$table->date('date');
			$table->double('nn_chips')->default(0);
			$table->double('junket_chips')->default(0);
			$table->double('cash_chips')->default(0);
			$table->double('cash_real')->default(0);
			$table->integer('user_id')->unsigned()->nullable();
			
			$table->timestamps();
			$table->softDeletes();
			
			$table->foreign('user_id')->on('users')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gentingtwo_deposits');
    }
}
