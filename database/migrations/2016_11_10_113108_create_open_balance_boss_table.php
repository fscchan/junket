<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpenBalanceBossTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('open_balance_boss', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('boss_id')->unsigned();
            $table->double('junket_chips')->default(0);
            $table->double('nn_chips')->default(0);
            $table->double('cash_chips')->default(0);
            $table->double('cash_real')->default(0);
            $table->timestamps();

            $table->foreign('boss_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
