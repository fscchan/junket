<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOtherDebtSourceToPaymentDebtTable extends Migration {
    public function up() {
        Schema::create('agent_debts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agent_id')->unsigned();
            $table->integer('transaction_id')->unsigned();
            $table->date('date')->nullable();
            $table->double('amount')->default(0);
            $table->boolean('is_paid')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('agent_id')->references('id')->on('agents')->onDelete('cascade');
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');
        });
        Schema::create('vendor_debts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id')->unsigned();
            $table->integer('transaction_id')->unsigned();
            $table->date('date')->nullable();
            $table->double('amount')->default(0);
            $table->boolean('is_paid')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');
        });
        Schema::create('company_debts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->integer('transaction_id')->unsigned();
            $table->date('date')->nullable();
            $table->double('amount')->default(0);
            $table->boolean('is_paid')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('cascade');
        });
        Schema::table('payment_debts', function (Blueprint $table) {
            $table->integer('company_id')->unsigned()->nullable()->after("customer_id");
            $table->integer('vendor_id')->unsigned()->nullable()->after("customer_id");
            $table->integer('agent_id')->unsigned()->nullable()->after("customer_id");
            $table->foreign('agent_id')->references('id')->on('agents')->onDelete('cascade');
            $table->foreign('vendor_id')->references('id')->on('vendors')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    public function down() {
        Schema::table('payment_debts', function (Blueprint $table) {
			$table->dropForeign(["agent_id"]);
			$table->dropForeign(["vendor_id"]);
			$table->dropForeign(["company_id"]);
			$table->dropColumn(["agent_id"]);
			$table->dropColumn(["vendor_id"]);
			$table->dropColumn(["company_id"]);
        });
		Schema::drop("agent_debts");
		Schema::drop("vendor_debts");
		Schema::drop("company_debts");
    }
}
