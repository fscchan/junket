<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesFromCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases_from_customers',function(Blueprint $table){
			$table->increments('id');
			$table->date('date');
			$table->integer('customer_id')->unsigned();
			$table->double('value')->default(0);
			$table->double('commission_percent')->default(0);
			$table->double('commission')->default(0);
			$table->integer('user_id')->unsigned();
			
			$table->timestamps();
			$table->softDeletes();
			
			$table->foreign('customer_id')->on('customers')->references('id')->onDelete('cascade');
			$table->foreign('user_id')->on('users')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchases_from_customers');
    }
}
