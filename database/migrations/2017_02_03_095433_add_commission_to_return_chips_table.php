<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCommissionToReturnChipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('return_chips',function(Blueprint $table){
			$table->double('commission_percent')->default(0);
			$table->double('commission')->default(0);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('return_chips',function(Blueprint $table){
			$table->dropColumn(['commission_percent','commission']);
		});
    }
}
