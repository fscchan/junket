<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses',function(Blueprint $table){
			$table->integer('bank_id')->unsigned()->nullable();
			
			$table->foreign('bank_id')->references('id')->on('banks')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses',function(Blueprint $table){
			$table->dropForeign(['bank_id']);
			$table->dropColumn('bank_id');
		});
    }
}
