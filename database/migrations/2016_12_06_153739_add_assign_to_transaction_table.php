<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAssignToTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions',function(Blueprint $table){
			$table->integer('assign_from')->unsigned()->nullable();
			$table->integer('assign_to')->unsigned()->nullable();
			
			$table->foreign('assign_from')->on('users')->references('id')->onDelete('cascade');
			$table->foreign('assign_to')->on('users')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions',function(Blueprint $table){
			$table->dropForeign(['assign_from','assign_to']);
			$table->dropColumn(['assign_from','assign_to']);
		});
    }
}
