<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToPaymentDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_debts',function(Blueprint $table){
			$table->integer('payment_method_id')->unsigned()->nullable();
			$table->string('payment_mode')->nullable();
			$table->integer('staff_id')->unsigned()->nullable();
			
			$table->foreign('payment_method_id')->on('payment_methods')->references('id')->onDelete('cascade');
			$table->foreign('staff_id')->on('users')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_debts',function(Blueprint $table){
			$table->dropForeign(['payment_method_id','staff_id']);
			
			$table->dropColumn(['payment_method_id','payment_mode','staff_id']);
		});
    }
}
