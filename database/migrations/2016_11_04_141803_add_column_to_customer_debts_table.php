<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToCustomerDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_debts', function (Blueprint $table) {
            $table->boolean('interest_type')->default(0);
            $table->float('interest_rate',8,2)->default(0);
            $table->double('interest_amount',50)->default(0);
            $table->integer('staff_id')->unsigned()->nullable();
            $table->foreign('staff_id')->references('id')->on('users')->onDelete('cascade');
            $table->date('due_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_debts', function (Blueprint $table) {
            $table->dropForeign(['staff_id']);
			$table->dropColumn(['interest_type','interest_rate','interest_amount','staff_id','due_date']);
        });
    }
}
