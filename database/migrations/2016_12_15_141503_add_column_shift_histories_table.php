<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnShiftHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shift_histories',function(Blueprint $table){
			$table->double('balance')->default(0);
			$table->double('nn_chips_out')->default(0);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shift_histories',function(Blueprint $table){
			$table->dropColumn(['balance','nn_chips_out']);
		});
    }
}
