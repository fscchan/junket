<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserHasAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_has_accounts',function(Blueprint $table){
			$table->integer('user_id')->unsigned();
			$table->integer('account_id')->unsigned();
			$table->timestamps();
			
			$table->foreign('user_id')->on('users')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_has_accounts');
    }
}
