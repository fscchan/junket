<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountToBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banks',function(Blueprint $table){
			$table->integer('account_id')->nullable()->unsigned();
			
			$table->foreign('account_id')->on('accounts')->references('id')->onDelete('set null');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banks',function(Blueprint $table){
			$table->dropForeign(['account_id']);
			$table->dropColumn('account_id');
		});
    }
}
