<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyToAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('agents',function(Blueprint $table){
			$table->integer('agent_company_id')->unsigned()->nullable();
			$table->foreign('agent_company_id')->on('agent_companies')->references('id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('agents',function(Blueprint $table){
			$table->dropForeign(['agent_company_id']);
			$table->dropColumn('agent_company_id');
		});
    }
}
