<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameCompanyDebtTable extends Migration {
    public function up() {
        Schema::rename("company_debts", "agent_company_debts");
		Schema::table('agent_company_debts', function (Blueprint $table) {
			$table->renameColumn("company_id", "agent_company_id");
		});
    }

    public function down() {
        Schema::rename("agent_company_debts", "company_debts");
		Schema::table('agent_company_debts', function (Blueprint $table) {
			$table->renameColumn("agent_company_id", "company_id");
		});
    }
}
