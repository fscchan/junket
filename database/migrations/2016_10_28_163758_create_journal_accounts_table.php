<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJournalAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journal_accounts',function(Blueprint $table){
			$table->increments('id');
			$table->date('date');
			$table->integer('user_id')->unsigned()->nullable();
			$table->string('entity',50)->nullable();
			$table->integer('entity_id')->nullable();
			$table->string('activity',50)->nullable();
			$table->integer('activity_id')->nullable();
			$table->text('remark')->nullable();
			$table->timestamps();
			
			$table->index('entity');
			$table->index('entity_id');
			$table->index('activity');
			$table->index('activity_id');
			
			$table->foreign('user_id')->on('users')->references('id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('journal_accounts');
    }
}
