<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts',function(Blueprint $table){
			$table->increments('id');
			$table->string('name');
			$table->integer('parent_id')->unsigned()->nullable();
			$table->string('nature_type');
			$table->integer('account_type_id')->unsigned()->nullable();
			$table->timestamps();
			$table->softDeletes();
			
			$table->foreign('parent_id')->on('accounts')->references('id')->onDelete('cascade');
			
			$table->foreign('account_type_id')->on('account_types')->references('id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('accounts');
	}
}
