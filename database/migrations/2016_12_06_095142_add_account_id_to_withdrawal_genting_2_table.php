<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountIdToWithdrawalGenting2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('withdrawal_genting_2', function (Blueprint $table) {
            $table->integer('account_id')->nullable()->unsigned()->references('id')->on('accounts')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('withdrawal_genting_2', function (Blueprint $table) {
            $table->dropColumn('account_id');
        });
    }
}
