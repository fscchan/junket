<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomerToPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchases',function(Blueprint $table){
			$table->integer('customer_id')->nullable()->unsigned();
			
			$table->foreign('customer_id')->on('customers')->references('id')->onDelete('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchases',function(Blueprint $table){
			$table->dropForeign(['customer_id']);
			$table->dropColumn('customer_id');
		});
    }
}
