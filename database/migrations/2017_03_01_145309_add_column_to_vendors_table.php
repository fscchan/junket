<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddColumnToVendorsTable extends Migration {
    public function up() {
		Schema::table("vendors", function (Blueprint $table) {
			$table->string("chip", 6)->default("NN")->after("name");
			$table->double("commision")->default(0)->after("chip");
			$table->softDeletes();
		});
		DB::statement("ALTER TABLE vendors ADD CONSTRAINT chip_options CHECK (chip IN ('NN', 'Junket'));");
    }

    public function down() {
		DB::statement("ALTER TABLE 'vendors' DROP CONSTRAINT 'chip_options';");
        Schema::table("vendors", function (Blueprint $table) {
            $table->dropColumn(["chip","commision","deleted_at"]);
        });
    }
}
