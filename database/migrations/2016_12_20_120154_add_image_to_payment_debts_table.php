<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageToPaymentDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_debts', function (Blueprint $table) {
            $table->string("image_name")->nullable();
            $table->integer("image_size")->nullable();
            $table->string("image_ext")->nullable();
            $table->string("image_path")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_debts', function (Blueprint $table) {
            $table->dropColumn(["image_name","image_size","image_ext","image_path"]);
        });
    }
}
