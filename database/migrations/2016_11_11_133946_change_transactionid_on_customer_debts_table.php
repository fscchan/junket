<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTransactionidOnCustomerDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_debts',function(Blueprint $table){
			$table->integer('transaction_id')->unsigned()->nullable()->change();
			$table->double('amount_due')->default(0);
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_debts',function(Blueprint $table){
			$table->dropColumn('amount_due');
		});
    }
}
