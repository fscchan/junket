<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyToAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts',function(Blueprint $table){
			$table->integer('company_id')->unsigned()->nullable();
			
			$table->foreign('company_id')->on('companies')->references('id')->onDelete('set null');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts',function(Blueprint $table){
			$table->dropForeign(['company_id']);
			$table->dropColumn('company_id');
		});
    }
}
