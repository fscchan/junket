<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeleteToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payment_debts',function(Blueprint $table){
			$table->softDeletes();
		});
		
        Schema::table('journal_accounts',function(Blueprint $table){
			$table->softDeletes();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_debts',function(Blueprint $table){
			$table->dropColumn('deleted_at');
		});
		
        Schema::table('journal_accounts',function(Blueprint $table){
			$table->dropColumn('deleted_at');
		});
    }
}
