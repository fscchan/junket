<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnJournalAccountDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('journal_account_details',function(Blueprint $table){
			$table->string('entity',50)->nullable();
			$table->integer('entity_id')->nullable();
			
			$table->index('entity');
			$table->index('entity_id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('journal_account_details',function(Blueprint $table){
			$table->dropColumn(['entity','entity_id']);
		});
    }
}
