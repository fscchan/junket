<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumn6ToAccountConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('account_configs',function(Blueprint $table){
			$table->integer('expense_morning')->unsigned()->nullable();
			$table->integer('expense_night')->unsigned()->nullable();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('account_configs',function(Blueprint $table){
			$table->dropColumn(['expense_morning','expense_night']);
		});
    }
}
