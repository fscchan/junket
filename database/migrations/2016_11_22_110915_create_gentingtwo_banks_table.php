<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGentingtwoBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gentingtwo_banks', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->enum('credit_debit',['IN','OUT']);
            $table->integer('payment_method_id')->unsigned()->nullable();
            $table->string('pic')->nullable();
            $table->string('ref_no')->nullable();
            $table->text('remark')->nullable();
            $table->double('amount')->default(0);
            $table->integer('user_id')->unsigned();
            $table->integer('bank_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('payment_method_id')
                ->references('id')
                ->on('payment_methods')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
				
            $table->foreign('bank_id')
                ->references('id')
                ->on('banks')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gentingtwo_banks');
    }
}
