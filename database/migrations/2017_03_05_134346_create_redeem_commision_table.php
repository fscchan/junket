<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRedeemCommisionTable extends Migration {
    public function up() {
		Schema::table("tipsmanagement", function(Blueprint $table) {
			$table->integer("customer_id")->unsigned()->default(0)->after("trans_id");
		});
		Schema::create("redeem_commision", function(Blueprint $table) {
			$table->increments("id");
			$table->integer("trans_id")->unsigned();
			$table->double("amount")->default(0);
			$table->boolean("redeemed")->default(0);
			$table->timestamp("redeemed_at")->nullable();
            $table->timestamps();
		});
    }

    public function down() {
		Schema::table('tipsmanagement',function(Blueprint $table){
			$table->dropForeign(['customer_id']);
			$table->dropColumn('customer_id');
		});
		
        Schema::drop('redeem_commision');
    }
}
