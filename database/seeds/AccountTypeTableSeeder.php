<?php

use Illuminate\Database\Seeder;
use App\AccountType;

class AccountTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 
     * group_account_type : assets,liabilities,equity,income,cogs,expense
     * 
     * @return void
     */
    public function run()
    {
        AccountType::create(['name'=>'Asset','group_account_type'=>'asset']);
		AccountType::create(['name'=>'Liabilities','group_account_type'=>'liabilities']);
		AccountType::create(['name'=>'Equity','group_account_type'=>'equity']);
		AccountType::create(['name'=>'Income','group_account_type'=>'income']);
		AccountType::create(['name'=>'Cost Of Good Sold','group_account_type'=>'cogs']);
		AccountType::create(['name'=>'Expense','group_account_type'=>'expense']);
    }
}
