<?php

use Illuminate\Database\Seeder;
use App\Vendor;

class VendorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vendor::create(['name' => 'Genting','name' => 'Genting 1']);
        Vendor::create(['name' => 'Genting','name' => 'Genting 2']);
    }
}
