<?php

use Illuminate\Database\Seeder;
use App\Account;

class AccountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * account_type_id : assets,liabilities,equity,income,cogs,expense
     *
     * @return void
     */
    public function run()
    {
        Account::create(['code'=>'11111','name'=>'NN Chips','nature_type'=>'debit','account_type_id'=>1]);
        Account::create(['code'=>'11112','name'=>'Cash Chips','nature_type'=>'debit','account_type_id'=>1]);
        Account::create(['code'=>'11113','name'=>'Cash Real','nature_type'=>'debit','account_type_id'=>1]);
        Account::create(['code'=>'11114','name'=>'Bank','nature_type'=>'debit','account_type_id'=>1]);
        Account::create(['code'=>'31111','name'=>'Boss Share','nature_type'=>'credit','account_type_id'=>3]);
        Account::create(['code'=>'31112','name'=>'Agent Share','nature_type'=>'credit','account_type_id'=>3]);
        Account::create(['code'=>'31113','name'=>'Capital','nature_type'=>'credit','account_type_id'=>3]);
        Account::create(['code'=>'11115','name'=>'Opening Balance Asset','nature_type'=>'debit','account_type_id'=>1]);
        Account::create(['code'=>'11116','name'=>'A/C Receivable','nature_type'=>'debit','account_type_id'=>1]);
        Account::create(['code'=>'61111','name'=>'Expense','nature_type'=>'debit','account_type_id'=>6]);
        Account::create(['code'=>'41111','name'=>'Tips','nature_type'=>'debit','account_type_id'=>4]);
        Account::create(['code'=>'41112','name'=>'Income','nature_type'=>'debit','account_type_id'=>4]);
        Account::create(['code'=>'41113','name'=>'Insurance','nature_type'=>'debit','account_type_id'=>4]);
    }
}
