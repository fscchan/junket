<?php

use Illuminate\Database\Seeder;
use App\CustomerType;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CustomerType::create(['name' => 'Local (CRP)']);
        CustomerType::create(['name' => 'Overseas (Junket)']);
    }
}
