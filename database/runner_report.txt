select co.date,u.name,co.chips_out,ifnull(cr.chips_return,0)as chips_return
from users u
join (
	select sh.date, sh.to_user_id as user_id, sh.shift_to as shift, sum(sh.nn_chips)as chips_out
	from shift_histories sh
	where sh.deleted_at is null and sh.status=1
	group by sh.date, sh.to_user_id,sh.shift_to
)as co on co.user_id=u.id
left join (
	select sh.date, sh.from_user_id as user_id, sh.shift_from as shift, sum(sh.nn_chips)as chips_return,
		sum(sh.cash_chips)as cash_chips,sum(cash_real)as cash_real
	from shift_histories sh
	where sh.deleted_at is null
	group by sh.date, sh.to_user_id,sh.shift_from
)as cr on cr.user_id=co.user_id and cr.date=co.date and cr.shift=co.shift
join user_has_roles uhr on uhr.user_id=u.id
where uhr.role_id=6

-- record on table
select * from (
	select sh.date, u.name, sum(sh.nn_chips)as balance_in, 0 as balance_out, 0 as debt, sh.to_user_id as user_id, sh.shift_to as shift
	from shift_histories sh
	join users u on u.id=sh.from_user_id
	where sh.deleted_at is null and sh.status=1
	group by sh.date,sh.shift_to,sh.to_user_id
	union all 
	select cd.date, concat(c.first_name," ",c.last_name)as name, 0 as balance_in, sum(cd.amount)as balance_out, 0 as debt, cd.user_id, sh.shift
	from customer_debts cd
	join customers c on c.id=cd.customer_id
	join (
		select distinct sh2.date, sh2.shift_to as shift, sh2.to_user_id as user_id
		from shift_histories sh2
		where sh2.deleted_at is null
	)as sh on sh.user_id=cd.user_id and sh.date=cd.date
	where cd.transaction_id is null and cd.deleted_at is null
	group by cd.date,c.id,sh.shift
	union all
	select date(t.date)as date, concat(c.first_name," ",c.last_name)as name, 0 as balance_in, sum(td.rolling)as balance_out, 0 as debt, td.user_id, t.shift
	from transaction_details td
	join transactions t on t.id=t.transaction_id
	join customers c on c.id=t.customer_id
	where t.deleted_at is null and (td.flags="1credit" or td.flags="credit")
	group by date(t.date),t.shift,td.user_id
	union all
	select pd.date, concat(c.first_name," ",c.last_name)as name, 0 as balance_in, 0 as balance_out, sum(pd.amount)as debt, pd.user_id, sh.shift
	from payment_debts pd
	join customers c on c.id=pd.customer_id
	join (
		select distinct sh2.date, sh2.shift_to as shift, sh2.to_user_id as user_id
		from shift_histories sh2
		where sh2.deleted_at is null
	)as sh on sh.user_id=pd.user_id and sh.date=pd.date
	group by pd.date,c.id,sh.shift
	union all
	select sh.date, u.name, 0 as balance_in, sum(sh.nn_chips)as balance_out, 0 as debt, sh.from_user_id, sh.shift_from as shift
	from shift_histories sh
	join users u on u.id=sh.to_user_id
	where sh.deleted_at is null
	group by sh.date,sh.shift_from,sh.from_user_id
)as sub

--opening and closing